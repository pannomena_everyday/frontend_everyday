<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
   Application specific config item
*/
/* staging */
$config['api_url'] = 'https://api-dot-monkeyeveryday-development.df.r.appspot.com/';

/* production */
// $config['api_url'] = 'https://api-dot-monkeyeveryday.df.r.appspot.com/';

$config['subject_colors'] = ['คณิตศาสตร์' => '#F6921E', 'คณิตศาสตร์ ระดับสูง' => '#FF1829', 'ฟิสิกส์' => '#4D20AF', 'ฟิสิกส์ ระดับสูง' => '#9100CE', 'ชีวะ' => '8BC53F', 'อังกฤษ' => '#EF528A', 'เคมี' => '#0041F9', 'ภาษาอังกฤษ' => '#EF528A', 'ชีววิทยา' => '#8BC53F', 'วิทยาศาสตร์' => '#00FAF8', 'ธรรมะ' => '#fce303', 'ภาษาไทย' => '#D3B94A', 'สังคม' => '#895F30', 'SAT' => '#346829'];

$config['google_client_id'] = '174742781908-nt6b6a52ctg78u8brcvrs1ho0ra4h7i7.apps.googleusercontent.com';
$config['google_client_secret'] = 'pxhyFCWYYdycbnVw6kIVkdcD';