<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Librarysheet_model extends PA_Model
{

  function __construct()
  {
    parent::__construct();
  }

  /**
   * หา library_sheet ที่มี sublevel_id เดียวกัน
   * @param $sheet_id
   * @author ampreaw
   **/
  public function find_by_sublevel_id($sublevel_id)
  {
    $this->db->select('*');
    $this->db->from('LibrarySheet');
    $this->db->where('LibrarySheet.librarySubLevelId', $sublevel_id);
    $this->db->where('is_active', 1);
    $this->db->order_by('sheetCode','ASC');

    return $this->db->get()->result();
  }

  /**
   * view sheet file
   * @param $sheet_id
   * @author ampreaw
   **/
  public function find_by_sheet_id($sheet_id)
  {
    $this->db->select('*');
    $this->db->from('LibrarySheet');
    $this->db->where('LibrarySheet.id', $sheet_id);
    $this->db->join('SheetFile', 'LibrarySheet.id = SheetFile.librarySheetId', 'Left');

    return $this->db->get()->row();
  }

  /**
   * หาข้อมูล sheet ที่มี sublevel_id เดียวกัน
   * @param $sublevel_id
   * @author ampreaw
   **/
  public function all_sheet_by_sublevel_id($sublevel_id)
  {
    $this->db->order_by('sheetCode', 'ASC');
    $sheets = $this->db->get_where('LibrarySheet', array('librarySubLevelId' => $sublevel_id, 'is_comingsoon' => 0));
    return $sheets;
  }

  /**
   * view librarysheet
   *
   * @param $librarysheet_id
   * @author ampreaw
   **/
  public function find_self($librarysheet_id)
  {
    $this->db->select('*');
    $this->db->from('LibrarySheet');
    $this->db->where('id', $librarysheet_id);

    return $this->db->get()->row();
  }

  public function all_by_id($librarysheet_id)
  {
    $this->db->select('*');
    $this->db->from('LibrarySheet');
    $this->db->order_by('sheetCode', 'ASC');
    $this->db->where_in('id', $librarysheet_id);

    return $this->db->get()->result();
  }

  public function all_by_id_and_sublevelId($librarysheet_id, $sublevel_id)
  {
    $this->db->select('*');
    $this->db->from('LibrarySheet');
    $this->db->order_by('sheetCode', 'ASC');
    $this->db->where('librarySubLevelId', $sublevel_id);
    $this->db->where_in('id', $librarysheet_id);

    return $this->db->get()->result();
  }

  /**
   * หาข้อมูล sheet จาก level_id และ sublevel_id
   *
   * @param Number $level_id
   * @param Number $sublevel_id
   * @return Array Object ข้อมูล sheet
   **/
  public function all_by_level_sublevel($level_id = null, $sublevel_id)
  {
    $this->db->select('*');
    $this->db->from('LibrarySheet');
    
    $this->db->order_by('sheetCode', 'ASC');

    if(!empty($level_id))
      $this->db->where('libraryLevelId', $level_id);
    
    $this->db->where('librarySubLevelId', $sublevel_id);

    return $this->db->get()->result();
  }
}

class Librarysheet extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}