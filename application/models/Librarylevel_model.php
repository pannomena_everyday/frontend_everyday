<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Librarylevel_model extends PA_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function all()
  {
    $this->db->select('*');
    $this->db->from('LibraryLevel');
    $this->db->order_by('libraryGroup', 'ASC');
    $this->db->order_by('order_item', 'ASC');
    $this->db->where('is_active', 1);

    $res = [];
    foreach ($this->db->get()->result() as $group)
    {
      $res[$group->libraryGroup]['subjectId'] = $group->subjectId;
      $res[$group->libraryGroup]['name'] = $group->libraryGroup;
      $res[$group->libraryGroup]['lists'][] = $group;
    }

    return $res;
  }

  public function all_by_subject($subject_id)
  {
    $this->db->select('*');
    $this->db->from('LibraryLevel');
    $this->db->order_by('libraryGroup', 'ASC');
    $this->db->order_by('order_item', 'ASC');
    $this->db->where('subjectId', $subject_id);
    $this->db->where('is_active', 1);

    $res = [];
    foreach ($this->db->get()->result() as $group)
    {
      $res[$group->libraryGroup]['subjectId'] = $group->subjectId;
      $res[$group->libraryGroup]['name'] = $group->libraryGroup;
      $res[$group->libraryGroup]['title'] = $group->title;
      $res[$group->libraryGroup]['is_comingsoon'] = $group->is_comingsoon;
      $res[$group->libraryGroup]['lists'][] = $group;
    }

    return $res;
  }

  public function all_by_subject_group_id($subject_id)
  {
    $this->db->select('*');
    $this->db->from('LibraryLevel');
    $this->db->order_by('libraryGroup', 'ASC');
    $this->db->order_by('order_item', 'ASC');
    $this->db->where('subjectId', $subject_id);
    $this->db->where('is_active', 1);

    $res = [];
    foreach ($this->db->get()->result() as $group)
    {
      $res[$group->id]['subjectId'] = $group->subjectId;
      $res[$group->id]['name'] = $group->libraryGroup;
      $res[$group->id]['title'] = $group->title;
      $res[$group->id]['is_comingsoon'] = $group->is_comingsoon;
      $res[$group->id]['imgUrl'] = $group->imgUrl;
    }

    return $res;
  }
  
  
  public function all_group_subject()
  {
    $this->db->select('*');
    $this->db->from('LibraryLevel');
    $this->db->order_by('subjectId', 'ASC');
    $this->db->order_by('libraryGroup', 'ASC');
    $this->db->order_by('order_item', 'ASC');
    $this->db->where('is_active', 1);

    $res = [];
    foreach ($this->db->get()->result() as $group)
    {
      $res[$group->libraryGroup.$group->subjectId]['subjectId'] = $group->subjectId;
      $res[$group->libraryGroup.$group->subjectId]['name'] = $group->libraryGroup;
      $res[$group->libraryGroup.$group->subjectId]['lists'][] = $group;
    }

    return $res;
  }

  public function find_by_id($libraryLevelId)
  {
    $this->db->select('*');
    $this->db->from('LibraryLevel');
    $this->db->where('id', $libraryLevelId);

    return $this->db->get()->row();
  }

  /**
   * หา level ทั้งหมดที่มี libraryGroup เดียวกัน
   *
   * @return void
   * @author 
   **/
  public function find_by_librarygroup($libraryGroup)
  {
    $this->db->select('*');
    $this->db->from('LibraryLevel');
    $this->db->where('libraryGroup', $libraryGroup);
    // $this->db->order_by('sheetCode','ASC');

    return $this->db->get()->result();
  }

  public function find_count_subject_from_plan_key($plan_key_list, $subject){
    $filter_plan_key = implode("','", $plan_key_list);
    $sql = "SELECT COUNT(*) AS count_plan FROM PlanLibrary AS PL
    INNER JOIN Plan AS P
    ON PL.planId  = P.id
    WHERE P.is_active = 1
    AND PL.library_data->'$.subject' = ".$subject."
    AND P.plan_key IN ('".$filter_plan_key."')";
    return $this->db->query($sql)->row();
  }
  
  public function recursive_get_plan_key($plan_trees){
    $plan_key_list = array();
    foreach ($plan_trees as $plan) {
      if(isset($plan->plan_key)){
        $plan_key_list[] = $plan->plan_key;
      }
      
      if(isset($plan->children)){
        if(count($plan->children) > 0){
          $plan_key_list = array_merge($plan_key_list, $this->recursive_get_plan_key($plan->children));
        }
      }
    }
    return $plan_key_list;
  }

}

class Librarylevel extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}