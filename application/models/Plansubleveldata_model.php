<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plansubleveldata_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
  }

  /**
   * หาข้อมูล level และ sublevel จาก plan id
   *
   * @return void
   * @author 
   **/
  public function find_level_sublevel($plan_id)
  {
    $this->db->select('levelId, subLevelId');
    $this->db->from('PlanSubLevelData');
    $this->db->where('planId', $plan_id);
    $sublevel_datas = $this->db->get()->result();

    $sub_levels = [];
    foreach ($sublevel_datas as $sublevel_data)
      $sub_levels[$sublevel_data->subLevelId] = $sublevel_data->subLevelId;
    
    return $sub_levels;
  }
}

class Plansubleveldata extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}