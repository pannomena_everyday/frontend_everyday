<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plan_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
  }

  /**
   * ข้อมูล Step ของ id plan ที่ส่งมา
   *
   * @param Number $plan_id
   * @return Array
   **/
  public function get_step($plan_id)
  {
    $this->db->select('id, plan_key');
    $this->db->from('Plan');
    $this->db->where('id', $plan_id);
    $this->db->where('is_active', 1);
    $plan = $this->db->get()->row();

    $res = [];
    foreach (array_slice(explode("-", $plan->plan_key), 1) as $value)
      $res[] = intval($value);

    return $res;
  }
}

class Plan extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}