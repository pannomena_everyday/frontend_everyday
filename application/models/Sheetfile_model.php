<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sheetfile_model extends PA_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function find_by_level_id($libraryLevelId)
  {
    $this->db->select_max('SheetFile.id');
    $this->db->from('SheetFile');
    $this->db->join('LibrarySheet', 'SheetFile.librarySheetId = LibrarySheet.id');
    $this->db->join('LibrarySubLevel', 'LibrarySheet.librarySubLevelId = LibrarySubLevel.id');
    $this->db->join('LibraryLevel', 'LibrarySubLevel.libraryLevelId = LibraryLevel.id');
    $this->db->where(array(
      'LibraryLevel.id' => $libraryLevelId,
      'LibrarySheet.is_active' => '1'
    ));
    $this->db->group_by('SheetFile.librarySheetId');

    $subQuery =  $this->db->get_compiled_select();

    $this->db->select('SheetFile.*, LibrarySheet.id, LibrarySheet.librarySubLevelId, LibrarySubLevel.id, LibrarySubLevel.libraryLevelId, LibraryLevel.id')
          ->from('SheetFile')
          ->join('LibrarySheet', 'SheetFile.librarySheetId = LibrarySheet.id')
          ->join('LibrarySubLevel', 'LibrarySheet.librarySubLevelId = LibrarySubLevel.id')
          ->join('LibraryLevel', 'LibrarySubLevel.libraryLevelId = LibraryLevel.id')
          ->join("($subQuery)  tmp","tmp.id = SheetFile.id")
          ->where(array(
      'LibraryLevel.id' => $libraryLevelId,
      'LibrarySheet.is_active' => '1'
    ));

    return $this->db->get()->result();
  }

  public function find_by_libsheet_id($libsheet_id)
  {
    $this->db->select('*');
    $this->db->from('SheetFile');
    $this->db->where('librarySheetId', $libsheet_id);
    $this->db->order_by('id', 'DESC');
    $this->db->limit(1);

    return $this->db->get()->row();
  }

}

class Sheetfile extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}