<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sheetvideo_model extends PA_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function find_by_sublevel_id($sublevel_id)
  {
    $this->db->select('SheetVideo.*, LibrarySheet.id AS sheet_id, LibrarySheet.librarySubLevelId, LibrarySheet.sheetCode, LibrarySubLevel.id AS sublevel_id, LibrarySubLevel.libraryLevelId, LibrarySubLevel.title AS title_sublevel, LibraryLevel.id AS level_id');
    $this->db->from('SheetVideo');
    $this->db->join('LibrarySheet', 'SheetVideo.librarySheetId = LibrarySheet.id');
    $this->db->join('LibrarySubLevel', 'LibrarySheet.librarySubLevelId = LibrarySubLevel.id');
    $this->db->join('LibraryLevel', 'LibrarySubLevel.libraryLevelId = LibraryLevel.id');
    $this->db->where(array('LibrarySubLevel.id' => $sublevel_id));
    $this->db->order_by('LibrarySheet.sheetCode', 'ASC');
    $this->db->order_by('SheetVideo.order_item', 'ASC');

    return $this->db->get()->result();
  }

  public function find_by_level_id($libraryLevelId)
  {
    $this->db->select('SheetVideo.*, LibrarySheet.id, LibrarySheet.librarySubLevelId, LibrarySubLevel.id, LibrarySubLevel.libraryLevelId, LibraryLevel.id');
    $this->db->from('SheetVideo');
    $this->db->join('LibrarySheet', 'SheetVideo.librarySheetId = LibrarySheet.id');
    $this->db->join('LibrarySubLevel', 'LibrarySheet.librarySubLevelId = LibrarySubLevel.id');
    $this->db->join('LibraryLevel', 'LibrarySubLevel.libraryLevelId = LibraryLevel.id');
    $this->db->where(array('LibraryLevel.id' => $libraryLevelId));

    return $this->db->get()->result();
  }

  public function find_vdo_by_sheet_id($id)
  {
    $this->db->select('*');
    $this->db->from('SheetVideo');
    $this->db->where(array('librarySheetId' => $id));
    $this->db->join('GcsVideo', 'SheetVideo.link = GcsVideo.id');
    $this->db->order_by('SheetVideo.order_item', 'ASC');

    return $this->db->get()->result();
  }

  public function find_vdo_by_sheet_id_with_progress($id, $memberId)
  {
    $this->db->select('SheetVideo.id AS sheetVideoId, SheetVideo.*, GcsVideo.*, MemberSheetVideo.duration AS progress');
    $this->db->from('SheetVideo');
    $this->db->join('GcsVideo', 'SheetVideo.link = GcsVideo.id');
    $this->db->join('MemberSheetVideo', 'SheetVideo.id = MemberSheetVideo.sheetVideoId AND MemberSheetVideo.memberId =  \''.$memberId.'\'', 'left');
    $this->db->where(array('librarySheetId' => $id));
    $this->db->order_by('SheetVideo.order_item', 'ASC');
    
    return $this->db->get()->result();
  }

  public function find_progress_of_sheet($id, $memberId)
  {
    $this->db->select('Count(*) AS vdo, SUM(MemberSheetVideo.progress) AS total_progress, SUM(MemberSheetVideo.complete) AS complete, MAX(MemberSheetVideo.updatedtime) AS updatedtime');
    $this->db->from('SheetVideo');
    $this->db->join('MemberSheetVideo', 'SheetVideo.id = MemberSheetVideo.sheetVideoId AND MemberSheetVideo.memberId =  \''.$memberId.'\'', 'left');
    $this->db->where(array(
      'librarySheetId' => $id
    ));
    
    return $this->db->get()->row();
  }

  public function find_progress_of_sublevel($level_id, $sublevel_id, $memberId)
  {
    $this->db
            ->select('Count(*) AS vdo, SUM(MemberSheetVideo.progress) AS total_progress, MAX(MemberSheetVideo.updatedtime) AS updatedtime')
            ->from('LibrarySheet')
            ->join('SheetVideo' ,'SheetVideo.librarySheetId = LibrarySheet.id')
            ->join('MemberSheetVideo', 'SheetVideo.id = MemberSheetVideo.sheetVideoId AND MemberSheetVideo.memberId =  \''.$memberId.'\'', 'left')
            ->where(array(
              'LibrarySheet.librarySubLevelId' => $sublevel_id,
              'LibrarySheet.libraryLevelId' => $level_id,
              'LibrarySheet.is_comingsoon' => 0
            ));
    
    return $this->db->get()->row();
  }
  
  public function find_progress_of_level($level_id, $memberId)
  {
    $this->db
            ->select('Count(*) AS vdo, SUM(MemberSheetVideo.progress) AS total_progress, MAX(MemberSheetVideo.updatedtime) AS updatedtime')
            ->from('LibraryLevel')
            ->join('LibrarySheet', 'LibraryLevel.id = LibrarySheet.libraryLevelId')
            ->join('SheetVideo' ,'SheetVideo.librarySheetId = LibrarySheet.id')
            ->join('MemberSheetVideo', 'SheetVideo.id = MemberSheetVideo.sheetVideoId AND MemberSheetVideo.memberId =  \''.$memberId.'\'', 'left')
            ->where(array(
              'LibraryLevel.id' => $level_id,
              'LibraryLevel.is_comingsoon' => 0,
              'LibrarySheet.is_comingsoon' => 0
            ));
    
    return $this->db->get()->row();
  }

  public function find_progress_of_member($level_id, $memberId)
  {
    $this->db
            ->select('Count(*) AS vdo')
            ->from('LibraryLevel')
            ->join('LibrarySheet', 'LibraryLevel.id = LibrarySheet.libraryLevelId')
            ->join('SheetVideo' ,'SheetVideo.librarySheetId = LibrarySheet.id')
            ->join('MemberSheetVideo', 'SheetVideo.id = MemberSheetVideo.sheetVideoId AND MemberSheetVideo.memberId =  \''.$memberId.'\'', 'left')
            ->where(array(
              'LibraryLevel.is_comingsoon' => 0,
              'LibrarySheet.is_comingsoon' => 0,
              'MemberSheetVideo.complete' => 1
            ))
            ->where_in('LibraryLevel.id', $level_id);
    
    return $this->db->get()->row();
  }
  public function find_all_vdo_of_member($level_id, $memberId)
  {
    $this->db
            ->select('Count(*) AS vdo')
            ->from('LibraryLevel')
            ->join('LibrarySheet', 'LibraryLevel.id = LibrarySheet.libraryLevelId')
            ->join('SheetVideo' ,'SheetVideo.librarySheetId = LibrarySheet.id')
            ->join('MemberSheetVideo', 'SheetVideo.id = MemberSheetVideo.sheetVideoId AND MemberSheetVideo.memberId =  \''.$memberId.'\'', 'left')
            ->where(array(
              'LibraryLevel.is_comingsoon' => 0,
              'LibrarySheet.is_comingsoon' => 0
            ))
            ->where_in('LibraryLevel.id', $level_id);
    
    return $this->db->get()->row();
  }
  
  public function find_all_continue_vdo_of_member($level_id, $memberId)
  {
    $this->db
            ->select('Count(*) AS vdo')
            ->from('LibraryLevel')
            ->join('LibrarySheet', 'LibraryLevel.id = LibrarySheet.libraryLevelId')
            ->join('SheetVideo' ,'SheetVideo.librarySheetId = LibrarySheet.id')
            ->join('MemberSheetVideo', 'SheetVideo.id = MemberSheetVideo.sheetVideoId AND MemberSheetVideo.memberId =  \''.$memberId.'\'')
            ->where(array(
              'LibraryLevel.is_comingsoon' => 0,
              'LibrarySheet.is_comingsoon' => 0
            ))
            ->where_in('LibraryLevel.id', $level_id);
    
    return $this->db->get()->row();
  }
  
  public function generate_progress_sheet($my_plan, $myProfile){
    
    $progress_sheets = array();
    $progress_continue_sheets = array();
    $recent_vdo = null;
    if (!empty($my_plan['sheets'])){
      foreach ($my_plan['sheets'] as $sheet){
        if(!$sheet->is_test){
          $progress = $this->find_progress_of_sheet($sheet->id, $myProfile->id);
          $total_progress = $progress->vdo * 100;
          $current = $progress->total_progress == null || $progress->total_progress == '' ? 0 : $progress->total_progress;
          if($total_progress <= 0){
            $progress_bar = 0;
          }else{
            $progress_bar = round($current / $total_progress * 100);
          }
          $progress_sheets["profile".$myProfile->id."-sheet".$sheet->id] = $progress_bar;
          if($progress->complete != null && $progress->vdo != $progress->complete){
            $progress_obj = array(
              "progress" => $progress_bar,
              "updatedtime" => $progress->updatedtime,
              "sheet" => $sheet
            );
            $progress_continue_sheets[] = $progress_obj;
          }
        }
      }
      usort($progress_continue_sheets, function($a, $b) {
        return strtotime($b['updatedtime']) - strtotime($a['updatedtime']);
     });
    }

    if(count($progress_continue_sheets) > 0){
      $recent_vdo = array(
        "level" => $progress_continue_sheets[0]['sheet']->libraryLevelId,
        "sublevel" => $progress_continue_sheets[0]['sheet']->librarySubLevelId,
        "sheet" => $progress_continue_sheets[0]['sheet']->id
      );
    }

    return array(
      "progress_sheets" => $progress_sheets,
      "progress_continue_sheets" => $progress_continue_sheets,
      "recent_vdo" => $recent_vdo
    );

  }

  public function getVideoLikeByMemberId($memberId,$sheetVideoId){
    $this->db->select('*')
      ->from('VideoReview')
      ->where(array('memberId'=>$memberId, 'videoId'=>$sheetVideoId))
      ->order_by('updatedtime', 'desc');
    return $this->db->get();
  }

  public function getVideoLikeAll($sheetVideoId){
    $this->db->select('count(*) as ctn')
      ->from('VideoReview')
      ->where(array('videoId' => $sheetVideoId, 'action' => 1));
    return $this->db->get()->result()[0]->ctn;
  }

  public function getVideoDislikeAll($sheetVideoId){
    $this->db->select('count(*) as ctn')
      ->from('VideoReview')
      ->where(array('videoId' => $sheetVideoId, 'action' => 0));
    return $this->db->get()->result()[0]->ctn;
  }

  public function getVideoView($sheetVideoId){
    $this->db->select('count(*) as ctn')
      ->from('VideoViewLog')
      ->where(array('videoId' => $sheetVideoId));
      
    // $this->db->select('count(*) as ctn')
    // ->from('MemberSheetVideo')
    // ->join('SheetVideo' ,'SheetVideo.id = MemberSheetVideo.sheetVideoId')
    // ->where(array('SheetVideo.id' => $sheetVideoId, 'MemberSheetVideo.duration >' => 0));
    return $this->db->get()->result()[0]->ctn;
  }

}

class Sheetvideo extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}