<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subject_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
  }

  public function all()
  {
    $this->db->select('*');
    $this->db->from('Subject');
    $this->db->order_by('order_item', 'ASC');
    $this->db->where('is_active', 1);

    return $this->db->get()->result();
  }

  public function all_arr()
  {
    $this->db->select('*');
    $this->db->from('Subject');
    $this->db->order_by('order_item', 'ASC');
    $this->db->where('is_active', 1);
    $subjects = $this->db->get()->result();

    $res = [];
    foreach ($subjects as $subject)
      $res[$subject->id] = $subject->title;

    return $res;
  }
}

class Subject extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}