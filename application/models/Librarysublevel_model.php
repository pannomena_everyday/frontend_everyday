<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Librarysublevel_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
  }

  public function find_by_id($sublevel_id)
  {
    $this->db->select('id, title, substr(title, 1, 1) AS prefix');
    $this->db->from('LibrarySubLevel');
    $this->db->where('id', $sublevel_id);

    return $this->db->get()->row(0);
  }

  public function find_by_level_id($libraryLevelId)
  {
    $this->db->select('*');
    $this->db->from('LibrarySubLevel');
    $this->db->where('LibrarySubLevel.libraryLevelId', $libraryLevelId);
    $this->db->where('is_active', 1);
    $this->db->order_by('remark', 'ASC');

    return $this->db->get()->result();
  }

  public function all_by_librarylevelids($plan_id, $library_level_id)
  {
    $this->db->select('LibrarySubLevel.id AS id, LibrarySubLevel.*, PlanSubLevelData.planId AS plan_id');
    $this->db->from('LibrarySubLevel');
    $this->db->join('PlanSubLevelData', 'LibrarySubLevel.id = PlanSubLevelData.subLevelId');
    $this->db->where('libraryLevelId', $library_level_id);
    $this->db->where('PlanSubLevelData.planId', $plan_id);
    $this->db->where('is_active', 1);
    $this->db->group_by('PlanSubLevelData.id');
    $this->db->order_by('PlanSubLevelData.order_item', 'ASC');
    return $this->db->get()->result();
  }

  public function all_for_guest()
  {
    $this->db->select('*');
    $this->db->from('LibrarySubLevel');
    $this->db->order_by('CONVERT( title USING tis620 )', 'ASC' );

    $res = [];
    $thai_sra = ['เ', 'แ', 'ไ', 'ใ', 'โ'];
    foreach ($this->db->get()->result() as $sub_level)
    {
      if(in_array(mb_substr($sub_level->title, 0, 1,'UTF-8'), $thai_sra))
        $res[mb_substr($sub_level->title, 1, 1,'UTF-8')]['items'][] = $sub_level;
      else
        $res[mb_substr($sub_level->title, 0, 1,'UTF-8')]['items'][] = $sub_level;
    }

    return $res;
  }

  /**
   * หาข้อมูล sub_level ที่ is_lastupdate = 1
   *
   * @return void
   * @author
   **/
  public function last_update()
  {
    $this->db->select('*');
    $this->db->from('LibrarySubLevel');
    $this->db->where('is_lastupdate', 1);
    $this->db->order_by('lastupdate_order_item', 'asc');
    return $this->db->get()->result();
  }

  /**
   * หาข้อมูล sub_level ที่ is_populate = 1
   *
   * @return void
   * @author
   **/
  public function populate()
  {
    $this->db->select('*');
    $this->db->from('LibrarySubLevel');
    $this->db->where('is_populate', 1);
    $this->db->order_by('populate_order_item', 'asc');
    return $this->db->get()->result();
  }
}

class Librarysublevel extends PA_Model_Object
{
  function __construct()
  {
    parent::__construct();
  }
}