<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
  }

  public function curlRequest($method, $url, $data, $token = null){
    $headers = array("accept: application/json",'Content-Type: application/json');
    if($token != null ){
      array_push($headers,"Authorization: Bearer ".$token);
    }
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    if(!empty($data))
    {
      curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    }
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
    $response = curl_exec($curl);
    curl_close($curl);
    return json_decode($response);
  }
  
  public function curlRequestWithTimeout($method, $url, $data, $token = null, $timeout=5000){
    $headers = array("accept: application/json",'Content-Type: application/json');
    if($token != null ){
      array_push($headers,"Authorization: Bearer ".$token);
    }
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT_MS, $timeout);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    if(!empty($data))
    {
      curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    }
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
    $response = curl_exec($curl);
    curl_close($curl);
    return json_decode($response);
  }

  public function getMemberFromMobile($mobile){
    $this->db->select('*');
    $this->db->from('Member');
    $this->db->where(array("mobile" => $mobile));
    return $this->db->get();
  }

  public function checkEmailExist($email){
    $this->db->select('*');
    $this->db->from('Member');
    $this->db->where(array("email" => $email));
    return $this->db->get();
  }

  public function checkMobileExist($mobile){
    $this->db->select('*');
    $this->db->from('Member');
    $this->db->where(array("mobile" => $mobile));
    return $this->db->get();
  }

  public function checkIdCardExist($id_card){
    $this->db->select('*');
    $this->db->from('Member');
    $this->db->where(array("id_card" => $id_card));
    return $this->db->get();
  }

  public function getEmailForgot($val){
    $this->db->select('*');
    $this->db->from('Member');
    $this->db->where(array("mobile" => $val));
    $this->db->or_where(array("email" => $val));
    return $this->db->get();
  }

  public function getProvinceBKKFrist(){
    $sql = "SELECT z.ProvinceID, z.ProvinceName
    FROM (SELECT *, CASE WHEN ProvinceID = 106 THEN 0 ELSE 1 END AS checkBKK FROM Province) as z
    ORDER BY z.checkBKK ASC, z.ProvinceName ASC";
    return $this->db->query($sql);
  }

  public function ChangePasswordProcess($val,$password){
    $this->db->select('*');
    $this->db->from('Member');
    $this->db->where(array("mobile" => $val));
    $this->db->or_where(array("email" => $val));
    $ret = $this->db->get();
    if($ret->num_rows() == 0){
      return false;
    }else{
      $memberId = $ret->result()[0]->id;
      $data = array(
        'password' => $password,
      );
      $this->db->where('memberId', $memberId);
      $this->db->update('MemberCredential', $data);
      if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
      }else{
            $this->db->trans_commit();
            return true;
      }
    }
  }

  public function checkToken($token){
    if($token == null || $token == ""){
      delete_cookie('token');
      header("Location: ".base_url()."Index/SignIn");
    }
    // $checkToken = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    // if(!isset($checkToken->firstname)){
    //   delete_cookie('token');
    //   header("Location: ".base_url()."Index/SignIn");
    // }
    $data = array(
      'token' => $token
    );
    $checkToken = $this->main_model->curlRequest("POST", $this->config->item('api_url').'members/login-with-token',$data);
    if(!isset($checkToken->status)){
      delete_cookie('token');
      header("Location: ".base_url()."Index/SignIn");
    }else{
      if(!$checkToken->status){
        delete_cookie('token');
        header("Location: ".base_url()."Index/SignIn");
      }
    }
  }
}

class Main extends PA_Model_Object
{
  function __construct()
  {
    parent::__construct();
  }
}