<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Planlibrary_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
  }

  /**
   * ข้อมูล title จาก table planlibrary
   *
   * @return Array
   **/
  public function get_arr_title()
  {
    $this->db->select('id, title, planId');
    $this->db->from('PlanLibrary');
    $planlibraries = $this->db->get()->result();

    $res = [];
    foreach ($planlibraries as $planlibrary)
      $res[$planlibrary->planId] = $planlibrary->title;

    return $res;
  }

  public function count_plan_library_by_subject($subject_id)
  {
    $sql = "SELECT COUNT(*) AS subject FROM PlanLibrary WHERE library_data->\"$.subject\" = $subject_id";
    return $this->db->query($sql)->row();
  }

}

class Planlibrary extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}