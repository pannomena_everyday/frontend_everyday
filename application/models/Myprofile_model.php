<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myprofile_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
  }

  public function getMemberFromMobile($mobile){
    $this->db->select('*');
    $this->db->from('member');
    $this->db->where(array("mobile" => $mobile));
    return $this->db->get();
  }

  public function getMyNoteSubject($memberId){
    $this->db->distinct();
    $this->db->select('s.*');
    $this->db->from('MemberSheetUpload su');
    $this->db->join('LibrarySheet ls','ls.id = su.librarySheetId');
    $this->db->join('Subject s','s.id = ls.subjectId');
    $this->db->where(array("su.memberId" => $memberId));
    $this->db->order_by('s.order_item', 'asc');
    return $this->db->get();
  }

  public function GetSheetTitle($memberId,$keyword,$subject) {
    $this->db->select_max('id');
    $this->db->from('MemberSheetUpload');
    $this->db->where(array("memberId" => $memberId));
    $this->db->group_by("librarySheetId");
    $maxId = $this->db->get()->result();
    $arr = array();
    foreach($maxId as $row){
      array_push($arr, $row->id);
    }
    $this->db->select('su.path, ls.title, ls.imgUrl');
    $this->db->from('MemberSheetUpload su');
    $this->db->join('LibrarySheet ls','ls.id = su.librarySheetId');
    $this->db->where(array("su.memberId" => $memberId, "ls.subjectId" => $subject));
    $this->db->like('ls.title', $keyword, 'both');
    $this->db->where_in('su.id', $arr);
    $this->db->order_by('su.id', 'desc');
    return $this->db->get()->result();
  }

  public function getMyNoteHTML($memberId,$subject,$search,$dateFrom,$dateTo){
    $this->db->select_max('id');
    $this->db->from('MemberSheetUpload');
    $this->db->where(array("memberId" => $memberId));
    $this->db->group_by("librarySheetId");
    $maxId = $this->db->get()->result();
    $arr = array();
    foreach($maxId as $row){
      array_push($arr, $row->id);
    }
    $this->db->select('su.path, ls.title, ls.imgUrl');
    $this->db->from('MemberSheetUpload su');
    $this->db->join('LibrarySheet ls','ls.id = su.librarySheetId');
    $this->db->where(array("su.memberId" => $memberId, "ls.subjectId" => $subject));
    $this->db->where_in('su.id', $arr);
    if($search != ''){ $this->db->like('ls.title', $search, 'both'); }
    // if($dateFrom != ''){ $this->db->where(array("DATE_FORMAT(su.createdtime, '%Y-%m-%d') >=" => $dateFrom)); }
    // if($dateTo != ''){ $this->db->where(array("DATE_FORMAT(su.createdtime, '%Y-%m-%d') <=" => $dateTo)); }
    if($dateFrom != ''){ $this->db->where(array("date(su.createdtime + interval 7 hour) >=" => $dateFrom)); }
    if($dateTo != ''){ $this->db->where(array("date(su.createdtime + interval 7 hour) <=" => $dateTo)); }
    $this->db->order_by('su.id', 'desc');
    return $this->db->get();
  }

  public function getMemberPlan(){
    $this->db->select('*');
    $this->db->from('PlanLibrary');
    $this->db->order_by('planId','asc');
    return $this->db->get();
  }

  public function getMemberPreTest($memberId,$planId){
    $this->db->select('m.score,m.answers');
    $this->db->from('MemberPlanTest m');
    //$this->db->join('MemberPlanTest m','m.planId = p.planId','left');
    $this->db->where(array("m.memberId" => $memberId, "m.planId" => $planId/*, "m.complete" => 1*/));
    $this->db->order_by('m.updatedtime','desc');
    return $this->db->get();
  }

  public function getMemberTest($memberId,$planId){
    $this->db->select_max('id');
    $this->db->from('MemberSubLevelTest');
    $this->db->where(array("memberId" => $memberId, "planId" => $planId));
    $this->db->group_by("memberId, planId, levelId");
    $maxId = $this->db->get()->result();
    $arr = array(0);
    foreach($maxId as $row){
      array_push($arr, $row->id);
    }
    $sql = "SELECT SUM(socre) as score, asnswers as answers
      FROM MemberSubLevelTest
      WHERE id IN (".implode(',', $arr).")
        AND memberId = ".$memberId."
        AND planId = ".$planId."
      GROUP BY planId, memberId, asnswers";
    return $this->db->query($sql);
  }

  public function getPerformanceData($memberId){
    $this->db->select('m.*, CONCAT(sj.title," / ",l.libraryGroup," / ",l.title," / ",sl.title," / ",ls.title," / ",s.title) as txtTitle');
    $this->db->from('MemberPlanTest m');
    $this->db->join('SheetVideo s','m.sheetVideoId = s.id');
    $this->db->join('LibrarySheet ls','s.librarySheetId = ls.id');
    $this->db->join('LibrarySubLevel sl','ls.librarySubLevelId = sl.id');
    $this->db->join('LibraryLevel l','ls.libraryLevelId = l.id');
    $this->db->join('Subject sj','ls.subjectId = sj.id');
    $this->db->where(array("m.memberId" => $memberId));
    $this->db->order_by('m.updatedtime','desc');
    return $this->db->get();
  }

  public function getLogData($memberId,$dateFrom,$dateTo){
    // $this->db->select('m.*, s.title as videoTitle, ls.title as sheetTitle, sl.title as subLevelTitle, l.title as levelTitle
    //   , sj.title as subjectTitle, l.libraryGroup as libraryGroup');
    $this->db->select('m.*, CONCAT(sj.title," / ",l.libraryGroup," / ",l.title," / ",sl.title," / ",ls.title," / ",s.title) as txtTitle');
    $this->db->from('MemberSheetVideo m');
    $this->db->join('SheetVideo s','m.sheetVideoId = s.id');
    $this->db->join('LibrarySheet ls','s.librarySheetId = ls.id');
    $this->db->join('LibrarySubLevel sl','ls.librarySubLevelId = sl.id');
    $this->db->join('LibraryLevel l','ls.libraryLevelId = l.id');
    $this->db->join('Subject sj','ls.subjectId = sj.id');
    $this->db->where(array("m.memberId" => $memberId));
    if($dateFrom != '' && $dateTo != ''){
      // $this->db->where(array("DATE_FORMAT(m.updatedtime, '%Y-%m-%d') >=" => $dateFrom));
      // $this->db->where(array("DATE_FORMAT(m.updatedtime, '%Y-%m-%d') <=" => $dateTo));
      $this->db->where(array("date(m.updatedtime + interval 7 hour) >=" => $dateFrom));
      $this->db->where(array("date(m.updatedtime + interval 7 hour) <=" => $dateTo));
    }
    $this->db->order_by('m.updatedtime','desc');
    $this->db->order_by('m.sheetVideoId','desc');
    return $this->db->get();
  }

  public function getProvinces(){
    $sql = "SELECT * FROM provinces ORDER BY CASE WHEN id = 1 THEN 0 ELSE 1 END ASC, name_th ASC";
    return $this->db->query($sql);
    // $this->db->select('*');
    // $this->db->from('provinces');
    // $this->db->order_by('name_th','asc');
    // return $this->db->get();
  }

  public function getProvince(){
    $sql = "SELECT * FROM Province ORDER BY CASE WHEN ProvinceID = 106 THEN 0 ELSE 1 END ASC, ProvinceName ASC";
    return $this->db->query($sql);
  }

  public function getAmphures($provinceId){
    $this->db->select('*');
    $this->db->from('amphures');
    $this->db->where(array("province_id" => $provinceId));
    $this->db->order_by('name_th','asc');
    return $this->db->get();
  }

  public function getDistricts($amphureId){
    $this->db->select('*');
    $this->db->from('districts');
    $this->db->where(array("amphure_id" => $amphureId));
    $this->db->order_by('name_th','asc');
    return $this->db->get();
  }

  public function getZipCode($districtId){
    $this->db->select('*');
    $this->db->from('districts');
    $this->db->where(array("id" => $districtId));
    return $this->db->get();
  }

  public function getBillingAddress($memberId){
    $this->db->select('*');
    $this->db->from('BillingAddress');
    $this->db->where(array("memberId" => $memberId));
    return $this->db->get();
  }

  public function getSubscriberByID($id){
    $this->db->select('*');
    $this->db->from('SubscriptionType');
    $this->db->where(array("id" => $id));
    return $this->db->get();
  }

  public function getBillingAddressIncludeDetail($memberId){
    return  $this->db
            ->select('BillingAddress.* , provinces.name_th as province_name, districts.name_th as district_name, amphures.name_th as amphure_name')
            ->from('BillingAddress')
            ->join('provinces', 'BillingAddress.provinceId = provinces.id')
            ->join('districts', 'BillingAddress.districtId = districts.id')
            ->join('amphures', 'BillingAddress.amphureId = amphures.id')
            ->where('BillingAddress.memberId', $memberId)
            ->get()->row();
      }

  public function insertBillingAddress($memberId,$fullname,$mobile,$address,$provinceId,$amphureId,$districtId,$zipCode){
    $this->db->trans_start();
    if($this->db->get_where('BillingAddress', array("memberId" => $memberId))->num_rows() == 0){
      $data = array(
        'memberId' => $memberId,
        'fullname' => $fullname,
        'mobile' => $mobile,
        'address' => $address,
        'provinceId' => $provinceId,
        'amphureId' => $amphureId,
        'districtId' => $districtId,
        'zipCode' => $zipCode,
        'createdtime' => date("Y-m-d H:i:s"),
        'updatedtime' => date("Y-m-d H:i:s")
      );
      $this->db->insert('BillingAddress', $data);
      if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
      }else{
            $this->db->trans_commit();
            return true;
      }
    }else{
      $data = array(
        'memberId' => $memberId,
        'fullname' => $fullname,
        'mobile' => $mobile,
        'address' => $address,
        'provinceId' => $provinceId,
        'amphureId' => $amphureId,
        'districtId' => $districtId,
        'zipCode' => $zipCode,
        'updatedtime' => date("Y-m-d H:i:s")
      );
      $this->db->where('memberId', $memberId);
      $this->db->update('BillingAddress', $data);
      if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
      }else{
            $this->db->trans_commit();
            return true;
      }
    }
  }

}

class Myprofile_m extends PA_Model_Object
{
  function __construct()
  {
    parent::__construct();
  }
}