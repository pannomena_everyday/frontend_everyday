<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Membertest_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(array('main_model','myprofile_model'));
  }

  public function find_by_member_sublevel_test($member_id, $sublevel_sheet_id)
  {
    $this->db->select('*');
    $this->db->from('MemberSubLevelTest');
    $this->db->where('memberId', $member_id);
    $this->db->where('subLevelTestId', $sublevel_sheet_id);
    $this->db->where('complete', 0);

    return $this->db->get()->row();
  }
  
  public function find_latest_complete_member_sublevel_test($member_id, $sublevel_sheet_id)
  {
    $this->db->select('*');
    $this->db->from('MemberSubLevelTest');
    $this->db->where('memberId', $member_id);
    $this->db->where('subLevelTestId', $sublevel_sheet_id);
    $this->db->where('complete', 1);
    $this->db->order_by('createdtime', 'DESC');

    return $this->db->get()->row();
  }
  
  public function find_by_member_plan_test($member_id, $plan_id)
  {
    $this->db->select('*');
    $this->db->from('MemberPlanTest');
    $this->db->where('memberId', $member_id);
    $this->db->where('planId', $plan_id);
    
    return $this->db->get()->row();
  }

  
  public function find_on_going_member_plan_test($member_id)
  {
    $this->db->select('*');
    $this->db->from('MemberPlanTest');
    $this->db->where('memberId', $member_id);
    $this->db->where('complete', 0);
    return $this->db->get()->row();
  }

  public function find_on_going_member_sublevel_test($member_id)
  {
    $this->db->select('*');
    $this->db->from('MemberSubLevelTest');
    $this->db->where('memberId', $member_id);
    $this->db->where('complete', 0);
    return $this->db->get()->row();
  }

  public function check_on_going_test($member_id, $token){
    $sublevel_test = $this->find_on_going_member_sublevel_test($member_id);
    if(isset($sublevel_test)){
      $current_test = $this->main_model->curlRequest("GET", $this->config->item('api_url').'member-sub-level-tests/'.$sublevel_test->id, null, $token);
      $answer = $current_test->asnswers;
      redirect('Test/Question/'.$answer->sublevel_test_id.'/'.$answer->level_id.'/'.$answer->plan_id, 'refresh');
      die();
    }
    
    $plan_test = $this->find_on_going_member_plan_test($member_id);
    if(isset($plan_test)){
      redirect('preTest/question/?plan='.$plan_test->planId, 'refresh');
      die();
    }

  }
  
  public function check_billing_exists($myProfile){
    if($myProfile->subscriptionTypeId == 1)
      return false;

    $billAddr = $this->myprofile_model->getBillingAddress($myProfile->id);
    if(!isset($billAddr->result()[0])){
      redirect('payment/billing', 'refresh');
      die();
    }
  }
  
}
