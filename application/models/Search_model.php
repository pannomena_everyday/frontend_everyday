<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
  }

  public function GetSubjectByKeyword($keyword) {
    $sql = "SELECT l.subjectId as id, s.title FROM LibraryLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active = 1
    UNION
    SELECT l.subjectId as id, s.title FROM LibrarySubLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active = 1
    UNION
    SELECT l.subjectId, s.title FROM LibrarySheet as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active = 1
    UNION
    SELECT l.subjectId, s.title FROM SheetVideo as v
      INNER JOIN LibrarySheet as l ON l.id = v.librarySheetId
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (v.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR v.title COLLATE utf8_general_ci LIKE '%$keyword%')";
    return $this->db->query($sql);
  }

  public function GetSubjectByKeyword_Guest($keyword) {
    $sql = "SELECT l.subjectId as id, s.title FROM LibraryLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active = 1
    UNION
    SELECT l.subjectId as id, s.title FROM LibrarySubLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active = 1
    UNION
    SELECT l.subjectId, s.title FROM LibrarySheet as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active = 1";
    return $this->db->query($sql);
  }

  public function GetLibraryByKeywordAndSubjectId($keyword,$subjectId) {
    ($subjectId != '' && $subjectId != null)? $whereSubject = " AND l.subjectId = '".$subjectId."'" : $whereSubject = '';
    // $sql = "SELECT l.id as id, l.title, 1 as ord FROM LibraryLevel as l
    //   INNER JOIN Subject as s ON s.id = l.subjectId
    // WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active ".$whereSubject."
    // UNION
    // SELECT l.libraryGroup as id, l.libraryGroup COLLATE utf8_general_ci, 0 as ord FROM LibraryLevel as l
    //   INNER JOIN Subject as s ON s.id = l.subjectId
    // WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active ".$whereSubject."
    // UNION
    // SELECT a.id, a.title, 1 as ord FROM LibrarySubLevel as l
    //   INNER JOIN Subject as s ON s.id = l.subjectId
    //   INNER JOIN LibraryLevel as a ON a.id = l.libraryLevelId
    // WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active ".$whereSubject."
    // UNION
    // SELECT a.id, a.title, 1 as ord FROM LibrarySheet as l
    //   INNER JOIN Subject as s ON s.id = l.subjectId
    //   INNER JOIN LibraryLevel as a ON a.id = l.libraryLevelId
    // WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active ".$whereSubject."
    // UNION
    // SELECT a.id, a.title, 1 as ord FROM SheetVideo as v
    //   INNER JOIN LibrarySheet as l ON l.id = v.librarySheetId
    //   INNER JOIN Subject as s ON s.id = l.subjectId
    //   INNER JOIN LibraryLevel as a ON a.id = l.libraryLevelId
    // WHERE (v.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR v.title COLLATE utf8_general_ci LIKE '%$keyword%')  ".$whereSubject."
    // ORDER BY ord asc, title asc";
    $sql = "SELECT DISTINCT l.libraryGroup as id, l.libraryGroup COLLATE utf8_general_ci as title, 0 as ord FROM LibraryLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active = 1 ".$whereSubject."
    ORDER BY ord asc, title asc";
    return $this->db->query($sql);
  }

  public function GetLibraryByKeywordAndSubjectId_Guest($keyword,$subjectId) {
    ($subjectId != '' && $subjectId != null)? $whereSubject = " AND l.subjectId = '".$subjectId."'" : $whereSubject = '';
    $sql = "SELECT l.id as id, l.title, 1 as ord FROM LibraryLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active ".$whereSubject."
    UNION
    SELECT l.libraryGroup as id, l.libraryGroup COLLATE utf8_general_ci, 0 as ord FROM LibraryLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active ".$whereSubject."
    UNION
    SELECT a.id, a.title, 1 as ord FROM LibrarySubLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
      INNER JOIN LibraryLevel as a ON a.id = l.libraryLevelId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active ".$whereSubject."
    UNION
    SELECT a.id, a.title, 1 as ord FROM LibrarySheet as l
      INNER JOIN Subject as s ON s.id = l.subjectId
      INNER JOIN LibraryLevel as a ON a.id = l.libraryLevelId
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%') and l.is_active ".$whereSubject."
    ORDER BY ord asc, title asc";
    return $this->db->query($sql);
  }

  public function GetSearchData($keyword) {
    $sql = "SELECT title COLLATE utf8_general_ci as title FROM LibraryLevel";
    $sql.= " WHERE (tag COLLATE utf8_general_ci LIKE '%$keyword%' OR title COLLATE utf8_general_ci LIKE '%$keyword%') and is_active = 1";
    $sql.= " UNION";
    $sql.= " SELECT title COLLATE utf8_general_ci as title FROM LibrarySubLevel";
    $sql.= " WHERE (tag COLLATE utf8_general_ci LIKE '%$keyword%' OR title COLLATE utf8_general_ci LIKE '%$keyword%') and is_active = 1";
    $sql.= " UNION";
    $sql.= " SELECT title COLLATE utf8_general_ci as title FROM LibrarySheet";
    $sql.= " WHERE (tag COLLATE utf8_general_ci LIKE '%$keyword%' OR title COLLATE utf8_general_ci LIKE '%$keyword%') and is_active = 1";
    $sql.= " UNION";
    $sql.= " SELECT title COLLATE utf8_general_ci as title FROM SheetVideo";
    $sql.= " WHERE tag COLLATE utf8_general_ci LIKE '%$keyword%' OR title COLLATE utf8_general_ci LIKE '%$keyword%'";
    return $this->db->query($sql)->result_array();
  }

  public function GetSearchResult_Library($keyword,$subjectId,$libraryId) {
    ($subjectId != '' && $subjectId != null)? $whereSubject = " AND l.subjectId = '".$subjectId."'" : $whereSubject = '';
    ($libraryId != '' && $libraryId != null)? $whereLibrary = " AND l.libraryGroup = '".$libraryId."'" : $whereLibrary = '';
    //($libraryId != '' && $libraryId != null)? $whereLibrary = " AND (l.id = '".$libraryId."' OR l.libraryGroup = '".$libraryId."')" : $whereLibrary = '';
    $sql = "SELECT DISTINCT 'libLevel' as txtType, sl.title, sl.imgUrl, sl.libraryLevelId as id, sl.id as subLevelId, sl.subjectId, sl.is_comingsoon, sl.only_subscriber
    FROM LibraryLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
      INNER JOIN LibrarySubLevel sl on sl.libraryLevelId = l.id
      INNER JOIN LibrarySheet ls on ls.librarySubLevelId = sl.id
    WHERE (l.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR l.title COLLATE utf8_general_ci LIKE '%$keyword%'
      OR sl.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR sl.title COLLATE utf8_general_ci LIKE '%$keyword%'
      OR ls.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR ls.title COLLATE utf8_general_ci LIKE '%$keyword%')
      and sl.is_active = 1 ".$whereSubject." ".$whereLibrary;
    return $this->db->query($sql);
  }

  public function GetSearchResult_Video($keyword,$subjectId,$libraryId) {
    ($subjectId != '' && $subjectId != null)? $whereSubject = " AND l.subjectId = '".$subjectId."'" : $whereSubject = '';
    ($libraryId != '' && $libraryId != null)? $whereLibrary = " AND ll.libraryGroup = '".$libraryId."'" : $whereLibrary = '';
    //($libraryId != '' && $libraryId != null)? $whereLibrary = " AND (l.libraryLevelId = '".$libraryId."' OR ll.libraryGroup = '".$libraryId."')" : $whereLibrary = '';
    $sql = "SELECT DISTINCT v.title, v.imgUrl, v.librarySheetId as id, sl.is_comingsoon, sl.only_subscriber
    FROM SheetVideo as v
      INNER JOIN LibrarySheet as l ON l.id = v.librarySheetId
      INNER JOIN LibrarySubLevel sl ON sl.libraryLevelId = l.libraryLevelId AND sl.id = l.librarySubLevelId
      INNER JOIN LibraryLevel ll on ll.id = sl.libraryLevelId
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (v.tag COLLATE utf8_general_ci LIKE '%$keyword%' OR v.title COLLATE utf8_general_ci LIKE '%$keyword%')  ".$whereSubject." ".$whereLibrary;
    return $this->db->query($sql);
  }

  public function GetVideoById($id) {
    $sql = "SELECT s.title, g.gcsName, s.imgUrl, s.id, l.sheetCode";
    $sql.= " FROM SheetVideo as s";
    $sql.= " INNER JOIN GcsVideo as g on g.id = s.link";
    $sql.= " INNER JOIN LibrarySheet as l on l.id = s.librarySheetId";
    $sql.= " WHERE s.id = '".$id."'";
    return $this->db->query($sql);
  }
}

class SearchModel extends PA_Model_Object
{
  function __construct()
  {
    parent::__construct();
  }
}