<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
  }

  public function GetSubjectByKeyword($keyword) {
    $sql = "SELECT l.subjectId as id, s.title FROM LibraryLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.datasearch LIKE '%$keyword%' OR l.title LIKE '%$keyword%') and l.is_active = 1
    UNION
    SELECT l.subjectId, s.title FROM LibrarySubLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.datasearch LIKE '%$keyword%' OR l.title LIKE '%$keyword%') and l.is_active = 1
    UNION
    SELECT l.subjectId, s.title FROM LibrarySheet as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.datasearch LIKE '%$keyword%' OR l.title LIKE '%$keyword%') and l.is_active = 1
    UNION
    SELECT l.subjectId, s.title FROM SheetVideo as v
      INNER JOIN LibrarySheet as l ON l.id = v.librarySheetId
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (v.datasearch LIKE '%$keyword%' OR v.title LIKE '%$keyword%')";
    return $this->db->query($sql);
  }

  public function GetLibraryByKeywordAndSubjectId($keyword,$subjectId) {
    ($subjectId != '' && $subjectId != null)? $whereSubject = " AND l.subjectId = '".$subjectId."'" : $whereSubject = '';
    $sql = "SELECT l.id as id, l.title FROM LibraryLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.datasearch LIKE '%$keyword%' OR l.title LIKE '%$keyword%') and l.is_active ".$whereSubject."
    UNION
    SELECT a.id, a.title FROM LibrarySubLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
      INNER JOIN LibraryLevel as a ON a.id = l.libraryLevelId
    WHERE (l.datasearch LIKE '%$keyword%' OR l.title LIKE '%$keyword%') and l.is_active ".$whereSubject."
    UNION
    SELECT a.id, a.title FROM LibrarySheet as l
      INNER JOIN Subject as s ON s.id = l.subjectId
      INNER JOIN LibraryLevel as a ON a.id = l.libraryLevelId
    WHERE (l.datasearch LIKE '%$keyword%' OR l.title LIKE '%$keyword%') and l.is_active ".$whereSubject."
    UNION
    SELECT a.id, a.title FROM SheetVideo as v
      INNER JOIN LibrarySheet as l ON l.id = v.librarySheetId
      INNER JOIN Subject as s ON s.id = l.subjectId
      INNER JOIN LibraryLevel as a ON a.id = l.libraryLevelId
    WHERE (v.datasearch LIKE '%$keyword%' OR v.title LIKE '%$keyword%')  ".$whereSubject;
    return $this->db->query($sql);
  }

  public function GetSearchData($keyword) {
    $sql = "SELECT title FROM LibraryLevel";
    $sql.= " WHERE (datasearch LIKE '%$keyword%' OR title LIKE '%$keyword%') and is_active = 1";
    $sql.= " UNION";
    $sql.= " SELECT title FROM LibrarySubLevel";
    $sql.= " WHERE (datasearch LIKE '%$keyword%' OR title LIKE '%$keyword%') and is_active = 1";
    $sql.= " UNION";
    $sql.= " SELECT title FROM LibrarySheet";
    $sql.= " WHERE (datasearch LIKE '%$keyword%' OR title LIKE '%$keyword%') and is_active = 1";
    $sql.= " UNION";
    $sql.= " SELECT title FROM SheetVideo";
    $sql.= " WHERE datasearch LIKE '%$keyword%' OR title LIKE '%$keyword%'";
    return $this->db->query($sql)->result_array();
  }

  public function GetSearchResult_Library($keyword,$subjectId,$libraryId) {
    ($subjectId != '' && $subjectId != null)? $whereSubject = " AND l.subjectId = '".$subjectId."'" : $whereSubject = '';
    ($libraryId != '' && $libraryId != null)? $whereLibrary = " AND l.id = '".$libraryId."'" : $whereLibrary = '';
    ($libraryId != '' && $libraryId != null)? $whereLibrary2 = " AND l.libraryLevelId = '".$libraryId."'" : $whereLibrary2 = '';
    $sql = "SELECT 'libLevel' as txtType, l.title, imgUrl, l.id, subjectId, l.is_comingsoon
      FROM LibraryLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.datasearch LIKE '%$keyword%' OR l.title LIKE '%$keyword%') and l.is_active ".$whereSubject." ".$whereLibrary."
    UNION
    SELECT 'libSubLevel' as txtType, l.title, imgUrl, libraryLevelId as id, subjectId, l.is_comingsoon
      FROM LibrarySubLevel as l
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (l.datasearch LIKE '%$keyword%' OR l.title LIKE '%$keyword%') and l.is_active ".$whereSubject." ".$whereLibrary2."
    UNION
    SELECT 'libSheet' as txtType, l.title, l.imgUrl, l.libraryLevelId as id, l.subjectId, sl.is_comingsoon
      FROM LibrarySheet as l
      INNER JOIN Subject as s ON s.id = l.subjectId
      INNER JOIN LibrarySubLevel as sl ON sl.id = l.librarySubLevelId
    WHERE (l.datasearch LIKE '%$keyword%' OR l.title LIKE '%$keyword%') and l.is_active ".$whereSubject." ".$whereLibrary2;
    return $this->db->query($sql);
  }

  public function GetSearchResult_Video($keyword,$subjectId,$libraryId) {
    ($subjectId != '' && $subjectId != null)? $whereSubject = " AND l.subjectId = '".$subjectId."'" : $whereSubject = '';
    ($libraryId != '' && $libraryId != null)? $whereLibrary = " AND l.libraryLevelId = '".$libraryId."'" : $whereLibrary = '';
    $sql = "SELECT v.title, g.gcsName, v.imgUrl, v.librarySheetId as id
    FROM SheetVideo as v
      INNER JOIN GcsVideo as g on g.id = v.link
      INNER JOIN LibrarySheet as l ON l.id = v.librarySheetId
      INNER JOIN Subject as s ON s.id = l.subjectId
    WHERE (v.datasearch LIKE '%$keyword%' OR v.title LIKE '%$keyword%')  ".$whereSubject." ".$whereLibrary;
    return $this->db->query($sql);
  }

  public function GetVideoById($id) {
    $sql = "SELECT s.title, g.gcsName, s.imgUrl, s.id, l.sheetCode";
    $sql.= " FROM SheetVideo as s";
    $sql.= " INNER JOIN GcsVideo as g on g.id = s.link";
    $sql.= " INNER JOIN LibrarySheet as l on l.id = s.librarySheetId";
    $sql.= " WHERE s.id = '".$id."'";
    return $this->db->query($sql);
  }
}

class SearchModel extends PA_Model_Object
{
  function __construct()
  {
    parent::__construct();
  }
}