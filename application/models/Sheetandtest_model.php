<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sheetandtest_model extends PA_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function find_by_sublevel_id($sublevel_id)
  {
    $this->db->select('*');
    $this->db->from('SheetAndTest');
    $this->db->where('subLevelId', $sublevel_id);

    return $this->db->get()->row();
  }
}

class Sheetandtest extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}