<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Memberplan_model extends PA_Model
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(['main_model', 'librarysheet_model', 'sheetvideo_model', 'subject_model', 'planlibrary_model', 'librarylevel_model', 'librarysublevel_model']);
  }

  public function find_subscription_type() {
    return $this->db
      ->where('is_active', 1)
      ->from('SubscriptionType')
      ->order_by('order_item', 'ASC')
      ->get()->result();
  }
  
  public function is_promotion_exist($promotion, $subscription_id) {

    $promotion = $this->db->escape_str($promotion);
    $subscription_id = $this->db->escape_str($subscription_id);
    $sql = "SELECT
              CASE 
                WHEN NOW() NOT BETWEEN starttime AND endtime THEN 1
                WHEN useLimit <= usaged THEN 2
                ELSE 0
              END as status
            FROM Promotion
            WHERE promoCode = '$promotion'
            AND is_active = 1
            AND JSON_CONTAINS(packages, '$subscription_id', '$')
    ";
    // $sql = "SELECT count(*) as count FROM Promotion
    //         WHERE promoCode = '$promotion'
    //         -- AND promoType = 2
    //         AND is_active = 1
    //         AND NOW() BETWEEN starttime AND endtime
    //         AND JSON_CONTAINS(packages, '$subscription_id', '$')
    // ";
    $result = $this->db->query($sql)->row();
    return $result;
    // echo json_encode($result);
    // die();
    // return $count->count > 0;
  }
  
  public function find_promotion($promotion) {
    return $this->db
      ->where('promoCode', $promotion)
      ->where('promoType', 2)
      ->from('Promotion')
      ->get()->row();
  }
  
  public function find_promotion_by_id($promotion) {
    return $this->db
      ->where('id', $promotion)
      ->where('promoType', 2)
      ->from('Promotion')
      ->get()->row();
  }

  public function find_promotion_by_code($promotion) {
    return $this->db
      ->where('promoCode', $promotion)
      ->where('promoType', 2)
      ->from('Promotion')
      ->get()->row();
  }
  
  /**
   * หาแผนการเรียนทั้งหมด Group by วิชา
   *
   * @return void
   **/
  public function get_all_plan_group_by_subjects($member_id)
  {
    $subjects = $this->subject_model->all_arr();
    $plans = $this->planlibrary_model->get_arr_title();

    $res = [];
    $this->db->select('*, PlanLibrary.title AS title_plan, PlanLibrary.library_data AS librariesProgress');
    $this->db->from('MemberPlan');
    $this->db->join('Plan', 'MemberPlan.planId = Plan.id AND is_active = 1');
    $this->db->join('PlanLibrary', 'PlanLibrary.planId = Plan.id');
    $this->db->where('memberId', $member_id);
    $this->db->where('Plan.is_active', 1);
    $this->db->order_by('MemberPlan.updatedtime', 'DESC');
    $member_plans = $this->db->get()->result();

    $levels = [];
    // หาข้อมูล PlanLevelData
    $this->db->select('planId, GROUP_CONCAT(levelId SEPARATOR ", ") AS levels');
    $this->db->from('PlanLevelData');
    $this->db->join('Plan', 'PlanLevelData.planId = Plan.id AND is_active = 1');
    $this->db->where('Plan.is_active', 1);
    $this->db->group_by('planId');
    $plan_levels = $this->db->get()->result();

    foreach ($plan_levels as $plan_level)
      $levels[$plan_level->planId] = json_decode('[' . $plan_level->levels . ']', true);

    $all_subjects = []; // วิชาทั้งหมดที่ member นี้มี
    foreach ($member_plans as $member_plan)
    {
      $libraries_progress = json_decode($member_plan->librariesProgress);

      if(!empty($levels[$member_plan->planId]))
      {
        $complete_vdo = $this->sheetvideo_model->find_progress_of_member($levels[$member_plan->planId], $member_id);
        $all_vdo = $this->sheetvideo_model->find_all_vdo_of_member($levels[$member_plan->planId], $member_id);
      }
      else
        $all_vdo = "";

      if(!empty($all_vdo) && isset($all_vdo->vdo) && $all_vdo->vdo > 0)
        $progress_bar = round($complete_vdo->vdo / $all_vdo->vdo * 100);
      else
        $progress_bar = 0;

      $library_levels =  $this->librarylevel_model->all_by_subject_group_id($libraries_progress->subject); // level

      // หาข้อมูล Subject
      $subject = $this->db->get_where('Subject', ['id' => $libraries_progress->subject])->row();
      $res[$libraries_progress->subject][$member_plan->planId]['plan_id'] = $member_plan->planId;
      $res[$libraries_progress->subject][$member_plan->planId]['updatedtime'] = $member_plan->updatedtime;
      $res[$libraries_progress->subject][$member_plan->planId]['progress_bar'] = $progress_bar;
      $res[$libraries_progress->subject][$member_plan->planId]['color_code'] = $subject->color_code;
      $res[$libraries_progress->subject][$member_plan->planId]['title_plan'] = $member_plan->title_plan;
      $res[$libraries_progress->subject][$member_plan->planId]['subject_id'] = $libraries_progress->subject;
      $res[$libraries_progress->subject][$member_plan->planId]['complete_vdo'] = isset($complete_vdo->vdo) ? $complete_vdo->vdo : 0;
      $res[$libraries_progress->subject][$member_plan->planId]['all_vdo'] = isset($all_vdo->vdo) ? $all_vdo->vdo : 0;

      foreach ($libraries_progress->levels as &$level) // level
      {
        $progress = $this->sheetvideo_model->find_progress_of_level($level->id, $member_id);
        $total_progress = $progress->vdo * 100;
        $current = $progress->total_progress == null || $progress->total_progress == '' ? 0 : $progress->total_progress;
        if($total_progress <= 0)
          $level->progress_bar = 0;
        else
          $level->progress_bar = round($current / $total_progress * 100);

        $library_level = $library_levels[$level->id];

        $level->is_comingsoon = $library_level['is_comingsoon'];
        $level->title = $library_level['title'];
        $level->imgUrl = $library_level['imgUrl'];
      }

      $res[$libraries_progress->subject][$member_plan->planId]['levels'] = $libraries_progress->levels;

      if(!in_array($libraries_progress->subject, $all_subjects))
        $all_subjects[] = $libraries_progress->subject;
    }

    $current_plan_all_subjects = []; // แผนการเรียนปัจจุบัน
    $all_plans = []; // แผนการเรียนที่เคยเลือกเรียน
    foreach ($all_subjects as $subject_id)
    {
      if(!in_array(current($res[$subject_id]), $current_plan_all_subjects))
        $current_plan_all_subjects[] = current($res[$subject_id]);

      foreach (array_slice($res[$subject_id], 1) as $key => $value) {
        $all_plans[] = $value;
      }
    }

    return ['current_plan' => $current_plan_all_subjects, 'all_plans' => $all_plans];
  }
}


class Memberplan extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}