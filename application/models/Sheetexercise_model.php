<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sheetexercise_model extends PA_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function find_by_librarySheetId($librarysheet_id)
  {
    $this->db->select('*');
    $this->db->from('SheetExercise');
    $this->db->order_by('ex_no', 'ASC');
    $this->db->where('librarySheetId', $librarysheet_id);

    return $this->db->get()->result();
  }
}

class Sheetexercise extends PA_Model_Object
{
  
  function __construct()
  {
    parent::__construct();
  }
}