<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function active_link($url = '')
{
  if(current_url() == base_url($url)) echo 'active';
}

function active_class($attr_val, $compared_val, $no_print = false, $class_name)
{
  if($attr_val == $compared_val)
  {
    if($no_print) return $class_name;
    echo $class_name;
  }
}

?>