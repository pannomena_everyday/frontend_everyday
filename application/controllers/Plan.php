<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(['main_model', 'subject_model', 'memberplan_model']);

    $token = get_cookie('token');
    $this->main_model->checkToken($token);

    // list วิชา
    $this->plan_trees = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-tree' , null, $token);
  }
  
  public function StartPlan()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'เริ่มต้นเรียนออนไลน์ ทุกที่ ทุกเวลา - Monkey Everyday';
    $this->data['meta_description'] = 'เรียนออนไลน์ ทุกที่ ทุกเวลา';
    $this->data["page_active"] = "myEveryDay";
    $this->data['view_name'] = 'v_startPlan';
    $this->load->view('layouts/main', $this->data);
  }
  
  public function AddPlan()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me', null, $token);

    $subjects = $this->subject_model->all_arr();
    $lang = get_cookie('lang');

    if($lang == null){ $lang = "en"; }
    $subject_id = $this->input->get('subject');
    $from_page = $this->input->get('from_page'); // มาจากหน้าไหน

    if(isset($subject_id))
    {
      $this->data['subject_title'] = $subjects[$subject_id];
    }
    else
    {
      $this->data['subject_title'] = null;
    }

    $subject_arr = []; // เก็บ id วิชา current ทั้งหมดของ user ที่ login
    if(!empty($from_page))
    {
      // หาข้อมูล Current subject ของคนที่ Login
      $all_plans = $this->memberplan_model->get_all_plan_group_by_subjects($myProfile->id);

      foreach ($all_plans['current_plan'] as $plan)
        $subject_arr[] = $subjects[$plan['subject_id']];

      $this->data["subject_arr"] = $subject_arr;
    }

    $this->data["page_active"] = "myEveryDay";
    $this->data['plan_trees'] = $this->plan_trees;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'เพิ่มแผนการเรียน';
    $this->data['meta_description'] = 'เริ่มต้นวางแผนการเรียนกันเลย';
    $this->data['view_name'] = 'v_addPlan';
    $this->load->view('layouts/main', $this->data);
  }

  /**
   * หาข้อมูล Sub plan tree
   *
   * @return void
   **/
  public function get_sub_plan()
  {
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $subject_id = $this->input->get('subject');

    $sub_plan_trees = [];
    foreach ($this->plan_trees as $plan_tree)
    {
      $sub_plan_trees[$plan_tree->id] = $plan_tree;
    }

    $this->_print_json(json_encode($sub_plan_trees[$subject_id]));
  }

  /**
   * หาข้อมูล Sub plan tree
   *
   * @return void
   **/
  public function get_children_sub_plan()
  {
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $subplan_id = $this->input->get('subplan');
    $subject_id = $this->input->get('subject');

    $sub_plan_trees = [];
    foreach ($this->plan_trees as $plan_tree)
    {
      if(!empty($plan_tree->children))
      {
        $sub_plan_trees[$plan_tree->id] = [];
        foreach ($plan_tree->children as $children)
        {
          $sub_plan_trees[$plan_tree->id]['children'][$children->id] = $children;
        }
      }
    }

    $temp_plan = $sub_plan_trees[$subject_id]['children'][$subplan_id];
    if($temp_plan->hasChild)
    {
      foreach ($temp_plan->children as $key => $children) {
        // find plan id
        $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-libraries/'.$children->plan_key , null, $token);
        // check is active
        $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$plan->planId , null, $token);
        if(!$plan->is_active){
          unset($temp_plan->children[$key]);
        }
      }

      if(count($temp_plan->children) == 0){
        $temp_plan->hasChild = false;
      }
    }
    // $this->_print_json(json_encode($sub_plan_trees[$subject_id]['children'][$subplan_id]));
    $this->_print_json(json_encode($temp_plan));
  }

  /**
   * หาข้อมูล Sub plan tree
   *
   * @return void
   **/
  public function get_children_sub_sub_plan()
  {
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $subplan_id = $this->input->get('subplan');
    $sub_sub_plan_id = $this->input->get('subsubplan');
    $subject_id = $this->input->get('subject');

    $sub_plan_trees = [];
    $sub_sub_plan_trees = [];
    foreach ($this->plan_trees as $plan_tree)
    {
      if(!empty($plan_tree->children))
      {
        $sub_plan_trees[$plan_tree->id] = [];
        foreach ($plan_tree->children as $children)
        {
          $sub_plan_trees[$plan_tree->id]['children'][$children->id] = $children;
        }
      }
    }

    foreach ($sub_plan_trees[$subject_id]['children'][$subplan_id]->children as $plan_tree)
    {
      $sub_sub_plan_trees[$plan_tree->id] = $plan_tree;
    }
    $this->_print_json(json_encode($sub_sub_plan_trees[$sub_sub_plan_id]));
  }

  /**
   * หาข้อมูล Sub plan tree
   *
   * @return void
   **/
  public function get_children_sub_sub_sub_plan()
  {
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $subplan_id = $this->input->get('subplan');
    $sub_sub_plan_id = $this->input->get('subsubplan');
    $sub_sub_sub_plan_id = $this->input->get('subsubsubplan');
    $subject_id = $this->input->get('subject');

    $sub_plan_trees = [];
    $sub_sub_plan_trees = [];
    $sub_sub_sub_plan_trees = [];

    foreach ($this->plan_trees as $plan_tree)
    {
      if(!empty($plan_tree->children))
      {
        $sub_plan_trees[$plan_tree->id] = [];
        foreach ($plan_tree->children as $children)
        {
          $sub_plan_trees[$plan_tree->id]['children'][$children->id] = $children;
        }
      }
    }
    
    foreach ($sub_plan_trees[$subject_id]['children'][$subplan_id]->children as $plan_tree)
    {
      $sub_sub_plan_trees[$plan_tree->id] = $plan_tree;
    }

    foreach ($sub_sub_plan_trees[$sub_sub_plan_id]->children as $plan_tree)
    {
      $sub_sub_sub_plan_trees[$plan_tree->id] = $plan_tree;
    }

    $this->_print_json(json_encode($sub_sub_sub_plan_trees[$sub_sub_sub_plan_id]));
  }

  /**
   * หาข้อมูล plan libraries
   *
   * @return void
   **/
  public function get_plan_libraries()
  {
    $this->load->model('memberplan_model');
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $plan_key = $this->input->get('plan_key');
    $children = $this->input->get('children');
    $sub_plan_tree = $this->input->get('sub_plan_tree');
    $sub_sub_plan_tree = $this->input->get('sub_sub_plan_tree');
    $sub_sub_sub_plan_tree = $this->input->get('sub_sub_sub_plan_tree');

    $step = (object) ['sub_plan_tree' => $sub_plan_tree, 'sub_sub_plan_tree' => $sub_sub_plan_tree, 'sub_sub_sub_plan_tree' => $sub_sub_sub_plan_tree];

    // plan libraries
    $plan_libraries = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-libraries/'.$plan_key, null, $token);

    // Profile
    $profile = $this->main_model->curlRequest("GET",$this->config->item('api_url').'members/me', null, $token);

    if($children)
    {
      $member_plan_data = [];
      $member_plan_data['memberId'] = intval($profile->id);
      $member_plan_data['planId'] = intval($plan_libraries->planId);

      // หาข้อมูล Plan เพื่อเช็ค is_active
      $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$member_plan_data['planId'], null, $token);

      if($plan->is_active && !empty($plan->planLibrary->library_data->subject) && !empty($plan->planLibrary->library_data->levels))
      {
        // save member plan
        $response = $this->main_model->curlRequest("POST", $this->config->item('api_url').'front-end/add-plan/', $member_plan_data, $token);
        if(isset($response->status) && $response->status === false)
        {
          $this->session->set_flashdata('error', 'นักเรียนได้เพิ่มแผนเรียนนี้ไว้แล้ว โปรดเลือกแผนการเรียนใหม่');
          $this->_print_json(json_encode(['status' => '404']));
        }
        else
        {
          $this->_print_json(json_encode($plan_libraries));
        }
      }
      else
      {
        $this->session->set_flashdata('error', 'แผนการเรียนนี้อยู่ระหว่างจัดทำ ยังไม่สามารถเพิ่มแผนการเรียนนี้ได้');
        $this->_print_json(json_encode(['status' => '404']));
      }
    }
  }
  
  public function AddPlanComplete()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $planId = $this->input->get('plan');
    $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$planId, null);

    $this->data["page_active"] = "myEveryDay";
    $this->data['meta_title'] = 'เพิ่มแผนการเรียนสำเร็จ';
    $this->data['meta_description'] = 'แผนการเรียนพร้อมแล้ว เริ่มเรียนได้เลย';
    $this->data["language"] = $lang;
    $this->data['plan'] = $plan;
    $this->data['view_name'] = 'v_addPlan_complete';
    $this->load->view('layouts/main', $this->data);
  }
  
}
