<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(['main_model']);
  }
  
  public function index()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'ติดต่อเรา - Monkey Everyday เรียนออนไลน์ ติวเข้ม เตรียมสอบ ฟรี';
    $this->data['meta_description'] = 'ปรึกษาปัญหาการเรียน ทั้งเนื้อหาและวิธีการเรียนได้ที่นี่';
    $this->data['view_name'] = 'v_contact';
    $this->load->view('layouts/main', $this->data);
  }
}
