<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PreTest extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(array('main_model', 'membertest_model'));
  }

  public function index()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }

    $token = get_cookie('token');
    $this->main_model->checkToken($token);

    $planId = $this->input->get('plan');
    $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$planId, null);
    if(isset($plan->error)){
      redirect('Library', 'refresh');
      die();
    }
    if(!isset($plan->planExercices))
    {
      if(count($plan->planExercices) == 0)
      {
        redirect('Plan/addPlanComplete?plan='.$planId);
        die();
      }
    }
    $this->data['plan'] = $plan;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'แบบทดสอบก่อนเรียน';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_preTest';
    $this->data["page_active"] = "myEveryDay";
    $this->load->view('layouts/main', $this->data);
  }

  public function Question()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }

    $api_url = $this->config->item('api_url');
    $planId = $this->input->get('plan');
    $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$planId, null);
 
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    
    $current_test = $this->membertest_model->find_by_member_plan_test($myProfile->id, $planId);
    if(!isset($current_test)){
      redirect("PreTest/Index/?plan=".$planId, 'refresh');
      die();
    }else{
      $current_test = $this->main_model->curlRequest("GET", $this->config->item('api_url').'member-plan-tests/'.$current_test->id, null, $token);
    }

    if($current_test->complete){
      redirect("PreTest/Result/".$planId."/".$current_test->id, 'refresh');
      die();
    }
    
    $strDate = new DateTime($current_test->createdtime); //current date/time
    $strDate->add(new DateInterval("PT7H"));
    $strDate = $strDate->format('Y/m/d H:i:s');

    $this->data['answer'] = $current_test->answers->answer;
    $this->data['test_id'] = $current_test->id;
    $this->data['start_time'] = $strDate;
    $this->data['api_url'] = $api_url;
    $this->data['plan'] = $plan;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'คำถาม Pre - Test';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_preTest_question';
    $this->data["page_active"] = "myEveryDay";
    $this->load->view('layouts/main', $this->data);
  }
  
  public function Result($planId, $test_id)
  {
      $token = get_cookie('token');
      $this->main_model->checkToken($token);
      // $planId = $this->input->post('plan_id');
      // if(!isset($planId)){
      //   redirect('MyEveryDay','refresh');
      // }
      $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$planId, null);
      $total_score = count($plan->planExercices);

      $current_test = $this->main_model->curlRequest("GET", $this->config->item('api_url').'member-plan-tests/'.$test_id, null, $token);
      $score = $current_test->score;
      $duration = $current_test->answers->use_time;
      $view_name = 'v_preTest_result2';
      $scoreRate = $score/$total_score * 100;
      if($scoreRate >= $plan->lowRate){
        $view_name = 'v_preTest_result';
      }

      $pass_plan = $planId;
      $fail_plan = $plan->lowPlanRecommendId;
      if($scoreRate >= $plan->heightRate){
        $pass_plan = $plan->heightPlanRecommendId;
        $fail_plan = $planId;
      }
      
      $fail_plan_obj = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$fail_plan, null);

      $lang = get_cookie('lang');
      if($lang == null){ $lang = "en"; }
      // $this->data['pass_plan'] = $planId;
      $this->data['image_title'] = $fail_plan_obj->planLibrary->title;
      $this->data['pass_plan'] = $pass_plan;
      $this->data['fail_plan'] = $fail_plan;
      $this->data['total_score'] = $total_score;
      $this->data['score'] = $score;
      $this->data['duration'] = $duration;
      $this->data["language"] = $lang;
      $this->data['meta_title'] = 'ผลการทดสอบ';
      $this->data['meta_description'] = '';
      $this->data['view_name'] = $view_name;
      $this->data["page_active"] = "myEveryDay";
      //$this->data['view_name'] = 'v_preTest_result2';
      $this->load->view('layouts/main', $this->data);
  }
  
  public function Result2()
  {
      $lang = get_cookie('lang');
      if($lang == null){ $lang = "en"; }
      $this->data["language"] = $lang;
      $this->data['meta_title'] = '';
      $this->data['meta_description'] = '';
      $this->data['view_name'] = 'v_preTest_result2';
      $this->load->view('layouts/main', $this->data);
  }
  public function AddPlan()
  {
    $planId = $this->input->post('plan');
    $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$planId, null);
    
    $token = get_cookie('token');
    $this->main_model->checkToken($token);

    $profile = $this->main_model->curlRequest("GET",$this->config->item('api_url').'members/me', null, $token);

    $plan_libraries = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-libraries/'.$plan->plan_key, null, $token);
    $member_plan_data = [];
    $member_plan_data['memberId'] = intval($profile->id);
    $member_plan_data['librariesProgress'] = $plan_libraries->library_data;
    $member_plan_data['planId'] = intval($plan_libraries->planId);

    // save member plan
    $response = $this->main_model->curlRequest("POST", $this->config->item('api_url').'front-end/add-plan/', $member_plan_data, $token);
    if(isset($response->status) && $response->status === false)
    {
      $this->session->set_flashdata('error', 'นักเรียนได้เพิ่มแผนเรียนนี้ไว้แล้ว โปรดเลือกแผนการเรียนใหม่');
      $this->_print_json(json_encode(['status' => '404']));
    }
    else
    {
      $this->_print_json(json_encode(['status' => '200']));
    }
  }

  public function UpdateAnswer()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $test_id = $this->input->post('test_id');
    $answer_list = $this->input->post('answerList');

    $api_url = $this->config->item('api_url');
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $current_test = $this->main_model->curlRequest("GET", $this->config->item('api_url').'member-plan-tests/'.$test_id, null, $token);

    if($current_test->memberId == $myProfile->id && $current_test->complete == 0){
      $request = array(
        'answers' => array(
          'answer' => ($answer_list)
        )
      );
      $current_test = $this->main_model->curlRequest("PATCH", $this->config->item('api_url').'member-plan-tests/'.$test_id, $request, $token);
    }

  }

  
  public function FinishTest()
  {
    
    $planId = $this->input->post('plan_id');
    $token = get_cookie('token');
    $duration = $this->input->post('duration');
    $answer = $this->input->post('answer');
    $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$planId, null);
    $answerList = json_decode($answer);
    $new_answer = [];
    foreach ($answerList as $answer) {
      $new_answer[$answer->question_id] = $answer;
    }
    
    $score = 0;
    foreach ($plan->planExercices as $question) {
      if(isset($new_answer[$question->id])){
        if($new_answer[$question->id]->question_choice == $question->answer){
          $score += 1;
        }
      }
    }

    $test_id = $this->input->post('test_id');
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $current_test = $this->main_model->curlRequest("GET", $this->config->item('api_url').'member-plan-tests/'.$test_id, null, $token);

    if($current_test->memberId == $myProfile->id){
      $request = array(
        'score' => $score,
        'complete' => true,
        'answers' => array(
          'use_time' => $duration,
          'answer' => $answerList
        )
      );
      $current_test = $this->main_model->curlRequest("PATCH", $this->config->item('api_url').'member-plan-tests/'.$test_id, $request, $token);
    }
  }

  
  public function createTest()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $plan_id = $this->input->post('plan_id');
    $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$plan_id, null);
    if(isset($plan->error)){
      show_error('No plan', 400);
      die(); 
    }

    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    
    $current_test = $this->membertest_model->find_by_member_plan_test($myProfile->id, $plan_id);
    if(!isset($current_test)){
      $request = array(
        'score' => 0,
        'complete' => false,
        'answers' => array('answer' => array()),
        'memberId' => $myProfile->id,
        'planId' => (int)$plan_id,
        'createdtime' => gmdate("Y-m-d H:i:s.").'000Z'
      );
      $current_test = $this->main_model->curlRequest("POST", $this->config->item('api_url').'member-plan-tests', $request, $token);
    }
  }
}
