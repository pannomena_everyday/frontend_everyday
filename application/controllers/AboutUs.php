<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AboutUs extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(['main_model']);
  }
  
  public function index()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'เกี่ยวกับเรา - Monkey Everyday เรียนออนไลน์ ติวเข้ม เตรียมสอบ ฟรี';
    $this->data['meta_description'] = 'เน้นการเรียนที่เห็นผล เพื่อสร้างคนให้เรียนเป็น';
    $this->data['view_name'] = 'v_aboutUs';
    $this->load->view('layouts/main', $this->data);
  }
}
