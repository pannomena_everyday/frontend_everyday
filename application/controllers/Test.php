<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(array('sheetexercise_model', 'librarysheet_model', 'main_model', 'membertest_model'));
  }
  
  public function index($sublevel_sheet_id, $level_id, $plan_id)
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $api_url = $this->config->item('api_url');

    $tests = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/sublevel-tests/'.$sublevel_sheet_id, null);
    if(isset($tests->error)){
      redirect('Library', 'refresh');
      die();
    }
    $this->data['tests'] = $tests;
    $this->data['sublevel_sheet_id'] = $sublevel_sheet_id;
    $this->data['level_id'] = $level_id;
    $this->data['plan_id'] = $plan_id;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'คำอธิบายแบบทดสอบ';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_test';
    $this->data["page_active"] = "myEveryDay";
    $this->load->view('layouts/main', $this->data);
  }
  
  public function Question($sublevel_sheet_id, $level_id, $plan_id)
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);

    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }

    $api_url = $this->config->item('api_url');
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    
    $current_test = $this->membertest_model->find_by_member_sublevel_test($myProfile->id, $sublevel_sheet_id);
    if(!isset($current_test)){
      $latest_complete = $this->membertest_model->find_latest_complete_member_sublevel_test($myProfile->id, $sublevel_sheet_id);
      if(isset($latest_complete)){
        redirect("Test/Result/".$sublevel_sheet_id."/".$level_id."/".$plan_id."/".$latest_complete->id, 'refresh');
      }else{
        redirect("Test/Index/".$sublevel_sheet_id."/".$level_id."/".$plan_id, 'refresh');
      }
      die();
    }else{
      $current_test = $this->main_model->curlRequest("GET", $this->config->item('api_url').'member-sub-level-tests/'.$current_test->id, null, $token);
    }


    $tests = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/sublevel-tests/'.$sublevel_sheet_id, null);
    $strDate = new DateTime($current_test->createdtime); //current date/time
    $strDate->add(new DateInterval("PT7H"));
    $strDate = $strDate->format('Y/m/d H:i:s');

    $this->data['answer'] = $current_test->asnswers->answer;
    $this->data['test_id'] = $current_test->id;
    $this->data['start_time'] = $strDate;
    $this->data['level_id'] = $level_id;
    $this->data['plan_id'] = $plan_id;
    $this->data['api_url'] = $api_url;
    $this->data['tests'] = $tests;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'แบบทดสอบ';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_test_question';
    $this->data["page_active"] = "myEveryDay";
    $this->load->view('layouts/main', $this->data);
  }
  
  public function Result($sublevel_sheet_id, $level_id, $plan_id, $test_id)
  {
      $token = get_cookie('token');
      $this->main_model->checkToken($token);
      // $plan_id = $this->input->post('plan_id');
      // if(!isset($plan_id)){
      //   redirect('MyEveryDay','refresh');
      // }

      // $level_id = $this->input->post('level_id');
      // $sublevel_sheet_id = $this->input->post('sublevel_sheet_id');
      $tests = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/sublevel-tests/'.$sublevel_sheet_id, null); 
      $next_sheet = "";
      $prev_sheet = "";
      // $sublevel_id = $tests->subLevelId;
      $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$plan_id, null); 

      $step_plan = array();
      foreach($plan->planLibrary->library_data->levels as $level){
        foreach ($level->sublevels as $sublevel) {
          foreach ($sublevel->sheets_n_testing as $idx_sheet => $sheet) {
            $step_plan[] = array(
              'level' => $level->id,
              'sub_level' => $sublevel->id,
              'sheet' => $sheet->id
            );
          }
        }
      }

      $next_obj = null;
      $prev_obj = null;

      foreach($step_plan as $idx => $step){
        if($sublevel_sheet_id == $step['sheet'] && $tests->subLevelId == $step['sub_level'] && $level_id == $step['level']){
          try {
            @$next_sheet = $step_plan[$idx + 1]['sheet'];
            @$next_obj = $step_plan[$idx + 1];
          } catch (Exception $e) {
            $next_sheet = "";
            $next_obj = null;
          }
          
          try {
            @$prev_sheet = $step_plan[$idx - 1]['sheet'];
            @$prev_obj = $step_plan[$idx - 1];
          } catch (Exception $e) {
            $prev_sheet = "";
            $prev_obj = null;
          } 
        }
      }

      // $test_id = $this->input->post('test_id');
      // $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
      $current_test = $this->main_model->curlRequest("GET", $this->config->item('api_url').'member-sub-level-tests/'.$test_id, null, $token);

      $lang = get_cookie('lang');
      $view_name = 'v_test_result2';
      $is_pass = 0;
      $score = $current_test->socre;
      if(@count($current_test->asnswers->answer) != 0 ){
        $is_pass = $score / count($tests->subLevelTestQuestions);
      }
      if($is_pass >= 0.6){ $view_name = 'v_test_result';}
      if($lang == null){ $lang = "en"; }
      
      $this->data['subject'] = @$plan->planLibrary->library_data->subject;
      $this->data['plan_id'] = $plan_id;
      $this->data['level_id'] = $level_id;
      $this->data['sheet_id'] = $sublevel_sheet_id;
      $this->data['test_id'] = $test_id;
      $this->data['next_sheet'] = $next_sheet;
      $this->data['prev_sheet'] = $prev_sheet;
      $this->data['next_obj'] = $next_obj;
      $this->data['prev_obj'] = $prev_obj;
      $this->data['score'] = $score;
      $this->data['total_score'] = count($tests->subLevelTestQuestions);
      $this->data["language"] = $lang;
      $this->data['meta_title'] = 'ผลการทดสอบ';
      $this->data['meta_description'] = '';
      $this->data['view_name'] = $view_name;
      $this->data["page_active"] = "myEveryDay";
      $this->data['use_time'] = $current_test->asnswers->use_time > $tests->test_time ? $tests->test_time : $current_test->asnswers->use_time;
      $this->load->view('layouts/main', $this->data);
  }
  
  public function Result2()
  {
      $lang = get_cookie('lang');
      if($lang == null){ $lang = "en"; }
      $this->data["language"] = $lang;
      $this->data['meta_title'] = '';
      $this->data['meta_description'] = '';
      $this->data['view_name'] = 'v_test_result2';
      $this->load->view('layouts/main', $this->data);
  }
  
  public function Score($sheet_id, $level_id, $plan_id, $test_id)
  {
      $token = get_cookie('token');
      $this->main_model->checkToken($token);
      $lang = get_cookie('lang');
      if($lang == null){ $lang = "en"; }
      
      $current_test = $this->main_model->curlRequest("GET", $this->config->item('api_url').'member-sub-level-tests/'.$test_id, null, $token);
      $tests = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/sublevel-tests/'.$sheet_id, null); 
      $is_pass = false;
      $score = $current_test->socre;
      if(@count($current_test->asnswers->answer) != 0 ){
        $is_pass = ($score / count($tests->subLevelTestQuestions)) >= 0.6;
      }
      $test_answer = [];
      foreach ($current_test->asnswers->answer as $answer) {
        $test_answer[$answer->question_id] = $answer;
      }
  
      $tests = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/sublevel-tests/'.$sheet_id, null); 
      $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$plan_id, null); 

      $step_plan = array();
      foreach($plan->planLibrary->library_data->levels as $level){
        foreach ($level->sublevels as $sublevel) {
          foreach ($sublevel->sheets_n_testing as $idx_sheet => $sheet) {
            $step_plan[] = array(
              'level' => $level->id,
              'sub_level' => $sublevel->id,
              'sheet' => $sheet->id
            );
          }
        }
      }
      $next_sheet = "";
      $prev_sheet = "";
      $next_obj = null;
      $prev_obj = null;

      foreach($step_plan as $idx => $step){
        if($sheet_id == $step['sheet'] && $tests->subLevelId == $step['sub_level'] && $level_id == $step['level']){
          try {
            @$next_sheet = $step_plan[$idx + 1]['sheet'];
            @$next_obj = $step_plan[$idx + 1];
          } catch (Exception $e) {
            $next_sheet = "";
            $next_obj = null;
          }
          
          try {
            @$prev_sheet = $step_plan[$idx - 1]['sheet'];
            @$prev_obj = $step_plan[$idx - 1];
          } catch (Exception $e) {
            $prev_sheet = "";
            $prev_obj = null;
          } 
        }
      }

      $this->data['subject'] = @$plan->planLibrary->library_data->subject;
      $this->data['is_pass'] = $is_pass;
      $this->data['test_answer'] = $test_answer;
      $this->data['test_id'] = $test_id;
      $this->data['plan_id'] = $plan_id;
      $this->data['level_id'] = $level_id;
      $this->data['sheet_id'] = $sheet_id;
      $this->data['next_sheet'] = $next_sheet;
      $this->data['prev_sheet'] = $prev_sheet;
      $this->data['next_obj'] = $next_obj;
      $this->data['prev_obj'] = $prev_obj;
      $this->data['tests'] = $tests;
      $this->data["language"] = $lang;
      $this->data['meta_title'] = 'เฉลยผลการทดสอบ';
      $this->data['meta_description'] = '';
      $this->data['view_name'] = 'v_test_score';
      $this->data["page_active"] = "myEveryDay";
      $this->load->view('layouts/main', $this->data);
  }
  
  public function Exercise($page = 'Library')
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $api_url = $this->config->item('api_url');
    $librarysheet_id = $this->input->get('librarysheet');
    $librarysheet = $this->librarysheet_model->find_self($librarysheet_id); // หา librarysheet
    $sheet_exercises = $this->sheetexercise_model->find_by_librarySheetId($librarysheet_id); //หา exercise ที่มี librarysheet_id เดียวกัน
    // var_dump($sheet_exercises);
    if($page == 'MyEveryDay'){
      $this->data["page_active"] = "myEveryDay";
    }else{
      $this->data["page_active"] = "library";
    }
    $this->data['page'] = $page;
    $this->data['api_url'] = $api_url;
    $this->data['librarysheet'] = $librarysheet;
    $this->data['sheet_exercises'] = $sheet_exercises;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'ตรวจคำตอบ';
    $this->data['meta_description'] = 'ตรวจคำตอบพร้อมวิเคราะห์คะแนน';
    $this->data['view_name'] = 'v_test_exercise';
    $this->load->view('layouts/main', $this->data);
  }

  public function UpdateAnswer()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $test_id = $this->input->post('test_id');
    $answer_list = $this->input->post('answerList');

    $api_url = $this->config->item('api_url');
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $current_test = $this->main_model->curlRequest("GET", $this->config->item('api_url').'member-sub-level-tests/'.$test_id, null, $token);

    if($current_test->memberId == $myProfile->id && $current_test->complete == 0){
      $objAnswer = $current_test->asnswers;
      $objAnswer->answer = $answer_list;
      $request = array(
        'asnswers' => $objAnswer
      );
      $current_test = $this->main_model->curlRequest("PATCH", $this->config->item('api_url').'member-sub-level-tests/'.$test_id, $request, $token);
    }

  }

  public function FinishTest()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $plan_id = $this->input->post('plan_id');
    $level_id = $this->input->post('level_id');
    $sublevel_sheet_id = $this->input->post('sublevel_sheet_id');
    $tests = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/sublevel-tests/'.$sublevel_sheet_id, null);
    
    $answerList = json_decode($this->input->post('answer'));
    $new_answer = [];
    foreach ($answerList as $answer) {
      $new_answer[$answer->question_id] = $answer;
    }
    $score = 0;
    foreach ($tests->subLevelTestQuestions as $question) {
      if(isset($new_answer[$question->id])){
        if($new_answer[$question->id]->question_choice == $question->answer){
          $score += 1;
        }
      }
    }
    
  $test_id = $this->input->post('test_id');
  $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
  $current_test = $this->main_model->curlRequest("GET", $this->config->item('api_url').'member-sub-level-tests/'.$test_id, null, $token);

    if($current_test->memberId == $myProfile->id){
      $use_time = $this->input->post('time') > $tests->test_time ? $tests->test_time : $this->input->post('time');
      $request = array(
        'socre' => $score,
        'complete' => true,
        'asnswers' => array(
          'use_time' => $use_time,
          'answer' => $answerList,
          'plan_id' => $plan_id,
          'level_id' => $level_id,
          'sublevel_test_id' => $sublevel_sheet_id
        )
      );
      $current_test = $this->main_model->curlRequest("PATCH", $this->config->item('api_url').'member-sub-level-tests/'.$test_id, $request, $token);
    }
  }

  public function createTest()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $plan_id = $this->input->post('plan_id');
    $level_id = $this->input->post('level_id');
    $sublevel_sheet_id = $this->input->post('sublevel_sheet_id');

    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $tests = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/sublevel-tests/'.$sublevel_sheet_id, null);
    if(isset($tests->error)){
      show_error('No plan', 400);
      die(); 
    }

    $current_test = $this->membertest_model->find_by_member_sublevel_test($myProfile->id, $sublevel_sheet_id);
    if(!isset($current_test)){
      $request = array(
        'socre' => 0,
        'complete' => false,
        'planId' => (int)$plan_id,
        'levelId' => (int)$level_id,
        'asnswers' => array(
          'answer' => array(),
          'plan_id' => $plan_id,
          'level_id' => $level_id,
          'sublevel_test_id' => $sublevel_sheet_id
        ),
        'memberId' => $myProfile->id,
        'subLevelTestId' => (int)$sublevel_sheet_id,
        'createdtime' => gmdate("Y-m-d H:i:s.").'000Z'
      );
      $current_test = $this->main_model->curlRequest("POST", $this->config->item('api_url').'member-sub-level-tests', $request, $token);
    }
  }
}
