<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define("API_URL_ENPOINT", "https://api-dot-monkey-everyday-test.df.r.appspot.com/library-levels");
// define("API_MEMBER_LOGIN","/members/login");
// define("API_MEMBER_ME","/members/me");

class Library extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->data['active_menu'] = 'Library';
    $this->data['not_clear_filter'] = true;
    $this->load->model(array('librarylevel_model', 'librarysublevel_model', 'librarysheet_model', 'sheetvideo_model', 'subject_model', 'main_model', 'sheetexercise_model', 'sheetfile_model', 'sheetandtest_model', 'membertest_model'));
  }
  
  public function index()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $subjects = $this->subject_model->all();
    $this->membertest_model->check_billing_exists($myProfile);
    if($myProfile->subscriptionTypeId == 1)
    {
      $sublevels = $this->librarysublevel_model->all_for_guest();
      $list_key = array();
      foreach ($sublevels as $key => $value) {
        $list_key[] = $key;
      }

      $this->data['list_key'] = $list_key;
      $this->data['subjects'] = $subjects;
      $this->data['sublevels'] = $sublevels;
      $this->data["language"] = $lang;
      $this->data["page_active"] = "library";
      $this->data['meta_description'] = 'เนื้อหาครอบคลุมตั้งแต่ประถมปลาย จนถึงมหาวิทยาลัย';
      $this->data['view_name'] = 'v_libraryGuest';
    }
    else
    {
      $this->membertest_model->check_on_going_test($myProfile->id, $token);
      $libraries = $this->librarylevel_model->all_group_subject();
      $this->data['subjects'] = $subjects;
      $this->data['libraries'] = $libraries;
      $this->data["language"] = $lang;
      $this->data["page_active"] = "library";
      $this->data['view_name'] = 'v_library';
      $this->data['meta_description'] = 'เนื้อหาครอบคลุมตั้งแต่ประถมปลาย - มหาวิทยาลัย พร้อมเรียบเรียงสิ่งที่ควรเรียนก่อน - หลังให้';
    }

    $this->data['meta_title'] = 'คลังความรู้ - Monkey Everyday';
    $this->load->view('layouts/main', $this->data);
  }
  
  public function All()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $subjects = $this->subject_model->all(); // หาวิชาทั้งหมด
    $subject_id = $this->input->get('subject');
    $libraries = $this->librarylevel_model->all_by_subject($subject_id); // หา level ทั้งหมดที่มี libraryGroup เดียวกัน

    $this->data['subjects'] = $subjects;
    $this->data['libraries'] = $libraries;
    $this->data['meta_title'] = 'คลังความรู้ - Monkey Everyday';
    $this->data['meta_description'] = 'เนื้อหาครอบคลุมตั้งแต่ประถมปลาย - มหาวิทยาลัย พร้อมเรียบเรียงสิ่งที่ควรเรียนก่อน - หลังให้';
    $this->data["page_active"] = "library";
    $this->data['view_name'] = 'v_library_all';
    $this->load->view('layouts/main', $this->data);
  }
  
  public function SubList($id = null)
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }

    $token = get_cookie('token');
    $this->main_model->checkToken($token);

    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    if($myProfile->subscriptionTypeId == 1)
    {
      $sublevel = $this->librarysublevel_model->find_by_id($id);
      $videos = $this->sheetvideo_model->find_by_sublevel_id($id);
      $subjects = $this->subject_model->all();
      $sheets = $this->librarysheet_model->find_by_sublevel_id($id);

      $this->data['subjects'] = $subjects;
      $this->data["title"] = $sublevel->title;
      $this->data['prefix'] = $sublevel->prefix;
      $this->data["videos"] = $videos;
      $this->data['sheets'] = $sheets;
      $this->data["language"] = $lang;
      $this->data["page_active"] = "library";
      $this->data['meta_title'] = 'รายชื่อ Video';
      $this->data['meta_description'] = '';
      $this->data['view_name'] = 'v_libraryGuestList';
    }
    else
    {
      // id ของ libraryLevelId = id
      $level = $this->librarylevel_model->find_by_id($id);
      $sublevels = $this->librarysublevel_model->find_by_level_id($id);
      $videos = $this->sheetvideo_model->find_by_level_id($id);
      $sheet_files = $this->sheetfile_model->find_by_level_id($id);
      $subjects = $this->subject_model->all();

      $this->data['subjects'] = $subjects;
      $this->data['level'] = $level;
      $this->data['sublevels'] = $sublevels;
      $this->data['videos'] = $videos;
      $this->data['sheet_files'] = $sheet_files;
      $this->data["language"] = $lang;
      $this->data["page_active"] = "library";
      $this->data['meta_title'] = 'เรียนออนไลน์ฟรี ทุกระดับชั้น - Monkey Everyday ';
      $this->data['meta_description'] = 'รวบรวมครบทั้ง VDO, ชีทเรียน และแบบฝึกหัด สำหรับทุกระดับชั้น';
      $this->data['view_name'] = 'v_library_list';
    }

    $this->load->view('layouts/main', $this->data);
  }

  public function Video($id = null)
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);

    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }

    $api_url = $this->config->item('api_url');
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    if(!isset($myProfile->firstname)){
      delete_cookie('token');
      header("Location: ".base_url()."Index/SignIn");
    }

    $sheet = $this->librarysheet_model->find_by_sheet_id($id); //หาข้อมูล view ของตัวเอง
    $sheet_file = $this->sheetfile_model->find_by_libsheet_id($id);
    $sublevel_id = $sheet->librarySubLevelId;
    $sheets = $this->librarysheet_model->all_sheet_by_sublevel_id($sublevel_id); //หา sheet ที่มี $sublevel_id เดียวกัน
    // $vdos = $this->sheetvideo_model->find_vdo_by_sheet_id($id); //หา vdo ที่มี sheetId เดียวกัน
    $vdos = $this->sheetvideo_model->find_vdo_by_sheet_id_with_progress($id, $myProfile->id); //หา vdo ที่มี sheetId เดียวกัน

    $ar = array();
    $maxNo = $sheets->num_rows();
    $n = 1;
    $prev = "";
    $next = "";
    foreach ($sheets->result() as $row) {
      $ar[$n] = $row->id;
      if($row->id == $id)
        {$currentNo = $n;}
      $n++;
    }
    if($currentNo !=1)
      $prev = "<a href='".base_url()."Library/Video/".$ar[$currentNo-1]."'><img src='".base_url()."resources/img/backArrow.png' class='img-arrow'><p class='m-400-hide' style='color: #fff;'>ชีทก่อนหน้า</p></a>";
    if($currentNo < $maxNo)
      $next = "<a href='".base_url()."Library/Video/".$ar[$currentNo+1]."'><img src='".base_url()."resources/img/nextArrow.png' class='img-arrow'><p class='m-400-hide' style='color: #fff;'>ชีทถัดไป</p></a>";

    if($myProfile->subscriptionTypeId == 1)
    {
      $this->data['api_url'] = $api_url;
      $this->data['myProfile'] = $myProfile;
      $this->data['sheet'] = $sheet;
      $this->data['sheet_file'] = $sheet_file;
      $this->data['vdos'] = $vdos;
      $this->data["language"] = $lang;
      $this->data['prev'] = $prev;
      $this->data['next'] = $next;
      $this->data['meta_title'] = 'Video';
      $this->data['meta_description'] = '';
      $this->data['view_name'] = 'v_video_guest';
    }
    else
    {
      $this->data['id'] = $id;
      $this->data['api_url'] = $api_url;
      $this->data['myProfile'] = $myProfile;
      $this->data['sheet'] = $sheet;
      $this->data['sheet_file'] = $sheet_file;
      $this->data['vdos'] = $vdos;
      $this->data["language"] = $lang;
      $this->data['prev'] = $prev;
      $this->data['next'] = $next;
      $this->data['meta_title'] = 'Video บทเรียน';
      $this->data['meta_description'] = '';
      $this->data['view_name'] = 'v_video';
    }
    $this->data["page"] = "Library";
    $this->data["page_active"] = "library";
    $this->data['token'] = $token;
    $this->load->view('layouts/main', $this->data);
  }

}
