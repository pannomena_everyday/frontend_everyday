<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'third_party/omise-php/lib/Omise.php';
define('OMISE_API_VERSION', '2020-08-20'); // เข้าระบบ omise และคลิกที่อีเมล คลิก api version เลือกระหว่าง ทดสอบ กับ ใช้งานจริง
define('OMISE_PUBLIC_KEY', 'pkey_5ngeuhhp2qod19n4ijo');
// define('OMISE_SECRET_KEY', 'skey_test_5kjnqw7tl6ido28mjny');

class Payment extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(array('memberplan_model', 'main_model', 'myprofile_model'));
  }
  
  public function index()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);

    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $data["language"] = $lang;

    $subscriber_id = $this->input->get('subscriber_id');
    $promotion_code = $this->input->get('promotion');
    $subscription_type = $this->main_model->curlRequest("GET", $this->config->item('api_url').'subscription-types/'.$subscriber_id, null, null);
    
    $subscribe_obj = array(
      'subscriptionTypeId' => (int)$subscriber_id,
      'promotionCode' => $promotion_code,
      'isValidate' => true
    );
    $validate = $this->main_model->curlRequest("POST", $this->config->item('api_url').'members/subscribe-package', $subscribe_obj, $token);

    //check subscription is exist
    if(isset($validate->error)){
      redirect('Membertype','refresh');
    }
    // check subscription is active
    if(!isset($validate->packaage)){
      redirect('Membertype','refresh'); 
    }
    
    // $this->data['total_discount'] = $total_discount;
    // $this->data['price'] = $subscription_type->price *  $subscription_type->month_period;
    // $this->data['omise_price'] = (($subscription_type->price *  $subscription_type->month_period) - $total_discount) * 100;
    // $this->data['price'] = $subscription_type->price;
    // $this->data['omise_price'] = (($subscription_type->price) - $total_discount) * 100;
    $this->data['payment'] = $validate;
    $this->data['subscription_type'] = $subscription_type;
    $this->data['meta_title'] = '';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_payment';
    $this->load->view('layouts/main', $this->data);
  }

  public function add_card()
  { 
    $this->data['promotion_code'] = $this->input->post('promotion_code');
    $this->data['subscription_id'] = $this->input->post('subscription_id');
    $this->data['amount'] = $this->input->post('amount');
    $this->data['meta_title'] = '';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_payment_add_card';
    $this->load->view('layouts/main', $this->data);
  }

  public function create()
  {
    // echo json_encode($this->input->post());
    // die();
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    
    $subscription_id = $this->input->post('subscription_id');
    $promotion_code = $this->input->post('promotion_code');
    $subscription_type = $this->main_model->curlRequest("GET", $this->config->item('api_url').'subscription-types/'.$subscription_id, null, null);

    $subscribe_obj = array(
      'subscriptionTypeId' => (int)$subscription_id,
      'promotionCode' => $promotion_code,
      'isValidate' => true
    );
    $validate = $this->main_model->curlRequest("POST", $this->config->item('api_url').'members/subscribe-package', $subscribe_obj, $token);

    //check subscription is exist
    if(isset($validate->error)){
      redirect('Membertype','refresh');
    }
    // check subscription is active
    if(!isset($validate->packaage)){
      redirect('Membertype','refresh'); 
    }
    // echo $omise_amount;
    $omiseToken = $this->input->post('omiseToken');
    try {
      $patchCard = array(
        'card_token' => $omiseToken
      );
      $card = $this->main_model->curlRequest("PATCH", $this->config->item('api_url').'members/omise-update-card', $patchCard, $token);
      if(isset($card->error))
      {
        redirect("Payment/payment_error?promotion_code=$promotion_code&subscription_id=$subscription_id?msg=Card Error");
      }
      
      $subscribe_obj = array(
        'subscriptionTypeId' => (int)$subscription_id,
        'promotionCode' => $promotion_code,
      );
      $payment = $this->main_model->curlRequest("POST", $this->config->item('api_url').'members/subscribe-package', $subscribe_obj, $token);
      if(isset($payment->error))
      {
        redirect("Payment/payment_error?promotion_code=$promotion_code&subscription_id=$subscription_id&msg=Payment Server Error");
        // die();
      }
      if(isset($payment->status))
      {
        if(!$payment->status)
        {
          redirect("Payment/payment_error?promotion_code=$promotion_code&subscription_id=$subscription_id&msg=$payment->message");
          // die();
        }
      }

      
      $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);  

      $billAddr = $this->myprofile_model->getBillingAddress($myProfile->id);
      $this->data['payment'] = $payment;
      $this->data['validate'] = $validate;
      $this->data['url_next'] = isset($billAddr->result()[0]) ? 'payment/SendMailFromBillingInfomation' : 'payment/billing';
      $this->data['subscription_type'] = $subscription_type;
      $this->data['email'] = $myProfile->email;
      $this->data['meta_title'] = '';
      $this->data['meta_description'] = '';
      $this->data['view_name'] = 'v_payment_success';
      $this->load->view('layouts/main', $this->data);
          
    } catch (Exception $ex) {
      redirect('Membertype','refresh'); 
    }
  }

  public function payment_error()
  {
    $this->data['promotion_code'] = $this->input->get('promotion_code');
    $this->data['subscription_id'] = $this->input->get('subscription_id');
    $this->data['msg'] = $this->input->get('msg');
    $this->data['meta_title'] = '';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_payment_error';
    $this->load->view('layouts/main', $this->data);
  }

  
  public function billing()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token); 
    $this->data['dataProvince'] = $this->myprofile_model->getProvinces();
    $this->data['dataAmphure'] = $this->myprofile_model->getAmphures('');
    $this->data['dataDistrict'] = $this->myprofile_model->getDistricts('');
    $this->data['dataZipCode'] = $this->myprofile_model->getZipCode('');
    
    $this->data['myProfile'] = $myProfile;
    $this->data['amount'] = $this->input->post('amount');
    $this->data['invoice_no'] = $this->input->post('invoice_no');
    $this->data['subscription_id'] = $this->input->post('subscription_id');
    $this->data['payment_date'] = $this->input->post('payment_date');
    $this->data['meta_title'] = '';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_payment_billing';
    $this->load->view('layouts/main', $this->data);
  }
  
  public function addBillingAndSendMail()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    
    // $amount = $this->input->post('amount');
    // $invoice_no = $this->input->post('invoice_no');
    // $payment_date = $this->input->post('payment_date');
    $memberId = $this->input->post('memberId');
    $fullname = $this->input->post('fullname');
    $mobile = $this->input->post('mobile');
    $address = $this->input->post('addr');
    $provinceId = $this->input->post('provinceId');
    $amphureId = $this->input->post('amphureId');
    $districtId = $this->input->post('districtId');
    $zipCode = $this->input->post('zipCode');
    $this->myprofile_model->insertBillingAddress($memberId,$fullname,$mobile,$address,$provinceId,$amphureId,$districtId,$zipCode);
    return true;
  }

  public function SendMailFromBillingInfomation()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    
    if(isset($myProfile->memberPackages->currentSubscriptionTypeId)){
      if($myProfile->memberPackages->currentSubscriptionTypeId != 0){
        redirect('MyProfile/MyMembership', 'refresh');
      }
    }
    redirect('Library', 'refresh');
  }

  public function change_card()
  { 
    $this->data['meta_title'] = '';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_payment_change_card';
    $this->load->view('layouts/main', $this->data);
  }

  
  public function updateCard()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    
    $id = $this->input->post('id');
    $patchCard = array(
      'card_token' => $id
    );
    $card = $this->main_model->curlRequest("PATCH", $this->config->item('api_url').'members/omise-update-card', $patchCard, $token);
    if(isset($card->error))
    {
      echo json_encode(array(
        'status' => false
      ));
      die();
    }
    $this->main_model->curlRequest("POST", $this->config->item('api_url').'members/re-subscribe-package', array('init'=> ''), $token);
    echo json_encode(array(
      'status' => true
    ));
  }

}
