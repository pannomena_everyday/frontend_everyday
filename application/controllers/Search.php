<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(array('search_model','main_model'));
  }

  public function index(string $keywords = null)
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $keyword = $this->input->get('keyword', TRUE);
    $this->data['searchKeyword'] = $keyword;
    $this->data['myProfile'] = $myProfile;
    $this->data['searchResult'] = $this->getSearchResultHtml($keyword,'','');
    $this->data['dataEducation'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'education-levels',null);
    $this->data['dataMajors'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'education-majors',null);
    $this->data['api_url'] = $this->config->item('api_url');
    $this->data['meta_title'] = 'เรียนออนไลน์ฟรี ค้นหาบทเรียนได้ที่นี่';
    $this->data['meta_description'] = "กดตรงช่อง 'ค้นหาบทเรียน' แล้วพิมพ์หัวข้อที่ต้องการเรียนได้เลย";
    $this->data['view_name'] = 'popup_search';
    $this->load->view('layouts/main', $this->data);
  }

  public function GetSearchData(){
    $keyword = $this->input->post('keyword');
    $data = $this->search_model->GetSearchData($keyword);
    echo json_encode($data);
  }

  public function SearchEncode(){
    $keyword = $this->input->post('keyword');
    echo urlencode(strtr(base64_encode($keyword), '+/', '-_'));
  }

  public function playVideo($vid = null)
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $data = $this->search_model->GetVideoById($vid);
    $key = base64_decode(strtr(date("now"), '-_', '+/'));
    if($data->num_rows() > 0){
      // echo $data->result()[0]->gcsName;
      echo '<script type="module" src="'.base_url().'resources/js/everyday-player.bundled.js"></script>';
      echo '<body style="margin: 1px !important;">
        <everyday-player
        nid="'.$myProfile->id_card.'"
        stdName="'.$myProfile->firstname.' '.$myProfile->lastname.'"
        src="'.$this->getVideo($data->result()[0]->gcsName)[0].'"
        name="'.$data->result()[0]->title.'"
        imageUrl="'.$data->result()[0]->imgUrl.'"
        code="'.$data->result()[0]->sheetCode.'"
        tag="test-everyday"
        contentId="'.$key.'">
        </everyday-player>
      </body>';
    }else{
      echo "No video file.";
    }
  }

  public function getVideo($videoName){
    $headers = array("accept: application/json",'Content-Type: application/json');
    $method = 'GET';
    $api_url = $this->config->item('api_url');
    $url = $api_url.'gen-signed-url/'.$videoName;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
    $response = curl_exec($curl);
    curl_close($curl);
    return json_decode($response);
  }

  public function getSearchResultHtml($keyword,$subjectId,$libraryId){
    if($keyword != null){
      $token = get_cookie('token');
      $this->main_model->checkToken($token);
      $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
      if($myProfile->subscriptionTypeId == 'disable'){
        $keyDecode = urldecode(base64_decode(strtr($keyword, '-_', '+/')));
        $libResult = $this->search_model->GetSearchResult_Library($keyDecode,$subjectId,$libraryId);
        $ret = array();
        $ret['keyword'] = $keyDecode;
        $ret['libResult'] = $libResult->num_rows();
        $ret['videoResult'] = 0;
        $ret['dataSubject'] = '<option value="">เลือก วิชา</option>';
        $subject = $this->search_model->GetSubjectByKeyword_Guest($keyDecode);
        foreach($subject->result() as $row){
          if($subjectId == $row->id){
            $ret['dataSubject'] .= "<option value='".$row->id."' selected=selected>".$row->title."</option>";
          }else{
            $ret['dataSubject'] .= "<option value='".$row->id."'>".$row->title."</option>";
          }
        }
        $ret['dataLibrary'] = '<option value="">เลือก ระดับชั้น / เนื้อหา</option>';
        $library = $this->search_model->GetLibraryByKeywordAndSubjectId_Guest($keyDecode,$subjectId);
        foreach($library->result() as $row){
          if($libraryId == $row->id){
            $ret['dataLibrary'] .= "<option value='".$row->id."' selected=selected>".$row->title."</option>";
          }else{
            $ret['dataLibrary'] .= "<option value='".$row->id."'>".$row->title."</option>";
          }
        }
        $ret['data'] = '<div id="search-sheet" class="search-box containner-padding d-block">
          <div class="row">';
            foreach($libResult->result() as $row){
              if($row->is_comingsoon == 1){
                $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                  <img src="'.$row->imgUrl.'"/>
                  <span class="f-white">'.$row->title.'</span>
                  <div class="status"><div class="comingSoon">COMING SOON</div></div>
                </div>';
              }else{
                if($myProfile->subscriptionTypeId == 1){
                  $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                    <a href="'.base_url().'Library/SubList/'.$row->subLevelId.'?subject='.$row->subjectId.'">
                      <img src="'.$row->imgUrl.'"/>
                      <span class="f-white">'.$row->title.'</span>
                    </a>
                  </div>';
                }else{
                  $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                    <a href="'.base_url().'Library/SubList/'.$row->id.'?subject='.$row->subjectId.'">
                      <img src="'.$row->imgUrl.'"/>
                      <span class="f-white">'.$row->title.'</span>
                    </a>
                  </div>';
                }
              }
            }
            $ret['data'] .= '</div>
        </div>';
      }else{ // check guest
        $keyDecode = urldecode(base64_decode(strtr($keyword, '-_', '+/')));
        $libResult = $this->search_model->GetSearchResult_Library($keyDecode,$subjectId,$libraryId);
        $videoResult = $this->search_model->GetSearchResult_Video($keyDecode,$subjectId,$libraryId);
        $ret = array();
        $ret['keyword'] = $keyDecode;
        $ret['libResult'] = $libResult->num_rows();
        $ret['videoResult'] = $videoResult->num_rows();
        $ret['dataSubject'] = '<option value="">เลือก วิชา</option>';
        $subject = $this->search_model->GetSubjectByKeyword($keyDecode);
        foreach($subject->result() as $row){
          if($subjectId == $row->id){
            $ret['dataSubject'] .= "<option value='".$row->id."' selected=selected>".$row->title."</option>";
          }else{
            $ret['dataSubject'] .= "<option value='".$row->id."'>".$row->title."</option>";
          }
        }
        $ret['dataLibrary'] = '<option value="">เลือก ระดับชั้น / เนื้อหา</option>';
        $library = $this->search_model->GetLibraryByKeywordAndSubjectId($keyDecode,$subjectId);
        foreach($library->result() as $row){
          if($libraryId == $row->id){
            $ret['dataLibrary'] .= "<option value='".$row->id."' selected=selected>".$row->title."</option>";
          }else{
            $ret['dataLibrary'] .= "<option value='".$row->id."'>".$row->title."</option>";
          }
        }

        $ret['data'] = '<div id="search-video" class="search-box containner-padding">
          <div class="row">';
            foreach($videoResult->result() as $row){
              //$link = $this->getVideo($row->gcsName);
              if($row->only_subscriber == 1){
                if($myProfile->subscriptionTypeId == 1){
                  $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                    <img class="searchImg" src="'.$row->imgUrl.'" data-vid="'.$row->id.'"/>
                    <img class="searchLock" src="'.base_url('resources/img/lock.png').'"/>
                    <span class="f-white">'.$row->title.'</span>
                  </div>';
                }else{
                  $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                    <a href="'.base_url().'Library/Video/'.$row->id.'">
                      <img class="searchImg" src="'.$row->imgUrl.'" data-vid="'.$row->id.'"/>
                    </a>
                    <span class="f-white">'.$row->title.'</span>
                  </div>';
                }
              }else if($row->is_comingsoon == 1){
                $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                  <img class="searchImg" src="'.$row->imgUrl.'" data-vid="'.$row->id.'"/>
                  <span class="f-white">'.$row->title.'</span>
                </div>';
              }else{
                $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                  <a href="'.base_url().'Library/Video/'.$row->id.'">
                    <img class="searchImg" src="'.$row->imgUrl.'" data-vid="'.$row->id.'"/>
                  </a>
                  <span class="f-white">'.$row->title.'</span>
                </div>';
              }
            }
            $ret['data'] .= '</div>
        </div>';

        $ret['data'] .= '<div id="search-sheet" class="search-box containner-padding">
          <div class="row">';
            foreach($libResult->result() as $row){
              if($row->only_subscriber == 1){
                if($myProfile->subscriptionTypeId == 1){
                  $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                    <img class="searchImg" src="'.$row->imgUrl.'"/>
                    <img class="searchLock" src="'.base_url('resources/img/lock.png').'"/>
                    <span class="f-white">'.$row->title.'</span>
                  </div>';
                }else{
                  $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                    <a href="'.base_url().'Library/SubList/'.$row->id.'?subject='.$row->subjectId.'">
                      <img class="searchImg" src="'.$row->imgUrl.'"/>
                      <span class="f-white">'.$row->title.'</span>
                    </a>
                  </div>';
                }
              }else if($row->is_comingsoon == 1){
                $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                  <img class="searchImg" src="'.$row->imgUrl.'"/>
                  <span class="f-white">'.$row->title.'</span>
                  <div class="status"><div class="comingSoon">COMING SOON</div></div>
                </div>';
              }else{
                if($myProfile->subscriptionTypeId == 1){
                  $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                    <a href="'.base_url().'Library/SubList/'.$row->subLevelId.'?subject='.$row->subjectId.'">
                      <img class="searchImg" src="'.$row->imgUrl.'"/>
                      <span class="f-white">'.$row->title.'</span>
                    </a>
                  </div>';
                }else{
                  $ret['data'] .= '<div class="col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10 search-box-div">
                    <a href="'.base_url().'Library/SubList/'.$row->id.'?subject='.$row->subjectId.'">
                      <img class="searchImg" src="'.$row->imgUrl.'"/>
                      <span class="f-white">'.$row->title.'</span>
                    </a>
                  </div>';
                }
              }
            }
            $ret['data'] .= '</div>
        </div>';
      }
    }else{
      $ret['libResult'] = $this->search_model->GetSearchResult_Library('adgdfgdasdds','','')->num_rows();
      $ret['videoResult'] = $this->search_model->GetSearchResult_Video('adgdfgdasdds','','')->num_rows();
      $ret['dataSubject'] = $this->search_model->GetSubjectByKeyword('adgdfgdasdds','','');
      $ret['keyword'] = '';
      $ret['data'] = '';
    }
    return $ret;
  }

  public function getSearchResultJS(){
    $keyword = $this->input->post('keyword');
    $subjectId = $this->input->post('subjectId');
    $libraryId = $this->input->post('libraryId');
    $ret = $this->getSearchResultHtml($keyword,$subjectId,$libraryId);
    echo json_encode($ret);
  }
}
