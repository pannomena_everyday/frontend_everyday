<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyProfile extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(['main_model', 'librarysheet_model', 'sheetvideo_model', 'memberplan_model', 'librarylevel_model', 'myprofile_model', 'membertest_model']);

    $this->token = get_cookie('token');
    $this->main_model->checkToken($this->token);

    // // list วิชา
    $this->plan_trees = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-tree' , null, $this->token);
  }

  public function index()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    
    require_once APPPATH.'third_party/google-api-php/vendor/autoload.php';
    $google_client = new Google_Client();
    $google_client->setClientId($this->config->item('google_client_id'));
    $google_client->setClientSecret($this->config->item('google_client_secret'));
    $google_client->setRedirectUri(base_url('Index/connect_google'));
    $google_client->addScope('email');

    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $this->membertest_model->check_billing_exists($myProfile);

    $education = $this->main_model->curlRequest("GET", $this->config->item('api_url').'education-levels/'.$myProfile->education->level,null,$token);
    $this->data['google_client'] = $google_client;
    $this->data['dataEducation'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'education-levels',null);
    //$this->data['dataProvince'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'provinces',null);
    $this->data['dataProvince'] = $this->myprofile_model->getProvince()->result();
    $this->data['token'] = get_cookie('token');
    $this->data['education'] = $education;
    $this->data['myProfile'] = $myProfile;
    $this->data['province'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'provinces/'.$myProfile->education->provinceId,null);
    $this->data["language"] = $lang;
    $this->data['api_url'] = $this->config->item('api_url');
    $this->data['meta_title'] = 'บัญชีผู้ใช้';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_myProfile';
    $this->load->view('layouts/main', $this->data);
  }

  public function updateName(){
    $firstname = $this->input->post('firstname');
    $this->input->set_cookie(array('name' => 'name','value' => $firstname,'expire' => (60*60*24*7),'secure' => false));
    return true;
  }

  public function updateProfileImg()
  {
    //$token = get_cookie('token');
    //$myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $filename = $this->input->post('filename');
    $this->input->set_cookie(array('name' => 'profileImg','value' => $filename,'expire' => (60*60*24*7),'secure' => false));
  }

  public function ChangeMyProfile()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);

    $this->data['myProfile'] = $myProfile;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = '';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_myProfile_myPlan';
    $this->load->view('layouts/main', $this->data);
  }

  public function MyPlan()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }

    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    if($myProfile->subscriptionTypeId != 1)
    {
      $this->data['myProfile'] = $myProfile;
      $this->data["language"] = $lang;
      $this->data['api_url'] = $this->config->item('api_url');
      $this->data['view_name'] = 'v_myProfile_myPlan';
    }
    else redirect('Library', 'refresh');

    $this->data['meta_title'] = 'แผนการเรียนของฉัน';
    $this->data['meta_description'] = '';
    $this->load->view('layouts/main', $this->data);
  }

  public function AllPlan()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }

    $this->load->model('subject_model');
    $subjects = $this->subject_model->all_arr();

    $all_plan_trees = []; // วิชาที่ Active ที่สามารเพิ่มได้ทั้งหมด
    foreach ($this->plan_trees as $plan_tree)
      $all_plan_trees[$plan_tree->title] = $plan_tree->title;

    function date_compare($a, $b)
    {
      $t1 = strtotime($a['updatedtime']);
      $t2 = strtotime($b['updatedtime']);
      return $t2 - $t1;
    } 
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    if($myProfile->subscriptionTypeId != 1)
    {
      $subject_arr = []; // เก็บชื่อ วิชา current ทั้งหมดของ user ที่ login
      // หาข้อมูล Member plan และ จัด Group ตามวิชา
      $all_plans = $this->memberplan_model->get_all_plan_group_by_subjects($myProfile->id);

      foreach ($all_plans['current_plan'] as $plan)
        $subject_arr[$subjects[$plan['subject_id']]] = $subjects[$plan['subject_id']];

      $this->data['can_add_plan'] = !empty(array_diff($all_plan_trees, $subject_arr));
      $this->data['myProfile'] = $myProfile;
      $this->data['api_url'] = $this->config->item('api_url');
      $this->data['all_plans'] = $all_plans;
      $this->data["language"] = $lang;
      $this->data['view_name'] = 'v_myProfile_AllPlan';
      $this->data['inpage_script'] = '_allPlan_js';
      $this->data['external_scripts'] = array(
        base_url("resources/slick-master/slick/slick.min.js")
      );
    }
    else redirect('Library', 'refresh');

    $this->data['meta_title'] = 'แผนการเรียนทั้งหมด';
    $this->data['meta_description'] = '';
    $this->load->view('layouts/main', $this->data);
  }

  /**
   * หาข้อมูล Sub level แสดงในหน้า all plan
   *
   * @param Number GET['plan_id']
   * @param Number GET['library_level_id']
   * @return Json
   **/
  public function get_sub_level()
  {
    $this->load->model(['librarysublevel_model', 'subject_model']);
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me', null, $this->token); // ข้อมูล Member
    $subject_colors = $this->config->item('subject_colors');
    $subjects = $this->subject_model->all_arr();

    $sub_levels = $this->librarysublevel_model->all_by_librarylevelids($this->input->get('plan_id'), $this->input->get('library_level_id'));

    foreach ($sub_levels as &$sub_level)
    {
      $sub_level->color_code = $this->db->get_where('Subject', ['id' => $sub_level->subjectId])->row()->color_code;
      $progress = $this->sheetvideo_model->find_progress_of_sublevel($sub_level->libraryLevelId, $sub_level->id, $myProfile->id);
      $total_progress = $progress->vdo * 100;
      $current = $progress->total_progress == null || $progress->total_progress == '' ? 0 : $progress->total_progress;
      if($total_progress <= 0)
        $sub_level->progress_bar = 0;
      else
        $sub_level->progress_bar = round($current / $total_progress * 100);
      
    }

    $this->_print_json(json_encode($sub_levels));
  }

  /**
   * หาข้อมูล Sub Sheet แสดงในหน้า all plan
   *
   * @param Number GET['plan_id']
   * @param Number GET['library_level_id']
   * @param Number GET['sub_level_id']
   * @return Json
   **/
  public function get_sub_sheet()
  {
    $this->load->model(['librarysheet_model', 'sheetvideo_model']);
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me', null, $this->token); // ข้อมูล Member
    $res = [];

    $sheets = []; // sheet ธรรมดา
    $test_sheets = []; // sheet test
    // Sheets
    $test_sheet = $this->main_model->curlRequest("GET", $this->config->item('api_url').'library-sub-levels/'.$this->input->get('sub_level_id').'/sheet-and-test', null);
    if(!empty($test_sheet))
    {
      foreach ($test_sheet->data->sheets_n_testing as &$sheet)
      {

        if($sheet->is_test)
        {
          $sheet->plan_id = $this->input->get('plan_id');
          $sheet->libraryLevelId = $this->input->get('library_level_id');
          $sheet->librarySubLevelId = $this->input->get('sub_level_id');
          $test_sheets[] = $sheet;
        }
        else
        {
          $progress = $this->sheetvideo_model->find_progress_of_sheet($sheet->id, $myProfile->id);
          $total_progress = $progress->vdo * 100;
          $current = $progress->total_progress == null || $progress->total_progress == '' ? 0 : $progress->total_progress;
          if($total_progress <= 0)
            $sheet->progress_bar = 0;
          else
            $sheet->progress_bar = round($current / $total_progress * 100);
          
          $sheet_detail = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/sheets/'.$sheet->id, null);
          $sheet->plan_id = $this->input->get('plan_id');
          $sheet->is_test = 0;
          $sheet->sheetcode = $sheet_detail->sheetCode;
          $sheets[] = $sheet;
        }
      }
    }

    $res['sheets'] = $sheets;
    $res['test_sheets'] = $test_sheets;

    $this->_print_json(json_encode($res));
  }

  public function AddPlan()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $this->data['meta_title'] = '';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_myProfile_AddPlan';
    $this->load->view('layouts/main', $this->data);
  }

  public function ChangePlan($plan_id, $subject_id)
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);

    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }

    $this->load->model(['planlibrary_model', 'subject_model', 'plan_model']);

    $plan_trees = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-tree' , null, $token);
    $planlibraries = $this->planlibrary_model->get_arr_title();
    $subjects = $this->subject_model->all_arr();
    $plan_step = $this->plan_model->get_step($plan_id); // หา Step ของ plan

    $this->data["page_active"] = "myEveryDay";
    $this->data['plan_step'] = json_encode($plan_step);
    $this->data['subject_title'] = $subjects[$subject_id];
    $this->data['subject_id'] = $subject_id;
    $this->data['plan_trees'] = $plan_trees;
    $this->data['plan_title'] = $planlibraries[$plan_id];
    $this->data['plan_id'] = $plan_id;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'เปลี่ยนแผนการเรียน';
    $this->data['meta_description'] = 'เปลี่ยนแผนการเรียนได้ง่าย ๆ ทำได้ทุกเวลา';
    $this->data['view_name'] = 'v_myProfile_ChangePlan';
    $this->load->view('layouts/main', $this->data);
  }

  /**
   * หาข้อมูล Sub plan tree
   *
   * @return void
   **/
  public function get_sub_plan()
  {
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $subject_id = $this->input->get('subject');

    $sub_plan_trees = [];
    foreach ($this->plan_trees as $plan_tree)
    {
      $sub_plan_trees[$plan_tree->id] = $plan_tree;
    }

    $this->_print_json(json_encode($sub_plan_trees[$subject_id]));
  }

  /**
   * หาข้อมูล Sub plan tree
   *
   * @return void
   **/
  public function get_children_sub_plan()
  {
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $subplan_id = $this->input->get('subplan');
    $subject_id = $this->input->get('subject');

    $sub_plan_trees = [];
    foreach ($this->plan_trees as $plan_tree)
    {
      if(!empty($plan_tree->children))
      {
        $sub_plan_trees[$plan_tree->id] = [];
        foreach ($plan_tree->children as $children)
        {
          $sub_plan_trees[$plan_tree->id]['children'][$children->id] = $children;
        }
      }
    }
    $temp_plan = $sub_plan_trees[$subject_id]['children'][$subplan_id];
    foreach ($temp_plan->children as $key => $children) {
      // find plan id
      $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-libraries/'.$children->plan_key , null, $token);
      // check is active
      $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$plan->planId , null, $token);
      if(!$plan->is_active){
        unset($temp_plan->children[$key]);
      }
    }
    if(count($temp_plan->children) == 0){
      $temp_plan->hasChild = false;
    }else{
      foreach ($temp_plan->children as $sub_children) {
        if(!isset($sub_children->children)){
          continue;
        }
      
        //check active plan in children
        if(isset($temp_plan->children)){
          if(count($temp_plan->children) > 0){
            foreach ($temp_plan->children as $sub_children) {
              //
              if(isset($sub_children->children)){
                if(count($sub_children->children) > 0){
                  foreach ($sub_children->children as $key => $children) {
                    // find plan id
                    $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-libraries/'.$children->plan_key , null, $token);
                    // check is active
                    $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$plan->planId , null, $token);
                    if(!$plan->is_active){
                      unset($sub_children->children[$key]);
                    }
                  }
                }
                
                if(count($sub_children->children) == 0){
                  $sub_children->hasChild = false;
                }
              }
              //
            }
          }
          
          if(count($temp_plan->children) == 0){
            $temp_plan->hasChild = false;
          }
        }
      }
    }
    // $this->_print_json(json_encode($sub_plan_trees[$subject_id]['children'][$subplan_id]));
    $this->_print_json(json_encode($temp_plan));
  }

  /**
   * หาข้อมูล Sub plan tree
   *
   * @return void
   **/
  public function get_children_sub_sub_plan()
  {
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $subplan_id = $this->input->get('subplan');
    $sub_sub_plan_id = $this->input->get('subsubplan');
    $subject_id = $this->input->get('subject');

    $sub_plan_trees = [];
    $sub_sub_plan_trees = [];
    foreach ($this->plan_trees as $plan_tree)
    {
      if(!empty($plan_tree->children))
      {
        $sub_plan_trees[$plan_tree->id] = [];
        foreach ($plan_tree->children as $children)
        {
          $sub_plan_trees[$plan_tree->id]['children'][$children->id] = $children;
        }
      }
    }

    foreach ($sub_plan_trees[$subject_id]['children'][$subplan_id]->children as $plan_tree)
    {
      $sub_sub_plan_trees[$plan_tree->id] = $plan_tree;
    }

    $temp_plan = $sub_sub_plan_trees[$sub_sub_plan_id];
    // check active plan in main
    if(isset($temp_plan->children)){
      if(count($temp_plan->children) > 0){
        foreach ($temp_plan->children as $key => $children) {
          // find plan id
          $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-libraries/'.$children->plan_key , null, $token);
          // check is active
          $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$plan->planId , null, $token);
          if(!$plan->is_active){
            unset($temp_plan->children[$key]);
          }
        }
      }
      
      if(count($temp_plan->children) == 0){
        $temp_plan->hasChild = false;
      }
    }

    //check active plan in children
    if(isset($temp_plan->children)){
      if(count($temp_plan->children) > 0){
        foreach ($temp_plan->children as $sub_children) {
          //
          if(isset($sub_children->children)){
            if(count($sub_children->children) > 0){
              foreach ($sub_children->children as $key => $children) {
                // find plan id
                $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-libraries/'.$children->plan_key , null, $token);
                // check is active
                $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$plan->planId , null, $token);
                if(!$plan->is_active){
                  unset($sub_children->children[$key]);
                }
              }
            }
            
            if(count($sub_children->children) == 0){
              $sub_children->hasChild = false;
            }
          }
          //
          
        }
      }
      
      if(count($temp_plan->children) == 0){
        $temp_plan->hasChild = false;
      }
    }

    $this->_print_json(json_encode($temp_plan));
  }

  /**
   * หาข้อมูล Sub plan tree
   *
   * @return void
   **/
  public function get_children_sub_sub_sub_plan()
  {
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $subplan_id = $this->input->get('subplan');
    $sub_sub_plan_id = $this->input->get('subsubplan');
    $sub_sub_sub_plan_id = $this->input->get('subsubsubplan');
    $subject_id = $this->input->get('subject');

    $sub_plan_trees = [];
    $sub_sub_plan_trees = [];
    $sub_sub_sub_plan_trees = [];

    foreach ($this->plan_trees as $plan_tree)
    {
      if(!empty($plan_tree->children))
      {
        $sub_plan_trees[$plan_tree->id] = [];
        foreach ($plan_tree->children as $children)
        {
          $sub_plan_trees[$plan_tree->id]['children'][$children->id] = $children;
        }
      }
    }
    
    foreach ($sub_plan_trees[$subject_id]['children'][$subplan_id]->children as $plan_tree)
    {
      $sub_sub_plan_trees[$plan_tree->id] = $plan_tree;
    }

    foreach ($sub_sub_plan_trees[$sub_sub_plan_id]->children as $plan_tree)
    {
      $sub_sub_sub_plan_trees[$plan_tree->id] = $plan_tree;
    }

    $this->_print_json(json_encode($sub_sub_sub_plan_trees[$sub_sub_sub_plan_id]));
  }

  /**
   * หาข้อมูล plan libraries
   *
   * @return void
   **/
  public function get_plan_libraries()
  {
    $this->load->model('memberplan_model');
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $plan_key = $this->input->get('plan_key');
    $children = $this->input->get('children');

    if($children)
      $this->_print_json(json_encode(['status' => '200', 'data' => ['plan_key' => $plan_key, 'children' => $children]]));
  }

  /**
   * บันทึกลง table memer plan
   *
   * @return void
   **/
  public function change_plan_submit()
  {
    // ค่า Token ที่ login มา
    $token = get_cookie('token');
    $plan_key = $this->input->get('plan_key');
    $children = $this->input->get('children');
    $subject = $this->input->get('subject');

    // plan libraries
    $plan_libraries = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan-libraries/'.$plan_key, null, $token);
    // Profile
    $profile = $this->main_model->curlRequest("GET",$this->config->item('api_url').'members/me', null, $token);

    if($children)
    {
      $member_plan_data = [];
      $member_plan_data['memberId'] = intval($profile->id);
      $member_plan_data['librariesProgress'] = $plan_libraries->library_data;
      $member_plan_data['planId'] = intval($plan_libraries->planId);

      // หาข้อมูล Plan เพื่อเช็ค is_active
      $plan = $this->main_model->curlRequest("GET", $this->config->item('api_url').'front-end/plan/'.$member_plan_data['planId'], null, $token);

      if($plan->is_active && !empty($plan->planLibrary->library_data->subject) && !empty($plan->planLibrary->library_data->levels))
      {
        // save member plan
        $response = $this->main_model->curlRequest("POST", $this->config->item('api_url').'front-end/add-plan/', $member_plan_data, $token);
        if(isset($response->status) && $response->status === false)
        {
          $this->session->set_flashdata('error', 'นักเรียนได้เพิ่มแผนเรียนนี้ไว้แล้ว โปรดเลือกแผนการเรียนใหม่');
          $this->_print_json(json_encode(['status' => '404']));
        }
        else
          $this->_print_json(json_encode(['status' => 500]));
      }
      else
      {
        $this->session->set_flashdata('error', 'ยังไม่สามารถเพิ่มแผนการเรียนนี้ได้ เนื่องจากแผนการเรียนยังไม่สมบูรณ์');
        $this->_print_json(json_encode(['status' => '404']));
      }
    }
  }

  public function MyPerformance()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $this->data['dataPerformance'] = $this->MyPerformanceHTML($myProfile->id);
    $this->data['myProfile'] = $myProfile;
    $this->data['api_url'] = $this->config->item('api_url');
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'ติดตามผลงาน';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_myProfile_myPerformance';
    $this->load->view('layouts/main', $this->data);
  }

  public function MyPerformanceHTML($memberId)
  {
    $html = '';
    $memberPlan = $this->myprofile_model->getMemberPlan();
    foreach($memberPlan->result() as $plan){
      $memberPreTest = $this->myprofile_model->getMemberPreTest($memberId,$plan->planId);
      $memberTest = $this->myprofile_model->getMemberTest($memberId,$plan->planId);
      if($memberPreTest->num_rows() > 0 || $memberTest->num_rows() > 0){
        $html .= '<div class="">
        <div class="row f20 m-b-10">
          <div class="col-md-9 f-22 bold mt-5 mb-3">'.$plan->title.'</div>
        </div>';
      }
      foreach($memberPreTest->result() as $row){
        $answers = json_decode($row->answers);
        $preTestTotalAnswer = count($answers->answer);
        $preTestPercent = floor(($row->score*100)/$preTestTotalAnswer);
        $preTestPercentUse = 100-$preTestPercent;
        $html .= '<div class="">
          <div class="row f20 m-b-10">
            <div class="col-md-9">
            แบบทดสอบก่อนเรียน
            </div>
            <div class="col-md-3 t-right blue">
              '.$row->score.'/'.$preTestTotalAnswer.'
            </div>
          </div>
          <div class="prog-bar-myPer">
            <div class="prog-bar">
              <div class="background" style=" -webkit-clip-path: inset(0 '.$preTestPercentUse.'% 0 0); clip-path: inset(0 '.$preTestPercentUse.'% 0 0);">
              </div>
            </div>
          </div>
          <div class="prog-text f18">'.$preTestPercent.'%</div>
        </div>';
      }
      foreach($memberTest->result() as $row2){
        $testAnswers = json_decode($row2->answers);
        $testTotalAnswer = count($testAnswers->answer);
        $testPercent = floor(($row2->score*100)/$testTotalAnswer);
        $testPercentUse = 100-$testPercent;
        $html .= '<div class="">
          <div class="row f20 m-b-10">
            <div class="col-md-9">
            แบบทดสอบ
            </div>
            <div class="col-md-3 t-right blue">
              '.$row2->score.'/'.$testTotalAnswer.'
            </div>
          </div>
          <div class="prog-bar-myPer">
            <div class="prog-bar">
              <div class="background" style=" -webkit-clip-path: inset(0 '.$testPercentUse.'% 0 0); clip-path: inset(0 '.$testPercentUse.'% 0 0);">
              </div>
            </div>
          </div>
          <div class="prog-text f18">'.$testPercent.'%</div>
        </div>';
      }
    }
    return $html;
  }

  public function MyNote()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);

    $this->data['subject'] = $this->myprofile_model->getMyNoteSubject($myProfile->id);
    $this->data['myProfile'] = $myProfile;
    $this->data['api_url'] = $this->config->item('api_url');
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'คลัง Sheet';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_myNote';
    $this->load->view('layouts/main', $this->data);
  }

  public function GetSheetTitle()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $keyword = $this->input->post('keyword');
    $subject = $this->input->post('subject');
    $data = $this->myprofile_model->GetSheetTitle($myProfile->id,$keyword,$subject);
    echo json_encode($data);
  }

  public function getMyNoteHTML()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $subject = $this->input->post('subject');
    $search = $this->input->post('search');
    $dateFrom = $this->input->post('dateFrom');
    $dateTo = $this->input->post('dateTo');
    $data = $this->myprofile_model->getMyNoteHTML($myProfile->id,$subject,$search,$dateFrom,$dateTo);
    if($data->num_rows() == 0){
      echo '<div class="mb-4 ml-3">ไม่พบข้อมูล</div>';
    }else{
      foreach($data->result() as $row){
        echo '<div class="col-xs-2-5-10 col-sm-5-10 col-md-2-10 col-lg-2-10 m-b-30">
          <a target="_blank" href="'.$this->genSign($row->path)[0].'">
            <img class="mw-100" src="'.$row->imgUrl.'" class="log-cover"/>
            <p class="m-t-5">'.$row->title.'</p>
          </a>
        </div>';
      }
    }
  }

  public function genSign($filename){
    $headers = array("accept: application/json",'Content-Type: application/json');
    $method = 'GET';
    $api_url = $this->config->item('api_url');
    $url = $api_url.'gen-signed-url/'.$filename;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
    $response = curl_exec($curl);
    curl_close($curl);
    return json_decode($response);
  }

  public function MyMembership()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $this->membertest_model->check_billing_exists($myProfile);
    //$this->data['subscription'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/'.$myProfile->id.'/member-subscriptions',null,$token);
    $this->data['subscription'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/omise-schedule',null,$token);
    // var_dump($subscription);
    // echo '<br>';
    // echo $subscription[0]->requestData->period;
    if(isset($myProfile->memberPackages->currentSubscriptionTypeId)){
      $currentSubscriber = $this->myprofile_model->getSubscriberByID($myProfile->memberPackages->currentSubscriptionTypeId);
      $this->data['currentTotal'] = ($myProfile->memberPackages->currentSubscriptionInfo->price)-($myProfile->memberPackages->currentSubscriptionInfo->discount);
      if(isset($myProfile->memberPackages->nextSubscriptionInfo->price) && isset($myProfile->memberPackages->nextSubscriptionInfo->discount)){
        $this->data['nextTotal'] = ($myProfile->memberPackages->nextSubscriptionInfo->price)-($myProfile->memberPackages->nextSubscriptionInfo->discount);
      }else{
        $this->data['nextTotal'] = 1;
      }
      if($currentSubscriber->num_rows() > 0){
        $this->data['currentSubscriberTitle'] = $currentSubscriber->result()[0]->title;
        $this->data['currentExpire'] = date("d-m-Y", strtotime($myProfile->memberPackages->currentExpireDate));
        if(isset($myProfile->memberPackages->nextBillDate)){
          $this->data['nextBill'] = date("d-m-Y", strtotime($myProfile->memberPackages->nextBillDate));
        }
      }
      if(isset($myProfile->memberPackages->nextSubscriptionTypeId)){
        $nextSubscriber = $this->myprofile_model->getSubscriberByID($myProfile->memberPackages->nextSubscriptionTypeId);
        $this->data['nextSubscriberTitle'] = $nextSubscriber->result()[0]->title;
      }
    }
    //nextSubscriptionTypeId
    $billAddr = $this->myprofile_model->getBillingAddress($myProfile->id);
    $this->data['dataProvince'] = $this->myprofile_model->getProvinces();
    $this->data['dataAmphure'] = $this->myprofile_model->getAmphures((isset($billAddr->result()[0]->provinceId) ? $billAddr->result()[0]->provinceId : ''));
    $this->data['dataDistrict'] = $this->myprofile_model->getDistricts((isset($billAddr->result()[0]->amphureId) ? $billAddr->result()[0]->amphureId : ''));
    $this->data['dataZipCode'] = $this->myprofile_model->getZipCode((isset($billAddr->result()[0]->districtId) ? $billAddr->result()[0]->districtId : ''));
    $this->data['fullname'] = (isset($billAddr->result()[0]->fullname) ? $billAddr->result()[0]->fullname : '');
    $this->data['mobile'] = (isset($billAddr->result()[0]->mobile) ? $billAddr->result()[0]->mobile : '');
    $this->data['address'] = (isset($billAddr->result()[0]->address) ? $billAddr->result()[0]->address : '');
    $this->data['provinceId'] = (isset($billAddr->result()[0]->provinceId) ? $billAddr->result()[0]->provinceId : '');
    $this->data['amphureId'] = (isset($billAddr->result()[0]->amphureId) ? $billAddr->result()[0]->amphureId : '');
    $this->data['districtId'] = (isset($billAddr->result()[0]->districtId) ? $billAddr->result()[0]->districtId : '');
    $this->data['zipCode'] = (isset($billAddr->result()[0]->zipcode) ? $billAddr->result()[0]->zipcode : '');
    $this->data['dataBillAddr'] = $billAddr;
    $this->data['myProfile'] = $myProfile;
    $this->data['api_url'] = $this->config->item('api_url');
    $this->data["language"] = $lang;
    $this->data['token'] = $token;
    $this->data['meta_title'] = 'สถานะสมาชิก';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_myMembership';
    $this->load->view('layouts/main', $this->data);
  }

  public function getAmphureHTML(){
    $provinceId = $this->input->post('provinceId');
    $dataAmphure = $this->myprofile_model->getAmphures($provinceId);
    echo '<option value=""> ระบุ เขต / อำเภอ</option>';
    foreach($dataAmphure->result() as $row){
      echo "<option value='".$row->id."'>".$row->name_th."</option>";
    }
  }

  public function getDistrictHTML(){
    $amphureId = $this->input->post('amphureId');
    $dataDistrict = $this->myprofile_model->getDistricts($amphureId);
    echo '<option value=""> ระบุ แขวง / ตำบล</option>';
    foreach($dataDistrict->result() as $row){
      echo "<option value='".$row->id."'>".$row->name_th."</option>";
    }
  }

  public function getZipCodeHTML(){
    $districtId = $this->input->post('districtId');
    $dataZipCode = $this->myprofile_model->getZipCode($districtId);
    foreach($dataZipCode->result() as $row){
      echo "<option value='".$row->zip_code."'>".$row->zip_code."</option>";
    }
  }

  public function updateMembership()
  {
    $memberId = $this->input->post('memberId');
    $fullname = $this->input->post('fullname');
    $mobile = $this->input->post('mobile');
    $address = $this->input->post('addr');
    $provinceId = $this->input->post('provinceId');
    $amphureId = $this->input->post('amphureId');
    $districtId = $this->input->post('districtId');
    $zipCode = $this->input->post('zipCode');
    if($this->myprofile_model->insertBillingAddress($memberId,$fullname,$mobile,$address,$provinceId,$amphureId,$districtId,$zipCode)){
      echo 'แก้ไขข้อมูลเรียบร้อยแล้ว';
    }else{
      echo 'ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง';
    }
  }

  public function MyLog()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    //$this->data['dataLog'] = $this->myprofile_model->getLogData($myProfile->id,'','');
    $this->data['myProfile'] = $myProfile;
    $this->data['api_url'] = $this->config->item('api_url');
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'ประวัติการเรียน';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_myLog';
    $this->load->view('layouts/main', $this->data);
  }

  public function getMyLogHTML()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $dateFrom = $this->input->post('dateFrom');
    $dateTo = $this->input->post('dateTo');
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $data = $this->myprofile_model->getLogData($myProfile->id,$dateFrom,$dateTo);
    if($data->num_rows() == 0){
      echo 'ไม่พบข้อมูล';
    }else{
      foreach($data->result() as $row){
        $date = date("Y-m-d H:i:s", strtotime('+7 hours', strtotime($row->updatedtime)));
        echo '<div class="row log-div">
          <div class="col-md-2">
            <p class="logDate">'.$this->DateThai($date).'</p>
            <p class="logTime"><i class="fa fa-clock-o" aria-hidden="true"></i> '.$this->getTime($date).'</p>
          </div>
          <div class="col-md-10">
            <p class="logDetail">'.$row->txtTitle.'</p>
          </div>
        </div>';
      }
    }
  }

  public function disconnect_social($id)
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $request = array(
      'socialid' => $id
    );

    $this->main_model->curlRequest("PATCH", $this->config->item('api_url').'member/disconnect-with-social', $request, $token);
    redirect('MyProfile', 'refresh');
  }

  function DateThai($strDate)
  {
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strHour= date("H",strtotime($strDate));
    $strMinute= date("i",strtotime($strDate));
    $strSeconds= date("s",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
  }
  function getTime($strDate)
  {
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strHour= date("H",strtotime($strDate));
    $strMinute= date("i",strtotime($strDate));
    $strSeconds= date("s",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strHour:$strMinute";
  }

  public function changeProfile()
  {
    echo "success";
  }

}
