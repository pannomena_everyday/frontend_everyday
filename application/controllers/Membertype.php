<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membertype extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(array('main_model', 'memberplan_model'));
  }
  
  public function index()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $currentPackage = -1;
    if(isset($myProfile->memberPackages->currentSubscriptionTypeId)){
      $currentPackage = $myProfile->memberPackages->currentSubscriptionTypeId;
    }
    $membership = $this->input->get('membership');
    $this->data['unsubscribe'] = false;
    if(isset($membership))
      $this->data['unsubscribe'] = true;

    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    // $subscriptionTypesUrl = '/subscription-types?filter='.urlencode('{"where": {"is_active": true}}');
    
    // $subscriptionTypes = $this->main_model->curlRequest("GET", $this->config->item('api_url').$subscriptionTypesUrl, null, null);
    $subscriptionTypes = $this->memberplan_model->find_subscription_type();

    $this->data['currentPackage'] = $currentPackage;
    $this->data["subscriptionTypes"] = $subscriptionTypes;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'ประเภทสมาชิก - Monkey Everyday เรียนออนไลน์ ติวเข้ม เตรียมสอบ ฟรี';
    $this->data['meta_description'] = 'Free/ Premium';
    $this->data['view_name'] = 'v_membertype';
    $this->data['external_scripts'] = array(
      base_url("resources/slick-master/slick/slick.min.js")
    );
    $this->load->view('layouts/main', $this->data);
  }

  function unsubscribe()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $response = $this->main_model->curlRequest("POST", $this->config->item('api_url').'members/unsubscribe-package', array('init'=> ''), $token);
    $this->_print_json(json_encode(
      array(
        "status" => true,
        "response" => $response
      )
    ));
  }

  public function check_promotion()
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $promotion = $this->input->post('promotion');
    $subscription_id = $this->input->post('subscription_id');
    $subscribe_obj = array(
      'subscriptionTypeId' => (int)$subscription_id,
      'promotionCode' => $promotion,
      'isValidate' => true
    );
    $validate = $this->main_model->curlRequest("POST", $this->config->item('api_url').'members/subscribe-package', $subscribe_obj, $token);
    
    if(isset($validate->error)){
      $response = array(
        "status" => false,
        "message" => ""
      );
    }
    elseif(isset($validate->status))
    {
      if(!$validate->status)
      {
        $response = array(
          "status" => false,
          "message" => $validate->message
        );
      }
    }
    else
    {
    $response = array(
      "status" => true
    );
    }
    $this->_print_json(json_encode($response));
  }
}
