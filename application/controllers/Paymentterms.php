<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymentterms extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('main_model');
  }
  
  public function index()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'เงื่อนไขการใช้เว็บไซต์ - Monkey Everyday เรียนออนไลน์ ติวเข้ม เตรียมสอบ ฟรี';
    $this->data['meta_description'] = 'เงื่อนไขและข้อตกลงการใช้บริการเว็บเรียนออนไลน์';
    $this->data['view_name'] = 'v_paymentterms';
    $this->load->view('layouts/main', $this->data);
  }
}
