<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privacypolicy extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('main_model');
  }
  
  public function index()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'นโยบายความเป็นส่วนตัว - Monkey Everyday เรียนออนไลน์ ติวเข้ม เตรียมสอบ ฟรี';
    $this->data['meta_description'] = 'เกี่ยวกับความเป็นส่วนตัวและความปลอดภัยของข้อมูลส่วนบุคคลของผู้ใช้บริการเว็บเรียนออนไลน์';
    $this->data['view_name'] = 'v_privacypolicy';
    $this->load->view('layouts/main', $this->data);
  }
}
