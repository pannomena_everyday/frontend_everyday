<?php

use League\OAuth2\Client\Provider\Google;

defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(array('main_model', 'search_model', 'librarysublevel_model'));
  }

  public function serverName()
  {
    if (isset($_SERVER['SERVER_NAME'])){
      echo $_SERVER['SERVER_NAME'];
    }else{
      echo "none";
    }
  }

  public function index()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $token = get_cookie('token');
    //$this->main_model->checkToken($token);

    $last_update = $this->librarysublevel_model->last_update();
    $populates = $this->librarysublevel_model->populate();

    //$this->data['myProfile'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $this->data['dataBanners'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'banners',null);
    $this->data['dataTestimonials'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'testimonials',null);
    $this->data['last_update'] = $last_update;
    $this->data['populates'] = $populates;
    $this->data["language"] = $lang;
    $this->data['dayOfWeek'] = $dayOfWeek = date("l", strtotime("now"));
    $this->data['meta_title'] = 'เรียนออนไลน์กับ Monkey Everyday';
    $this->data['meta_description'] = 'Self-Learning Platform เรียนออนไลน์ ฟรี! คลังข้อสอบ เนื้อหาครอบคลุมตั้งแต่ ป.ปลาย ม.ต้น ม.ปลาย จนถึง มหาวิทยาลัย พร้อมระบบวางแผนการเรียน ใช้งานง่าย เก่งขึ้นได้ทุกที่ ดีขึ้นได้ทุกวัน!';
    $this->data['t'] = 'v_index';
    $this->data['view_name'] = 'v_index';
    $this->load->view('layouts/main', $this->data);
  }

  public function SignIn()
  {
    require_once APPPATH.'third_party/google-api-php/vendor/autoload.php';
    $google_client = new Google_Client();
    $google_client->setClientId($this->config->item('google_client_id'));
    $google_client->setClientSecret($this->config->item('google_client_secret'));
    $google_client->setRedirectUri(base_url('Index/auth_google'));
    $google_client->addScope('email');

    $this->LogOut();
    $this->data["isLogout"] = true;
    $this->data["google_client"] = $google_client;
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["rememberMobile"] = get_cookie('rememberMobile');
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'เข้าสู่ระบบ';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_signIn';
    $this->load->view('layouts/main', $this->data);
  }

  public function SignUp()
  {
    $this->LogOut();
    $this->data["isLogout"] = true;
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data['dataEducation'] = $this->main_model->curlRequest("GET", $this->config->item('api_url').'education-levels',null);
    $this->data['dataProvince'] = $this->main_model->getProvinceBKKFrist();
    //var_dump($dataProvince);
    //print_r($dataProvince[0]);
    //echo $dataProvince[0]->ProvinceName;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'สมัครสมาชิก -  เริ่มต้นเรียนฟรีกับ Monkey Everyday';
    $this->data['meta_description'] = 'เรียนออนไลน์ฟรีได้ทันที สมัครเลย';
    $this->data['view_name'] = 'v_signUp';
    $this->load->view('layouts/main', $this->data);
  }

  public function checkEmailExist()
  {
    $email = $this->input->post('email');
    echo $this->main_model->checkEmailExist($email)->num_rows();
  }

  public function checkMobileExist()
  {
    $mobile = $this->input->post('mobile');
    echo $this->main_model->checkMobileExist($mobile)->num_rows();
  }

  public function checkIdCardExist()
  {
    $id_card = $this->input->post('id_card');
    echo $this->main_model->checkIdCardExist($id_card)->num_rows();
  }

  public function checkTimeOTP()
  {
    date_default_timezone_set("Asia/Bangkok");
    $checkTimeOTP = get_cookie('checkTimeOTP');
    if($checkTimeOTP == null){
      $ret = array(
        'result' => true
      );
    }else{
      $ret = array(
        'result' => false
      );
    }
    echo json_encode($ret);
  }

  public function setTimeOTP()
  {
    date_default_timezone_set("Asia/Bangkok");
    // 1Min
    $this->input->set_cookie(array('name' => 'checkTimeOTP','value' => strtotime("+30 second"),'expire' => (30),'secure' => false));
  }

  public function LogIn()
  {
    if(!isset($_SESSION)){session_start();}
    $mobile = $this->input->post('mobile');
    $password = $this->input->post('password');
    $remember = $this->input->post('remember');
    $arrData = array(
        "mobile" => $mobile,
        "password" => $password
    );
    $ret = $this->main_model->curlRequest("POST", $this->config->item('api_url').'members/login',$arrData);
    if(isset($ret->roles[0])){
      $roles = strtolower($ret->roles[0]);
    }else{
      $roles = "";
    }
    if(isset($ret->token) && $roles != "admin"){
        $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$ret->token);
        $this->input->set_cookie(array('name' => 'name','value' => $myProfile->firstname,'expire' => (60*60*24*7),'secure' => false));
        $this->input->set_cookie(array('name' => 'profileImg','value' => $myProfile->profile_image,'expire' => (60*60*24*7),'secure' => false));
        $this->input->set_cookie(array('name' => 'token','value' => $ret->token,'expire' => (60*60*24*7),'secure' => false));
        $this->input->set_cookie(array('name' => 'subscriberType','value' => $ret->subscriberType,'expire' => (60*60*24*7),'secure' => false));
        //set_cookie('name',$myProfile->firstname,time()+(60*60*24));
        //set_cookie('token',$ret->token,time()+(60*60*24));
        //set_cookie('subscriberType',$ret->subscriberType,time()+(60*60*24));
        if($remember == 1){
            //set_cookie('rememberMobile',$mobile,time()+(60*60*24*365));
            $this->input->set_cookie(array('name' => 'rememberMobile','value' => $mobile,'expire' => (60*60*24*365),'secure' => false));
        }else{
            delete_cookie('rememberMobile');
        }
        echo "success";
    }else{
      if(isset($ret->token)){
        echo "Username or password is incorrect.";
      }else{
        echo "Error: ".$ret->error->message; //var_dump($ret); //$ret->error;
      }
    }
    session_write_close();
    //header("Location: ".base_url()."MyEveryDay");
  }

  public function LogOut()
  {
    delete_cookie('token');
    delete_cookie('subscriberType');
    delete_cookie('profileImg');
    delete_cookie('name');
  }

  public function ForgotPassword()
  {
    $this->LogOut();
    $this->data["isLogout"] = true;
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'ลืมรหัสผ่าน';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_forgotPassword';
    $this->load->view('layouts/main', $this->data);
  }

  public function ForgotPasswordProcess()
  {
    $val = $this->input->post('val');
    $valUrl = urlencode(strtr(base64_encode($val), '+/', '-_'));
    // $salt = "c937gfijvc87yasd9";
    // $key = "NazirAhmadHeratHost";
    // $cryptKey = $salt.$key;
    // $mobileEncoded = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5($cryptKey), $mobile, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
    // $mobileUrl = hash('sha256',$mobileEncoded);
    $imgLogo = base_url()."resources/img/logo_everyday.png";

    $ret = $this->main_model->getEmailForgot($val);
    if($ret->num_rows() > 0){
      require_once('resources/PHPMailer-5.2/PHPMailerAutoload.php');
      $mail = new PHPMailer();
      $mail->IsHTML(true);
      $mail->IsSMTP();
      $mail->SMTPAuth = true; // enable SMTP authentication
      $mail->SMTPSecure = ""; // sets the prefix to the servier
      $mail->Host = "smtp.office365.com"; // sets GMAIL as the SMTP server
      $mail->Port = 587; // set the SMTP port for the GMAIL server
      $mail->Username = "noreply@monkeyeveryday.com"; // GMAIL username
      $mail->Password = "y&C&z5Br8fKzhpy&"; // GMAIL password
      $mail->CharSet = "utf-8";
      $mail->From = "noreply@monkeyeveryday.com"; // "name@yourdomain.com";
      //$mail->AddReplyTo = "support@thaicreate.com"; // Reply
      $mail->FromName = "noreply@monkeyeveryday.com";  // set from Name
      $mail->Subject = "[Action Required] Monkey Everyday Reset Password";
      $mail->Body = '<h2> Monkey Everyday </h2>
        <h2> ลืมรหัสผ่าน </h2>
        <p> โปรดคลิกปุ่มด้านล่างเพื่อเปลี่ยนรหัสผ่านของคุณ </p>
        <a href="'.base_url().'Index/ChangePassword?val='.$valUrl.'"><button style="padding: 20px 40px; background-color: #000; color: #fff"> เปลี่ยนรหัสผ่าน </button></a>
        <p> หากคุณไม่ได้ส่งคําขอนี้ให้เพิกเฉยต่ออีเมลนี้ </p>
        <div style="border-bottom: 1px #ccc solid; margin: 20px 0;"></div>';
      $mail->AddAddress($ret->result()[0]->email); // to Address
      if(!$mail->send()) {
          echo 'false';
          //echo 'Mailer Error: ' . $mail->ErrorInfo;
          //echo "<script>console.log('".$mail->ErrorInfo."')</script>";
      } else {
          //echo 'ส่งข้อมูลการเปลี่ยนรหัสผ่านไปที่อีเมลที่ลงทะเบียนไว้เรียบร้อยแล้ว';
          echo 'true';
      }
    }else{
        echo "ไม่พบข้อมูล กรุณากรอกข้อมูลที่ถูกต้อง";
    }
  }

  public function testMail()
  {
      require_once('resources/PHPMailer-5.2/PHPMailerAutoload.php');
      $mail = new PHPMailer();
      $mail->IsHTML(true);
      $mail->IsSMTP();
      $mail->SMTPAuth = true; // enable SMTP authentication
      $mail->SMTPSecure = ""; // sets the prefix to the servier
      $mail->Host = "smtp.office365.com"; // sets GMAIL as the SMTP server
      $mail->Port = 587; // set the SMTP port for the GMAIL server
      $mail->Username = "noreply@monkeyeveryday.com"; // GMAIL username
      $mail->Password = "y&C&z5Br8fKzhpy&"; // GMAIL password
      $mail->CharSet = "utf-8";
      $mail->From = "noreply@monkeyeveryday.com"; // "name@yourdomain.com";
      //$mail->AddReplyTo = "support@thaicreate.com"; // Reply
      $mail->FromName = "noreply@monkeyeveryday.com";  // set from Name
      $mail->Subject = "[Action Required] Monkey Everyday Reset Password";
      $mail->Body = '<center>
        <h2> Monkey Everyday </h2>
        <h2> ลืมรหัสผ่าน </h2>
      </center>';
      $mail->AddAddress('chaitat7.t@gmail.com'); // to Address
      if(!$mail->send()) {
          echo 'false';
          //echo 'Mailer Error: ' . $mail->ErrorInfo;
          //echo "<script>console.log('".$mail->ErrorInfo."')</script>";
      } else {
          //echo 'ส่งข้อมูลการเปลี่ยนรหัสผ่านไปที่อีเมลที่ลงทะเบียนไว้เรียบร้อยแล้ว';
          echo 'true';
      }
  }

  public function ChangePassword()
  {
    $this->LogOut();
    $this->data["isLogout"] = true;
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $mobile = $this->input->get('val', TRUE);
    $this->data['mobile'] = urldecode(base64_decode(strtr($mobile, '-_', '+/')));
    // $mobileDecode = base64_encode($mobile);
    // $salt = "c937gfijvc87yasd9";
    // $key = "NazirAhmadHeratHost";
    // $cryptKey = $salt.$key;
    // $this->data['mobile'] = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $mobileDecode ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
    $this->data['meta_title'] = 'รีเซ็ตรหัสผ่าน';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_changePassword';
    $this->load->view('layouts/main', $this->data);
  }

  public function ChangePasswordProcess()
  {
    $mobile = $this->input->post('mobile');
    $password = $this->input->post('password');
    if($this->main_model->ChangePasswordProcess($mobile,$password)){
      echo "true";
    }else{
      echo "false";
    }
  }

  public function sendReport()
  {
    $rp_header = $this->input->post('rp_header');
    $rp_detail = $this->input->post('rp_detail');
    $rp_email = $this->input->post('rp_email');
    //$email = 'chaitat7.t@gmail.com';
    $recipients = Array('chaitat7.t@gmail.com','ben@pannomena.com');

    if($rp_header == 1){
        $rp_header = "ปัญหาการใช้งาน";
    }else if($rp_header == 2){
        $rp_header = "ข้อมูลไม่ถูกต้อง";
    }

    $config = array();
    $config['protocol'] = 'smtp';
    $config['smtp_host'] = 'ssl://smtp.gmail.com';
    $config['smtp_user'] = 'noreply@monkeyeveryday.com';
    $config['smtp_pass'] = 'Noreply@monkeyeveryday';
    $config['smtp_port'] = '465';
    //$config['smtp_crypto']  = 'ssl';
    //$config['starttls'] = TRUE;
    $config['charset']    = 'utf-8';
    $config['mailtype'] = 'html';
    $config['wordwrap'] = TRUE;
    //$config['validate'] = FALSE;

    $this->load->library('email');
    $this->email->initialize($config);
    $this->email->set_newline("\r\n");
    $this->email->from('noreply@monkeyeveryday.com');
    $this->email->to(implode(', ', $recipients));
    $this->email->subject('[Report] EveryDay website');
    $content = '<h2>Header: '.$rp_header.'</h2>';
    $content .= '<p>Detail: '.$rp_detail.'</p>';
    $content .= '<p>Email: '.$rp_email.'</p>';
    $this->email->message($content);

    if(!$this->email->send())
    {
        echo $this->email->print_debugger();
    }else{
        echo "Send Email successfully.";
    }
  }

  public function auth_google()
  {
    require_once APPPATH.'third_party/google-api-php/vendor/autoload.php';
    $auth_code = $this->input->get('code'); 
    $google_client = new Google_Client();
    $google_client->setClientId($this->config->item('google_client_id'));
    $google_client->setClientSecret($this->config->item('google_client_secret'));
    $google_client->setRedirectUri(base_url('Index/auth_google'));
    $google_client->addScope('email');
    $google_token = $google_client->fetchAccessTokenWithAuthCode($auth_code);

    if(!isset($google_token['error']))
    {
      $data = $google_client->verifyIdToken($google_token['id_token']);
      $userId = $data['sub'];
      // login
      if(!isset($_SESSION)){session_start();}
      $isLogin = false;
      $arrData = array(
        'socialid' => $userId
      );
      $ret = $this->main_model->curlRequest("POST", $this->config->item('api_url').'members/login-with-social',$arrData);
      if(isset($ret->roles[0])){
        $roles = strtolower($ret->roles[0]);
      }else{
        $roles = "";
      }
      if(isset($ret->token) && $roles != "admin"){
          $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$ret->token);
          $this->input->set_cookie(array('name' => 'name','value' => $myProfile->firstname,'expire' => (60*60*24*7),'secure' => false));
          $this->input->set_cookie(array('name' => 'profileImg','value' => $myProfile->profile_image,'expire' => (60*60*24*7),'secure' => false));
          $this->input->set_cookie(array('name' => 'token','value' => $ret->token,'expire' => (60*60*24*7),'secure' => false));
          $this->input->set_cookie(array('name' => 'subscriberType','value' => $ret->subscriberType,'expire' => (60*60*24*7),'secure' => false));
          $isLogin = true;
      }
        session_write_close();
      //end login
      if($isLogin){
        redirect('Library', 'refresh');
      }
    }
      redirect('Index/Signin' , 'refresh');
  }

  public function connect_google()
  {
    require_once APPPATH.'third_party/google-api-php/vendor/autoload.php';
    
    $token = get_cookie('token');
    $this->main_model->checkToken($token);

    $auth_code = $this->input->get('code'); 
    $google_client = new Google_Client();
    $google_client->setClientId($this->config->item('google_client_id'));
    $google_client->setClientSecret($this->config->item('google_client_secret'));
    $google_client->setRedirectUri(base_url('Index/connect_google'));
    $google_client->addScope('email');
    $google_token = $google_client->fetchAccessTokenWithAuthCode($auth_code);
    if(!isset($google_token['error']))
    {
      $data = $google_client->verifyIdToken($google_token['id_token']);
      $userId = $data['sub'];
      $request = array(
        'socialtype' => 'GG',
        'socialid' => $userId
      );
      $this->main_model->curlRequest("PATCH", $this->config->item('api_url').'member/connect-with-social', $request, $token);
    }

    redirect('MyProfile', 'refresh');
  }


  public function acceptCookie()
  {
    // $cookie = array(
    //     'name'   => 'EverydayAcceptCookie',
    //     'value'  => 'Accept',
    //     'expire' =>  (60*60*24*1),
    //     'secure' => false
    // );
    // $this->input->set_cookie($cookie);
    $this->input->set_cookie(array('name' => 'EverydayAcceptCookie','value' => 'Accept','expire' => (60*60*24*180),'secure' => false));
  }

  public function testEncode()
  {
    $this->data["language"] = 'TH';
    $this->data['mobile'] = '0882243432';
    $this->data['view_name'] = 'v_changePassword';
    $this->load->view('layouts/main', $this->data);
  }

}
