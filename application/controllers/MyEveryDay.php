<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyEveryDay extends PA_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model(array('librarylevel_model', 'librarysublevel_model', 'librarysheet_model', 'sheetvideo_model', 'subject_model', 'main_model', 'sheetfile_model', 'memberplan_model', 'membertest_model', 'planlibrary_model'));

    $token = get_cookie('token');
    $this->main_model->checkToken($token);
  }
  
  public function index()
  {
    $subject_colors = $this->config->item('subject_colors');
    $subject_arr = $this->subject_model->all_arr();

    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }

    $token = get_cookie('token');
    $this->main_model->checkToken($token);

    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);
    $this->membertest_model->check_billing_exists($myProfile);
    if($myProfile->roles['0'] == 'customer')
      $this->membertest_model->check_on_going_test($myProfile->id, $token);

    $subjects = $this->subject_model->all();
    //$user_plans = $this->memberplan_model->generateUserPlanMyEveryDay($token);

    // หาข้อมูล Current subject ของคนที่ Login
    $all_plans = $this->memberplan_model->get_all_plan_group_by_subjects($myProfile->id);

    $my_plans = [];
    foreach ($all_plans['current_plan'] as $plan)
      $my_plans[$plan['subject_id']] = $plan;

    if(count($all_plans['current_plan']) == 0) // ถ้าไม่มีแผนการเรียนเลยให้ redirect ไปที่ Plan/StartPlan'
      redirect('Plan/StartPlan');

    if($myProfile->subscriptionTypeId != 1)
    {
      if(!empty($this->input->get('subject')) && !empty($my_plans[$this->input->get('subject')]))
      {
        $subject_id = $this->input->get('subject');
        $this->data['my_plan'] = $my_plans[$subject_id];
        $this->data['current_subject'] = $my_plans[$subject_id]['subject_id'];
        $this->data['color_code'] = $my_plans[$subject_id]['color_code'];
        $this->data['current_plan_id'] = $my_plans[$subject_id]['plan_id'];
        $this->data['my_plan']['sheets'] = $this->find_sheet_tests($this->data['current_plan_id'], $this->data['current_subject']);
      }
      else
      {
        if(!empty($this->input->get('subject')))
        {
          $this->data['my_plan'] = [];
          $this->data['current_subject'] = $this->input->get('subject');
          $this->data['current_plan_id'] = null;
          $this->data['color_code'] = $this->db->get_where('Subject', ['id' => $this->input->get('subject')])->row()->color_code;
          $this->data['my_plan']['sheets'] = [];
        }
        else
        {
          if(!empty($my_plans))
          {
            $this->data['my_plan'] = current($my_plans);
            $this->data['current_subject'] = current($my_plans)['subject_id'];
            $this->data['color_code'] = current($my_plans)['color_code'];
            $this->data['current_plan_id'] = current($my_plans)['plan_id'];
            $this->data['my_plan']['sheets'] = $this->find_sheet_tests($this->data['current_plan_id'], $this->data['current_subject']);
          }
          else
          {
            $this->data['my_plan'] = [];
            $this->data['current_subject'] = null;
            $this->data['current_plan_id'] = null;
            $this->data['color_code'] = '#F6921E'; // สีส้ม
            $this->data['my_plan']['sheets'] = [];
          }
        }
      }


      $count_plan = 0;
      if($this->data['current_subject'] != null)
        $count_plan = $this->planlibrary_model->count_plan_library_by_subject($this->data['current_subject'])->subject;

      $this->data['count_plan'] = $count_plan;

      $progress = $this->sheetvideo_model->generate_progress_sheet($this->data['my_plan'], $myProfile);
      $this->data['progress_sheets'] = $progress['progress_sheets'];
      $this->data['progress_continue_sheets'] = $progress['progress_continue_sheets'];
      
      if(!empty($this->input->get('l')))
      {
        $recent_vdo = array(
          "level" => $this->input->get('l'),
          "sublevel" => $this->input->get('sl'),
          "sheet" => $this->input->get('sh')
        );
        $this->data['recent_vdo'] = $recent_vdo;
      }
      else{
        $this->data['recent_vdo'] = $progress['recent_vdo'];
      }
    }
    else
      redirect('Library', 'refresh');

    $this->data['myProfile'] = $myProfile;
    $this->data['subjects'] = $subjects;
    $this->data["language"] = $lang;
    $this->data['meta_title'] = 'วางแผนการเรียนรายบุคคล - Monkey Everyday';
    $this->data['meta_description'] = 'วางแผนการเรียนง่าย ๆ ได้ด้วยตัวเอง';
    $this->data["page_active"] = "myEveryDay";
    $this->data['view_name'] = 'v_myEveryDay';
    $this->data['inpage_script'] = '_myEveryDay_js';
    $this->data['external_scripts'] = array(
      base_url("resources/slick-master/slick/slick.min.js")
    );
    $this->load->view('layouts/main', $this->data);
  }

  /**
   * หาข้อมูล Sheet test
   *
   * @return void
   **/
  public function find_sheet_tests($plan_id, $subject_id)
  {
    $this->load->model('Plansubleveldata_model');

    $sub_levels = $this->Plansubleveldata_model->find_level_sublevel($plan_id);

    $sheets_n_testing = [];
    foreach ($sub_levels as $sub_level)
    {
      $sheets = $this->librarysheet_model->all_by_level_sublevel(null, $sub_level);
      if(!empty($sheets))
      {
        foreach ($sheets as &$sheet)
        {
          $sheet->plan_id = $plan_id;
          $sheet->is_test = 0;
          $sheets_n_testing[] = $sheet;
        }
      }
    }

    return $sheets_n_testing;
  }
  
  public function addPlan()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $this->data["page_active"] = "myEveryDay";
    $this->data['meta_title'] = '';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_myEveryDay_addPlan';
    $this->load->view('layouts/main', $this->data);
  }
  
  public function Video($id = null)
  {
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }

    $api_url = $this->config->item('api_url');
    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token);

    $sheet = $this->librarysheet_model->find_by_sheet_id($id); //หาข้อมูล view ของตัวเอง
    $sheet_file = $this->sheetfile_model->find_by_libsheet_id($id);
    $sublevel_id = $sheet->librarySubLevelId;
    $sheets = $this->librarysheet_model->all_sheet_by_sublevel_id($sublevel_id); //หา sheet ที่มี $sublevel_id เดียวกัน
    // $vdos = $this->sheetvideo_model->find_vdo_by_sheet_id($id); //หา vdo ที่มี sheetId เดียวกัน
    $vdos = $this->sheetvideo_model->find_vdo_by_sheet_id_with_progress($id, $myProfile->id); //หา vdo ที่มี sheetId เดียวกัน

    $ar = array();
    $maxNo = $sheets->num_rows();
    $n = 1;
    $prev = "";
    $next = "";
    foreach ($sheets->result() as $row) {
      $ar[$n] = $row->id;
      if($row->id == $id)
        {$currentNo = $n;}
      $n++;
    }
    if($currentNo !=1)
      $prev = "<a href='".base_url()."MyEveryDay/Video/".$ar[$currentNo-1]."'><img src='".base_url()."resources/img/backArrow.png' class='img-arrow'><p class='m-400-hide' style='color: #fff;'>ชีทก่อนหน้า</p></a>";
    if($currentNo < $maxNo)
      $next = "<a href='".base_url()."MyEveryDay/Video/".$ar[$currentNo+1]."'><img src='".base_url()."resources/img/nextArrow.png' class='img-arrow'><p class='m-400-hide' style='color: #fff;'>ชีทถัดไป</p></a>";

    $this->data['api_url'] = $api_url;
    $this->data['myProfile'] = $myProfile;
    $this->data['sheet'] = $sheet;
    $this->data['sheet_file'] = $sheet_file;
    $this->data['vdos'] = $vdos;
    $this->data["language"] = $lang;
    $this->data['prev'] = $prev;
    $this->data['next'] = $next;
    $this->data['id'] = $id;
    $this->data['token'] = get_cookie('token');
    $this->data["page"] = "MyEveryDay";
    $this->data["page_active"] = "myEveryDay";
    if($myProfile->subscriptionTypeId == 1){
      redirect('Library', 'refresh');
    }else{
      $this->data['view_name'] = 'v_video';
    }

    $this->data['meta_title'] = 'Video บทเรียน';
    $this->data['meta_description'] = '';
    $this->load->view('layouts/main', $this->data);
  }
  
  public function SheetTest()
  {
    $lang = get_cookie('lang');
    if($lang == null){ $lang = "en"; }
    $this->data["language"] = $lang;
    $this->data["page_active"] = "myEveryDay";
    $this->data['meta_title'] = 'แบบทดสอบ';
    $this->data['meta_description'] = '';
    $this->data['view_name'] = 'v_video_sheetTest';
    $this->load->view('layouts/main', $this->data);
  }

  public function getLikeHTML(){
    $memberId = $this->input->post('memberId');
    $sheetVideoId = $this->input->post('sheetVideoId');
    $dataByMemberId = $this->sheetvideo_model->getVideoLikeByMemberId($memberId,$sheetVideoId);
    $dataLikeAll = $this->sheetvideo_model->getVideoLikeAll($sheetVideoId);
    $dataDislikeAll = $this->sheetvideo_model->getVideoDislikeAll($sheetVideoId);
    if($dataByMemberId->num_rows() > 0){
      if($dataByMemberId->result()[0]->action == 1){
        $imgLike = base_url().'resources/img/like_active.png';
      }else{
        $imgLike = base_url().'resources/img/like.png';
      }
      if($dataByMemberId->result()[0]->action == 0){
        $imgDislike = base_url().'resources/img/unlike_active.png';
      }else{
        $imgDislike = base_url().'resources/img/unlike.png';
      }
    }else{
      $imgLike = base_url().'resources/img/like.png';
      $imgDislike = base_url().'resources/img/unlike.png';
    }

    $html = '<div class="div-like-btn my-2">
      <img class="vid-like-btn" onclick="clickLike(\''.$sheetVideoId.'\',\'1\',\'\')" src="'.$imgLike.'"/>
      <span class="totalLike mr-3">ถูกใจ '.$dataLikeAll.'</span>
    </div>
    <div class="div-like-btn my-2">
      <img class="vid-unlike-btn" onclick="clickLike(\''.$sheetVideoId.'\',\'0\',\'\')" src="'.$imgDislike.'"/>
      <span class="totalUnlike">ไม่ถูกใจ '.$dataDislikeAll.'</span>
    <div>';
    $data = array(
      'ret' => true
      ,'html' => $html
    );
    echo json_encode($data);
  }

  public function getVideoView(){
    $sheetVideoId = $this->input->post('sheetVideoId');
    $view = $this->sheetvideo_model->getVideoView($sheetVideoId);
    if($view >= 1000){
      $view = ($view/1000).' K';
    }
    $html = '<i class="fa fa-eye" aria-hidden="true"></i> '.$view;
    $data = array(
      'ret' => true
      ,'html' => $html
    );
    echo json_encode($data);
  }

  public function UpdateProgress()
  {
    $list_progress = $this->input->post('list_progress');
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    
    foreach ($list_progress as $progress) {
      $request = array(
        'memberId' => intval($progress['profile_id']),
        'progress' => intval($progress['progress']),
        'duration' => intval($progress['currentTime']),
        'sheetVideoId' => intval($progress['vdo_id']),
        'complete' => intval($progress['progress']) >= 90
      );
      $this->main_model->curlRequest("PATCH", $this->config->item('api_url').'front-end/progress-sheet-video/'.$progress['vdo_id'], $request, $token);

    }
  }
  
  public function UpdateView()
  {
    $vdo_id = $this->input->post('vdo_id');
    $token = get_cookie('token');
    $this->main_model->checkToken($token);
    $this->main_model->curlRequest("PATCH", $this->config->item('api_url').'front-end/video-view/'.$vdo_id, null, $token);
  }
}

