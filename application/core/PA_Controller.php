<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PA_Controller extends CI_Controller {

  protected $current_user = null;
  protected $current_language = null;

  function __construct()
  {
    parent::__construct();
    //เตรียม ข้อความสำหรับ alert ที่มากับ session
    $message = $this->session->flashdata('message');
    $error = $this->session->flashdata('error');
    $warning = $this->session->flashdata('warning');
    $info = $this->session->flashdata('info');
    
    $this->load->vars('message', $message);
    $this->load->vars('error', $error);
    $this->load->vars('warning', $warning);
    $this->load->vars('info', $info);
    $this->load->vars('body_class', "");
    $this->load->vars('active_menu', '');
     
    //ถ้า login แล้วให้โหลดตัวแปร current user ด้วย
    if($this->ion_auth->logged_in())
    {
      $user = $this->ion_auth->user()->row();

      if(! $this->_is_authorized($user) && ! $this->_is_admin()) //เช็คว่าเข้าหน้านั้นๆ ได้หรือไม่ ถ้าเป็น admin ไม่ต้องเช็ค
        show_error("Unauthorized Access to '{$this->router->directory}{$this->router->class}/{$this->router->method}' for user '{$user->username}'", 401); //ถ้าเข้าไม่ได้แสดง http 401

      $this->current_user = $this->user_model->find($user->id);
      $this->load->vars('current_user', $this->current_user);
    }
  }
  
  protected function _create_pagination($results_count, $current_url = '')
  { 
    $config['base_url'] = base_url($current_url);
    $config['total_rows'] = $results_count;
    $config['per_page'] = $this->config->item('items_per_page');
    $config['attributes'] = array('class' => 'page-link');
    $config['page_query_string'] = TRUE;
    $config['query_string_segment'] = 'page';
    $config['use_page_numbers'] = TRUE;
    $config['reuse_query_string'] = TRUE;
    $config['first_link'] = FALSE;
    $config['last_link'] = FALSE;
    $config['next_link'] = 'ถัดไป';
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = 'ก่อนหน้า';
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close'] = '</span></li>';
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);
    
    return $this->pagination->create_links();
  }

  /**
   * สร้างไฟล์ csv
   *
   * @param string $filename
   * @param string $query
   * @return void
   */
  protected function _export_csv($filename, $query)
  {
    $this->load->helper('download');
    $csv_data = $this->dbutil->csv_from_result($query);
    
    force_download($filename, "\xEF\xBB\xBF" . $csv_data);
  }

  protected function _get_csrf_nonce()
  {
    $this->load->helper('string');
    $key   = random_string('alnum', 8);
    $value = random_string('alnum', 20);
    $this->session->set_flashdata('csrfkey', $key);
    $this->session->set_flashdata('csrfvalue', $value);

    return array($key => $value);
  }
  
  /**
   * เอาให้พวก method ที่ต้องเช็คตัวแปรก่อนจะทำงานไว้ใช้
   *
   * @param int $id ของ row ในตาราง db
   * @param string $class_name ชื่อ class ที่จะเช็ค 
   * @return instance ของ class_name ที่ให้มา
   */
  protected function _get_instance($id = null, $model_name = null, $is_api = false)
  {
    if(!$id || !$model_name)
    {
      if($is_api)
        return 'false';
      else
        show_error('Invalid Request', 400);
    }
    
    $instance = $this->$model_name->find($id);
    
    if(!$instance)
    {
      if($is_api)
        return 'false';
      else
        show_error('Instance of '.$model_name.' not found', 400);
    }    
    
    return $instance;
  }
  
  /**
   * เอาให้พวก method ที่ต้องเช็คตัวแปรก่อนจะทำงานไว้ใช้
   *
   * @param int $news_id ของ row ในตาราง db
   * @param string $class_name ชื่อ class ที่จะเช็ค 
   * @return instance ของ class_name ที่ให้มา
   */
  protected function _get_instance_new($news_id = null, $model_name = null, $is_api = false)
  {
    if(!$news_id || !$model_name)
    {
      if($is_api)
        return 'false';
      else
        show_error('Invalid Request', 400);
    }
    
    $instance = $this->$model_name->find_new($news_id);
    
    if(!$instance)
    {
      if($is_api)
        return 'false';
      else
        show_error('Instance of '.$model_name.' not found', 400);
    }    
    
    return $instance;
  }
  /**
   * echo json message โดยมีหัวเป็น json
   *
   * @param string $json_message 
   * @return void
   */
  protected function _print_json($json_message)
  {
    header("Access-Control-Allow-Origin: *"); // allow api to access via multiple origin
    header("Access-Control-Allow-Methods: GET, POST");
    header("Access-Control-Allow-Headers: Content-Type");
    header('Content-Type: application/json; charset=utf-8'); // always return json for api
    die($json_message);
  }
  
  protected function _is_admin()
  {
    return $this->ion_auth->is_admin();
  }
  
  
  protected function _is_group($group_name)
  {
    return $this->ion_auth->in_group($group_name);
  }
  
  
  protected function _require_admin()
  {
    // if not login, return to login page
    if (!$this->_require_login() || !$this->ion_auth->is_admin())
    {
      $this->session->set_flashdata('error', 'ขออภัย คุณไม่มีสิทธิ์เข้าใช้งานส่วนนี้');
      redirect(base_url(), 'refresh');
    }
  }
  
  
  protected function _require_group($group_name)
  {
    // if not login, return to login page
    if(!$this->_require_login() || !$this->ion_auth->in_group($group_name))
    {
      $this->session->set_flashdata('error', 'คุณไม่มีสิทธิในการเข้าถึงหน้านี้');
      redirect(base_url(), 'refresh');
    }
  }

  protected function _require_login()
  {
    // if not login, return to login page
    if (!$this->ion_auth->logged_in())
    {
      // prepare requested url.
      $requested_url = $_SERVER["REQUEST_URI"];
      
      $this->session->set_flashdata('error', 'ขออภัย คุณต้องเข้าสู่ระบบก่อน');
      // redirect('login', 'refresh');
      redirect('login?redirect_to='.rawurlencode($requested_url));
    }
    
    return true;
  }
  
  /**
   * send email to notify user
   *
   * @param string $to email
   * @param string $subject mail title
   * @param string $body email body. can load view inside e.g. $this->load->view('email_templates/notify_user_password_changed', array('car'=>$this), TRUE)
   * @param string $from sender email, if not specify use default from 'site_title' in config/ion_auth.php
   */
  protected function _send_mail($to = null, $subject = 'ไม่มีหัวข้อ', $body = 'ไม่มีเนื้อหา', $from = null)
  {
    $from = ($from) ? $from : $this->config->item('noreply_email', 'ion_auth');
  
    $this->load->library('email');
    $this->load->helper('email');
  
    // if either $from or $to are not valid email
    if (!valid_email($to) && !valid_email($from)) return; 
  
    // send an email
    $this->email->set_newline("\r\n"); //ขึ้นบันทัดใหม่
    $this->email->from($from, $this->config->item('site_title', 'ion_auth'));
    $this->email->to($to);
    $this->email->subject($subject);
    $this->email->message($body);

    if($this->email->send())
    {
      return true;
    }
    else
    {
      // echo $this->email->print_debugger();
      return false;
    }
  }

  protected function _valid_csrf_nonce()
  {
    $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
    if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue'))
      return TRUE;
    else
      return FALSE;
  }

  /**
   * เช็คดูว่า user ที่ login เข้าหน้าที่ขอมาได้หรือเปล่า
   *
   * @param string $user 
   * @return true ถ้่าเข้่าได้ และ false ถ้่าเข้าไม่ได้
   */
  private function _is_authorized($user = null)
  {
    if(! $user) return false; //ไม่ได้ให้ user มา
    
    $privilege_name = "{$this->router->directory}{$this->router->class}/{$this->router->method}";
    $allowed_groups = $this->config->item(str_replace('/', ':', $privilege_name));
    
    return $this->ion_auth->in_group($allowed_groups);
  }
}
