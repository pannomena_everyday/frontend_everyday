<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PA_Model extends CI_Model {
  
  protected $is_master_data = false;
  protected $instance_class_name = ''; // ถ้าระบุ จะเรียกใช้ class name นี้แทน instance convention
  
  function __construct()
  {
    parent::__construct();
  }

  public function __call($fn_name, $args)
  {
    // allow overloading all method from BH_Model
    if ($fn_name == 'all')
      return call_user_func_array(array($this, '_all'), $args);
    else if (method_exists($this, $fn_name))
      return call_user_func_array(array($this, $fn_name), $args);
  }
  
  /**
   * หา record ทั้งหมด
   *
   * @param string $limit จำนวน record ที่จะ select ออกมา มี default คือ 50 
   * @return array ของ result ใน database
   */
  public function _all($limit = 100)
  {
    $this->db->reset_query();
    $user = $this->load->get_var('current_user');
    
    $class_name = $this->instance_class_name ? $this->instance_class_name : $this->_str_lreplace('_model', '', get_called_class());
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', get_called_class())));
    $query = $this->db->get($table_name, $limit, 0);
    
    // return $query->result($this->_str_lreplace('_model', '', $class_name));
    return $query->result($class_name);
  }
  
  
  /**
   * ใส่ข้อมูลลงใน database
   *
   * @param key-value array $data เช่น array('name' => $name, 'email' => $email, 'url' => $url) โดย key คือชื่อของ column
   * @return true ถ้าสำเร็จ และ false ถ้าไม่สำเร็จ
   */
  public function create($data)
  {
    $this->db->reset_query();
    
    $data['createdtime'] = date('Y-m-d H:i:s'); 
    $data['updatedtime'] = date('Y-m-d H:i:s');
    
    $class_name = get_called_class();
    $table_name = strtolower($this->_str_lreplace('_model', '', $class_name));
    
    if($this->db->insert($table_name, $data))
      return $this->db->insert_id();
    
    //update master data version if it is
    if($this->is_master_data) 
      $this->db->update('master_data_versions', array('version' => $data['updatedtime']), "table_name = '{$table_name}'");
  
    return false;
  }
  
  
  /**
   * สร้าง dropdown array
   *
   * @param string $value_col 
   * @param string $text_col 
   * @return array key value สำหรับเอาไปใส่ใน form_dropdown helper
   */
  public function dropdown_array()
  {
    $this->db->reset_query();
    
    if (func_num_args())
    {
      $conditions = func_get_arg(0);
      $limit = isset($conditions['limit']) ? $conditions['limit'] : 100;
      $order_by_text = isset($conditions['order_by_text']) ? $conditions['order_by_text'] : false;
      $value_col = isset($conditions['value_col']) ? $conditions['value_col'] : 'id';
      $text_col = isset($conditions['text_col']) ? $conditions['text_col'] : 'name';
      $placeholder = isset($conditions['placeholder']) ? $conditions['placeholder'] : 'Please Select';
    }
    else
    { // no argument
      $limit = 100;
      $order_by_text = false;
      $value_col = 'id';
      $text_col = 'name';
      $placeholder = 'Please Select';
    }
    
    $class_name = get_called_class();
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', $class_name)));
    $fields = $this->db->field_data($table_name);
    
    $this->db->select($value_col.','.$text_col);
    
    if($this->db->field_exists('weight', $table_name))
      $this->db->order_by('weight', 'ASC');
    
    if($order_by_text)
      $this->db->order_by('CONVERT ('.$text_col.' USING tis620)', 'ASC');
    
    $query = $this->db->get($table_name, $limit, 0);
    
    $options = array();
    $options[''] = $placeholder; 
    foreach ($query->result($class_name) as $row)
      $options[$row->$value_col] = $row->$text_col;
      
    return $options;
  }
  
  
  /**
   * ลบข้อมูลลงใน database
   *
   * @param $id ที่จะลบ
   * @return true ถ้าสำเร็จ และ false ถ้าไม่สำเร็จ
   */
  public function delete($id, $master_data = false)
  {
    $this->db->reset_query();
    $class_name = get_called_class();
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', $class_name)));
    return $this->db->delete($table_name, array('id' => $id));
  }
  
  /**
   * ลบข้อมูลลงใน database
   *
   * @param $id ที่จะลบ
   * @return true ถ้าสำเร็จ และ false ถ้าไม่สำเร็จ
   */
  public function delete_new($news_id, $master_data = false)
  {
    $this->db->reset_query();
    $class_name = get_called_class();
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', $class_name)));
    return $this->db->delete($table_name, array('news_id' => $news_id));
  }
  
  /**
   * หา 1 record จาก id ที่กำหนด
   *
   * @param mixins string $id หรือ array('conditions' => %WHERE Statement%) %WHERE Statement% เป็น string SQL WHERE
   * @return record ที่ค้นหาหรือ
   */
  public function find()
  {
    $this->db->reset_query();
    $class_name = $this->instance_class_name ? $this->instance_class_name : $this->_str_lreplace('_model', '', get_called_class());
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', get_called_class())));
    
    $query_input = func_get_arg(0);
    
    if(is_array($query_input))
    {
      $this->db->where($query_input['conditions']);
      $query = $this->db->get($table_name, 1, 0);
    }
    else
      $query = $this->db->get_where($table_name, array('id' => $query_input), 1, 0);

    return $query->row(0, $class_name);
  }

  /**
   * หา 1 record จาก company_id ที่กำหนด
   *
   * @param mixins string $id หรือ array('conditions' => %WHERE Statement%) %WHERE Statement% เป็น string SQL WHERE
   * @return record ที่ค้นหาหรือ
   */
  public function find_by_company()
  {
    $this->db->reset_query();
    $class_name = $this->instance_class_name ? $this->instance_class_name : $this->_str_lreplace('_model', '', get_called_class());
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', get_called_class())));
    
    $query_input = func_get_arg(0);
    
    if(is_array($query_input))
    {
      $this->db->where($query_input['conditions']);
      $query = $this->db->get($table_name, 1, 0);
    }
    else
      $query = $this->db->get_where($table_name, array('company_id' => $query_input), 1, 0);

    return $query->row(0, $class_name);
  }
  
  /**
   * หา 1 record จาก id ที่กำหนด ใช้เฉพาะ news เท่านั้น
   *
   * @param mixins string $id หรือ array('conditions' => %WHERE Statement%) %WHERE Statement% เป็น string SQL WHERE
   * @return record ที่ค้นหาหรือ
   */
  public function find_new()
  {
    $this->db->reset_query();
    $class_name = $this->instance_class_name ? $this->instance_class_name : $this->_str_lreplace('_model', '', get_called_class());
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', get_called_class())));
    
    $query_input = func_get_arg(0);
    
    if(is_array($query_input))
    {
      $this->db->where($query_input['conditions']);
      $query = $this->db->get($table_name, 1, 0);
    }
    else
      $query = $this->db->get_where($table_name, array('news_id' => $query_input), 1, 0);

    return $query->row(0, $class_name);
  }

  /**
   * หา record จาก column ที่กำหนด
   *
   * @param string $column_name ชื่อ column  
   * @param any $column_value value ที่ต้องการหา
   * @return record ที่ค้นหาหรือ
   */
  public function find_by_column($column_name, $column_value)
  {
    $this->db->reset_query();
    // $class_name = get_called_class();
    $class_name = $this->instance_class_name ? $this->instance_class_name : $this->_str_lreplace('_model', '', get_called_class());
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', get_called_class())));
    $query = $this->db->get_where($table_name, array($column_name => $column_value), 1, 0);
    return $query->row(0, $class_name);
  }
  
  public function get_instance_class_name()
  {
    $class_name = $this->instance_class_name ? $this->instance_class_name : $this->_str_lreplace('_model', '', get_called_class());
        
    return $class_name;
  }

  /**
   * เอาออกมาเป็น enum
   *
   * @return void
   */
  public function get_result_array($limit = 100)
  {
    $results = $this->all($limit);
    
    $result_array = array();
    
    foreach($results as $result) 
      $result_array[$result->id] = $result->name;
    
    return $result_array;
  }
  
  
  /**
   * หา records จาก where ที่กำหนด เอาไว้ทำ function search
   *
   * @param array $conditions มี $like_array เป็น key val เช่น array('title' => $match, 'page1' => $match, 'page2' => $match), order_by ของ sql, offset, และ limit
   * @return records ที่ค้นหา
   */
  public function search()
  {
    $this->db->reset_query();
    
    if (func_num_args())
    {
      $conditions = func_get_arg(0);
      $like = isset($conditions['like']) ? $conditions['like'] : null;
      $where = isset($conditions['where']) ? $conditions['where'] : null;
      $where_in = isset($conditions['where_in']) ? $conditions['where_in'] : null;
      $joins = isset($conditions['joins']) ? $conditions['joins'] : null;
      $order_by = isset($conditions['order_by']) ? $conditions['order_by'] : null;
      $offset = isset($conditions['offset']) ? $conditions['offset'] : 0;
      $limit = isset($conditions['limit']) ? $conditions['limit'] : 100;
    }
    else
    { // no argument
      $like = null;
      $order_by = null;
      $offset = 0;
      $limit = 100;
      $joins = null;
      $where = null;
      $where_in = null;
    }
    
    // $class_name = get_called_class();
    $class_name = $this->instance_class_name ? $this->instance_class_name : $this->_str_lreplace('_model', '', get_called_class());
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', get_called_class())));
    
    if($this->db->field_exists('created_by', $table_name))
      $this->db->select("*, {$table_name}.id AS id, {$table_name}.createdtime AS createdtime, {$table_name}.created_by AS created_by, {$table_name}.updatedtime AS updatedtime, {$table_name}.updated_by AS updated_by");
    else
      $this->db->select("*, {$table_name}.id AS id, {$table_name}.createdtime AS createdtime, {$table_name}.updatedtime AS updatedtime");
    
    if ($joins)
      foreach ($joins as $key=> $item)
        $this->db->join($key, $joins[$key]);

    if ($like)
    {
      $search_likes = array();
      foreach($like as $key => $value)
      {
        // ignore empty query
        if (empty($value) && $value !== '0') continue;

        $combined_keys = explode("-", $key);
        if(count($combined_keys) > 1)
          $key = implode(".", $combined_keys);
        
        $search_likes[$key] = $value;
      }
      $this->db->like($search_likes);
    }

    if ($where)
      foreach ($where as $key => $value)
        $this->db->where($key, $value);
    
    if ($where_in)
      foreach ($where_in as $key => $value)
        $this->db->where_in($key, $value);
    
    if ($order_by)
      $this->db->order_by($order_by);

    $this->db->limit($limit);
    $this->db->offset($offset);
    $query = $this->db->get($table_name, $limit, $offset);
    
    return $query->result($class_name);
  }
  
  /**
   * หาจำนวน record ที่ match กับ like
   *
   * @return จำนวน record ที่ match
   */
  public function search_count()
  {
    $this->db->reset_query();
    
    if (func_num_args())
    {
      $conditions = func_get_arg(0);
      $like = isset($conditions['like']) ? $conditions['like'] : null;
      $where = isset($conditions['where']) ? $conditions['where'] : null;
      $joins = isset($conditions['joins']) ? $conditions['joins'] : null;
    }
    else
    { // no argument
      $like = null;
    }
    
    $class_name = get_called_class();
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', $class_name)));
    
    if ($joins)
      foreach ($joins as $key=> $item)
        $this->db->join($key, $joins[$key]);

    if($like)
    {
      $search_likes = array();
      foreach($like as $key => $value)
      {
        // ignore empty query
        if (empty($value) && $value !== '0') continue;

        $combined_keys = explode("-", $key);
        if(count($combined_keys) > 1)
          $key = implode(".", $combined_keys);
        
        $search_likes[$key] = $value;
      }
      $this->db->like($search_likes);
    }
    
    if ($where)
      foreach ($where as $key => $value)
        $this->db->where($key, $where[$key]);
    
    if($like || $joins || $where)
      return $this->db->count_all_results($table_name);
    
    return $this->db->count_all($table_name);
  }
  
  
  /**
   * update ข้อมูลในฐานข้อมูล
   *
   * @param key-value array $data เช่น array('name' => $name, 'email' => $email, 'url' => $url) โดย key คือชื่อของ column
   * @param string $where เช่น "author_id = 1 AND status = 'active'"
   */
  public function update($data, $where)
  {
    $this->db->reset_query();
    
    $user = $this->ion_auth->user()->row();
    
    $class_name = get_called_class();
    $table_name = strtolower(plural($this->_str_lreplace('_model', '', $class_name)));
    
    //update master data version if it is
    if($this->is_master_data) 
      $this->db->update('master_data_versions', array('version' => $data['updatedtime']), "table_name = '{$table_name}'");
    
    return $this->db->update($table_name, $data, $where);
  }
  
  
  private function _str_lreplace($search, $replace, $subject)
  {
    $this->db->reset_query();
    
    $pos = strrpos($subject, $search);

    if($pos !== false) 
      $subject = substr_replace($subject, $replace, $pos, strlen($search));

    return $subject;
  }
}


class PA_Model_Object extends CI_Model {
  protected $table_name = ''; // ถ้าระบุ จะเรียกใช้ table name นี้แทน convention
  
  function __construct()
  {
    parent::__construct();
    
    $class_name = get_called_class();
    $table_name = $this->table_name ? $this->table_name : strtolower(plural($this->_str_lreplace('_model', '', $class_name)));
    $fields = $this->db->field_data($table_name);
    
    foreach ($fields as $field)
    {
      $field_name = $field->name;
      
      if(! isset($this->$field_name))
        if(is_null($field->default))
          $this->$field_name = null;
        else
          $this->$field_name = $field->default;
    }
  }
  
  public function get_name()
  {
    return $this->name;
  }
  
  
  private function _str_lreplace($search, $replace, $subject)
  {
    $pos = strrpos($subject, $search);

    if($pos !== false) 
      $subject = substr_replace($subject, $replace, $pos, strlen($search));

    return $subject;
  }

    /**
   * check if file existed by backoffice and then give back url
   *
   * @param string $path relative path to backend respected to uploads folder
   * @param boolen $get_base64string flag for getting base64string instead of url
   * @return string $url of this file. Otherwise NULL is returned
   **/
  protected function _check_file($path, $get_base64string = false) {
    // if(!file_exists('coupons/'.$lp_coupon_id.'/icon.jpg'))
    $file_path = FCPATH.'uploads/'.$path;
    $file_url = '/uploads/'.$path;

    if ( !file_exists($file_path) ) return NULL;

    if ($get_base64string)
    {

      $img_bin = fread(fopen($file_path, "r"), filesize($file_path));
      return base64_encode($img_bin);
    }

    return $file_url;
  }
  
}
