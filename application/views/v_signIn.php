<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<div id="wrap">
  <div id="container" class="bg-gray-f7">
    <div class="containner-padding">
      <div class="box-800">
        <div class="center bg-white f26 f-w-500">เข้าสู่ระบบ</div>
        <div class="input-group input-focus form-signIn">
          <div class="input-group-prepend">
            <span class="input-group-text bg-white"><i class="fa fa-user"></i></span>
          </div>
          <input type="text" id="mobile" value="<?=$rememberMobile;?>" placeholder="อีเมล หรือเบอร์โทรศัพท์ " class="form-control border-left-0 f18">
        </div>
        <div class="input-group input-focus form-signIn">
          <div class="input-group-prepend">
            <span class="input-group-text bg-white"><i class="fa fa-lock"></i></span>
          </div>
          <input type="password" id="password" placeholder="รหัสผ่าน" class="form-control border-left-0 f18">
          <div class="input-group-prepend toggle-password" data-id="password">
            <span class="input-group-text bg-white f-ccc"><i class="fa fa-eye-slash"></i></span>
          </div>
        </div>
        <div class="form-check form-signIn f18 pl-0">
          <div class="div-remember">
            <input type="checkbox" class="form-check-input" id="remember" <?php if($rememberMobile != ""){ echo "checked";}?>>
            <label class="form-check-label" for="remember"> จดจำฉันในระบบ</label>
          </div>
          <div class="div-forgotPassword"><a href="<?=base_url()."Index/ForgotPassword"?>">ลืมรหัสผ่าน</a></div>
          <!-- <div class="right fgtPass">ลืมรหัสผ่าน</div> -->
        </div>
        <button class="signInBtn btn1 btn-black">เข้าสู่ระบบ</button>
        <div class="center line-middle"><span>หรือ</span></div>
        <!-- <a href="#" onclick="facebookLogin();"><button class="signInBtn facebook"><img src="<?=base_url('resources/img/facebook.png')?>" class="icon-social"> เข้าสู่ระบบด้วยบัญชี Facebook</button></a> -->
        <a class="d-none" href="<?= $google_client->createAuthUrl()?>"><button class="signInBtn google"><img src="<?=base_url('resources/img/google.png')?>" class="icon-social"> เข้าสู่ระบบด้วยบัญชี Google</button></a>
        <a href="<?=base_url()."Index/SignUp"?>"><button class="signInBtn signUp btn-white">ลงทะเบียน</button></a>
      </div>

    </div> <!-- //.containner-padding -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js"></script>
<!-- <script src="facebookLogin.js"></script> -->

<script>
  $('.fgtPass').click(function(){
    alert('กรุณาติดต่อทีมงานผ่านระบบ myChat (รูปหน้าลิงมุมล่างขวาของจอ)');
  });

  $(".toggle-password").click(function() {
    $(this).find('i').toggleClass("fa-eye fa-eye-slash");
    var id = $(this).data("id");
    var input = $("#"+id);
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });

  $('.signInBtn.btn1').click(function(){
  	var mobile = $.trim($('#mobile').val());
    var password = $.trim($('#password').val());
    var remember = 0;
    if($('#remember').is(':checked')){
      remember = 1;
    }
    if(mobile == ''){
      alert('กรุณากรอกอีเมล หรือ เบอร์โทรศัพท์ ที่ถูกต้อง');
    }else if(password == ''){
      alert('กรุณากรอกรหัสผ่าน');
    }else{
      $.ajax({
        url: "<?=base_url()."Index/LogIn"?>",
        type: "post",
        data: {mobile:mobile,password:password,remember:remember} ,
        success: function (data) {
            if(data == 'success'){
              window.location.href = '<?=base_url("Library");?>';
            }else{
              //alert(data);
              alert('กรุณากรอกอีเมล หรือ เบอร์โทรศัพท์และรหัสผ่านที่ถูกต้อง');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
      });
    }
  });

  $('#password').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      var mobile = $.trim($('#mobile').val());
      var password = $.trim($('#password').val());
      var remember = 0;
      if($('#remember').is(':checked')){
        remember = 1;
      }
      if(mobile == ''){
        alert('กรุณากรอกอีเมล หรือ เบอร์โทรศัพท์ ที่ถูกต้อง');
      }else if(password == ''){
        alert('กรุณากรอกรหัสผ่าน');
      }else{
        $.ajax({
          url: "<?=base_url()."Index/LogIn"?>",
          type: "post",
          data: {mobile:mobile,password:password,remember:remember} ,
          success: function (data) {
              if(data == 'success'){
                window.location.href = '<?=base_url("Library");?>';
              }else{
                //alert(data);
                alert('กรุณากรอกอีเมล หรือ เบอร์โทรศัพท์และรหัสผ่านที่ถูกต้อง');
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
          }
        });
      }
    }
  });

  $(document).ready(function(e) {
     var APP_ID = "634439817220906";
     window.fbAsyncInit = function() {
      FB.init({
        appId      : APP_ID,
        xfbml      : true,
        version    : 'v8.0'
      });
      };
  });

  function facebookLogin(){
    FB.login(function(response) {
      var fbuid = response.authResponse.userID; // ไอดีเฟสบุ้ค
      if (response.authResponse) {
       FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
        console.log(JSON.stringify(response));
         var name = response.name; // ชื่อและนามสกุล
         var email = response.email; // อีเมล
       });
      }else{
      }
    }, {scope:'email'}); // รายการ permission ที่เว็บไซต์เราร้องขอข้อมูล
  }

  (function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
