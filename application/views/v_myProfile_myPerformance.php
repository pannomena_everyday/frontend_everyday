<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">

<div id="wrap">
  <div id="container">
    <div class="myProfileHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="myProfile-slide slider">
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile"?>"><img src="<?=base_url()."resources/img/account.png"?>" class="icon-subject"/>บัญชีผู้ใช้ </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyPlan"?>"><img src="<?=base_url()."resources/img/myplan.png"?>" class="icon-subject"/>แผนการเรียน </a> 
            </div>
            <div class="myProfileBox active">
              <img src="<?=base_url()."resources/img/performance_active.png"?>" class="icon-subject"/><span>ติดตามผลงาน</span>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyNote"?>"><img src="<?=base_url()."resources/img/sheet.png"?>" class="icon-subject"/>คลัง Sheet </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyLog"?>"><img src="<?=base_url()."resources/img/loghistory.png"?>" class="icon-subject"/>ประวัติการเรียน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyMembership"?>"><img src="<?=base_url()."resources/img/membership.png"?>" class="icon-subject"/>สถานะสมาชิก </a>
            </div>
          </section>
        </div>
      </div>
    </div>

    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <?php $this->load->view('i_myProfileStatus'); ?>

        <section class="myProfile-form">

          <!-- <div class="myPer-div">
            <div class="row f20 m-b-10">
              <div class="col-md-9 f-22 bold">
                แบบทดสอบก่อนเรียน
              </div>
              <div class="col-md-3 t-right t-left-575 blue">
                Total 50
              </div>
            </div>
            <div class='prog-bar-myPer'>
              <div class="prog-bar">
                <div class="background" style=" -webkit-clip-path: inset(0 <?=(100-70)?>% 0 0); clip-path: inset(0 <?=(100-70)?>% 0 0);">
                </div>
              </div>
            </div>
            <div class="prog-text f18">30%</div>
          </div>  -->
          <!-- //.myPer-div -->
          <?=$dataPerformance?>

        </section>
      </div>
    </div> <!-- //.containner-padding -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script>
  if($( window ).width() < 480){
    $(".myProfile-slide").slick('slickGoTo', 2);
  }
  $( "#profile" ).click(function() {
    $('#profileBox').toggle('slow');
  });
</script>