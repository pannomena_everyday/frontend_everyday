<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<link rel="stylesheet" href="<?=base_url('resources/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css');?>">
<script type="module" src="<?=base_url();?>resources/js/everyday-player.bundled.js"></script>

<link rel="stylesheet" koko="in" href="<?=base_url();?>resources/css/tutorial.min.css">
<style>
  #step-4 .btn-next {
    /*---- hide step end ------*/
      display: none;
  }
  #step-4 .popover-navigation {
      -ms-flex-pack: center !important;
      justify-content: center !important;
  }
  .step-4{
    z-index:1101 !important;
  }
  .step-4 .view, .step-4 .date, .step-4 .wrapper-video, .step-4 .owl-nav{
    opacity: 0.1;
  }
</style>
<div id="wrap" class="bg-black">
  <div id="container">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="startPlanInfo js-restart-tour">
          <div class="subHeader-info tour-info"><img src="<?=base_url()."resources/img/information.png"?>" /></div>
        </div>
          <div class="vid-uploadSheet <?=($page_active == 'myEveryDay' ? '' : '')?>">
            <div id="uploadSheet">
              <img src="<?=base_url();?>resources/img/uploadsheet.png"/>
              <p>อัปโหลดชีท</p>
            </div>
        </div>
        <input class="sheet-upload" type="file"/>

        <div class="d-flex justify-content-between">
          <div class="align-self-center vid-prev"><?=$prev?></div>
          <div class="vid-header center">
            <div class="vid-h1"><?=$sheet->title?> : <?=$sheet->sheetCode?> | <?=count($vdos)?> วีดีโอ</div>
            <?php
            if(isset($sheet_file->name)){
              $pdf = getPdf($api_url, $sheet_file->name);
              echo '<a href="'.$pdf[0].'" target="_blank" class="vid-downloadSheet" id="downloadSheet"><button class="vid-Btn btn-green-white">ดาวน์โหลด Sheet</button></a>';
            }
            ?>
          </div>
          <div class="align-self-center vid-next"><?=$next?></div>
        </div>
      </div>
    </div>

        <div style="width: 100%; text-align: center; /*height: 700px; background: #ccc;*/">
        <div class="vid-h2 txt"></div>
          <div class="owl-carousel owl-theme video-set-alignment" id="playVideo">
            <?php
            $no = 0;
            foreach ($vdos as $vdo):
              $link = getVideo($api_url,$vdo->gcsName);
            ?>
              <div class="item">
                <div class="vid-h2 d-none">VIDEO <?=++$no?>/<?=count($vdos)?> : <?=$vdo->title?></div>
                <div class="wrapper-video" style="width: 100%;">
                  <everyday-player
                    nid="<?=$myProfile->mobile?>"
                    stdName="<?=$myProfile->firstname?> <?=$myProfile->lastname?>"
                    src="<?=$link[0]?>"
                    name="<?=$vdo->title?>"
                    imageUrl="<?=$vdo->imgUrl?>"
                    code="<?=$sheet->sheetCode?>"
                    tag="everyday"
                    contentId="content_<?=$vdo->order_item?>"
                    resumePlayback="<?=$vdo->progress == null ? 0 : $vdo->progress?>"
                    profile_id=<?=$myProfile->id?>
                    vdo_id=<?=$vdo->sheetVideoId?>
                    >
                  </everyday-player>
                </div>
                <div class="vid-detail-box">
                <?php
                    /*
                  <!--
                    <div class="vid-detail d1"><i class="fa fa-clock-o" aria-hidden="true"></i> 5 นาที</div>
                  -->
                  */
                ?>
                  <div class="vid-detail d2 view" data-vid='<?=$vdo->sheetVideoId?>'></div>
                  <div class="vid-detail d3 date"><i class="fa fa-calendar-o" aria-hidden="true"></i> <?=DateThai($vdo->updatedtime)?></div>
                  <div class="vid-detail vidLike d4" id="vidLike" data-vid='<?=$vdo->sheetVideoId?>'>
                    <?php
                      /*
                        <!-- <img class="vid-like-btn" onclick="clickLike('<?=$vdo->sheetVideoId?>','1','')" data-val="1" src="<?=base_url();?>resources/img/like_active.png"/>
                        <span class="totalLike mr-3">ถูกใจ 100k</span>
                        <img class="vid-unlike-btn" onclick="clickLike('<?=$vdo->sheetVideoId?>','0','')" data-val="0" src="<?=base_url();?>resources/img/unlike_active.png"/>
                        <span class="totalUnlike">ไม่ถูกใจ 20</span> -->
                      */
                    ?>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
          <?php
          /*
          <!-- Video player parameter description -->
          <!-- src = video url
          name = video name
          nid = รหัสบัตรประชาชนของนักเรียน
          stdName = ชื่อนักเรียน
          imageUrl = url ของปกคลิป video
          code = sheet code เช่น MK-A-B-1-1
          tag = ใน staging ใช้ test-everyday ใน production ใช้ everyday
          contentId = uniqKey for player
          resumePlayback(optional) = เวลาเริ่มต้นของ player (วินาที)
          มีการ return attribute ของ element 3 นะครับคือ
          1. progress คือ % ที่เล่นถึง (0-100)
          2. currentTime คือ เวลาที่เล่นอยู่ในปัจจุบัน (วินาที)
          3. duration คือ ความยาวของ video (วินาที) -->
          */
          ?>
        </div>

    <div class="containner-1200 mx-auto" id="CheckAnswer">
      <div class="containner-1200-padding">
        <div class="vid-btn-div">
          <a href="<?=base_url('Test/Exercise/'.$page.'?librarysheet='.$id);?>"><button class="vid-Btn1 btn-gray">ตรวจคำตอบ</button></a>
        </div>
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<?php
function getVideo($api_url, $videoName){
  $headers = array("accept: application/json",'Content-Type: application/json');
  $method = 'GET';
  $url = $api_url.'gen-signed-url/'.$videoName;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
  $response = curl_exec($curl);
  curl_close($curl);
  return json_decode($response);
}
function getPdf($api_url, $pdfName){
  $headers = array("accept: application/json",'Content-Type: application/json');
  $method = 'GET';
  $url = $api_url.'gen-signed-url/'.$pdfName;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
  $response = curl_exec($curl);
  curl_close($curl);
  return json_decode($response);
}
function DateThai($strDate){
  $strDate = date("Y-m-d H:i:s", strtotime('+7 hours', strtotime($strDate)));
  $strYear = date("Y",strtotime($strDate))+543;
  $strMonth= date("n",strtotime($strDate));
  $strDay= date("j",strtotime($strDate));
  $strHour= date("H",strtotime($strDate));
  $strMinute= date("i",strtotime($strDate));
  $strSeconds= date("s",strtotime($strDate));
  $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
  $strMonthThai=$strMonthCut[$strMonth];
  return "$strDay $strMonthThai $strYear";
}
?>
<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.11.0/js/bootstrap-tour.js"></script>
<script>
  var memberId = '<?=$myProfile->id?>';
  $(window).on('load', function () {
    getVideoView();
    getVideoLikeHtml();
    for(let i=0; i < document.getElementsByTagName("everyday-player").length ; i++){
      document.getElementsByTagName("everyday-player")[i].videoController.video.addEventListener('play', () => updateProgressOnPlay(i));
    }
    var owl = $('.owl-carousel');
    //get init owl Carousel for set tutorial
    owl.on('initialized.owl.carousel', function(event){
      $('.js-restart-tour').on('click', function(e){
        e.preventDefault();
        tour.restart();
      }); 

      var tour = new Tour({
          name: 'video-tutor',
          backdrop: true,
          keyboard: false,
          autoscroll: false,
          template: '<div class="startPlan text-light popover tour tutorial-step"> <p class="popover-content mb-2"></p> <div class="d-flex justify-content-between popover-navigation"> <button class="btn btn-skip" data-role="end">ปิด <i class="fa fa-times-circle"></i></button> <button class="btn btn-next" data-role="next">ถัดไป <i class="fa fa-chevron-circle-right"></i></button></div></div>',
          onEnd: function(){
            //When end remove class step-5 
            $('#playVideo').removeClass('step-4');
            $('html,body').animate({ scrollTop: $('body').offset().top}, 'slow');
          },         
          steps: [{
              element: "#downloadSheet",
              content: "1. กดเพือรับเอกสารมาปริ๊น สำหรับจดและทำแบบฝึกหัดของเนื้อหา",
              placement: function(context, source) {
                var position = $(source).position();
                if (position.left >= 288) {
                  return "auto left";
                }
                if (position.left < 288 ) {
                  return "top";
                }
              }          
            },
            {
              element: "#playVideo",
              content: "2. ดูวีดีโอ",
              placement: 'top',
              onNext: function(){
                $('html,body').animate({ scrollTop: $('#CheckAnswer').offset().top}, 'slow');                
              }
            },  
            {
              element: "#CheckAnswer",
              content: "3. ทําแบบฝึกหัดในชีท และกดปุ่มนี้ เพื่อตรวจคำตอบ พร้อมทั้งดูคำใบ้ และเฉลย",
              placement: function(context, source) {
                var position = $(source).position();
                if (position.left >= 40) {
                  return "auto left";
                }
                if (position.left < 40 ) {
                  return "top";
                }
              },
              onNext: function(){
                $('html,body').animate({ scrollTop: $('#uploadSheet').offset().top}, 'slow');                
              }                 
            },
            {
              element: "#uploadSheet",
              content: "4. อัปโหลดชีทที่จด<br>และทำแบบฝึกหัด เพื่อจัดเก็บไว้ทบทวนบทเรียน",
              placement: function(context, source) {
                var position = $(source).position();
                if (position.left >= 40) {
                  return "auto left";
                }
                if (position.left < 40 ) {
                  return "top";
                }
              },
              onNext: function(){
                $('#playVideo').addClass('step-4'); //Add Class step-5 for set z-index
                $('html,body').animate({ scrollTop: $('#vidLike').offset().top}, 'slow');                
              } 
            },
            {
              element: "#vidLike",
              content: "5. ปุ่มแสดงความคิดเห็น ในความพึงพอใจในวีดีโอนี้<br>เพื่อนำไปปรับปรุงแก้ไข",
              placement: function(context, source) {
                var position = $(source).position();
                if (position.left >= 40) {
                  return "auto top";
                }
                if (position.left < 40 ) {
                  return "left";
                }
              }
            }
          ]
        })

        // Start the tour
        setTimeout(function() {
          //tour.init();
          //tour.start();
          //check current step for setp 4
          if(!tour.ended() && tour.getCurrentStep() == 4){
            var getStepVideo = tour.getStep(4);
            if (typeof(getStepVideo) != 'undefined' && getStepVideo.id === 'step-4'){
              $('#playVideo').addClass('step-4'); //Add Class step-4 for set z-index
            }
          }
        }, 1500);
    });      
    
    //Init owl Carousel
    owl.owlCarousel({
      stagePadding: 100,
      items:1,
      center: true,
      mouseDrag: false,
      touchDrag: false,
      margin:5,
      dots: false,
      nav: true,
      navText: ["<i class='mx-2 f30 fa fa-angle-left'></i>","<i class='mx-2 f30 fa fa-angle-right'></i>"],
      responsive:{
        0:{stagePadding: 5}
        ,575:{stagePadding: 50}
        ,1024:{stagePadding: 200}
        ,1200:{stagePadding: 300}
        ,1600:{stagePadding: 400}
      }
    });
    owl.on('click', '.owl-item', function(event) {
        var target = jQuery(this).index();
        owl.trigger('to.owl.carousel', target);
    });

    owl.on('translated.owl.carousel', function(event) {
      for(i=0; i < document.getElementsByTagName("everyday-player").length ; i++){
        document.getElementsByTagName("everyday-player")[i].videoController.video.pause()
      }
      var txt = $('.owl-item.center .vid-h2').html();
      $('.vid-h2.txt').html(txt);
      $('.owl-item').removeClass('owlBlur');
      $('.owl-item').not(".center").addClass('owlBlur');
    });

    var txt2 = $('.owl-item.center .vid-h2').html();
    $('.vid-h2.txt').html(txt2);
    $('.owl-item').removeClass('owlBlur');
    $('.owl-item').not(".center").addClass('owlBlur');

    setInterval(updateProgress, 10000);

    $(".vid-uploadSheet").on('click', function() {
      $(".sheet-upload").click();
    });

    $(".sheet-upload").on('change', function(){
      if (this.files && this.files[0]) {
        var fileType = $(".sheet-upload").val().split('\\').pop().split('.').pop().toLowerCase();
        var fileExtension = ['pdf'];
        if ($.inArray(fileType, fileExtension) == -1) {
          //alert("Only formats are allowed : "+fileExtension.join(', '));
          alert("อัปโหลดได้เฉพาะไฟล์ PDF");
        }else{
          var file = $(".sheet-upload")[0].files[0];
          var formData = new FormData();
          formData.append('file', file);
          var opts = {
            method: 'POST',
            headers: {
              "Authorization": "Bearer <?=$token;?>"
            },
            body: formData
          }
          fetch('<?=$this->config->item('api_url').'upload-file'?>', opts).then(function (response) {
            var data = response.json();
            // await
            data.then(function(result) {
              // console.log(result.data.filename)
              // update profile_image filename
              var filePath = result.data.filename;
              var split = result.data.name.split('.');
              if(typeof(split[0]) != "undefined" && split[0] !== null ){
                var filename = split[0];
              }else{
                var filename = result.data.name;
              }
              var opts2 = {
                method: 'POST',
                headers: {
                    "Content-Type":"application/json",
                    "Authorization": "Bearer <?=$token;?>"
                },
                body: JSON.stringify({
                  "name": filename
                  , "path": filePath
                  , "memberId": <?=$myProfile->id?>
                })
              };
              fetch('<?=$this->config->item('api_url').'library-sheets/'.$id.'/member-sheet-uploads'?>', opts2).then(function (response2) {
                if(response2.ok){
                  return response2;
                }else{
                  return response2.json();
                }
              })
              .then(function (body) {
                console.log(body)
                if(body.ok != true){
                  alert("ไม่สามารถอัพโหลดชีทได้ กรุณาลองใหม่อีกครั้ง");
                }else{
                  alert("อัพโหลดชีทเรียบร้อยแล้ว");
                }
              }); //then Upload profile
            }) //data.then
          })
        }
      }
    });
  });

  function clickLike(sheetVideoId,val){
    var opts = {
      method: 'POST',
      headers: {
          "Content-Type":"application/json",
          "Authorization": "Bearer <?=$token;?>"
      },
      body: JSON.stringify({
      "sheetVideoId": parseInt(sheetVideoId),
      "action": parseInt(val)
      })
    };
    fetch('<?=$this->config->item('api_url').'front-end/video-review';?>', opts).then(function (response) {
      response.json().then(function (result) {
        if(response.ok){
          getVideoLikeHtml();
        }
      });
    });
  };

  function getVideoLikeHtml(){
    $( ".vidLike" ).each(function( index ) {
      var thisVidLike = $(this);
      var sheetVideoId = $(this).data('vid');
      //console.log( index + ": " + sheetVideoId);
      $.ajax({
        type: "POST",
        url: "<?=base_url()."MyEveryDay/getLikeHTML";?>",
        data: {memberId:memberId,sheetVideoId:sheetVideoId},
        success: function (ret) {
          var obj = jQuery.parseJSON(ret);
          thisVidLike.html(obj.html);
        }
      });
    });
  }

  function getVideoView(){
    $( ".vid-detail.d2" ).each(function( index ) {
      var thisVidLike = $(this);
      var sheetVideoId = $(this).data('vid');
      //console.log( index + ": " + sheetVideoId);
      $.ajax({
        type: "POST",
        url: "<?=base_url()."MyEveryDay/getVideoView";?>",
        data: {sheetVideoId:sheetVideoId},
        success: function (ret) {
          var obj = jQuery.parseJSON(ret);
          thisVidLike.html(obj.html);
        }
      });
    });
  }

  var video_progress_send = []
  function updateProgress(){
    let list_progress = []
    $('everyday-player').each(function(){
      let el = $(this)
      let obj = {
        currentTime: Number.parseInt(el.attr('currentTime')),
        progress: Number.parseInt(el.attr('progress')),
        resumePlayback: Number.parseInt(el.attr('resumePlayback')),
        profile_id: Number.parseInt(el.attr('profile_id')),
        vdo_id: Number.parseInt(el.attr('vdo_id'))
      }

      if(obj.resumePlayback < obj.currentTime ){
        exists_vdo = video_progress_send.find(v => v.vdo_id == obj.vdo_id)
        if(exists_vdo){
          if(exists_vdo.currentTime < obj.currentTime){
            list_progress.push(obj)
            let idx = video_progress_send.findIndex(v => v.vdo_id == obj.vdo_id)
            video_progress_send[idx] = obj
          }
        }else{
          video_progress_send.push(obj)
        }
      }
    })

    if(list_progress.length > 0){
      $.post("<?=base_url()."MyEveryDay/UpdateProgress";?>", {list_progress})
        .done(res => console.log(res))
    }
  }

  function updateProgressOnPlay(idx){
    // console.log(this)
    let list_progress = []
    var count = -1
    $('everyday-player').each(function(){
      count += 1
      if(count != idx){
        return true;
      }
      let el = $(this)
      let obj = {
        currentTime: Number.parseInt(el.attr('currentTime')),
        progress: Number.parseInt(el.attr('progress')),
        resumePlayback: Number.parseInt(el.attr('resumePlayback')),
        profile_id: Number.parseInt(el.attr('profile_id')),
        vdo_id: Number.parseInt(el.attr('vdo_id'))
      }

      if(obj.resumePlayback == 0){
        list_progress.push(obj)
      }
    })

    if(list_progress.length > 0){
      $.post("<?=base_url()."MyEveryDay/UpdateProgress";?>", {list_progress})
        .done(res => console.log(res))
    }
    updateVideoView(idx)
  }
  var vdo_already_play = []
  
  function updateVideoView(idx){
    // console.log(el)
    let vdo_id = document.getElementsByTagName("everyday-player")[idx].getAttribute('vdo_id')
    if(!vdo_already_play.includes(vdo_id)){
      vdo_already_play.push(vdo_id)
      $.post("<?=base_url()."MyEveryDay/UpdateView";?>", {vdo_id})
    }
  }
</script>