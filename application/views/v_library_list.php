<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<style>
.sticky { position: fixed; top: 0; width: 100%; margin-top: -10px; }
.sticky + .content { padding-top: 170px; }
</style>
<div id="wrap">
  <div id="container">
    <div class="libraryHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="library-slide slider">
          <?php foreach ($subjects as $key => $subject) :?>
            <?php if($subject->is_active == 1):?>
            <div class="libraryBox">
              <a href="<?=base_url('Library/All?subject='.$subject->id);?>"><div class="btnLib <?=$subject->id;?>" data-box-subject="<?=$subject->id;?>" data-box-id="box_<?=$key?>" data-img="<?=$subject->icon1_url?>" data-img-active="<?=$subject->icon2_url?>"><img src="<?=$subject->icon2_url?>" class="icon-subject"/> <?=$subject->title?> </div></a>
            </div>
            <?php endif; ?>
          <?php endforeach; ?>
          </section>
        </div>
      </div>
    </div>

    <div id="box1">
      <div class="lib-list-header" >
        <div class="bg-image" style="background-image: url('<?=$level->imgUrl?>');"></div>
        <div class="containner-1200 mx-auto">
          <div class="containner-1200-padding">
            <div class="bg-text">
              <div class="lib-list-headerTxt">เรื่อง : <?=$level->title?></div>
              <div class="lib-list-headerTxt">วีดีโอ : <?=count($videos)?></div>
              <div class="lib-list-headerTxt">แบบฝึกหัด : <?=count($sheet_files)?></div>
            </div>
          </div>
        </div>
      </div>

      <?php $no = 0; foreach($sublevels as $sublevel): ?>
      <section class="library-section">
        <div class="containner-1200 mx-auto">
          <div class="containner-1200-padding">
            <div class="box-subject-items box-subject-<?=$sublevel->subjectId;?>">
              <?php if($sublevel->is_active == 1): ?>
                <div class="libraryTextHeader f26"><div class="lib-No"><?=++$no?></div><?=$sublevel->title?></div>
                <?php if(!empty($sublevel->is_comingsoon)): ?>
                  <div class="status-sublist"><div class="comingSoon">COMING SOON</div></div>
                <?php endif; ?>
              <?php $sublevel_id = $sublevel->id;
                    $sheets = $this->librarysheet_model->find_by_sublevel_id($sublevel_id);
                    // $sheets = $this->sheetandtest_model->find_by_sublevel_id($sublevel_id);
                    // $sheetandtest = json_decode($sheets->data);
                    // $sheet_list = array();
                    // foreach ($sheetandtest->sheets_n_testing as $sheet){
                    //   $sheet_list[] = $sheet->id;
                    // }
                    // $res_sheets = $this->librarysheet_model->all_by_id_and_sublevelId($sheet_list,$sublevel_id);
                    
                  ?>

              <div class="row">
                <!-- เช็คว่าถ้ามี status เป็น comingsoon จะไม่สามารถกด sheet ได้ -->
                <?php if($sublevel->is_comingsoon == 1): ?>
                  <?php foreach ($sheets as $sheet): ?>
                    <!-- เช็คข้อมูลที่เป็น sheet เรียนอย่างเดียว -->
                    <div class="col-xs-2-10 col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10">
                      <a zhref="">
                      <img src="<?=$sheet->imgUrl?>"/>
                      <p class="sheet-title m-lib"><?=$sheet->title?></p>
                      <p class="sheet-des text-ellipsis"><?=$sheet->description?></p>
                      </a>
                    </div>
                  <?php endforeach; ?>

                <!-- เช็คว่าถ้ามี status ปกติจะสามารถกด sheet ได้ -->
                <?php else: ?>
                  <!-- เช็คข้อมูลเฉพาะ sheet เรียนอย่างเดียว -->
                  <?php foreach ($sheets as $sheet): ?>
                    <!-- เช็คข้อมูลที่เป็น sheet เรียนอย่างเดียว -->
                    <?php if(empty($sheet->is_test)): ?>
                      <!-- เช็คข้อมูล ถ้า comingsoon ถ้าเป็น false จะกดเข้าไปดู vdo ได้ -->
                      <?php if(empty($sheet->is_comingsoon)): ?>
                        <div class="col-xs-2-10 col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10">
                          <a href="<?=base_url('Library/Video/'.$sheet->id)?>">
                          <img src="<?=$sheet->imgUrl?>"/>
                          <p class="sheet-title m-lib"><?=$sheet->title?></p>
                          <p class="sheet-des text-ellipsis"><?=$sheet->description?></p>
                          </a>
                        </div>

                      <!-- เช็คข้อมูล ถ้า comingsoon ถ้าเป็น true จะกดดู vdo ไม่ได้ -->
                      <?php else: ?>
                        <div class="col-xs-2-10 col-xs-2-5-10 col-sm-2-5-10 col-md-2-10 col-lg-2-10">
                          <a zhref="">
                          <img src="<?=$sheet->imgUrl?>"/>
                          <p class="m-lib"><?=$sheet->title?></p>
                          <p class="sheet-des text-ellipsis"><?=$sheet->description?></p>
                          </a>
                          <div class="status"><div class="comingSoon">COMING SOON</div></div>
                        </div>
                      <?php endif; ?>
                    <?php endif; ?>
                  <?php endforeach; ?>
                <?php endif; ?>
              </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </section>
      <?php endforeach; ?>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/slick-master/slick/slick.min.js"></script>
<script>
  $(document).ready(function() {
    $('.box-subject-items').addClass('d-none');
    $('.see-more').addClass('d-none');
    $('.btnLib').each(function() {
      $(this).removeClass('un-active');
      $(this).removeClass('active');

      img = $(this).data('img');
      $(this).find("img").attr("src", img);
      $(this).addClass('un-active');
    });

    // active เมนูด้านบนจาก sub ที่ส่งมา
    var activeSub = "<?=$this->input->get('subject');?>";
    activeSubject($('div.' + activeSub));

    $('.btnLib').on("click", function() {
      $('.btnLib').each(function() {
        $(this).removeClass('un-active');
        $(this).removeClass('active');

        img = $(this).data('img');
        $(this).find("img").attr("src", img);
        $(this).addClass('un-active');

        $('.box-subject-items').addClass('d-none');
        $('.see-more').addClass('d-none');
      });
      activeSubject($(this));
    });

    function activeSubject(btn) {
      boxId = $(btn).data('box-id');
      imgActive = $(btn).data('img-active');

      $('.box-subject-' + $(btn).data('box-subject')).removeClass('d-none');
      $('.see-more').removeClass('d-none');

      btn.find("img").attr("src", imgActive);
      btn.removeClass('un-active');
      btn.removeClass('active');
      btn.addClass('active');
    }
  });

  $(".library-slide").slick({
    slidesToShow: 6.5,
      slidesToScroll: 6,
      dots: false,
      arrows: false,
      infinite: false,
      responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              slidesToShow: 3.5,
              slidesToScroll: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
            arrows: false,
              slidesToShow: 2.5,
              slidesToScroll: 2,
            }
          }
        ]
  });
  
  window.onscroll = function() {myFunction()};

  var header = document.getElementById("myHeader");
  var sticky = header.offsetTop;

  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("sticky");
    } else {
      header.classList.remove("sticky");
    }
  }
</script>

