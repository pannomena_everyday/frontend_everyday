<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<div id="wrap">
	<div id="container" class="bg-black">
		<div class="center">
    		<div class="vid-h1">Sheet 1 : MJ NO103432 | 5 วีดีโอ</div>
    		<button class="vid-Btn">ดาวน์โหลด Sheet</button>
    		<div class="vid-h2">VIDEO 4/5 : ความหมายของเลขยกกำลัง</div>
		</div>
		<div style="width: 100%; height: 700px; background: #ccc;">
			Video
		</div>
		<div class="vid-detail-box">
			<div class="vid-detail d1">5 นาที</div>
			<div class="vid-detail d2">50 K</div>
			<div class="vid-detail d3">16 มีนาคม 2563</div>
			<div class="vid-detail d4">ถูกใจ 100k</div>
			<div class="vid-detail d5">ไม่ถูกใจ 20</div>
			<div>
    			<button class="vid-Btn1">ตรวจคำตอบ</button>
    			<button class="vid-Btn2">อัปโหลด Sheet</button>
			</div>
		</div>
		
		<section class="containner-padding">    		
    		<div class="div-plan">
    			<div class="f24 allPlanHeader">ม.ต้น / คณิตศาสตร์ / เนื้อหาสอบเข้า / ตะลุยโจทย์</div>
                <div class="subLevel">
                	<div class="subLevel-header"></div>
                	<div class="subLevel-slide">
                		<section class="regular slider">
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide1.png"?>">
                              <p>ความหมายของเลขยกกำลัง</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide2.png"?>">
                              <p>การหาค่าของเลขยกกำลัง แบบต่างๆ</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide3.png"?>">                 
                              <p>เทคนิคการหาค่าของ เลขยกกำลัง กรณีพิเศษ 1</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide4.png"?>">
                              <p>สับเซตและเพาเวอร์เซต</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide1.png"?>">
                              <p>สมบัติของจำนวนจริง...</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide2.png"?>">
                              <p>การแยกตัวประกอบพหุนาม</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide3.png"?>">
                              <p>การให้เหตุผลแบบอุปนัย</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide4.png"?>">
                              <p>สับเซตและเพาเวอร์เซต</p>
                            </div>
                    	</section>
                	</div>
                </div>
                <div class="subLevel subDetail">
                	<div class="vid-subLevel">Sub Level : พื้นฐานเลขยกกำลัง</div>
                	<div class="subLevel-slide">
                		<section class="regular slider">
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide1.png"?>">
                              <p>ความหมายของเลขยกกำลัง</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide2.png"?>">
                              <p>การหาค่าของเลขยกกำลัง แบบต่างๆ</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide3.png"?>">                 
                              <p>เทคนิคการหาค่าของ เลขยกกำลัง กรณีพิเศษ 1</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide4.png"?>">
                              <p>สับเซตและเพาเวอร์เซต</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide1.png"?>">
                              <p>สมบัติของจำนวนจริง...</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide2.png"?>">
                              <p>การแยกตัวประกอบพหุนาม</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide3.png"?>">
                              <p>การให้เหตุผลแบบอุปนัย</p>
                            </div>
                            <div>
                              <img src="<?=base_url()."resources/img/index_slide4.png"?>">
                              <p>สับเซตและเพาเวอร์เซต</p>
                            </div>
                    	</section>
                	</div>
                </div> <!-- End .subDetail -->
        	</div> <!-- End .div-plan -->
        </section>
    	
	</div> <!-- //.Container -->	
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/slick-master/slick/slick.min.js"></script>
<script>
    $(".regular").slick({
    	slidesToShow: 5,
      	slidesToScroll: 5,
      	dots: false,
      	infinite: false,
      	arrows: false,
      	responsive: [
      	    {
      	      breakpoint: 768,
      	      settings: {
      	        arrows: false,
      	        slidesToShow: 3,
      	      	slidesToScroll: 3,
      	      }
      	    },
      	    {
      	      breakpoint: 480,
      	      settings: {
      	    	arrows: false,
      	        slidesToShow: 2,
      	      	slidesToScroll: 2,
      	      }
      	    }
      	  ]
    });
</script>