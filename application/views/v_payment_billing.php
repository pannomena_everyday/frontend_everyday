<!-- css form payment -->


<!-- form บัญชีผู้ใช้ -->
<div id="wrap">
    <div id="container" class="bg-gray-f7">
        <div class="whiteContentBox set-full-page">
            <div class="div-subscriberTxt text-center">
                <div class="account-header">บัญชีผู้ใช้</div>
                <div class="account-name f30 text-truncate"><?= $myProfile->firstname ?> <?= $myProfile->lastname ?></div>
                <div class="mb-3"><?= $myProfile->mobile ?></div>
            </div>
            <hr>
            <form id="form-account" action="" method="post">
                <h4 class="mb-4">กรอกข้อมูลสำหรับใบเสร็จ</h4>
                <div class="mb-5 nfo-user">
                    <input type="checkbox" class="form-check-input" id="info-user">
                    <label class="form-check-label" for="info-user">ใช้ข้อมูลผู้สมัคร</label>
                </div>
                <div class="row mb-4">
                    <div class="col-md-6 mb-4 mb-md-0">
                        <label for="first-name" class="form-label text-blue">ชื่อ</label>
                        <input type="text" class="form-control" id="first-name" placeholder="ระบุชื่อ"
                            autocomplete="off">
                    </div>
                    <div class="col-md-6">
                        <label for="last-name" class="form-label text-blue">นามสกุล</label>
                        <input type="text" class="form-control" id="last-name" placeholder="ระบุนามสกุล"
                            autocomplete="off">
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col">
                        <label for="phone" class="form-label text-blue">เบอร์โทรศัพท์</label>
                        <input type="text" id="phone" name="phone" class="form-control" autocomplete="off"
                            inputmode="number" placeholder="088-888-8888">
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col">
                        <label for="address" class="form-label text-blue">ที่อยู่*</label>
                        <textarea class="form-control" placeholder="ระบุ ที่อยู่" id="address" style="height: 164px"
                            nores></textarea>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-6 mb-4 mb-md-0">
                        <label for="provinceId" class="form-label text-blue">จังหวัด</label>
                        <select class="form-control" id="provinceId" name="provinceId">
                            <option value=""> ระบุ จังหวัด</option>
                            <?php
                                foreach($dataProvince->result() as $row){
                                echo "<option value='".$row->id."'>".$row->name_th."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="amphureId" class="form-label text-blue">เขต/อำเภอ*</label>
                        <select class="form-control" id="amphureId" name="amphureId">
                            <option value="0">ระบุ เขต / อำเภอ</option>
                            <?php
                                foreach($dataAmphure->result() as $row){
                                    echo "<option value='".$row->id."'>".$row->name_th."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-md-6 mb-4 mb-md-0">
                        <label for="districtId" class="form-label text-blue">แขวง/ตำบล*</label>
                        <select class="form-control" id="districtId" name="districtId">
                            <option value="0">ระบุ แขวง / ตำบล</option>
                            <?php
                            foreach($dataDistrict->result() as $row){
                                echo "<option value='".$row->id."' ".(($row->id == $districtId) ? 'selected' : '').">".$row->name_th."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="zipCode" class="form-label text-blue">รหัสไปรษณีย์*</label>
                        <select class="form-control" id="zipCode" name="zipCode">
                            <option value=""> ระบุ รหัสไปรษณีย์</option>
                        </select>
                        <!-- <input type="text" class="form-control" id="zipCode" placeholder="ระบุรหัสไปรษณีย์"
                            autocomplete="off" maxlength="5"> -->
                    </div>
                </div>
                <div class="row justify-content-center">
                    <button class="col-6 col-sm-3 btn-lg rounded-pill btn-black js-sent-info" id="saveChange" type="button">ส่ง</button>
                </div>
            </form>
            <div class="modal fade" id="sent-info-modal" tabindex="-1" aria-labelledby="sent-info-modal"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-sm col-sm-6">
                    <div class="modal-content text-center p-4 d-flex justify-content-center">
                        <div class="mb-4"><img src="/resources/img/correct.svg" alt="correct" width="50"></div>
                        <h5 class="mb-4">บันทึกข้อมูลสำหรับใบเสร็จ<br>เรียบร้อยแล้ว</h5>
                        <a href="<?= base_url("Library") ?>"><button class="btn-lg rounded-pill btn-black" type="button">เริ่มเรียน</button></a>
                    </div>
                </div>
            </div>
        </div> <!-- //.subscriberBox -->
    </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script>
var userInformation = {
  firstname: '<?= $myProfile->firstname?>',
  lastname: '<?= $myProfile->lastname?>',
  mobile: '<?= $myProfile->mobile?>',
  provinceId: '<?= $myProfile->education->provinceId?>',
}

$("#info-user").change(function(){
  var isCheck = $(this).prop('checked')
  if(isCheck){
    $("#first-name").val(userInformation.firstname)
    $("#last-name").val(userInformation.lastname)
    $("#phone").val(userInformation.mobile)
    // $("#provinceId").val(userInformation.provinceId)
  }else{
    $("#first-name").val('')
    $("#last-name").val('')
    $("#phone").val('')
    // $("#provinceId").val('')
  }
})

$('#provinceId').change(function(){
    var provinceId = $(this).val();
    $.ajax({
      url: "<?=base_url()."MyProfile/getAmphureHTML"?>",
      type: "post",
      data: {provinceId:provinceId} ,
      success: function (data) {
        $('#amphureId').html(data);
        $('#districtId').html('<option value=""> ระบุ แขวง / ตำบล</option>');
        $('#zipCode').html('<option value=""> ระบุ รหัสไปรษณีย์</option>');
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });
  });

  $('#amphureId').change(function(){
    var amphureId = $(this).val();
    $.ajax({
      url: "<?=base_url()."MyProfile/getDistrictHTML"?>",
      type: "post",
      data: {amphureId:amphureId} ,
      success: function (data) {
        $('#districtId').html(data);
        $('#zipCode').html('<option value=""> ระบุ รหัสไปรษณีย์</option>');
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });
  });

  $('#districtId').change(function(){
    var districtId = $(this).val();
    $.ajax({
      url: "<?=base_url()."MyProfile/getZipCodeHTML"?>",
      type: "post",
      data: {districtId:districtId} ,
      success: function (data) {
        $('#zipCode').html(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });
  });


  $('#saveChange').click(function(){
    var memberId = '<?=$myProfile->id?>';
    var firstname = $.trim($('#first-name').val());
    var lastname = $.trim($('#last-name').val());
    var mobile = $.trim($('#phone').val());
    var addr = $.trim($('#address').val());
    var provinceId = $.trim($('#provinceId').val());
    var amphureId = $.trim($('#amphureId').val());
    var districtId = $.trim($('#districtId').val());
    var zipCode = $.trim($('#zipCode').val());
    var useUserInformation = $("#info-user").prop('checked')
    var fullname = firstname + ' ' + lastname
    
    // var amount = '<?=$amount?>';
    // var payment_date = '<?=$payment_date?>';
    // var subscription_id = '<?=$subscription_id?>';
    // var invoice_no = '<?=$invoice_no?>';


    if(firstname == ''){
      alert('กรุณากรอกชื่อ');
    }else if(lastname == ''){
        alert('กรุณาระบุนามสกุล')
    }else if(mobile == ''){
      alert('กรุณากรอกหมายเลขโทรศัพท์');
    }else if(addr == ''){
      alert('กรุณากรอกที่อยู่');
    }else if(provinceId == ''){
      alert('กรุณาเลือกจังหวัด');
    }else if(amphureId == ''){
      alert('กรุณาเลือกเขต/อำเภอ');
    }else if(districtId == ''){
      alert('กรุณาเลือกแขวง/ตำบล');
    }else if(zipCode == ''){
      alert('กรุณาเลือกรหัสไปรษณีย์');
    }else{
      $.ajax({
        url: "<?=base_url()."Payment/addBillingAndSendMail"?>",
        type: "post",
        // data: {memberId:memberId,fullname:fullname,mobile:mobile,addr:addr,provinceId:provinceId,amphureId:amphureId,districtId:districtId,zipCode:zipCode, useUserInformation, amount, payment_date, subscription_id, invoice_no} ,
        data: {memberId:memberId,fullname:fullname,mobile:mobile,addr:addr,provinceId:provinceId,amphureId:amphureId,districtId:districtId,zipCode:zipCode, useUserInformation} ,
        success: function (data) {
          $('#sent-info-modal').modal({
              backdrop: 'static',
              keyboard: false
          });
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    }
  });
</script>

<!-- End form บัญชีผู้ใช้ -->