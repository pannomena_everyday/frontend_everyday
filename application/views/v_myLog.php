<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<div id="wrap">
  <div id="container">
    <div class="myProfileHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="myProfile-slide slider">
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile"?>"><img src="<?=base_url()."resources/img/account.png"?>" class="icon-subject"/>บัญชีผู้ใช้ </a>
            </div>
            <?php if($myProfile->subscriptionTypeId != 1): ?>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyPlan"?>"><img src="<?=base_url()."resources/img/myplan.png"?>" class="icon-subject"/>แผนการเรียน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyPerformance"?>"><img src="<?=base_url()."resources/img/performance.png"?>" class="icon-subject"/>ติดตามผลงาน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyNote"?>"><img src="<?=base_url()."resources/img/sheet.png"?>" class="icon-subject"/>คลัง Sheet </a>
            </div>
            <?php endif; ?>
            <div class="myProfileBox active">
              <img src="<?=base_url()."resources/img/loghistory_active.png"?>" class="icon-subject"/><span>ประวัติการเรียน</span>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyMembership"?>"><img src="<?=base_url()."resources/img/membership.png"?>" class="icon-subject"/>สถานะสมาชิก </a>
            </div>
          </section>
        </div>
      </div>
    </div>
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <?php $this->load->view('i_myProfileStatus', $this->data); ?>

        <section class="myNote-container">
          <div class="myNote-header">
            <div class="row">
              <div class="col-sm-12 col-md-6 log-div-6">
                <div class="logSearch txt">ตั้งแต่วันที่</div>
                <div class="logSearch input-group myLogDate">
                  <input id="dateFrom" class="form-control" placeholder=""  type="text" autocomplete="off">
                  <span class="input-group-append">
                    <button class="btn btn-outline-secondary text-white btnDateFrom" type="button">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </span>
                </div>
              </div>
              <div class="col-sm-12 col-md-6 log-div-6">
                <div class="logSearch txt">ถึง</div>
                <div class="logSearch input-group myLogDate">
                  <input id="dateTo" class="form-control" placeholder=""  type="text" autocomplete="off">
                  <span class="input-group-append">
                    <button class="btn btn-outline-secondary text-white btnDateTo" type="button">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </span>
                </div>
              </div><!-- .myNoteDate-div -->
            </div><!-- .row -->
          </div><!-- .myNote-header -->
          <div class="myLog-body mb-4"></div><!-- .myNote-body -->
        </section><!-- .myNote-container -->
      </div>
    </div> <!-- //.containner-padding -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->
<?php
function DateThai($strDate)
{
  $strYear = date("Y",strtotime($strDate))+543;
  $strMonth= date("n",strtotime($strDate));
  $strDay= date("j",strtotime($strDate));
  $strHour= date("H",strtotime($strDate));
  $strMinute= date("i",strtotime($strDate));
  $strSeconds= date("s",strtotime($strDate));
  $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
  $strMonthThai=$strMonthCut[$strMonth];
  return "$strDay $strMonthThai $strYear";
}
function getTime($strDate)
{
  $strYear = date("Y",strtotime($strDate))+543;
  $strMonth= date("n",strtotime($strDate));
  $strDay= date("j",strtotime($strDate));
  $strHour= date("H",strtotime($strDate));
  $strMinute= date("i",strtotime($strDate));
  $strSeconds= date("s",strtotime($strDate));
  $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
  $strMonthThai=$strMonthCut[$strMonth];
  return "$strHour:$strMinute";
}
?>
<script>
  if($( window ).width() < 768){
    $(".myProfile-slide").slick('slickGoTo', 4);
  }
  $(document).ready(function() {
    getMyLogHTML();
    $('#dateFrom,#dateTo').datepicker({
      changeYear: true,
      changeMonth: true,
      dateFormat: 'yy-mm-dd',
      yearRange: '-100:+0'
    });
    $('.btnDateFrom').on('click', function() {
      $('#dateFrom').focus();
    });
    $('.btnDateTo').on('click', function() {
      $('#dateTo').focus();
    });

    $('#dateFrom,#dateTo').on('change', function() {
      if($.trim($('#dateFrom').val()) != '' && $.trim($('#dateTo').val()) != ''){
        getMyLogHTML();
      }
    });
  });

  function getMyLogHTML(){
    var dateFrom = $.trim($('#dateFrom').val());
    var dateTo = $.trim($('#dateTo').val());
    $.ajax({
      type: "POST",
      url: "<?=base_url()."MyProfile/getMyLogHTML";?>",
      data: {dateFrom:dateFrom,dateTo:dateTo},
      success: function (data) {
        $('.myLog-body').html(data);
      }
    });
  }

  $( "#profile" ).click(function() {
    $('#profileBox').toggle('slow');
  });
</script>