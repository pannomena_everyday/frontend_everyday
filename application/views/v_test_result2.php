<div id="wrap">
  <div id="container">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <section class="preTest-result">
          <!-- <div class="preTest-result-info"><img src="<?=base_url()."resources/img/information.png"?>"/></div> -->
          <img src="<?=base_url()."resources/img/PreTest_NotPassed.png"?>" />
          <div class="f-red f30">นักเรียนยังไม่ผ่านเกณฑ์ของเนื้อหานี้</div>
          <div  class="f24">แนะนำให้เรียนจุดที่ไม่มั่นใจใหม่อีกครั้ง</div>
          <div class="preTest-result-box">
            <div class="row">
              <div class="col-6 col-sm-6 col-md-3 box boxResult1">
                <div class="resultHeader">คิดเป็นเปอร์เซ็นต์</div>
                <div class="resultDetail"><?= round($score/$total_score * 100) ?>%</div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult2">
                <div class="resultHeader">คะแนนที่ได้</div>
                <div class="resultDetail"><?= $score ?></div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult3">
                <div class="resultHeader">คะแนนเต็ม</div>
                <div class="resultDetail"><?= $total_score ?></div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult4">
                <div class="resultHeader">เวลา (นาที)</div>
                <div class="resultDetail"><?= fmtMSS($use_time) ?></div>
              </div>
            </div>
          </div>
          <div class="d-flex justify-content-center flex-wrap">
          	<div class="p-2 m-w-300 w-100">
          		<a href="<?= $prev_sheet != "" ? base_url("MyEveryDay?subject=$subject&l=".$prev_obj['level']."&sl=".$prev_obj['sub_level']."&sh=".$prev_obj['sheet']) : base_url('MyEveryDay')?>"><button class="btn-60 btn-green-white">เรียนอีกครั้ง</button></a>
          	</div>
          	<div class="p-2 m-w-300 w-100">
          		<!-- <a href="<?= $next_sheet != "" ? base_url('MyEveryDay/Video/').$next_sheet : base_url('MyEveryDay')?>"><button class="btn-60 btn-white-green">เนื้อหาถัดไป</button></a> -->
          		<a href="<?= $next_sheet != "" ? base_url("MyEveryDay?subject=$subject&l=".$next_obj['level']."&sl=".$next_obj['sub_level']."&sh=".$next_obj['sheet']) : base_url('MyEveryDay')?>"><button class="btn-60 btn-white-green">เนื้อหาถัดไป</button></a>
          	</div>
          	<div class="p-2 m-w-300 w-100">
          		<a href="<?=base_url("Test/Score/".$sheet_id."/".$level_id."/".$plan_id."/".$test_id);?>"><button class="btn-60 btn-white-green">ดูกระดาษคำตอบ</button></a>
          	</div>
          </div>
        </section>
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<?php 
function fmtMSS($seconds) {
  $t = round($seconds);
  return sprintf('%02d:%02d',($t/60), $t%60);
}
?>