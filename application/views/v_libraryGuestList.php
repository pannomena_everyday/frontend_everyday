<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<div id="wrap">
  <div id="container">
    <div class="libraryHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="library-slide slider">
          <?php foreach ($subjects as $key => $subject) :?>
            <?php if($subject->is_active == 1):?>
            <div class="libraryBox">
              <a href="<?=base_url('Library');?>"><div class="btnLib <?=$subject->id;?>" data-box-subject="<?=$subject->id;?>" data-box-id="box_<?=$key?>" data-img="<?=$subject->icon1_url?>" data-img-active="<?=$subject->icon2_url?>"><img src="<?=$subject->icon2_url?>" class="icon-subject"/> <?=$subject->title?> </div></a>
            </div>
            <?php endif; ?>
          <?php endforeach; ?>
          </section>
        </div>
      </div>
    </div>
      <div class="bg-white">
        <div class="containner-1200 mx-auto">
          <div class="containner-1200-padding">
            <div class="p-4">
              <section id="box1" class="library-section-sub-guest">
                <div class="lib-txtHeader m-t-5"><?=$title?></div>
                <div class="lib-subTxtHeader"><a href="<?=base_url('Library/Guest')?>"><?=$prefix?></a> / <?=$title?></div>
                <div class="row">
                  <?php foreach ($videos as $vdo):?>
                  <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10 m-b-20">
                    <a href="<?=base_url('Library/Video/'.$vdo->librarySheetId)?>">
                      <img src="<?=$vdo->imgUrl?>"/>
                      <p class="libraryTitle text-left m-t-5"><?=$vdo->title?></p>
                    </a>
                  </div>
                  <?php endforeach; ?>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div> <!-- //.containner-padding -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/slick-master/slick/slick.min.js"></script>
<script>
  $(document).ready(function() {
    $('.box-subject-items').addClass('d-none');
    $('.see-more').addClass('d-none');
    $('.btnLib').each(function() {
      $(this).removeClass('un-active');
      $(this).removeClass('active');

      img = $(this).data('img');
      $(this).find("img").attr("src", img);
      $(this).addClass('un-active');
    });

    // active เมนูด้านบนจาก sub ที่ส่งมา
    var activeSub = "<?=$this->input->get('subject');?>";
    activeSubject($('div.' + activeSub));

    $('.btnLib').on("click", function() {
      $('.btnLib').each(function() {
        $(this).removeClass('un-active');
        $(this).removeClass('active');

        img = $(this).data('img');
        $(this).find("img").attr("src", img);
        $(this).addClass('un-active');

        $('.box-subject-items').addClass('d-none');
        $('.see-more').addClass('d-none');
      });
      activeSubject($(this));
    });

    function activeSubject(btn) {
      boxId = $(btn).data('box-id');
      imgActive = $(btn).data('img-active');

      $('.box-subject-' + $(btn).data('box-subject')).removeClass('d-none');
      $('.see-more').removeClass('d-none');

      btn.find("img").attr("src", imgActive);
      btn.removeClass('un-active');
      btn.removeClass('active');
      btn.addClass('active');
    }
  });
  
  $(".library-slide").slick({
    slidesToShow: 6,
      slidesToScroll: 6,
      dots: false,
      arrows: false,
      infinite: false,
      responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              slidesToShow: 3,
              slidesToScroll: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
            arrows: false,
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          }
        ]
  });
</script>