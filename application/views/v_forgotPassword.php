<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<div id="wrap">
  <div id="container" class="bg-gray-f7">
    <div class="containner-padding">
      <div class="box-800">
        <div class="center bg-white f26 f-w-500">เปลี่ยนรหัสผ่าน </div>
        <div class="center bg-white f22"> กรอกอีเมลที่ถูกต้องเพื่อสร้างรหัสผ่านใหม่ </div>
        <div class="input-group input-focus form-signIn">
          <div class="input-group-prepend">
            <span class="input-group-text bg-white"><i class="fa fa-user"></i></span>
          </div>
          <input type="text" id="val" placeholder="อีเมล" class="form-control border-left-0 f18">
        </div>
        <button class="sendBtn btn1 btn-black"> ส่งอีเมล </button>
      </div>

    </div> <!-- //.containner-padding -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- <script src="facebookLogin.js"></script> -->

<script>
  $('.sendBtn.btn1').click(function(){
    if(!$('.sendBtn').hasClass("btn-disable")){
      $('.sendBtn').addClass("btn-disable")
      var val = $.trim($('#val').val());
      if(val == ''){
        $('.sendBtn').removeClass("btn-disable")
        alert('กรุณากรอกข้อมูลให้ถูกต้อง');
      }else{
        $.ajax({
          url: "<?=base_url()."Index/ForgotPasswordProcess"?>",
          type: "post",
          data: {val:val} ,
          success: function (data) {
            if(data == 'true'){
              alert('ส่งข้อมูลการเปลี่ยนรหัสผ่านไปที่อีเมลที่ลงทะเบียนไว้เรียบร้อยแล้ว');
              window.location.href = '<?=base_url("Index/SignIn");?>';
            }else if(data == 'false'){
                $('.sendBtn').removeClass("btn-disable")
                alert('ไม่สามารถส่งอีเมลได้ กรุณาลองใหม่อีกครั้ง');
              }else{
                $('.sendBtn').removeClass("btn-disable")
                alert(data);
                console.log(data);
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });
      }
    }// check disable
  });

  $('#val').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      if(!$('.sendBtn').hasClass("btn-disable")){
        $('.sendBtn').addClass("btn-disable")
        var val = $.trim($('#val').val());
        if(val == ''){
          $('.sendBtn').removeClass("btn-disable")
          alert('กรุณากรอกข้อมูลให้ถูกต้อง');
        }else{
          $.ajax({
            url: "<?=base_url()."Index/ForgotPasswordProcess"?>",
            type: "post",
            data: {val:val} ,
            success: function (data) {
              if(data == 'true'){
                alert('ส่งข้อมูลการเปลี่ยนรหัสผ่านไปที่อีเมลที่ลงทะเบียนไว้เรียบร้อยแล้ว');
                window.location.href = '<?=base_url("Index/SignIn");?>';
              }else if(data == 'false'){
                $('.sendBtn').removeClass("btn-disable")
                alert('ไม่สามารถส่งอีเมลได้ กรุณาลองใหม่อีกครั้ง');
              }else{
                $('.sendBtn').removeClass("btn-disable")
                alert(data);
                console.log(data);
              }
            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
            }
          });
        }
      }// check disable
    }// check push enter
  });
</script>
