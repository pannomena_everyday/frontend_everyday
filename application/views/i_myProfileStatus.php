<section class="d-flex flex-column flex-sm-row align-items-center align-items-sm-start flex-nowrap m-t-10 m-b-10">
  <div class="p-2">
    <div class="myProfile-img">
      <?php if($myProfile->profile_image != ''){
        echo '<img class="profile-pic" src="'.getFileUrl($api_url,$myProfile->profile_image)[0].'"/>';
      }else{
        echo '<img class="profile-pic" src="'.base_url().'resources/img/profile_pic.png"/>';
      }
      ?>
      <div class="p-image">
        <i class="fa fa-camera upload-button"></i>
        <input class="file-upload" type="file" accept="image/*"/>
      </div>
    </div>
  </div>
  <!-- <div class="p-2 flex-grow-1"> -->
  <div class="p-2 flex-grow-1 align-self-center">
    <div class="d-flex flex-column">
      <div class="myProfile-name f30 align-self-center align-self-sm-start"><?=$myProfile->firstname." ".$myProfile->lastname?></div>
      <div class="myProfile-subscriber f24 align-self-center align-self-sm-start">
        <?=($myProfile->subscriptionTypeId == 1 ? 'GUEST' : 'SUBSCRIBER')?>
      </div>
    </div>
    <!-- <div class="myProfile-score d-flex"> -->
    <div class="myProfile-score d-none">
      <div class="flex-fill bd-right">
        <!-- <div class="f35">200</div> -->
        <div class="f16">ผลงาน</div>
      </div>
      <div class="flex-fill">
        <!-- <div class="f35">20</div> -->
        <div class="f16">คลังชีท</div>
      </div>
    </div>
  </div>
</section>

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/slick-master/slick/slick.min.js"></script>
<script>
  $(".myProfile-slide").slick({
    slidesToShow: 6,
      slidesToScroll: 6,
      dots: false,
      arrows: false,
      infinite: false,
      responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              slidesToShow: 3.5,
              slidesToScroll: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
            arrows: false,
              slidesToShow: 2.5,
              slidesToScroll: 2,
            }
          }
        ]
  });
</script>
