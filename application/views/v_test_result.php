<div id="wrap">
  <div id="container">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <section class="preTest-result">
          <!-- <div class="preTest-result-info"><img src="<?=base_url()."resources/img/information.png"?>"/></div> -->
          <img src="<?=base_url()."resources/img/PreTest_Pass.png"?>" />
          <div class="f-green f30">ยินดีด้วย! นักเรียนผ่านเกณฑ์ของเนื้อหานี้แล้ว</div>
          <div class="preTest-result-box">
            <div class="row">
              <div class="col-6 col-sm-6 col-md-3 box boxResult1">
                <div class="resultHeader">คิดเป็นเปอร์เซ็นต์</div>
                <div class="resultDetail"><?= round($score/$total_score * 100) ?>%</div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult2">
                <div class="resultHeader">คะแนนที่ได้</div>
                <div class="resultDetail"><?= $score ?></div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult3">
                <div class="resultHeader">คะแนนเต็ม</div>
                <div class="resultDetail"><?= $total_score ?></div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult4">
                <div class="resultHeader">เวลา (นาที)</div>
                <div class="resultDetail"><?= fmtMSS($use_time) ?></div>
              </div>
            </div>
          </div>
          <!-- <a href="<?=base_url()."MyEveryDay";?>"><button class="btn-60 btn-green-white m-w-400 my-1">เนื้อหาถัดไป</button></a> -->
          <a href="<?= $next_sheet != "" ? base_url("MyEveryDay?subject=$subject&l=".$next_obj['level']."&sl=".$next_obj['sub_level']."&sh=".$next_obj['sheet']) : base_url('MyEveryDay')?>"><button class="btn-60 btn-green-white m-w-400 my-1">เนื้อหาถัดไป</button></a>
          <a href="<?=base_url("Test/Score/".$sheet_id."/".$level_id."/".$plan_id."/".$test_id);?>"><button class="btn-60 btn-white-green m-w-400 my-1">ดูกระดาษคำตอบ</button></a>
          <!-- <a href="<?=base_url()."Test/Score";?>"><button class="btn-60 btn-white-green m-w-400 my-1">ดูกระดาษคำตอบ</button></a> -->
        </section>
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<?php
function fmtMSS($seconds) {
  $t = round($seconds);
  return sprintf('%02d:%02d',($t/60), $t%60);
}

?>