<link rel="stylesheet" koko="in" href="<?=base_url();?>resources/css/tutorial.min.css">
<style>
	#step-1 .btn-next {
		display: none;
	}
	#step-1 .popover-navigation {
		-ms-flex-pack: center !important;
		justify-content: center !important;
	}
</style>
<div id="wrap">
    <div id="container">
        <div class="containner-1200 mx-auto">
            <div class="containner-1200-padding">
                <div class="startPlan-info startPlanInfo js-restart-tour">
                    <div class="subHeader-info"><img src="<?=base_url()."resources/img/information.png"?>" /></div>
                </div>
                <div class="startPlanBox">
                    <div class="text-center"><img class="img-startplan"
                            src="<?=base_url()."resources/img/startyoureveryday.png"?>" /></div>
                </div>
                <div class="startPlanBtn-div d-flex justify-content-center flex-wrap flex-md-nowrap">
                    <div class="w-100 m-w-360 m-15">
                        <a class="w-100" href="<?=base_url("Plan/AddPlan")?>"><button class="btn-60 btn-green-white"
                                id="add-plan">เพิ่มแผนการเรียน</button></a>
                    </div>
                    <div class="w-100 m-w-360 m-15">
                        <a class="w-100" href="<?=base_url('Library')?>"><button class="btn-60 btn-white-green"
                                id="start-paln">เริ่มเรียนเลย!</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.11.0/js/bootstrap-tour.js"></script>
<script>
    $('.js-restart-tour').on('click', function(e){
        e.preventDefault();
        tour.restart();
    })	
	var tour = new Tour({
		name: 'start-plan',
		backdrop: true,
		keyboard: false,
		template: '<div class="startPlan text-light popover tour tutorial-step"> <p class="popover-content mb-2"></p> <div class="d-flex justify-content-between popover-navigation"> <button class="btn btn-skip" data-role="end">ปิด <i class="fa fa-times-circle"></i></button> <button class="btn btn-next" data-role="next">ถัดไป <i class="fa fa-chevron-circle-right"></i></button></div></div>',
		steps: [{
				element: "#add-plan",
				content: "กดปุ่มเพิ่มแผนการเรียน <br>เพื่อให้ระบบแนะนำแผนการเรียนให้ <br>หรือสามารถเพิ่มภายหลังได้",
				placement: "auto top",
				onNext: function() {
					$('.btn-next').hide();
				}
			},
			{
				element: "#start-paln",
				content: "เริ่มเรียนในเนื้อหาที่สนใจ",
				placement: function(context, source) {
					var position = $(source).position();
					if (position.left >= 399) {
						return "top";
					}
					if (position.left < 399 ) {
						return "bottom";
					}
				}
			}
		]
	}).init();
	// Start the tour
	setTimeout(function() {
		tour.start();
	}, 1000);
</script>