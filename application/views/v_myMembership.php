<?php //$myProfile->subscriptionTypeId = 1; ?>
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<style>
  .p-image {display: block;}
</style>
<div id="wrap">
  <div id="container">
    <div class="myProfileHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="myProfile-slide slider">
            <div class="myProfileBox active">
              <a href="<?=base_url()."MyProfile"?>"><img src="<?=base_url()."resources/img/account.png"?>" class="icon-subject"/>บัญชีผู้ใช้ </a>
            </div>
            <?php if($myProfile->subscriptionTypeId != 1): ?>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyPlan"?>"><img src="<?=base_url()."resources/img/myplan.png"?>" class="icon-subject"/>แผนการเรียน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyPerformance"?>"><img src="<?=base_url()."resources/img/performance.png"?>" class="icon-subject"/>ติดตามผลงาน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyNote"?>"><img src="<?=base_url()."resources/img/sheet.png"?>" class="icon-subject"/>คลัง Sheet </a>
            </div>
            <?php endif; ?>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyLog"?>"><img src="<?=base_url()."resources/img/loghistory.png"?>" class="icon-subject"/>ประวัติการเรียน </a>
            </div>
            <div class="myProfileBox active">
              <img src="<?=base_url()."resources/img/membership_active.png"?>" class="icon-subject"/><span>สถานะสมาชิก</span>
            </div>
          </section>
        </div>
      </div>
    </div>

    <div class="containner-1200 mx-auto my-4">
      <div class="containner-1200-padding">
      <?php $this->load->view('i_myProfileStatus', $this->data); ?>
      <section class="myProfile-form">
        <div class="d-flex justify-content-between flex-wrap mb-4">
          <div class="form-header f26">สถานะสมาชิก และ ข้อมูลการชำระเงิน</div>
          <button class="btn-40 btn-green-white saveChange membership">บันทึก</button>
        </div>
        <div class="row mb-4">
          <div class="col-md-6">
            <div class="d-flex justify-content-between align-items-center flex-wrap">
              <div class="form-header f22 membershipPayHeader">การชำระเงิน</div>
              <?php if($myProfile->subscriptionTypeId != 1){ ?>
              <div class="membershipActionDiv">
                <div data-toggle="modal" data-target="#changePackageModal" class="f16 f-blue changeMembertype" data-date="<?=(isset($currentExpire) ? (DateTime::createFromFormat('d-m-Y', $currentExpire))->format('d/m/Y') : '')?>" data-title="<?=(isset($currentSubscriberTitle) ? $currentSubscriberTitle : '')?>">เปลี่ยนแพ็กเกจ</div>
                <div data-toggle="modal" data-target="#cancelPackageModal" class="f16 f-a9 cancelMembertype" data-date="<?=(isset($currentExpire) ? (DateTime::createFromFormat('d-m-Y', $currentExpire))->format('d/m/Y') : '')?>" data-title="<?=(isset($currentSubscriberTitle) ? $currentSubscriberTitle : '')?>">ยกเลิกแพ็กเกจ</div>
              </div>
              <?php } ?>
            </div>
            <?php if($myProfile->subscriptionTypeId == 1){ ?>
            <a href="<?=base_url("Membertype")?>">
              <div class="bg-blue p-3 rounded divMemberPayment d-flex flex-column align-items-center justify-content-center">
                <div class="form-header f-w-500 f-white f22">สมัครแพ็กเกจ</div>
              </div>
            </a>
            <?php }else{ ?>
            <div class="bg-gray-f7 p-3 rounded divMemberPayment d-flex flex-column align-items-start justify-content-center">
              <div class="form-header f-w-500 f-blue f22">รายเดือน</div>
              <div class="form-header f-gray f18">
                <?php
                  if(isset($nextSubscriberTitle)){
                    echo (isset($currentSubscriberTitle) ? '<div>แพ็กเกจ: '.$currentSubscriberTitle.' หมดอายุ '.(DateTime::createFromFormat('d-m-Y', $currentExpire))->format('d/m/Y').'</div>' : '');
                    if($nextTotal == 0){
                      $promCode = '';
                      $startDate = '';
                      $endDate = '';
                      if(isset($myProfile->memberPackages->nextSubscriptionInfo->promotionUsage->promoCode)){
                        $promCode = ' ('.$myProfile->memberPackages->nextSubscriptionInfo->promotionUsage->promoCode.')';
                      }
                      if(isset($myProfile->memberPackages->nextSubscriptionInfo->packageStart)){
                        $startDate = ' เริ่มใช้งาน '.DateTime::createFromFormat('Y-m-d', $myProfile->memberPackages->nextSubscriptionInfo->packageStart)->format('d/m/Y');
                      }
                      if(isset($myProfile->memberPackages->nextSubscriptionInfo->packageEnd)){
                        $endDate = ' หมดอายุ '.DateTime::createFromFormat('Y-m-d', $myProfile->memberPackages->nextSubscriptionInfo->packageEnd)->format('d/m/Y');
                      }
                      echo (isset($nextSubscriberTitle) ? '<div>แพ็กเกจ: '.$nextSubscriberTitle.$promCode.$startDate.$endDate.'</div>' : '');
                    }else{
                      echo (isset($nextSubscriberTitle) && isset($nextBill) ? '<div>แพ็กเกจ: '.$nextSubscriberTitle.' เริ่มใช้งาน '.(DateTime::createFromFormat('d-m-Y', $nextBill))->format('d/m/Y').'</div>' : '');
                      echo (isset($nextBill) ? '<div>การชำระครั้งถัดไป: '.(DateTime::createFromFormat('d-m-Y', $nextBill))->format('d/m/Y').'</div>' : '');
                    }
                  }else if(isset($nextBill)){
                    echo (isset($currentSubscriberTitle) ? '<div>แพ็กเกจ: '.$currentSubscriberTitle.'</div>' : '');
                    echo (isset($nextBill) ? '<div>การชำระครั้งถัดไป: '.(DateTime::createFromFormat('d-m-Y', $nextBill))->format('d/m/Y').'</div>' : '');
                  }else{
                    if($currentTotal == 0){
                      $promCode = '';
                      $startDate = '';
                      $endDate = '';
                      if(isset($myProfile->memberPackages->currentSubscriptionInfo->promotionUsage->promoCode)){
                        $promCode = ' ('.$myProfile->memberPackages->currentSubscriptionInfo->promotionUsage->promoCode.')';
                      }
                      if(isset($myProfile->memberPackages->currentSubscriptionInfo->packageStart)){
                        $startDate = ' เริ่มใช้งาน '.DateTime::createFromFormat('Y-m-d', $myProfile->memberPackages->currentSubscriptionInfo->packageStart)->format('d/m/Y');
                      }
                      if(isset($myProfile->memberPackages->currentSubscriptionInfo->packageEnd)){
                        $endDate = ' หมดอายุ '.DateTime::createFromFormat('Y-m-d', $myProfile->memberPackages->currentSubscriptionInfo->packageEnd)->format('d/m/Y');
                      }
                      echo (isset($currentSubscriberTitle) ? '<div>แพ็กเกจ: '.$currentSubscriberTitle.$promCode.$startDate.$endDate.'</div>' : '');
                    }else{
                      echo (isset($currentSubscriberTitle) && isset($currentExpire) ? '<div>แพ็กเกจ: '.$currentSubscriberTitle.' หมดอายุ '.(DateTime::createFromFormat('d-m-Y', $currentExpire))->format('d/m/Y').'</div>' : '');
                      echo (isset($currentSubscriberTitle) && isset($currentExpire) ? '<div>แพ็กเกจ: Free เริ่มใช้งาน '.(DateTime::createFromFormat('d-m-Y', $currentExpire))->modify('+1 day')->format('d/m/Y').'</div>' : '');
                      echo (isset($currentSubscriberTitle) && isset($currentExpire) ? '<div>(สถานะสมาชิกเปลี่ยนเป็น Guest)</div>' : '');
                    }
                  }
                ?>
              </div>
            </div>
            <?php } ?>
          </div>
          <div class="col-md-6">
            <div class="form-header f22 membershipAmountHeader">ยอดชำระเงิน</div>
            <div class="bg-gray-f7 p-3 rounded divMemberPayment d-flex flex-column align-items-center justify-content-center">
              <?php
              if($myProfile->subscriptionTypeId == 1){
                echo '<div class="form-header f-w-500 f-blue f22">0 บาท</div>';
              }else if(isset($nextBill)){
                //$remark = json_decode($subscription->remark);
                $remark = '';
                if(isset($subscription->membersubscription->discount) && $subscription->membersubscription->discount > 0){
                  $priceBefore = $subscription->membersubscription->price/100;
                }
                echo '<div class="form-header f-w-500 f-blue f22">
                '.(isset($subscription->charge->amount) ? ($subscription->charge->amount/100).'<span class="f14"> บาท</sapn>' : '').'
                </div>
                <div class="form-header f-a9 f14">
                  '.(isset($priceBefore) ? 'ปกติ <span class="midLine">'.$priceBefore.'</span> บาท' : '').'
                </div>';
              }else{
                echo '<div class="form-header f-w-500 f-blue f22">0 บาท</div>';
              }
              ?>
            </div>
          </div>
          <?php if($myProfile->subscriptionTypeId != 1){ ?>
          <div class="col-md-6">
            <div class="bg-gray-f7 mt-4 p-3 rounded divMemberPayment d-flex justify-content-between">
              <div class="d-flex flex-column align-items-start justify-content-center">
                <div class="form-header f-w-500 f22">ข้อมูลการชำระเงิน</div>
                <div class="form-header f-gray f18">ดำเนินการโดย Omise</div>
              </div>
              <div class="align-self-end">
                <a class="f16 f-a9 changePayType" href="<?= base_url('Payment/change_card') ?>">เปลี่ยนบัตรเครดิต/เดบิต</a>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="f-blue">ชื่อ - นามสกุล</label>
              <input type="text" class="form-control" id="fullname" placeholder="ชื่อ - นามสกุล" value="<?=$fullname?>" >
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="f-blue">เบอร์โทรศัพท์</label>
              <input type="text" class="form-control" id="mobile" placeholder="เบอร์โทรศัพท์" value="<?=$mobile?>" >
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="f-blue">ที่อยู่</label>
              <textarea id="addr" class="form-control" rows="3" placeholder="ระบุ ที่อยู่"><?=$address?></textarea>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="f-blue">จังหวัด</label>
                <select id="provinceId" class="custom-select form-control f18">
                  <option value=""> ระบุ จังหวัด</option>
                  <?php
                  foreach($dataProvince->result() as $row){
                    echo "<option value='".$row->id."' ".(($row->id == $provinceId) ? 'selected' : '').">".$row->name_th."</option>";
                  }
                  ?>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="f-blue">เขต / อำเภอ</label>
                <select id="amphureId" class="custom-select form-control f18">
                  <option value=""> ระบุ เขต / อำเภอ</option>
                  <?php
                  foreach($dataAmphure->result() as $row){
                    echo "<option value='".$row->id."' ".(($row->id == $amphureId) ? 'selected' : '').">".$row->name_th."</option>";
                  }
                  ?>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="f-blue">แขวง / ตำบล</label>
                <select id="districtId" class="custom-select form-control f18">
                  <option value=""> ระบุ แขวง / ตำบล</option>
                  <?php
                  foreach($dataDistrict->result() as $row){
                    echo "<option value='".$row->id."' ".(($row->id == $districtId) ? 'selected' : '').">".$row->name_th."</option>";
                  }
                  ?>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="f-blue">รหัสไปรษณีย์</label>
                <select id="zipCode" class="custom-select form-control f18">
                  <option value=""> ระบุ รหัสไปรษณีย์</option>
                  <?php
                  foreach($dataZipCode->result() as $row){
                    echo "<option value='".$row->zip_code."' ".(($row->zip_code == $zipCode) ? 'selected' : '').">".$row->zip_code."</option>";
                  }
                  ?>
              </select>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<!-- Modal -->
<div class="modal fade" id="changePackageModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fa fa-times" aria-hidden="true"></i>
        </button>
        <div class="packageTxtModal center f-w-500 mt-5"></div>
        <div class="dateTxtModal center mb-3"></div>
        <div class="center mb-5 btnTrue">
          <button class="btn-40 btn-black mb-2 changePackageBtn">เปลี่ยนแพ็กเกจ</button>
        </div>
        <div class="center mb-5 btnFalse">
          <div class="txt1">หากต้องการเปลี่ยนแพ็กเกจใหม่</div>
          <div class="txt2 mb-3">สามารถทำได้ในเวลา 10.00 น. เป็นต้นไป</div>
          <button class="btn-40 btn-black mb-2 btnAccept" data-dismiss="modal">รับทราบ</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="cancelPackageModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fa fa-times" aria-hidden="true"></i>
        </button>
        <div class="packageTxtModal center f-w-500 mt-5"></div>
        <div class="dateTxtModal center mb-3"></div>
        <div class="center mb-5 btnTrue">
          <button class="btn-40 btn-black mb-2 cancelPackageBtn">ยืนยันยกเลิกแพ็กเกจ</button>
        </div>
        <div class="center mb-5 btnFalse">
          <div class="txt1">หากต้องการยกเลิกแพ็กเกจ</div>
          <div class="txt2 mb-3">สามารถทำได้ในเวลา 10.00 น. เป็นต้นไป</div>
          <button class="btn-40 btn-black mb-2 btnAccept" data-dismiss="modal">รับทราบ</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var chkPassword = false;
  if($( window ).width() < 768){
    $(".myProfile-slide").slick('slickGoTo', 4);
  }
  $(document).ready(function() {
    $('.btnTrue').hide();
    $('.btnFalse').hide();
    $('#changePackageModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var title = button.data('title')
      var date = button.data('date')
      var modal = $(this)
      modal.find('.packageTxtModal').text('แพ็กเกจ: ' + title)
      modal.find('.dateTxtModal').text('ใช้งานได้ถึงวันที่ ' + date)
      var opts = {
        method: 'POST',
        headers: {
          "Authorization": "Bearer <?=$token;?>"
        }
      }
      fetch('<?=$this->config->item('api_url').'members/can-sub-unsub-package'?>', opts).then(function (response) {
        var data = response.json();
        data.then(function(result) {
          if(result.status){
            modal.find('.btnTrue').show();
          }else{
            modal.find('.btnFalse').show();
          }
        }) //data.then
      }) //fetch
    }) //#changePackageModal

    $('#changePackageModal').on('hidden.bs.modal', function (event) {
      var modal = $(this)
      modal.find('.btnTrue').hide();
      modal.find('.btnFalse').hide();
    }) //#changePackageModal

    $('#cancelPackageModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var title = button.data('title')
      var date = button.data('date')
      var modal = $(this)
      modal.find('.packageTxtModal').text('แพ็กเกจ: ' + title)
      modal.find('.dateTxtModal').text('ใช้งานได้ถึงวันที่ ' + date)
      var opts = {
        method: 'POST',
        headers: {
          "Authorization": "Bearer <?=$token;?>"
        }
      }
      fetch('<?=$this->config->item('api_url').'members/can-sub-unsub-package'?>', opts).then(function (response) {
        var data = response.json();
        data.then(function(result) {
          if(result.status){
            modal.find('.btnTrue').show();
          }else{
            modal.find('.btnFalse').show();
          }
        }) //data.then
      }) //fetch
    }) //#cancelPackageModal

    $('#cancelPackageModal').on('hidden.bs.modal', function (event) {
      var modal = $(this)
      modal.find('.btnTrue').hide();
      modal.find('.btnFalse').hide();
    }) //#cancelPackageModal

  }); //$(document).ready

  $(".changePackageBtn").on('click', function() {
    window.location.href = '<?=base_url().'Membertype?membership'?>';
  });

  $(".cancelPackageBtn").on('click', function() {
    var opts = {
      method: 'POST',
      headers: {
        "Authorization": "Bearer <?=$token;?>"
      }
    }
    fetch('<?=$this->config->item('api_url').'members/unsubscribe-package'?>', opts).then(function (response) {
      if(response.ok){
        location.reload();
      }else{
        location.reload();
      }
    })
  });

  $('#provinceId').change(function(){
    var provinceId = $(this).val();
    $.ajax({
      url: "<?=base_url()."MyProfile/getAmphureHTML"?>",
      type: "post",
      data: {provinceId:provinceId} ,
      success: function (data) {
        $('#amphureId').html(data);
        $('#districtId').html('<option value=""> ระบุ แขวง / ตำบล</option>');
        $('#zipCode').html('<option value=""> ระบุ รหัสไปรษณีย์</option>');
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });
  });

  $('#amphureId').change(function(){
    var amphureId = $(this).val();
    $.ajax({
      url: "<?=base_url()."MyProfile/getDistrictHTML"?>",
      type: "post",
      data: {amphureId:amphureId} ,
      success: function (data) {
        $('#districtId').html(data);
        $('#zipCode').html('<option value=""> ระบุ รหัสไปรษณีย์</option>');
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });
  });

  $('#districtId').change(function(){
    var districtId = $(this).val();
    $.ajax({
      url: "<?=base_url()."MyProfile/getZipCodeHTML"?>",
      type: "post",
      data: {districtId:districtId} ,
      success: function (data) {
        $('#zipCode').html(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });
  });

  $('.saveChange').click(function(){
    var memberId = '<?=$myProfile->id?>';
    var fullname = $.trim($('#fullname').val());
    var mobile = $.trim($('#mobile').val());
    var addr = $.trim($('#addr').val());
    var provinceId = $.trim($('#provinceId').val());
    var amphureId = $.trim($('#amphureId').val());
    var districtId = $.trim($('#districtId').val());
    var zipCode = $.trim($('#zipCode').val());

    if(fullname == ''){
      alert('กรุณากรอกชื่อ-นามสกุล');
    }else if(mobile == ''){
      alert('กรุณากรอกหมายเลขโทรศัพท์');
    }else if(addr == ''){
      alert('กรุณากรอกที่อยู่');
    }else if(provinceId == ''){
      alert('กรุณาเลือกจังหวัด');
    }else if(amphureId == ''){
      alert('กรุณาเลือกเขต/อำเภอ');
    }else if(districtId == ''){
      alert('กรุณาเลือกแขวง/ตำบล');
    }else if(zipCode == ''){
      alert('กรุณาเลือกรหัสไปรษณีย์');
    }else{
      $.ajax({
        url: "<?=base_url()."MyProfile/updateMembership"?>",
        type: "post",
        data: {memberId:memberId,fullname:fullname,mobile:mobile,addr:addr,provinceId:provinceId,amphureId:amphureId,districtId:districtId,zipCode:zipCode} ,
        success: function (data) {
          alert(data);
          location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    }
  });

  $( "#profile" ).click(function() {
    $('#profileBox').toggle('slow');
  });
</script>