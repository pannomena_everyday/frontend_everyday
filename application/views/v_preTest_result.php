<div id="wrap">
  <div id="container">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <section class="preTest-result">
          <!-- <div class="preTest-result-info"><img src="<?=base_url()."resources/img/information.png"?>"/></div> -->
          <img src="<?=base_url()."resources/img/PreTest_Pass.png"?>" />
          <div class="f-green f30 m-t-20">แผนการเรียนนี้เหมาะสมกับนักเรียนแล้ว</div>
          <!-- <div class="f24">ม.ต้น / คณิตศาสตร์ / พื้นฐาน </div> -->
          <div class="preTest-result-box">
            <div class="row">
              <div class="col-6 col-sm-6 col-md-3 box boxResult1">
                <div class="resultHeader f-blue">คิดเป็นเปอร์เซ็นต์</div>
                <div class="resultDetail rainbow-text"><?= round($score/$total_score*100) ?>%</div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult2">
                <div class="resultHeader">คะแนนที่ได้</div>
                <div class="resultDetail"><?= $score ?></div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult3">
                <div class="resultHeader">คะแนนเต็ม</div>
                <div class="resultDetail"><?= $total_score ?></div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult4">
                <div class="resultHeader">เวลา (นาที)</div>
                <div class="resultDetail"><?= fmtMSS($duration)?></div>
              </div>
            </div>
          </div>
          <a href="#"><button class="btn-60 btn-green-white m-w-400"  onclick="addPlan('<?=$pass_plan?>')">เริ่มเรียนเลย!</button></a>
        </section>
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->
<?php
function fmtMSS($seconds) {
  $t = round($seconds);
  return sprintf('%02d:%02d',($t/60), $t%60);
}
?>
<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script type="text/javascript">
  function addPlan(plan){
    $.ajax({
      type: "POST",
      url: "<?=base_url()."PreTest/AddPlan";?>",
      data: {plan},
      dataType: "json",
      success: function (response) {
        window.location = "<?=base_url()."MyEveryDay";?>"
      }
    });
  }
</script>