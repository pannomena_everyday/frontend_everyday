<script src="<?=base_url('resources/js/jquery-1.11.0.js');?>"></script>
<script src="<?=base_url('resources/jquery-ui-1.12.1/jquery-ui.min.js');?>"></script>
<script src="<?=base_url('resources/bootstrap-4.5.0-dist/js/bootstrap.bundle.min.js');?>"></script>
<script src="<?=base_url('resources/OwlCarousel2-2.3.4/dist/owl.carousel.js');?>"></script>
<script src="<?=base_url("resources/js/jv_localization/messages_th.min.js").'?v='.date('Y-m-d h:i:s') ?>"></script>
<script src="<?=base_url("resources/js/bootstrap-notify.min.js").'?v='.date('Y-m-d h:i:s') ?>"></script>
<script src="<?=base_url("resources/js/pace.min.js").'?v='.date('Y-m-d h:i:s') ?>"></script>

<?php if (isset($external_scripts)) : ?>
  <?php foreach ($external_scripts as $external_script):?>
    <script src="<?=$external_script ?>"></script>
  <?php endforeach; ?>
<?php endif; ?>

<?php if (isset($page_scripts)) : ?>
  <?php foreach ($page_scripts as $page_script):?>
    <script src="<?=base_url("assets/js/$page_script") ?>"></script>
  <?php endforeach; ?>
<?php endif; ?>

<?php if (isset($inpage_script)) : ?>
  <script type="text/javascript">
    <?php $this->load->view($inpage_script); ?>
  </script>
<?php endif; ?>

<script type="text/javascript">
  <?php if (!empty($message)): ?>
    $.notify({ message: '<?=$message ?>' },{ type: 'success', placement: { from: "top", align: "center" }, delay: 3000});
  <?php elseif (!empty($error)): ?>
    $.notify({ message: '<?=$error ?>' },{ type: 'danger', placement: { from: "top", align: "center" }, delay: 3000});
  <?php endif; ?>
</script>