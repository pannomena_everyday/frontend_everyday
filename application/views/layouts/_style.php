<link rel="shortcut icon" type="image/png" href="<?=base_url('resources/img/fav_everyday.png');?>">
<link rel="stylesheet" href="<?=base_url('resources/jquery-ui-1.12.1/jquery-ui.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/bootstrap-4.5.0-dist/css/bootstrap.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/fonts/stylesheet.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/font-awesome-4.7.0/css/font-awesome.min.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/css/style.css');?>">
<link rel="stylesheet" href="<?=base_url("resources/css/pace-theme.css").'?v='.date('Y-m-d h:i:s') ?>">

<?php if (isset($external_styles)) : ?>
  <?php foreach ($external_styles as $external_style):?>
    <link rel="stylesheet" href="<?=$external_style ?>">
  <?php endforeach; ?>
<?php endif; ?>
<?php if (isset($page_styles)) : ?>
  <?php foreach ($page_styles as $page_style):?>
    <link rel="stylesheet" href="<?=base_url("assets/css/$page_style"); ?>">
  <?php endforeach; ?>
<?php endif; ?>