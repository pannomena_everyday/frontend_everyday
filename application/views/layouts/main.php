<?php
if(get_cookie('token') != null && !isset($isLogout)){
    $isLogin = true;
}else{
    $isLogin = false;
}
if(!isset($page_active)){$page_active = "";}
$api_url = $this->config->item('api_url');
function getFileUrl($api_url, $filename){
  $headers = array("accept: application/json",'Content-Type: application/json');
  $method = 'GET';
  $url = $api_url.'gen-signed-url/'.$filename;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
  $response = curl_exec($curl);
  curl_close($curl);
  return json_decode($response);
}
?>
<!DOCTYPE html>
<html id="everyday" lang="th">
  <head>
    <!-- Title and Meta -->
    <?php $this->load->view('layouts/_title_and_meta'); ?>
    <!-- Styles -->
    <?php $this->load->view('layouts/_style'); ?>
    <!-- Analytic -->
    <?php $this->load->view('layouts/_analytic'); ?>
    <style type="text/css">
        /* Menu */
        body{transition: margin-right .51s;}
        .sidenav { height: 100%; width: 0; position: fixed; z-index: 1; top: 0; right: 0; background-color: #111; overflow-x: hidden; transition: 0.5s; padding-top: 60px; z-index: 999;}
        .sidenav a { padding: 8px 8px 8px 32px; text-decoration: none; font-size: 30px; color: #818181; display: block; transition: 0.3s; }  
        .sidenav a:hover { color: #f1f1f1; }
        .sidenav .closebtn { position: absolute; top: 0; right: 25px; font-size: 36px; margin-left: 50px;}
        #main_menu div.menuList a.dropdown-item{color: #000;}
        .input-group input.inp-search{/*width: 100%; */background-color: #000; color: #fff;}
        .input-group input.inp-search:focus{background-color: #000; color: #fff; box-shadow: none;}
        .input-group input.inp-search::placeholder {color: #fff; opacity: 1;}
        .input-group > .input-group-append > .btn{border-radius: 0;}
        .btn-outline-secondary:hover {background-color: #000;}
        @media screen and (max-height: 450px) { .sidenav {padding-top: 15px;} .sidenav a {font-size: 18px;} }
        #profileBox{display: none;width: 300px; font-size: 22px; box-shadow: 0px 3px 6px #00000029;border-radius: 10px;opacity: 1;}
        #profileBox div{padding: 10px 20px;}
        /* End menu */
        /* Chat-overlay */
        .chat-overlay {
          position: fixed;
          bottom: 24px;
          right: 24px;
          width: 60px;
          height: 60px;
          z-index: 2147483645;
          cursor: pointer;
          pointer-events: none;
        }
        /* End Chat-overlay */
    </style>
    <script>
      /* Menu */
      function openNav() {
        $('#mySidenav').css({"width": "30%"});
        $('#mySidenav').removeClass("navHide");
        $('#mySidenav').addClass("navShow");
        $('body').css({/*"margin-right": "30%","opacity": "0.7"*/});
      }
      function closeNav() {
        $('#mySidenav').css({"width": "0px"});
        $('#mySidenav').removeClass("navShow");
        $('#mySidenav').addClass("navHide");
        $('body').css({/*"margin-right": "0px","opacity": "1"*/});
      }
      /* End menu */
    </script>
  </head>
  <body class="">
    <div id="header" class="containner-padding d-flex justify-content-between align-items-center">
        <!-- Logo -->
        <div class="logo mr-4">
          <a href="<?=base_url('#');?>"><img src="<?=base_url('resources/img/logo_everyday.png')?>" class="logo-monkey" /></a>
        </div>
        <!-- Search -->
        <div class="input-search p-2 flex-grow-1">
          <?php if($isLogin){ ?>
            <div class="row">
              <div class="input-group m-w-1000 col-md-12 gradient-box">
                <input id="search" type="search" onsearch="clearSearch()" aria-haspopup="true" aria-expanded="false" autocomplete="off" class="form-control inp-search py-2 border-0" placeholder="ค้นหาบทเรียน">
                <div id="DropdownSearch" class="txtsearch dropdown-menu" aria-labelledby="search"></div>
                <span class="input-group-append">
                  <button class="btn-search btn btn-outline-secondary border-0 text-white" type="button">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
              </div>
            </div>
          <?php } ?>
        </div>
      <!-- Menu -->
        <div id="main_menu">
          <div class="d-flex justify-content-end align-items-center flex-wrap"> <!-- mini-flex -->
            <?php if($isLogin){ ?>
              <?php $token = get_cookie('token');
                    $this->main_model->checkToken($token);
                    $myProfile = $this->main_model->curlRequest("GET", $this->config->item('api_url').'members/me',null,$token); ?>
              <?php if($myProfile->subscriptionTypeId != 1){ ?>
              <div class="menuList ml-3 ml-md-4 m-menu-hide">
                <a href="<?=base_url()."MyEveryDay"?>">
                  <?php
                  if($page_active == "myEveryDay"){
                      echo "<img src='".base_url('resources/img/myeveryday_active.png')."' class='icon-nav'>";
                  }else{
                      echo "<img src='".base_url('resources/img/myeveryday.png')."' class='icon-nav'>";
                  }
                  ?>
                  <p class="f-white">myEveryday </p>
                </a>
              </div>
              <?php } ?>
            <?php } ?>
            <?php if($isLogin): ?>
              <div class="menuList ml-3 ml-md-4 m-menu-hide">
                <a href="<?=base_url()."Library"?>">
                  <?php
                  if($page_active == "library"){
                      echo "<img src='".base_url('resources/img/library_active.png')."' class='icon-nav'>";
                  }else{
                      echo "<img src='".base_url('resources/img/library.png')."' class='icon-nav'>";
                  }
                  ?>
                  <p class="f-white">myLibrary </p>
                </a>
              </div>
            <?php else: ?>
              <div class="menuList ml-3 ml-md-4">
                <a href="<?=base_url('Index/SignIn')?>">
                  <?php
                  if($page_active == "library"){
                      echo "<img src='".base_url('resources/img/library_active.png')."' class='icon-nav'>";
                  }else{
                      echo "<img src='".base_url('resources/img/library.png')."' class='icon-nav'>";
                  }
                  ?>
                  <p class="f-white m-menu-hide">myLibrary </p>
                </a>
              </div>
            <?php endif; ?>
            <?php if($isLogin){ ?>
              <div class="menuList ml-3 ml-md-4 m-menu-hide">
                <a href="<?=base_url()."myexam"?>">
                  <img src="<?=base_url('resources/img/myexam.png')?>" class='icon-nav'/>
                  <p class="f-white">myExam</p>
                </a>
              </div>
            <?php } ?>
            <?php if($isLogin){ ?>
              <div class="menuList ml-3 ml-md-4 menuMember dropdown">
                <div class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php
                  $profileImg = get_cookie('profileImg');
                  if($profileImg != ''){
                    echo '<img class="rounded-circle" src="'.getFileUrl($api_url,$profileImg)[0].'"/>';
                  }else{
                    echo '<img class="rounded-circle" src="'.base_url().'resources/img/profile_pic.png"/>';
                  }
                  ?>
                  <p class="f-white"><?=get_cookie('name')?></p>
                </div>
                <div class="dropdown-menu dropdown-menu-right" >
                  <a class="dropdown-item" href="<?=base_url()."MyProfile"?>"><img class="icon" src="<?=base_url('resources/img/icon_user.svg')?>"> บัญชีผู้ใช้ </a>
                  <!-- <a class="dropdown-item" href="<?=base_url()."MyProfile/MyPlan"?>"><img src="<?=base_url('resources/img/list_menu_2.png')?>"> แผนการเรียน </a>
                  <a class="dropdown-item" href="<?=base_url()."MyProfile/MyPerformance"?>"><img src="<?=base_url('resources/img/list_menu_3.png')?>"> ติดตามผลงาน </a>
                  <a class="dropdown-item" href="<?=base_url()."MyProfile/MyNote"?>"><img src="<?=base_url('resources/img/list_menu_4.png')?>"> คลัง Sheet</a>
                  <a class="dropdown-item" href="<?=base_url()."MyProfile/MyLog"?>"><img src="<?=base_url('resources/img/list_menu_5.png')?>"> ประวัติการเรียน </a>
                  <a class="dropdown-item" href="#"><img src="<?=base_url('resources/img/list_menu_6.png')?>"> สถานะสมาชิก </a> -->
                  <a class="dropdown-item" href="<?=base_url()."myexam/profile"?>"><img class="icon" src="<?=base_url('resources/img/icon_history.svg')?>"> ประวัติการสอบ </a>
                  <?php if($myProfile->subscriptionTypeId == 1){ ?>
                    <a class="dropdown-item menu-hilight" href="<?=base_url()."Membertype"?>"><img class="icon" src="<?=base_url('resources/img/icon_change.svg')?>"> เปลี่ยนแพ็กเกจ </a>
                  <?php } ?>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item btnLogOut" href="#"><img class="icon" src="<?=base_url('resources/img/icon_logout.svg')?>"> ออกจากระบบ </a>
                </div>
              </div>
            <?php }else{ ?>
              <div class="menuList ml-3 ml-md-4">
                <a href="<?=base_url()."Index/SignIn"?>">
                  <img src="<?=base_url('resources/img/login.png')?>" class="icon-nav">
                  <p class="f-white m-menu-hide">เข้าสู่ระบบ </p>
                </a>
              </div>
            <?php } ?>
        <!-- Nav Btn -->
        <div class="menuList ml-3 ml-md-4">
            <div class="menuHamberg" onclick="openNav()">
                  <img src="<?=base_url('resources/img/menu.png')?>" />
                </div>
            </div>
          </div> <!-- end mini-flex -->
      </div>
      <!-- Side Nav -->
      <div id="mySidenav" class="sidenav fonts_th">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <?php if($isLogin){ ?>
              <div class="m-menu-show">
                <!-- <a href="<?=base_url()."Search"?>">
                  <img src="<?=base_url('resources/img/icon_search.png')?>" class=""> Search
                </a> -->
                <div class="px-3">
                  <div class="input-group m-w-1000 col-md-12 gradient-box">
                    <input id="search3" type="search" onsearch="clearSearch()" aria-haspopup="true" aria-expanded="false" autocomplete="off" class="form-control inp-search py-2 border-0" placeholder="ค้นหาบทเรียน">
                    <div id="DropdownSearch3" class="txtsearch3 dropdown-menu" aria-labelledby="search"></div>
                    <span class="input-group-append">
                      <button class="btn-search3 btn btn-outline-secondary border-0 text-white bg-black" type="button">
                        <i class="fa fa-search"></i>
                      </button>
                    </span>
                  </div>
                </div>
              </div>
              <?php if($myProfile->subscriptionTypeId != 1){ ?>
              <a class="m-menu-show" href="<?=base_url()."MyEveryDay"?>">myEveryday </a>
              <?php } ?>
              <a class="m-menu-show"  href="<?=base_url()."Library"?>">myLibrary</a>
              <a class="m-menu-show"  href="<?=base_url()."myexam"?>">myExam</a>
            <?php }else{ ?>
              <a class="m-menu-show"  href="<?=base_url()."Index/SignIn"?>">myLibrary</a>
              <a class="m-menu-show"  href="<?=base_url()."Index/SignIn"?>">เข้าสู่ระบบ</a>
            <?php }?>
            <a href="<?=base_url('AboutUs')?>">เกี่ยวกับเรา </a>
            <a href="<?=base_url('Contact')?>">ติดต่อเรา </a>
            <!-- <a href="#">ร่วมงานกับเรา </a> -->
        </div>
    </div>

    <!-- /#page-wrapper -->
    <?php if(isset($view_name)): ?>
      <?php $this->load->view($view_name); ?>
    <?php else: ?>
      <div id="wrap">No View Specified</div>
    <?php endif; ?>
    <!-- <div style="position: relative; width: 100%; max-width: 1920px; margin: 0 auto;">
      <div class="helpBtn" data-toggle="collapse" data-target="#formReport" aria-expanded="false" aria-controls="formReport">
        <img src="<?=base_url('resources/img/help.png')?>" />
      </div>
      <div id="formReport" class="indexReport collapse">
        <div class="rpHeader">
          <p>รายงานปัญหาการใช้งาน </p>
          <div class="rpCloseBtn"><i class="fa fa-times" aria-hidden="true"></i></div>
        </div>
          <div class="rpDetail">
            <select class="rp_header">
              <option value=""> เลือกหัวข้อ </option>
              <option value="1"> ปัญหาการใช้งาน </option>
              <option value="2"> ข้อมูลไม่ถูกต้อง </option>
            </select>
            <input type="text" placeholder="อีเมล์" class="rp_email" />
            <textarea class="rp_detail"></textarea>
            <button class="rpBtn"> ส่ง </button>
          </div>
      </div>
    </div> -->
    <div id="footer" class="containner-padding d-flex flex-column flex-md-row justify-content-center justify-content-md-between align-items-center">
        <!-- <div class="footer-left"><a href="<?=base_url()."terms"?>">Terms of use</a> | Privacy Policy</div> -->
        <div class="footer-left">
          <a href="<?=base_url()."terms"?>">Terms of use</a> 
          | <a href="<?=base_url()."privacypolicy"?>">Privacy Policy</a>
          | <a href="<?=base_url()."paymentterms"?>">Payment and Promotion Policy</a>
        </div>
        <div class="footer-right">Copyright © 2021 Monkey Everyday Co., Ltd. All rights reserved</div>
    </div>

    <div class="clearfix"></div>
    <!-- Javascript files -->
    <?php $this->load->view('layouts/_script');?>
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <img src="<?=base_url('resources/img/my_chat.png')?>" class="chat-overlay">
    <script>
      window.fbAsyncInit = function () {
        FB.init({
          xfbml: true,
          version: 'v9.0',
        });
      };

      (function (d, s, id) {
        var js,
          fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      })(document, 'script', 'facebook-jssdk');
    </script>

    <!-- Your Chat Plugin code -->
    <div
      class="fb-customerchat"
      attribution="setup_tool"
      page_id="102519165032189"
      theme_color="#0A7CFF"
      logged_in_greeting="สวัสดีครับ! หากมีข้อสงสัยสามารถสอบถามได้เลยนะครับ"
      logged_out_greeting="สวัสดีครับ! หากมีข้อสงสัยสามารถสอบถามได้เลยนะครับ"
    ></div>
  </body>
  <?php if(get_cookie('EverydayAcceptCookie') == null){ ?>
  <div id="cookie" class="containner-padding">
    <p class="p-2">เว็บไซต์นี้มีการเก็บข้อมูลการใช้งานเว็บไซต์ (Cookies) เพื่อมอบบริการที่ดีที่สุดสำหรับคุณ โดยการเข้าใช้งานเว็บไซต์ ถือเป็นการยอมรับในเงื่อนไขการใช้งานเว็บไซต์</p>
    <button id="acceptCookie">ยอมรับ</button>
  </div>
  <?php }  ?>
  <script>
      $(document).ready(function () {
        // Search 1
        $("#search").keyup(function () {
          var keyword = $.trim($("#search").val());
          if(keyword.length >= 3){
            $.ajax({
                type: "POST",
                url: "<?=base_url()."Search/GetSearchData";?>",
                data: {keyword: keyword},
                dataType: "json",
                success: function (data) {
                  $('#DropdownSearch').empty();
                  $.each(data, function (key,value) {
                      if (data.length >= 0)
                      //$('#DropdownSearch').append('<li role="displaySearch" ><a role="menuitem dropdownSearchli" class="dropdownlivalue">' + value['ProvinceName'] + '</a></li>');
                      $('#DropdownSearch').append('<button class="dropdown-item dropdownlivalue" type="button">' + value['title'] + '</button>');
                  });
                  if (data.length > 0) {
                      $('#search').attr("data-toggle", "dropdown");
                      $('#DropdownSearch').dropdown('toggle');
                      $('#DropdownSearch').show();
                  }else{
                    $('#DropdownSearch').hide();
                  }
                  $('#search').delay( 100 ).removeAttr('data-toggle');
                }
            });
          }else{
            $('#DropdownSearch').empty();
            $('#search').removeAttr('data-toggle');
            $('#DropdownSearch').hide();
          }
        });

        $('.txtsearch').on('click', 'button', function () {
            $('#DropdownSearch').hide();
            $('#search').val($(this).text());
            $('#search').removeAttr('data-toggle');
            searchEvent();
        });
        // End Search1

        // Search 3
        $("#search3").keyup(function () {
          var keyword = $.trim($("#search3").val());
          if(keyword.length >= 3){
            $.ajax({
                type: "POST",
                url: "<?=base_url()."Search/GetSearchData";?>",
                data: {keyword: keyword},
                dataType: "json",
                success: function (data) {
                  $('#DropdownSearch').empty();
                  $.each(data, function (key,value) {
                      if (data.length >= 0)
                      $('#DropdownSearch3').append('<button class="dropdown-item dropdownlivalue" type="button">' + value['title'] + '</button>');
                  });
                  if (data.length > 0) {
                      $('#search').attr("data-toggle", "dropdown");
                      $('#DropdownSearch3').dropdown('toggle');
                      $('#DropdownSearch3').show();
                  }else{
                    $('#DropdownSearch3').hide();
                  }
                  $('#search3').delay( 100 ).removeAttr('data-toggle');
                }
            });
          }else{
            $('#DropdownSearch3').empty();
            $('#search3').removeAttr('data-toggle');
            $('#DropdownSearch3').hide();
          }
        });

        $('.txtsearch3').on('click', 'button', function () {
            $('#DropdownSearch3').hide();
            $('#search3').val($(this).text());
            $('#search3').removeAttr('data-toggle');
            searchEvent3();
        });
        // End Search 2

      }); // Document on ready

      $("#search").on('keyup', function (e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
          searchEvent();
        }
      });

      $('.btn-search').click(function(){
        searchEvent();
      });

      function searchEvent(){
        var keyword = $.trim($("#search").val());
        if(keyword.length > 0){
          $.ajax({
            url: "<?=base_url()."Search/SearchEncode"?>",
            type: "post",
            data: {keyword:keyword} ,
            success: function (response) {
              window.location.href = "<?=base_url().'Search/index?keyword='?>"+response;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
          });
        }
      }

      $("#search3").on('keyup', function (e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
          searchEvent3();
        }
      });

      $('.btn-search3').click(function(){
        searchEvent3();
      });

      function searchEvent3(){
        var keyword = $.trim($("#search3").val());
        if(keyword.length > 0){
          $.ajax({
            url: "<?=base_url()."Search/SearchEncode"?>",
            type: "post",
            data: {keyword:keyword} ,
            success: function (response) {
              window.location.href = "<?=base_url().'Search/index?keyword='?>"+response;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
          });
        }
      }

      $('#acceptCookie').click(function(){
        $.ajax({
          url: "<?=base_url()."Index/acceptCookie"?>",
          type: "post",
          data: {} ,
          success: function (response) {
            $('#cookie').hide();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });
      });

      if($('.menuList').hasClass('library-active'))
        $('.library').find('img').attr('src', '<?=base_url('resources/img/library_active.png')?>');
      else
        $('.library').find('img').attr('src', '<?=base_url('resources/img/library.png')?>');

      $('.btnLogOut').click(function(){
        $.ajax({
          url: "<?=base_url()."Index/LogOut"?>",
          type: "post",
          data: {user:""} ,
          success: function (response) {
            window.location.href = "<?=base_url('Index/SignIn')?>";
            //location.reload();
            // You will get response from your PHP page (what you echo or print)
          },
          error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
          }
        });
      });

      $('.rpBtn').click(function(){
          var rp_header = $('.rp_header').val();
          var rp_detail = $('.rp_detail').val();
          var rp_email = $('.rp_email').val();
          if(rp_header == ""){
			alert('Please select header');
      }else if(rp_email == ""){
      alert('Please input email');
      }else if(rp_detail == ""){
      alert('Please input detail');
      }else{
        $.ajax({
          url: "<?=base_url()."Index/sendReport"?>",
          type: "post",
          data: {rp_header:rp_header,rp_detail:rp_detail,rp_email:rp_email} ,
          success: function (response) {
            alert(response);
          $('#formReport').collapse('hide');
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });
      }
	  });

    $('.rpCloseBtn').click(function(){
      $('#formReport').collapse('hide');
    });

    // $('.input-search .input-group-append').click(function(){
    //     window.location.href = "<?=base_url('Search')?>";
    // });
  </script>

<?php if(!isset($not_clear_filter)): ?>
<script>
  sessionStorage.removeItem('current_library_subject');
  sessionStorage.removeItem('current_library_cat');
</script>
<?php endif; ?>
</html>
