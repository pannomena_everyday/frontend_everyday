<link rel="stylesheet" href="<?=base_url('resources/slick-master/slick/slick.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/slick-master/slick/slick-theme.css');?>">
<link rel="stylesheet" koko="in" href="<?=base_url();?>resources/css/tutorial.min.css">
<style>
  .regular > .slick-list >.slick-track{
    margin-left:0;
    margin-right:0;
  }
  a.disabled {
    pointer-events: none;
    cursor: default;
  }
</style>
<div id="wrap">
  <div id="container" class="bg-white">
    <div class="allPlan-Header">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="myProfile-slide slider">
            <div class="myProfileBox">
              <?php if($can_add_plan):?>
                <a href="<?=base_url('Plan/AddPlan?from_page=allplan')?>"><img src="<?=base_url()."resources/img/addplan.png"?>" class="icon-subject"/> เพิ่มแผนการเรียน </a>
              <?php else:?>
                <a class="disabled" style="color: #bdbdbd;" href="<?=base_url('Plan/AddPlan?from_page=allplan')?>"><img src="<?=base_url()."resources/img/addplan.png"?>" class="icon-subject" style="opacity:0.3"> เพิ่มแผนการเรียน </a>
                <p>นักเรียนมีแผนการเรียนครบทุกวิชาแล้ว</p>
              <?php endif;?>
            </div>
            <!-- <div class="myProfileBox">
              <a href="<?=base_url('MyProfile/ChangePlan')?>"><img src="<?=base_url()."resources/img/editplan.png"?>" class="icon-subject"/>แก้ไขแผนการเรียน </a>
            </div> -->
          </section>
        </div>
      </div>
    </div>
    <div class="bg-white">
      <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <section>
        <?php if(empty($all_plans['current_plan']) && empty($all_plans['all_plans'])):?>
          <div class="f20 text-center">ยังไม่มีแผนการเรียน กรุณาคลิกปุ่มเพิ่มแผนการเรียนด้านบน</div>
        <?php else:?>
          <?php foreach ($all_plans as $index => $all_plan):?>
            <?php if($index == "current_plan"):?>
              <div class="f30">แผนการเรียนปัจจุบัน</div>
              <?php foreach ($all_plan as $index => $current_plan):?>
                <a style="float: right; padding-right: 10px;" href="<?=base_url('MyEveryDay/?subject='.$current_plan['subject_id'])?>">แก้ไขแผนการเรียน</a><br>
                <div class="d-flex justify-content-between align-content-center" >
                  <div class='new-prog-bar-cont align-self-center flex-grow-1'>
                    <div class="prog-bar">
                      <div class="background" style=" -webkit-clip-path: inset(0 <?=100-$current_plan['progress_bar']?>% 0 0); clip-path: inset(0 <?=100-$current_plan['progress_bar']?>% 0 0);"></div>
                    </div>
                  </div>
                  <div class="p-2 new-progPercent">
                    <span class="f16"><?=$current_plan['progress_bar']?>%</span> 
                  </div>
                </div>
                <div class="my-4">
                  <div class="Level" style="background: <?=$current_plan['color_code']?> 0% 0% no-repeat padding-box;">
                    <div class="subLevel-header">แผนการเรียน : <?=$current_plan['title_plan']?></div>
                  </div>

                  <div class="subLevel">
                    <div class="subLevel-header"></div>
                    <div class="subLevel-slide">
                      <section class="regular levelSlider slider">
                        <?php foreach ($current_plan['levels'] as $index => $level):?>
                          <div class="myEveryDay-slide-box level-box" data-is-comingsoon="<?= $level->is_comingsoon ?>" data-level-id="<?=$level->id;?>" data-title-level="<?=$level->title?>" data-plan-id="<?=$current_plan['plan_id'];?>" data-index_row="<?=$index?>">
                            <div class="d-flex relative">
                              <div class="content">
                                <img src="<?=$level->imgUrl?>">
                                <div class="progress">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: <?=$level->progress_bar?>%" aria-valuenow="<?=$level->progress_bar?>" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="m-t-5 textBtmMyeverday"><?=$level->title?></div>
                                <?php if($level->is_comingsoon == 1): ?>
                                  <div class="status"><div class="comingSoon">COMING SOON</div></div>
                                <?php endif; ?>
                              </div>
                              <div class="arrow align-self-center"> <div class="icon-arrow"><i class="fa fa-angle-right"></i></div> </div>
                            </div>
                          </div>
                        <?php endforeach;?>
                      </section>
                    </div>
                  </div>

                  <div class="subLevel-H-Box subLevel-box-header d-none" data-plan-id="<?=$current_plan['plan_id'];?>" style="background: <?=$current_plan['color_code']?> 0% 0% no-repeat padding-box;">
                    <div class="subLevel-header p-l-40 subLevel-title"></div>
                  </div>
                  <div class="subLevel subDetail p-t-50 d-none" data-plan-id="<?=$current_plan['plan_id'];?>">
                    <div class="subLevel-slide">
                      <section class="regular subSlider slider"></section>
                    </div>
                  </div>

                  <div class="subLevel-H-Box sheet-header-box d-none" data-plan-id="<?=$current_plan['plan_id'];?>" style="background: <?=$current_plan['color_code']?> 0% 0% no-repeat padding-box;">
                    <div class="subLevel-header p-l-40 sheet-title"></div>
                  </div>
                  <div class="subLevel subSheet p-t-50 d-none" data-plan-id="<?=$current_plan['plan_id'];?>">
                    <div class="subLevel-slide">
                      <section class="regular sheetSlider slider"></section>
                    </div>
                  </div>
                </div>
                <br>
              <?php endforeach;?>
            <?php else:?>
              <?php if(!empty($all_plans['all_plans'])):?>
                <hr style="border: 3px solid #495057">
                <div class="f30">ประวัติแผนการเรียน</div>
                <?php foreach ($all_plan as $index => $all):?>
                  <div class="d-flex justify-content-between align-content-center" >
                    <div class='new-prog-bar-cont align-self-center flex-grow-1'>
                      <div class="prog-bar">
                        <div class="background" style=" -webkit-clip-path: inset(0 <?=100-$all['progress_bar']?>% 0 0); clip-path: inset(0 <?=100-$all['progress_bar']?>% 0 0);"></div>
                      </div>
                    </div>
                    <div class="p-2 new-progPercent">
                      <span class="f16"><?=$all['progress_bar']?>%</span> 
                    </div>
                  </div>

                  <div class="my-4">
                    <div class="Level" style="background: <?=$all['color_code']?> 0% 0% no-repeat padding-box;">
                      <div class="subLevel-header">แผนการเรียน : <?=$all['title_plan']?></div>
                    </div>

                    <div class="subLevel">
                      <div class="subLevel-header"></div>
                      <div class="subLevel-slide">
                        <section class="regular levelSlider slider">
                          <?php foreach ($all['levels'] as $index => $level):?>
                            <div class="myEveryDay-slide-box level-box" data-is-comingsoon="<?= $level->is_comingsoon ?>" data-level-id="<?=$level->id;?>" data-title-level="<?=$level->title?>" data-plan-id="<?=$all['plan_id'];?>" data-index_row="<?=$index?>">
                              <div class="d-flex relative">
                                <div class="content">
                                  <img src="<?=$level->imgUrl?>">
                                  <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: <?=$level->progress_bar?>%" aria-valuenow="<?=$level->progress_bar?>" aria-valuemin="0" aria-valuemax="100"></div>
                                  </div>
                                  <div class="m-t-5 textBtmMyeverday"><?=$level->title?></div>
                                  <?php if($level->is_comingsoon == 1): ?>
                                    <div class="status"><div class="comingSoon">COMING SOON</div></div>
                                  <?php endif; ?>
                                </div>
                                <div class="arrow align-self-center"> <div class="icon-arrow"><i class="fa fa-angle-right"></i></div> </div>
                              </div>
                            </div>
                          <?php endforeach;?>
                        </section>
                      </div>
                    </div>

                    <div class="subLevel-H-Box subLevel-box-header d-none" data-plan-id="<?=$all['plan_id'];?>" style="background: <?=$all['color_code']?> 0% 0% no-repeat padding-box;">
                      <div class="subLevel-header p-l-40 subLevel-title"></div>
                    </div>
                    <div class="subLevel subDetail p-t-50 d-none" data-plan-id="<?=$all['plan_id'];?>">
                      <div class="subLevel-slide">
                        <section class="regular subSlider slider"></section>
                      </div>
                    </div>

                    <div class="subLevel-H-Box sheet-header-box d-none" data-plan-id="<?=$all['plan_id'];?>" style="background: <?=$all['color_code']?> 0% 0% no-repeat padding-box;">
                      <div class="subLevel-header p-l-40 sheet-title"></div>
                    </div>
                    <div class="subLevel subSheet p-t-50 d-none" data-plan-id="<?=$all['plan_id'];?>">
                      <div class="subLevel-slide">
                        <section class="regular sheetSlider slider"></section>
                      </div>
                    </div>
                  </div>
                <?php endforeach;?>
              <?php endif;?>
            <?php endif;?>
          <?php endforeach;?>
        <?php endif;?>
        </section>
      </div>
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->