<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<div id="wrap">
  <div id="container" class="bg-gray-f7">
    <div class="containner-padding">
      <div class="box-800">
        <!-- <div class="tt"></div> -->
        <div class="center bg-white f26 f-w-500"> รีเซ็ตรหัสผ่าน </div>
        <div class="center bg-white f22"> กรอกรหัสผ่านใหม่ของคุณ </div>
        <div class="input-group input-focus form-signIn">
            <input type="password" id="password" placeholder="รหัสผ่าน" class="form-control border-left-0 f18">
            <div class="input-group-prepend toggle-password" data-id="password">
              <span class="input-group-text bg-white f-ccc"><i class="fa fa-eye-slash"></i></span>
            </div>
        </div>
      <div class="input-group input-focus form-signIn">
        <input type="password" id="conf_password" placeholder="ยืนยันรหัสผ่าน" class="form-control border-left-0 f18">
        <div class="input-group-prepend toggle-password" data-id="conf_password">
          <span class="input-group-text bg-white f-ccc"><i class="fa fa-eye-slash"></i></span>
        </div>
      </div>
        <button class="sendBtn btn1 btn-black"> เปลี่ยนรหัสผ่านใหม่ </button>
      </div>

    </div> <!-- //.containner-padding -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- <script src="facebookLogin.js"></script> -->
<script src="<?=base_url('resources/bcrypt.js-master/bcrypt.min.js');?>"></script>
<script>
  $(document).ready(function() {
      // var bcrypt = dcodeIO.bcrypt;
      // var salt = bcrypt.genSaltSync(10);
      // var hash = bcrypt.hashSync("aA111111", salt);
      // //$('.tt').html(hash);
      // bcrypt.compare("aA111111", hash, function(err, res) {
      //     // res === true
      //     if(res === true){
      //       $('.tt').html('true');
      //     }else{
      //       $('.tt').html('false');
      //     }
      // });
      //$2a$10$sjc5thhevNB7DUZqALzzrehVWQDwfMbQYaHa93NIm0mH4WsHSLczS
      //$2a$10$IdWxum2QJ2W6u5O861LJW.K/meNSrdyiQby..WpBJ6Q9ssV4zAH7O
  });

  $(".toggle-password").click(function() {
    $(this).find('i').toggleClass("fa-eye fa-eye-slash");
    var id = $(this).data("id");
    var input = $("#"+id);
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });

  $('.sendBtn.btn1').click(function(){
    var strength = 0;
    var mobile = '<?=$mobile?>';
    var password = $.trim($('#password').val());
    var conf_password = $.trim($('#conf_password').val());
    var arr = [/.{8,}/, /[a-z]+/, /[0-9]+/, /[A-Z]+/];
    jQuery.map(arr, function(regexp) {
      if(password.match(regexp))
        strength++;
    });
    if(password == ''){
      alert('กรุณากรอกรหัสผ่าน');
    }else if(strength < 4){
      alert('กําหนดรหัสผ่าน \nตัวอักษรมากกว่าหรือเท่ากับ 8 ตัว \nตัวอักษรตัวใหญ่อย่างน้อย 1 ตัว \nตัวอักษรตัวเล็กอย่างน้อย 1 ตัว \nตัวเลขอย่างน้อย 1 ตัว');
    }else if(conf_password == ''){
      alert('กรุณากรอกยืนยันรหัสผ่าน');
    }else if(password != conf_password){
      alert('รหัสผ่านและยืนยันรหัสผ่านไม่ตรงกัน');
    }else{
      var bcrypt = dcodeIO.bcrypt;
      var salt = bcrypt.genSaltSync(10);
      var hashPassword = bcrypt.hashSync(password, salt);
      $.ajax({
        url: "<?=base_url()."Index/ChangePasswordProcess"?>",
        type: "post",
        data: {mobile:mobile,password:hashPassword} ,
        success: function (data) {
          if(data == 'true'){
            alert('เปลี่ยนรหัสผ่านเรียบร้อยแล้ว');
            window.location.href = '<?=base_url("Index/SignIn");?>';
          }else{
            alert('ไม่สามารถเปลี่ยนรหัสผ่านได้ กรุณาลองอีกครั้ง');
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    }
  });

  $('#conf_password').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      var strength = 0;
      var mobile = '<?=$mobile?>';
      var password = $.trim($('#password').val());
      var conf_password = $.trim($('#conf_password').val());
      var arr = [/.{8,}/, /[a-z]+/, /[0-9]+/, /[A-Z]+/];
      jQuery.map(arr, function(regexp) {
        if(password.match(regexp))
          strength++;
      });
      if(password == ''){
        alert('กรุณากรอกรหัสผ่าน');
      }else if(strength < 4){
        alert('กําหนดรหัสผ่าน \nตัวอักษรมากกว่าหรือเท่ากับ 8 ตัว \nตัวอักษรตัวใหญ่อย่างน้อย 1 ตัว \nตัวอักษรตัวเล็กอย่างน้อย 1 ตัว \nตัวเลขอย่างน้อย 1 ตัว');
      }else if(conf_password == ''){
        alert('กรุณากรอกยืนยันรหัสผ่าน');
      }else if(password != conf_password){
        alert('รหัสผ่านและยืนยันรหัสผ่านไม่ตรงกัน');
      }else{
        var bcrypt = dcodeIO.bcrypt;
        var salt = bcrypt.genSaltSync(10);
        var hashPassword = bcrypt.hashSync(password, salt);
        $.ajax({
          url: "<?=base_url()."Index/ChangePasswordProcess"?>",
          type: "post",
          data: {mobile:mobile,password:hashPassword} ,
          success: function (data) {
            if(data == 'true'){
              alert('เปลี่ยนรหัสผ่านเรียบร้อยแล้ว');
              window.location.href = '<?=base_url("Index/SignIn");?>';
            }else{
              alert('ไม่สามารถเปลี่ยนรหัสผ่านได้ กรุณาลองอีกครั้ง');
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });
      }
    }
  });

</script>
