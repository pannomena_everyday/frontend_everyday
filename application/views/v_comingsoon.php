<div id="wrap">
	<div id="container">
		<img class="imgRotate w-100" src="" data-desktop="Desktop_banner_01_coming soon.jpg" data-tablet="Tablet_banner_01_coming soon.jpg" data-mobile="Mobile_banner_01_coming soon1.jpg" />
	</div> <!-- //.Container -->
</div> <!-- //#Warp -->
<script src="<?=base_url('resources/js/jquery-1.11.0.js');?>"></script>
<script  type="text/javascript">
  $(document).ready(function() {
    getImgByScreenSize();
  });

  $( window ).resize(function() {
    getImgByScreenSize();
  });

  function getImgByScreenSize(){
    $('.imgRotate').each(function(){
        var desktop = '<?=base_url("resources/img/comingsoon/");?>'+$(this).data( "desktop" );
        var tablet = '<?=base_url("resources/img/comingsoon/");?>'+$(this).data( "tablet" );
        var mobile = '<?=base_url("resources/img/comingsoon/");?>'+$(this).data( "mobile" );
        if ($( window ).width() <= 414) {
          $(this).attr("src",mobile);
        } else if ($( window ).width() <= 1024) {
          $(this).attr("src",tablet);
        } else{
          $(this).attr("src",desktop);
        }
    });
  }
</script>