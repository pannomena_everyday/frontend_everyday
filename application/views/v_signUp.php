<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<style>

</style>
<div id="wrap">
  <div id="container" class="bg-gray-f7">
    <div class="containner-padding">

      <div class="signUpStepBtnDiv">
        <div class="signUpStepLine"></div>
        <div class="d-flex justify-content-between">
          <div class="signUpStepBtnBox d-flex flex-column align-items-center">
            <div class="signUpStepBtn step1 active rounded-circle d-flex justify-content-center align-items-center">
              <span>1</span>
            </div>
            <div class="text-center">ข้อมูลบัญชีผู้ใช้</div>
          </div>
          <div class="signUpStepBtnBox d-flex flex-column align-items-center">
            <div class="signUpStepBtn step2 rounded-circle d-flex justify-content-center align-items-center">
              <span>2</span>
            </div>
            <div class="text-center">ข้อมูลส่วนตัว</div>
          </div>
          <div class="signUpStepBtnBox d-flex flex-column align-items-center">
            <div class="signUpStepBtn step3 rounded-circle d-flex justify-content-center align-items-center">
              <span>3</span>
            </div>
            <div class="text-center">ข้อมูลการศึกษา</div>
          </div>
          <div class="signUpStepBtnBox d-flex flex-column align-items-center">
            <div class="signUpStepBtn step4 rounded-circle d-flex justify-content-center align-items-center">
              <span>4</span>
            </div>
            <div class="text-center">ยืนยันตัวตน</div>
          </div>
        </div>
      </div>

      <div class="box-800">
        <div class="center bg-white f26">ลงทะเบียน</div>
        <div class="signUp-boxstep step1">
          <div class="input-group input-focus form-signIn">
            <input type="text" id="email" placeholder="อีเมล" class="form-control border-left-0 f18">
            <div class="txtError email"></div>
          </div>
          <div class="input-group input-focus form-signIn">
            <input type="password" id="password" placeholder="รหัสผ่าน" class="form-control border-left-0 f18">
            <div class="input-group-prepend toggle-password" data-id="password">
              <span class="input-group-text bg-white f-ccc"><i class="fa fa-eye-slash"></i></span>
            </div>
          </div>
          <div class="input-group input-focus form-signIn">
            <input type="password" id="conf_password" placeholder="ยืนยันรหัสผ่าน" class="form-control border-left-0 f18">
            <div class="input-group-prepend toggle-password" data-id="conf_password">
              <span class="input-group-text bg-white f-ccc"><i class="fa fa-eye-slash"></i></span>
            </div>
          </div>
          <div class="SignUp-policy f12">
            *การกดถัดไปเป็นการยอมรับใน เงือนไขการใช้บริการ และนโยบายความเป็นส่วนตัวของเรา
          </div>
          <div class="align-right signUpBtn-div">
            <button class="signUpBtn btn-black">ถัดไป</button>
          </div>
        </div> <!-- Step 1 -->

        <div class="signUp-boxstep step2" style="display: none;">
          <div class="row">
            <div class="col-md-6">
              <div class="input-group input-focus form-signIn">
                <input type="text" id="firstname" placeholder="ชื่อ" class="form-control border-left-0 f18">
              </div>
            </div>
            <div class="col-md-6">
              <div class="input-group input-focus form-signIn">
                <input type="text" id="lastname" placeholder="นามสกุล" class="form-control border-left-0 f18">
              </div>
            </div>
            <div class="col-md-12">
              <div class="input-group input-focus form-signIn">
                <input autocomplete="off" type="text" placeholder="วันเกิด" id="birthdate" class="form-control border-left-0 f18">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-white f-ccc icon-calendar"><i class="fa fa-calendar-o"></i></span>
                </div>
              </div>
              <div class="input-group input-focus form-signIn">
                <select id="gender" class="custom-select form-control f18">
                  <option value="" selected>เพศ</option>
                  <option value="M">ชาย</option>
                  <option value="F">หญิง</option>
                </select>
              </div>
              <div class="input-group input-focus form-signIn d-none">
                <input type="text" id="id_card" placeholder="เลขบัตรประชาชน" class="form-control border-left-0 f18">
              </div>
              <div class="d-none" style="margin-top:10px;">ข้อมูลส่วนบุคคลของท่านจะถูกเก็บเป็นความลับ</div>
            </div>
          </div>
          <div class="align-right signUpBtn-div">
            <button class="signUpBtnBack btn-gray" onclick="backToStap(1)">ย้อนกลับ</button>
            <button class="signUpBtn btn-black">ถัดไป</button>
          </div>
        </div> <!-- Step 2 -->

        <div class="signUp-boxstep step3" style="display: none;">
          <div class="input-group input-focus form-signIn">
            <input type="text" id="schoolname" placeholder="ชื่อโรงเรียน/มหาวิทยาลัย" class="form-control border-left-0 f18">
          </div>
          <div class="input-group input-focus form-signIn">
            <select id="level" class="custom-select form-control f18">
              <option value="0" selected>ระดับการศึกษา</option>
              <?php
              foreach($dataEducation as $row){
                  echo "<option value='".$row->id."'>".$row->title."</option>";
              }
              ?>
            </select>
          </div>
          <div class="input-group input-focus form-signIn">
            <select id="provinceid" class="custom-select form-control f18">
              <option value="" selected>จังหวัด</option>
              <?php
              foreach($dataProvince->result() as $row){
                  echo "<option value='".$row->ProvinceID."'>".$row->ProvinceName."</option>";
              }
              ?>
            </select>
          </div>

          <!-- <div class="input-group input-focus form-signIn">
            <input type="text" id="mobile" class="form-control inp-tel f18" placeholder="กรอกเบอร์โทรศัพท์เพื่อรับ OTP" aria-label="Recipient's username" aria-describedby="basic-addon2">
          </div> -->

          <div class="align-right signUpBtn-div">
            <button class="signUpBtnBack btn-gray" onclick="backToStap(2)">ย้อนกลับ</button>
            <button class="signUpBtn btn-black">ถัดไป</button>
          </div>
        </div> <!-- Step 3 -->

        <div class="signUp-boxstep step4" style="display: none;">
          <div class="input-group mt-5">
            <input type="text" id="mobile" class="form-control inp-tel f18 f-otp" placeholder="กรอกเบอร์โทรศัพท์เพื่อรับรหัส OTP">
            <div class="input-group-append sendOTP btn-sendOTP isActive">
              <span class="input-group-text bg-Btn-OTP f-otp">ส่ง</span>
            </div>
          </div>
          <div class="txtOTP my-2"></div>
          <div class="txtOTPCountdown my-2"></div>
          <div class="otp-box">
            <!-- <input class="otp-number num1" type="text" maxlength="1" />
            <input class="otp-number num2" type="text" maxlength="1" />
            <input class="otp-number num3" type="text" maxlength="1" />
            <input class="otp-number num4" type="text" maxlength="1" />
            <input class="otp-number num5" type="text" maxlength="1" />
            <input class="otp-number num6" type="text" maxlength="1" /> -->
            <input class="otp-numberAll" type="text" maxlength="6" />
            <div class="txt-resendOTP"></div>
          </div>
          <div class="align-right signUpBtn-div">
            <button class="signUpBtnBack btn-gray" onclick="backToStap(3)">ย้อนกลับ</button>
            <button class="registerBtn btn-black">ลงทะเบียน</button>
          </div>
        </div> <!-- Step 4 -->
      </div>
    </div> <!-- //.containner-padding -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->
<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script type="text/javascript">
  var reqMobile = '';
  var ref = '';
  var disableBtn = false;
  $(document).ready(function() {
    // $('.signUp-boxstep').hide();
    // $('.signUp-boxstep.step3').show();
    $('#birthdate').datepicker({
        changeYear: true,
        changeMonth: true,
        dateFormat: 'yy-mm-dd',
        yearRange: '-100:+0'
      });
      $('.icon-calendar').on('click', function() {
        $('#birthdate').focus();
    });

    clickStep();
    nextStep();

    // $('input.otp-number').on('keypress keyup keydown', function(event) {
    //   $(this).val($(this).val().replace(/[^\d].+/, ""));
    //   if ((event.which < 48 || event.which > 57)) {
    //     event.preventDefault();
    //   } else{
    //     $(this).next().focus();
    //   }
    // });
  });


  $(".toggle-password").click(function() {
    $(this).find('i').toggleClass("fa-eye fa-eye-slash");
    var id = $(this).data("id");
    var input = $("#"+id);
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });

  function checkID_Card(id){
    if(id.length != 13) return false;
    for(i=0, sum=0; i < 12; i++)
    sum += parseFloat(id.charAt(i))*(13-i); if((11-sum%11)%10!=parseFloat(id.charAt(12)))
    return false; return true;
  }

  function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
  }

  $("#id_card").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
      if ((event.which < 48 || event.which > 57 || $(this).val().length >= 13)) {
          event.preventDefault();
      }
  });

  $("#mobile").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
  });

  $(".otp-numberAll").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
  });

  function backToStap(step){
    $('.signUp-boxstep').hide();
    $('.signUp-boxstep.step'+step).fadeIn( "1000" );
    $('.signUpStepBtn').removeClass("active");
    var i;
    for (i=1; i<=step; i++) {
      $('.signUpStepBtn.step'+i).addClass("active");
    }
  }

  function clickStep() {
    $('.signUpStepBtn.step1').click(function(){
      $('.signUp-boxstep').hide();
      $('.signUp-boxstep.step1').show();
      $('.signUpStepBtn').removeClass("active");
      $('.signUpStepBtn.step1').addClass("active");
    });

    $('.signUpStepBtn.step2').click(function(){
      if($(".signUpStepBtn.step2.active")[0]){
        $('.signUp-boxstep').hide();
        $('.signUp-boxstep.step2').show();
        $('.signUpStepBtn').removeClass("active");
        $('.signUpStepBtn.step1').addClass("active");
        $('.signUpStepBtn.step2').addClass("active");
      }
    });

    $('.signUpStepBtn.step3').click(function(){
      if($(".signUpStepBtn.step3.active")[0]){
        $('.signUp-boxstep').hide();
        $('.signUp-boxstep.step3').show();
        $('.signUpStepBtn').removeClass("active");
        $('.signUpStepBtn.step1').addClass("active");
        $('.signUpStepBtn.step2').addClass("active");
        $('.signUpStepBtn.step3').addClass("active");
      }
    });
  }

  function nextStep() {
    $('.signUp-boxstep.step1 .signUpBtn').click(function(){
      var strength = 0;
      var email = $.trim($('#email').val());
      var password = $.trim($('#password').val());
      var conf_password = $.trim($('#conf_password').val());
      var arr = [/.{8,}/, /[a-z]+/, /[0-9]+/, /[A-Z]+/];
      jQuery.map(arr, function(regexp) {
        if(password.match(regexp))
          strength++;
      });
      if(email != ''){
        $.ajax({
          type: "POST",
          url: "<?=base_url()."Index/checkEmailExist";?>",
          data: {email:email},
          dataType: "json",
          success: function (data) {
            if(!validateEmail(email)){
              alert('รูปแบบอีเมลไม่ถูกต้อง');
            }else if(data != 0){
              alert("อีเมลนี้ '"+email+"' ได้ถูกใช้ในระบบไว้แล้ว");
            }else if(password == ''){
              alert('กรุณากรอกรหัสผ่าน');
            }else if(strength < 4){
              alert('กําหนดรหัสผ่าน \nตัวอักษรมากกว่าหรือเท่ากับ 8 ตัว \nตัวอักษรตัวใหญ่อย่างน้อย 1 ตัว \nตัวอักษรตัวเล็กอย่างน้อย 1 ตัว \nตัวเลขอย่างน้อย 1 ตัว');
            }else if(conf_password == ''){
              alert('กรุณากรอกยืนยันรหัสผ่าน');
            }else if(password != conf_password){
              alert('รหัสผ่านและยืนยันรหัสผ่านไม่ตรงกัน');
            }else{
              $('.signUp-boxstep.step1').hide();
              $('.signUp-boxstep.step2').fadeIn( "1000" );
              $('.signUpStepBtn.step2').addClass("active");
            }
          }
        });
      }else{
        alert('กรุณากรอกอีเมล');
      }
     });

    $('.signUp-boxstep.step2 .signUpBtn').click(function(){
      var firstname = $.trim($('#firstname').val());
      var lastname = $.trim($('#lastname').val());
      var birthdate = $.trim($('#birthdate').val());
      var gender = $.trim($('#gender').val());
      var id_card = $.trim($('#id_card').val());
      if(firstname == ''){
        alert('กรุณากรอกชื่อ');
      }else if(lastname == ''){
        alert('กรุณากรอกนามสกุล');
      }else if(birthdate == ''){
        alert('กรุณาเลือกวันเกิด');
      }else if(gender == ''){
        alert('กรุณาเลือกเพศ');
      }else{
        $('.signUp-boxstep.step2').hide();
        $('.signUp-boxstep.step3').fadeIn( "1000" );
        $('.signUpStepBtn.step3').addClass("active");
      }
    });

    $('.signUp-boxstep.step3 .signUpBtn').click(function(){
      var firstname = $.trim($('#firstname').val());
      var lastname = $.trim($('#lastname').val());
      var birthdate = $.trim($('#birthdate').val());
      var gender = $.trim($('#gender').val());
      var id_card = $.trim($('#id_card').val());
      var mobile = $.trim($('#mobile').val());
      var email = $.trim($('#email').val());
      var password = $.trim($('#password').val());
      var conf_password = $.trim($('#conf_password').val());
      var schoolname = $.trim($('#schoolname').val());
      var level = $.trim($('#level').val());
      //var majer =  $.trim($('#').val());
      var provinceId = $.trim($('#provinceid').val());
      var remember = 0;

      if(schoolname == ''){
        alert('กรุณากรอกชื่อโรงเรียน');
      }else if(level == ''){
        alert('กรุณาเลือกระดับการศึกษา');
      }else if(provinceId == ''){
        alert('กรุณาเลือกจังหวัด');
      }else{
        $('.signUp-boxstep.step3').hide();
        $('.signUp-boxstep.step4').fadeIn( "1000" );
        $('.signUpStepBtn.step4').addClass("active");
      }
    });

    $('.sendOTP.isActive').click(function(){
      if( $('.txtOTPCountdown').html() == ''){
        reqMobile = $.trim($('#mobile').val());
        if(reqMobile.length == 10){
          $.ajax({
            type: "POST",
            url: "<?=base_url()."Index/checkTimeOTP";?>",
            data: {mobile:reqMobile},
            success: function (ret) {
              var obj = $.parseJSON(ret);
              if(obj.result === true){
                $.ajax({
                  type: "POST",
                  url: "<?=base_url()."Index/checkMobileExist";?>",
                  data: {mobile:reqMobile},
                  dataType: "json",
                  success: function (data) {
                    if(data != 0){
                      alert("เบอร์โทรศัพท์นี้ '"+reqMobile+"' ได้ถูกใช้ในระบบไว้แล้ว");
                    }else{
                      var opts = {
                        method: 'POST',
                        headers: {"Content-Type":"application/json"},
                        body: JSON.stringify({"mobile": reqMobile})
                      };
                      fetch('<?=$this->config->item('api_url').'members/request-otp';?>', opts).then(function (response) {
                        response.json().then(function (result) {
                          if(response.ok){
                            ref = result.ref;
                            console.log(ref);
                            $('.txt-resendOTP').html("ยังไม่ได้รับรหัส OTP ส่งอีกครั้้ง");
                            $('.txtOTP').html('ส่งรหัส OTP ไปที่เบอร์โทรศัพท์แล้ว');
                            $('.btn-sendOTP').addClass('isDisable');
                            $('.btn-sendOTP').removeClass('isActive');
                            $.ajax({
                              url: "<?=base_url()."Index/setTimeOTP";?>",
                              success: function (data) {
                                var counter = 30;
                                var interval = setInterval(function() {
                                    counter--;
                                    // Display 'counter' wherever you want to display it.
                                    if (counter <= 0) {
                                        clearInterval(interval);
                                        $('.txtOTP').html('');
                                        $('.txtOTPCountdown').html('');
                                        $('.btn-sendOTP').addClass('isActive');
                                        $('.btn-sendOTP').removeClass('isDisable');
                                        return;
                                    }else{
                                      $('.txtOTPCountdown').html('กรุณารอสักครู่ หากไม่ได้รับ OTP กรุณากดส่งใหม่ใน <span class="f-blue">'+counter+'</span> วินาที');
                                    }
                                }, 1000);
                              }
                            });
                          }
                        });
                      });
                    } // else
                  } //success
                });// checkMobileExist
              }else{
                var counter = 30;
                var interval = setInterval(function() {
                    counter--;
                    // Display 'counter' wherever you want to display it.
                    if (counter <= 0) {
                        clearInterval(interval);
                        $('.txtOTP').html('');
                        $('.txtOTPCountdown').html('');
                        $('.btn-sendOTP').addClass('isActive');
                        $('.btn-sendOTP').removeClass('isDisable');
                        return;
                    }else{
                      $('.txtOTPCountdown').html('กรุณารอสักครู่ หากไม่ได้รับ OTP กรุณากดส่งใหม่ใน <span class="f-blue">'+counter+'</span> วินาที');
                    }
                }, 1000);
              }
            }
          });// checkTimeOtp
        }else{
          alert("กรุณากรอกหมายเลขโทรศัพท์ให้ถูกต้อง");
        }
      }
    });

    $('.registerBtn').click(function(){
      var firstname = $.trim($('#firstname').val());
      var lastname = $.trim($('#lastname').val());
      var birthdate = $.trim($('#birthdate').val());
      var gender = $.trim($('#gender').val());
      var id_card = $.trim($('#id_card').val());
      var email = $.trim($('#email').val());
      var password = $.trim($('#password').val());
      var conf_password = $.trim($('#conf_password').val());
      var schoolname = $.trim($('#schoolname').val());
      var level = $.trim($('#level').val());
      //var majer =  $.trim($('#').val());
      var provinceId = $.trim($('#provinceid').val());
      var remember = 0;
      var mobile = $.trim($('#mobile').val());
      // var otp = $.trim($('input.num1').val())+$.trim($('input.num2').val())+$.trim($('input.num3').val())+$.trim($('input.num4').val())+$.trim($('input.num5').val())+$.trim($('input.num6').val());
      var otp = $.trim($('input.otp-numberAll').val());
      // เช็คเบอร์โทรที่ขอ OTP ตรงกับเบอร์ที่กดสมัครหรือไม่
      if(reqMobile != mobile){
        alert("กรุณาขอรหัส OTP อีกครั้ง");
      }else{
        var opts = {
          method: 'POST',
          headers: {
              "Content-Type":"application/json"
          },
          body: JSON.stringify({
            "ref": ref,
            "otp": otp
          })
        };
        fetch('<?=$this->config->item('api_url').'members/verify-otp';?>', opts).then(function (response) {
          response.json().then(function (result) {
            if(response.ok){
              if(result.status){
                var opts2 = {
                  method: 'POST',
                  headers: {
                      "Content-Type":"application/json"
                  },
                  body: JSON.stringify({
                  "email": email,
                  "password": password,
                  "firstname": firstname,
                  "lastname": lastname,
                  "birddate": birthdate,
                  "gender": gender,
                  "id_card": id_card,
                  "mobile": mobile,
                  "verify": true,
                  "schoolname": schoolname,
                  "level": level,
                  "provinceId": parseInt(provinceId)
                  })
                };
                fetch('<?=$this->config->item('api_url').'members';?>', opts2).then(function (response2) {
                  return response2.json();
                })
                .then(function (body) {
                    //console.log(body)
                    if(body.error){
                      //alert(body.error.message);
                      var str = body.error.message;
                      //ER_DUP_ENTRY: 13
                      //'mobile' 8
                      var begin = str.substring(0, 13);
                      var last = str.substring(str.length - 8, str.length);
                      var last2 = str.substring(str.length - 7, str.length);
                      if(begin == "ER_DUP_ENTRY:" && last == "'mobile'"){
                        alert("หมายเลขโทรศัพท์นี้ '"+mobile+"' ได้ถูกใช้ในระบบไว้แล้ว")
                      }else if(begin == "ER_DUP_ENTRY:" && last2 == "'email'"){
                        alert("อีเมลนี้ '"+email+"' ได้ถูกใช้ในระบบไว้แล้ว")
                      }else{
                        alert(body.error.message);
                      }
                    }else{
                      $.ajax({
                        url: "<?=base_url()."Index/LogIn"?>",
                        type: "post",
                        data: {mobile:mobile,password:password,remember:remember} ,
                        success: function (data) {
                            if(data == 'success'){
                              alert("ระบบได้รับข้อมูลการลงทะเบียนเรียบร้อยแล้ว");
                              window.location.href = '<?=base_url("Membertype");?>';
                            }else{
                              alert(data);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                        }
                      });
                    }
                });
              }else{
                //alert(result.message);
                alert("กรุณากรอกรหัส OTP ที่ถูกต้อง");
              }
            }else{
              alert("เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง");
            }
          });
        }).catch(function(error) {
          alert("กรุณากรอกรหัส OTP ที่ถูกต้อง");
          console.log(error);
        });
      }
    });
  }
</script>

