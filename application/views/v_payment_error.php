
<!-- ชำระเงินไม่สำเร็จ -->
<div id="wrap">
    <div id="container" class="bg-gray-f7">
        <div class="whiteContentBox set-full-page d-flex justify-content-center align-items-center">
            <div class="col col-sm-11 col-md-9 col-lg-8 text-center">
                <div class="mb-4"><img src="/resources/img/fail.svg" alt="correct" width="50"></div>
                <h5 class="mb-70">ชำระเงินไม่สำเร็จ</h5>
                <p class="mb-5"><?= $msg ?></p>
                <!-- <p class=" mb-5">กรุณาติดต่อธนาคารผู้อกกบัตร<br>เพื่อสอบถามสาเหตุและวิธีการแก้ไข</p>
                <p class=" mb-5">กรุณาติดต่อธนาคารผู้อกกบัตร<br>เพื่อเช็ควงเงินหรือ ชำระผ่านบัตรเครดิต/เดบิตใบอื่น</p>
                <p class=" mb-5">กรุณาทำการสั่งซื้ออีกครั้ง<br>และกรอกข้อมูลบัตรให้ถูกต้อง</p> -->
                <div class="row">  
                    <div class="col-sm-6 mb-4 mb-sm-0">              
                        <a href="<?= base_url('Membertype')?>"><button class="col btn-lg rounded-pill btn-gray" type="button">ยกเลิก</button></a>
                    </div>
                    <div class="col-sm-6">              
                        <button class="col btn-lg rounded-pill btn-black" id="btnSubmit" type="button">กลับไปหน้าชำระเงิน</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="formSubcriber" method="POST" action="<?=base_url('payment')?>">
<input type="hidden" name="subscriber_id" value="<?= $subscription_id ?>">
<input type="hidden" name="promotion" value="<?= $promotion_code ?>">
</form>

<script>
var submit  = document.getElementById('btnSubmit'),
    form = document.getElementById("formSubcriber");


    submit.addEventListener('click', function() {
        form.submit()
    })
</script>
<!-- End ชำระเงินไม่สำเร็จ -->
