<div id="wrap">
  <div id="container">
    <div class="whiteContentBox">
      <div class="testTitle">คำอธิบาย</div>
      <div class="testDetail">
        <div class="testDetailRow d-flex align-content-center">
          <div class="testDetailNo align-self-center">1</div>
          <div class="px-2 p-t-5 flex-grow-1">ข้อสอบเรื่อง <?php if (!empty($tests->title)) { echo $tests->title; } ;?></div>
        </div>
        <div class="testDetailRow d-flex align-content-center">
          <div class="testDetailNo align-self-center">2</div>
          <div class="px-2 p-t-5 flex-grow-1">จำนวน <?php if (!empty($tests->subLevelTestQuestions)) { echo count($tests->subLevelTestQuestions); } ;?>
            ข้อ</div>
        </div>
        <div class="testDetailRow d-flex align-content-center">
          <div class="testDetailNo align-self-center">3</div>
          <div class="px-2 p-t-5 flex-grow-1">เวลาที่ใช้ในการสอบ <?php if (!empty($tests->test_time)) { echo fmtMSS($tests->test_time); } ;?> นาที</div>
        </div>
        <div class="testDetailRow d-flex align-content-center">
          <div class="testDetailNo align-self-center">4</div>
          <div class="px-2 p-t-5 flex-grow-1">สามารถทำแบบทดสอบได้เพียงครั้งเดียวเท่านั้น</div>
        </div>
        <div class="testDetailRow d-flex align-content-center">
          <div class="testDetailNo align-self-center">5</div>
          <div class="px-2 p-t-5 flex-grow-1">ต้องทำครั้งเดียวจนเสร็จ</div>
        </div>
      </div>
      <div class="center">
        <!-- <a href="<?=base_url('Test/question/'.$sublevel_sheet_id.'/'.$level_id.'/'.$plan_id);?>"><button class="test-btn btn-green-white"><i class="fa fa-play"></i> ทำแบบทดสอบ</button></a> -->
        <button onclick="createTest()" class="test-btn btn-green-white"><i class="fa fa-play"></i> ทำแบบทดสอบ</button>
      </div>
    </div>  
    
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<?php
function fmtMSS($seconds) {
  $t = round($seconds);
  return sprintf('%d',($t/60));
}
?>

<script>
var isNotSend = true;
function createTest(){
  if(isNotSend){
    isNotSend = false
    let payload = {
      sublevel_sheet_id: <?= $sublevel_sheet_id?>,
      plan_id: <?=$plan_id;?>,
      level_id: <?=$level_id;?>
    }
    $.post("<?=base_url("Test/createTest");?>", payload)
    .done(res => {
      window.location.href = '<?=base_url("Test/Question/".$sublevel_sheet_id."/".$level_id."/".$plan_id);?>';
    })
  }
}
</script>
