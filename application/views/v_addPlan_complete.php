<div id="wrap">
  <div id="container">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="startPlanBox">
          <div class="text-center"><img class="img-startplan" src="<?=base_url()."resources/img/addplancompletd.png"?>"/></div>
        </div>
        <div class="startPlanBtn-div d-flex justify-content-center flex-wrap flex-md-nowrap">
        <?php if(isset($plan->planExercices) && count($plan->planExercices) > 0): ?>
          <div class="w-100 m-w-360 m-15">
            <?php $planId = $this->input->get('plan')?>
            <a class="w-100" href="<?=base_url('PreTest?plan='.$planId)?>"><button class="btn-60 btn-green-white">ต้องการทำแบบทดสอบก่อนเรียน</button></a>
            
          </div>
        <?php endif; ?>
          <div class="w-100 m-w-360 m-15">
            <a class="w-100" href="<?=base_url('MyEveryDay')?>"><button class="btn-60 btn-white-green">เริ่มเรียนเลย!</button></a>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->