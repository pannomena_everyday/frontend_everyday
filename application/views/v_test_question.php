<div id="wrap">
  <div id="container">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <?php
        // echo "<pre>";
          // print_r($tests);
          // echo "</pre>";
          // $answerState = array();
        ?>
        
        <?php foreach ($tests->subLevelTestQuestions as $key => $test):?>
        <?php
          $previous = array_key_exists($key - 1, $tests->subLevelTestQuestions) ? $tests->subLevelTestQuestions[$key -1] : false;
          $next = array_key_exists($key + 1, $tests->subLevelTestQuestions) ? $tests->subLevelTestQuestions[$key +1] : false;
          // var_dump($previous);
          // var_dump($next);
        ?>
        <?php $question = getQuestion($api_url, $test->question_path); ?>
        <div class="questionBox <?= $test->ex_no != 0 ? "d-none" : ""?>" data-question-id="<?= $test->id ?>" id="question_id_<?= $test->id ?>">
          <div class="questionTitle d-flex flex-column flex-md-row justify-content-between align-content-center align-content-md-center flex-nowrap">
            <div class="f26 flex-grow-1 align-self-center">คำถามข้อที่ <?=$test->ex_no+1?> / <?=count($tests->subLevelTestQuestions)?></div>
            <div class="my-3 my-md-0 align-self-center"><i class="fa fa-clock-o" aria-hidden="true"></i> <span class="countdown"></span></div>
            <div class="d-flex justify-content-between align-content-center">
              <div class="align-self-center">
                <div class="btn-Q-modal AnswerSheetMobile m-767-show" data-toggle="modal" data-target=".Q-modal">
                  <img class="" src="<?=base_url('resources/img/AnswerSheet.png')?>" />
                </div>
              </div>
              <div class="px-0 px-md-3 align-self-center"><a href="#"><button onclick="sendAnswer()" class="Q-btnSubmit btn-green-white">ส่งคำตอบ</button></a></div>
            </div>
          </div>
          <div class="questionDetail">
            <div class="row">
              <div class="col-md-8 hfit">
                <div class='prog-bar-q'>
                    <div class="prog-bar">
                        <div class="background progress-bar" style="-webkit-clip-path: inset(0 <?=(100)?>% 0 0); clip-path: inset(0 <?=(100)?>% 0 0);"></div>
                      </div>
                    </div>
                    <div class="qImg-div">
                      <?php if($test->question_path != ''): ?>
                      <img class="qImg" src="<?=$question[0]?>" />
                    <?php endif; ?>
                    </div>
                </div>
              <div class="col-md-4 center">
                <div class="btn-Q-modal m-767-hide" data-toggle="modal" data-target=".Q-modal">
                    <img class="imgAnswerSheet" src="<?=base_url('resources/img/AnswerSheet.png')?>" />
                    <div class="txtAnswerSheet"><span>กระดาษคำตอบ</span></div>
                </div>
                <div class="d-flex flex-md-column mt-3 mt-md-0">
                  <?php 
                      $ansText =  array('A','B','C','D','E');
                      for($i = 0; $i < $test->num_of_answer; $i++): 
                      //create sessionstorage for answer state
                      // $objAnswer = array(
                      //   "question_id" => $test->id,
                      //   "question_choice" => $i,
                      //   "isActive" => false
                      // );
                      // array_push($answerState, $objAnswer);
                  ?>
                    <div class="choice ref-choice" question_id="<?= $test->id?>" question_choice="<?= $i?>"><?=$ansText[$i]?></div>
                  <?php endfor; ?>
                  <!-- <div class="choice">A</div>
                  <div class="choice">B</div>
                  <div class="choice">C</div>
                  <div class="choice">D</div>
                  <div class="choice">E</div> -->
                </div>
                <div class="Q-btn-div">
                  <?php if($test->ex_no != 0):?>
                    <a><button class="Q-btn back btn-white my-1 btn-go-prev" data-current-id="<?= $test->id?>" data-previous-id="<?= $previous->id ?>">กลับ</button></a>
                  <?php endif ?>
                  <?php if($test->ex_no != count($tests->subLevelTestQuestions)  - 1):?>
                  <a><button class="Q-btn next btn-gray my-1 btn-go-next" data-current-id="<?= $test->id?>" data-next-id="<?= $next->id ?>">ถัดไป</button></a>
                  <?php endif ?>
                </div>
              </div>
            </div>
          </div>
        </div> <!-- //.questionBox -->
        <?php endforeach; ?>
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<div class="modal fade Q-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="AnswerSheet">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <div class="row">
          <!-- <div class="<?= count($tests->subLevelTestQuestions) >= 16 ? 'col-md-6' : 'col-md-12' ?> d-flex align-items-center flex-column"> -->
          <div class="<?= count($tests->subLevelTestQuestions) >= 16 ? 'col-md-6' : 'col-md-12' ?>">
            <?php foreach ($tests->subLevelTestQuestions as $key => $test): ?>
              <div class="AnswerSheetTable mb-2 d-flex flex-nowrap justify-content-between align-items-center">
                  <span class="numberEx" data-question_id="<?= $test->id?>"><?=$test->ex_no+1?></span>
                  <div class="flex-grow-1">
                    <?php 
                      $ansText =  array('A','B','C','D','E');
                      for($i = 0; $i < $test->num_of_answer; $i++): 
                    ?>
                    <div class="choiceAnswerSheet rounded-circle" data-question_id="<?= $test->id?>" question_id="<?= $test->id?>" question_choice="<?= $i?>"><?=$ansText[$i]?></div>
                    <?php endfor ?>
                  </div>
              </div>
            <?php if($key == floor(count($tests->subLevelTestQuestions)/2) && count($tests->subLevelTestQuestions) >= 16): ?>
              </div>
              <div class="col-md-6 d-flex align-items-center flex-column">
            <?php endif ?>
            <?php endforeach ?>
          </div>
          <!-- <div class="col-md-6">
            <?php
            for($i=16; $i<=30; $i++){
                echo '<div class="AnswerSheetTable mb-2 d-flex flex-nowrap justify-content-between align-items-center">
                <span class="numberEx">'.$i.'. </span>
                <div class="flex-grow-1">
                  <div class="choiceAnswerSheet rounded-circle">A</div>
                  <div class="choiceAnswerSheet rounded-circle active">B</div>
                  <div class="choiceAnswerSheet rounded-circle">C</div>
                  <div class="choiceAnswerSheet rounded-circle">D</div>
                  <div class="choiceAnswerSheet rounded-circle">E</div>
                </div>
              </div>';
            }
            ?>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="timeAlert" tabindex="-1" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="timeAlertContent">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <div class="txt">เหลือเวลาอีก 5 นาที <br/> จะหมดเวลาในการทําข้อสอบ</div>
          <!-- <div class='prog-bar-timeAlert'>
              <div class="prog-bar">
                <div class="background" style=" -webkit-clip-path: inset(0 0 0 0); clip-path: inset(0 0 0 0);">
                </div>
              </div>
          </div> -->
          <div class="px-0 px-md-3 align-self-center"><button onclick="closeTimeAlert()" class="Q-btnTimeAlert btn-gray">รับทราบ</button></div>
          <!-- <div class="prog-text-question f18"><i class="fa fa-clock-o" aria-hidden="true"></i> 00 : 05 : 00</div> -->
          <!-- <div class="prog-text-ansSheet f18"><i class="fa fa-clock-o" aria-hidden="true"></i> 00 : 44 : 55</div> -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modelNotComplete" tabindex="-1" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="timeAlertContent">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <div class="txt">กรุณาตอบคำถามให้ครบ</div>
          <!-- <div class='prog-bar-timeAlert'>
              <div class="prog-bar">
                <div class="background" style=" -webkit-clip-path: inset(0 0 0 0); clip-path: inset(0 0 0 0);">
                </div>
              </div>
          </div> -->
          <div class="px-0 px-md-3 align-self-center"><button onclick="closeAnswerAlert()" class="Q-btnTimeAlert btn-gray">รับทราบ</button></div>
          <!-- <div class="prog-text-question f18"><i class="fa fa-clock-o" aria-hidden="true"></i> 00 : 05 : 00</div> -->
          <!-- <div class="prog-text-ansSheet f18"><i class="fa fa-clock-o" aria-hidden="true"></i> 00 : 44 : 55</div> -->
      </div>
    </div>
  </div>
</div>
<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/js/sweetalert2.all.min.js"></script>
<script>

  const testID = '<?= $test_id ?>'
  const sheetID = '<?= $tests->id ?>'
  const sessionStart = 'test_start_' + sheetID
  const sessionQuestion = 'test_question_' + sheetID
  // const sessionAnswerState = 'test_answer_state_' + sheetID
  const sessionAnswer = 'test_answer_' + sheetID
  setSessionStore(sessionStart, new Date('<?= $start_time?>').toISOString());
  var count = setTimer()
  initSessionStore()
    const boxQuestionId = 'question_id_'
    $(document).ready(function () {

      $('.choiceAnswerSheet').click(function (e) { 
        e.preventDefault();
        let id = $(this).data('question_id')
        $('.questionBox').addClass('d-none')
        $(`#${boxQuestionId}${id}`).removeClass('d-none')
        $('.Q-modal').modal('hide')
      });

      $('.numberEx').click(function (e) { 
        e.preventDefault();
        let id = $(this).data('question_id')
        $('.questionBox').addClass('d-none')
        $(`#${boxQuestionId}${id}`).removeClass('d-none')
        $('.Q-modal').modal('hide')
      });

      $('.btn-go-next').click(function (e) { 
        e.preventDefault();
        let nextId = $(this).data('next-id')
        let currentId = $(this).data('current-id')

        $(`#${boxQuestionId}${currentId}`).addClass('d-none')
        $(`#${boxQuestionId}${nextId}`).removeClass('d-none')
      });
      
      $('.btn-go-prev').click(function (e) { 
        e.preventDefault();
        let previousId = $(this).data('previous-id')
        let currentId = $(this).data('current-id')

        $(`#${boxQuestionId}${currentId}`).addClass('d-none')
        $(`#${boxQuestionId}${previousId}`).removeClass('d-none')
      });

      
      $('.ref-choice').click(function(){ 
        
        let question_id = $(this).attr('question_id');
        let question_choice = $(this).attr('question_choice');

        let answerList = getSessionStore(sessionAnswer)
        let selectedAnswer = answerList.find(a => a.question_id == question_id)
        if(selectedAnswer){
          let idx = answerList.findIndex(a => a.question_id == question_id)
          answerList[idx] = {question_id, question_choice}
        }else{
          answerList.push({question_id, question_choice})
        }
        setSessionStore(sessionAnswer, answerList)
        setStyleChoices()
        $.post("<?=base_url("Test/UpdateAnswer");?>", {answerList, test_id: testID})
        .done(res => console.log(res))
        // console.log( {answerList, test_id: testID})
        
      });
    });
  
function setProgressBar(){
  
  let answerList = getSessionStore(sessionAnswer)
  let btnActive = answerList.length
  
  let questionList = getSessionStore(sessionQuestion).length
  let percent = btnActive / questionList * 100
  
  $(".progress-bar").css({"clip-path" : `inset(0 ${100 - percent}% 0 0)`})
}
    
function setStyleChoices(){
  let answerList = getSessionStore(sessionAnswer)
  $('.ref-choice').removeClass('active')
  $('.choiceAnswerSheet').removeClass('active')
  answerList.forEach(item => {
    $(`[question_id='${item.question_id}'][question_choice='${item.question_choice}']`)
      .addClass('active')
  })
  
  setProgressBar()
}
    
function setSessionStore(item, value){
	localStorage.setItem(item, JSON.stringify(value));
}
function getSessionStore(item){
  return	JSON.parse(localStorage.getItem(item));
}

function initSessionStore(){
  setSessionStore(sessionAnswer, <?php echo json_encode($answer) ?>);
  setSessionStore(sessionQuestion, <?php echo json_encode($tests->subLevelTestQuestions) ?>);
  setStyleChoices()
}

function deleteStore(){
	localStorage.removeItem(sessionQuestion);
	localStorage.removeItem(sessionAnswer);
	localStorage.removeItem(sessionStart);
}

function setTimer(){
  let sessionDate = getSessionStore(sessionStart)
  if(sessionDate == null)
    return Number.parseInt(<?= $tests->test_time?>)
  let start = new Date(sessionDate)
  let now = new Date()
  let diffSec = Number.parseInt((now - start)/1000)
  
  return Number.parseInt(<?= $tests->test_time?>) - diffSec
}

function sendAnswer(){

  let answerList = getSessionStore(sessionAnswer)
  let btnActive = answerList.length
  let questionList = getSessionStore(sessionQuestion)
  
  if(btnActive !== questionList.length)
  {
    $('#modelNotComplete').modal('show')
    return false
  }
  updateFinishTest(answerList)

}


function sendAnswerForce(){

let answerList = getSessionStore(sessionAnswer)
let btnActive = answerList.length
let questionList = getSessionStore(sessionQuestion)
updateFinishTest(answerList)

}

function updateFinishTest(answerList){
  let answer = JSON.stringify(answerList)
  let payload = {
    sublevel_sheet_id: sheetID,
    plan_id: <?=$plan_id;?>,
    level_id: <?=$level_id;?>,
    answer: answer,
    test_id: testID,
    time: Number.parseInt(<?= $tests->test_time?>) - count
  }

  $.post("<?=base_url("Test/FinishTest");?>", payload)
        .done(res => {
            deleteStore()
            window.location.href = '<?=base_url("Test/Result/".$tests->id."/".$level_id."/".$plan_id."/".$test_id);?>';
        })
}


function post_to_url(path, params, method) {
    method = method || "post";

    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}

    $( document ).ready(function() {
      $('#timeAlert').on('hidden.bs.modal', function (e) {
        $('.Q-modal').modal('hide')
      })
      /*var timer2 = "45:00";
      var interval = setInterval(function() {
        var timer = timer2.split(':');
      //by parsing integer, I avoid all extra string processing
        var minutes = parseInt(timer[0], 10);
        var seconds = parseInt(timer[1], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes < 0) clearInterval(interval);
        seconds = (seconds < 0) ? 59 : seconds;
        seconds = (seconds < 10) ? '0' + seconds : seconds;
        //minutes = (minutes < 10) ?  minutes : minutes;
        $('.countdown').html(minutes + ':' + seconds);
        timer2 = minutes + ':' + seconds;
      }, 1000);*/
      function formatTime(seconds) {
          var h = Math.floor(seconds / 3600),
              m = Math.floor(seconds / 60) % 60,
              s = seconds % 60;
          if (h < 10) h = "0" + h;
          if (m < 10) m = "0" + m;
          if (s < 10) s = "0" + s;
          return h + ":" + m + ":" + s;
      }
      
      var counter = setInterval(timer, 1000);

      function timer() {
          count--;
          if(count == 300) {$('#timeAlert').modal('show')}
          $('.countdown').html(formatTime(count));
          if(count < 0 ){
            // refreshStore()
            sendAnswerForce()
            // location.reload()
          }
          if (count < 0) return clearInterval(counter);
          //document.getElementById('timer').innerHTML = formatTime(count);
      }
    });
  
  function closeTimeAlert(){
    $('#timeAlert').modal('hide')
  }

  
  function closeAnswerAlert(){
    $('#modelNotComplete').modal('hide')
  }

  // function updateMemberTest(answerList){
  //   $.post("<?=base_url()."Test/UpdateAnswer";?>", {answerList})
  // }
</script>

<?php
function getQuestion($api_url, $questionName){
  $headers = array("accept: application/json",'Content-Type: application/json');
  $method = 'GET';
  $url = $api_url.'gen-signed-url/'.$questionName;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
  $response = curl_exec($curl);
  curl_close($curl);
  return json_decode($response);
  }
?>