$(function() {
  $('.box-subject-items').addClass('d-none');
  $('.see-more').addClass('d-none');
  $('.btnLib').each(function() {
    $(this).removeClass('un-active');
    $(this).removeClass('active');

    img = $(this).data('img');
    $(this).find("img").attr("src", img);
    $(this).addClass('un-active');
  });

  // active เมนูด้านบนจาก sub ที่ส่งมา
  var activeSub = "<?=$current_subject;?>";
  activeSubject($('div.' + activeSub));

  $('.btnLib').on("click", function() {
    $('.btnLib').each(function() {
      $(this).removeClass('un-active');
      $(this).removeClass('active');

      img = $(this).data('img');
      $(this).find("img").attr("src", img);
      $(this).addClass('un-active');

      $('.box-subject-items').addClass('d-none');
      $('.see-more').addClass('d-none');
    });
    activeSubject($(this));
  });

  function activeSubject(btn) {
    boxId = $(btn).data('box-id');
    imgActive = $(btn).data('img-active');

    $('.box-subject-' + $(btn).data('box-subject')).removeClass('d-none');
    $('.see-more').removeClass('d-none');

    btn.find("img").attr("src", imgActive);
    btn.removeClass('un-active');
    btn.removeClass('active');
    btn.addClass('active');
  }
});

var hasRecentVdo = false, initSlick = false;

//Set init Slick
$(".myEveryday-slide, .regular, .subSlider, .sheetSlider").on('init.slick', function(event, slick, direction){
  initSlick = true;
});
var menuSlider = $(".myEveryday-slide").slick({
  slidesToShow: 6.5,
  slidesToScroll: 6,
  dots: false,
  arrows: false,
  infinite: false,
  //focusOnSelect: true,
  //swipeToSlide: true,
  responsive: [
  {
    breakpoint: 768,
    settings: {
      arrows: false, slidesToShow: 3.5, slidesToScroll: 3
    }
  },
  {
    breakpoint: 480,
    settings: {
      arrows: false, slidesToShow: 2.5, slidesToScroll: 2
    }
  }
  ]
});

$(".regular").slick({
  slidesToShow: 4.5,
  slidesToScroll: 4,
  dots: false,
  infinite: false,
  arrows: false,
  responsive: [
  {
    breakpoint: 1025,
    settings: {
      arrows: false, slidesToShow: 4.5, slidesToScroll: 4
    }
  },
  {
    breakpoint: 769,
    settings: {
      arrows: false, slidesToShow: 3.5, slidesToScroll: 3
    }
  },
  {
    breakpoint: 480,
    settings: {
      arrows: false, slidesToShow: 2.5, slidesToScroll: 2
    }
  }
  ]
});

$(".con_regular").slick({
  slidesToShow: 4.5,
  slidesToScroll: 4,
  dots: false,
  infinite: false,
  responsive: [
  {
    breakpoint: 1025,
    settings: {
      arrows: false, slidesToShow: 4.5, slidesToScroll: 4
    }
  },
  {
    breakpoint: 769,
    settings: {
      arrows: false, slidesToShow: 3.5, slidesToScroll: 3
    }
  },
  {
    breakpoint: 480,
    settings: {
      arrows: false, slidesToShow: 2.5, slidesToScroll: 2
    }
  }
  ]
});

$(".regular").on( "click", ".level-box", function() {
  $(".levelSlider").slick('slickGoTo', $(this).data('index_row')); //Tricker for active it self
  let isComingsoon = $(this).data('is-comingsoon');
  if(isComingsoon)
    return false
  $('.level-box').removeClass('active');
  $('.level-box').addClass('gray');
  $('.slide-box-sub').removeClass('gray');
  $('.slide-box-sub').removeClass('active');
  $('.slide-box-sub').addClass('gray');
  $(this).addClass('active');
  $(this).removeClass('gray');

  // ซ้อน sheet
  $('.sheet-header-box, .subSheet, .sheet-box, .sheet-test-box').addClass('d-none');

  let title = $(this).data('title-level');
  let levelId = $(this).data('level-id');
  let planId = $(this).data('plan-id');

  // แสดง Sublevel
  $('.subLevel-box-header, .subDetail').removeClass('d-none');
  $('.subLevel-title').text(title);
  $('.slide-box-sub').addClass('d-none');
  // แสดง Sub level
  $('.slide-box-sub[data-level-id="'+levelId+'"]').removeClass('d-none');

  $('.regular').slick('setPosition');
  if(!hasRecentVdo){
    $(`.subLevel-slider.regular`).slick('slickGoTo', 0)
  }
  hasRecentVdo=false

  callSublevel(planId, levelId);

  $(".subSlider  div.slick-slide:has( > div > div.d-none)").hide();
  $(".subSlider  div.slick-slide:has( > div > div:not(.d-none))").show();
  $(".subSlider .slick-slide").removeClass( "foo" );
  $(".subSlider .slick-slide div.myEveryDay-slide-box").not(".d-none").parent().parent().addClass( "foo" );
  $(".subSlider .foo").last().find(".icon-arrow").addClass( "d-none" );
});

// เลือก Sub level
$(".regular").on( "click", ".slide-box-sub", function() {
  $(".subSlider").slick('slickGoTo', $(this).data('index_row')); //Tricker for active it self
  let isComingsoon = $(this).data('is-comingsoon');
  if(isComingsoon)
    return false

  $('.slide-box-sub').removeClass('active');
  $('.slide-box-sub').addClass('gray');
  $(this).addClass('active');
  $(this).removeClass('gray');

  // ซ้อน sheet
  $('.sheet-box, .sheet-test-box').addClass('d-none');

  let title = $(this).data('title-level');
  let planId = $(this).data('plan-id');
  let levelId = $(this).data('level-id');
  let subLevelId = $(this).data('sub-level-id');

  // แสดงกล่อง sheet
  $('.sheet-header-box, .subSheet').removeClass('d-none');
  $('.sheet-title').text(title);

  $('.sheet-box[data-level-id="'+levelId+'"][data-sub-level-id="'+subLevelId+'"]').removeClass('d-none');
  $('.sheet-test-box[data-level-id="'+levelId+'"][data-sub-level-id="'+subLevelId+'"]').removeClass('d-none');

  $('.regular').slick('setPosition');
  $(`.sheet-slider.regular`).slick('slickGoTo', 0)

  $("div.slick-slide:has( > div > div.d-none)").hide();
  $("div.slick-slide:has( > div > div:not(.d-none))").show();
  $(".regular .slick-slide").removeClass( "foo" );
  $(".regular .slick-slide div.myEveryDay-slide-box").not(".d-none").parent().parent().addClass( "foo" );
  $(".regular .foo").last().find(".icon-arrow").addClass( "d-none" );

  callSubSheet(planId, levelId, subLevelId);
});


show_recent_vdo()
function show_recent_vdo(){
  const recent_vdo = JSON.parse('<?php echo json_encode($recent_vdo); ?>');
  if(recent_vdo != null){
    hasRecentVdo = true
    $(`.level-box[data-level-id="${recent_vdo.level}"]`).trigger('click')
    $(`.slide-box-sub[data-level-id="${recent_vdo.level}"][data-sub-level-id="${recent_vdo.sublevel}"]`).trigger('click')
    let index_level = $(`.level-box[data-level-id="${recent_vdo.level}"]`).data('index_row')
    let index_sublevel = $(`.slide-box-sub[data-level-id="${recent_vdo.level}"][data-sub-level-id="${recent_vdo.sublevel}"]`).data('index_row')
    if(index_level){
      try {
        $(`#level-slick.regular`).slick('slickGoTo', index_level)
      }
      catch(error) {
          setTimeout(show_recent_vdo, 100);
          return;
      }
    }
    
    if(index_sublevel){
      try {
        $(`.subLevel-slider.regular`).slick('slickGoTo', index_sublevel)
      }
      catch(error) {
          setTimeout(show_recent_vdo, 100);
          return;
      }
    }    
  }
}//end $();

// Active sublevel ล่าสุด
function activeSubCurrent() {
  let recentVdo = JSON.parse('<?php echo json_encode($recent_vdo); ?>');
  if(recentVdo.level && recentVdo.sublevel) {
    $(`.slide-box-sub[data-level-id="${recentVdo.level}"][data-sub-level-id="${recentVdo.sublevel}"]`).trigger('click');
    // slickGoTo ไปที่อันที่ current
    let indexSublevel = $(`.slide-box-sub[data-level-id="${recentVdo.level}"][data-sub-level-id="${recentVdo.sublevel}"]`).data('index_row');
    $(".subSlider").slick('slickGoTo', indexSublevel);
  }
}

function destroySlickSub() {
  if ($('.subSlider').hasClass('slick-initialized')) {
    $('.subSlider').slick('destroy');
  }      
}

function destroySlickSheet() {
  if ($('.sheetSlider').hasClass('slick-initialized')) {
    $('.sheetSlider').slick('destroy');
  }      
}

function getSliderSettings(){
  return {
    slidesToShow: 4.5,
    slidesToScroll: 4,
    dots: false,
    infinite: false,
    arrows: false,
    slickSetOption: true,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          arrows: false, slidesToShow: 4.5, slidesToScroll: 4
        }
      },
      {
        breakpoint: 769,
        settings: {
          arrows: false, slidesToShow: 3.5, slidesToScroll: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false, slidesToShow: 2.5, slidesToScroll: 2
        }
      }
    ]
  }
}

// เรียก Sublevel
function callSublevel(planId, levelId) {
  // Get ข้อมูล sub level
  $.ajax({
    type: "GET",
    url: "<?=base_url('MyProfile/get_sub_level')?>",
    data: {plan_id : planId, library_level_id : levelId},
    dataType: "json",
    success: function(data) {
      let boxSubLevel = $('.subSlider');
      destroySlickSub();
      boxSubLevel.empty();
      $.each(data, function(key, subLevel) {
        let comingsoon = subLevel.is_comingsoon == 0 ? 'd-none' : '';
        let arrowComingSoon = subLevel.is_comingsoon == 0 ? '' : 'getComingSoon';
        let $dataBox = "<div class='myEveryDay-slide-box slide-box-sub position-relative'" +
                      "data-is-comingsoon="+subLevel.is_comingsoon+" data-level-id="+subLevel.libraryLevelId+" " +
                      "data-sub-level-id="+subLevel.id+" data-title-level='"+subLevel.title+"' data-plan-id="+subLevel.plan_id+" data-index_row="+key+">"+
                      "<div class='d-flex'>" +
                      "<div class='content'>" +
                      "<img src="+subLevel.imgUrl+">" +
                      "<div class='progress'>" +
                      "<div class='progress-bar bg-success' role='progressbar' style='width: "+subLevel.progress_bar+"%' aria-valuenow='"+subLevel.progress_bar+"' aria-valuemin='0' aria-valuemax='100'></div>" +
                      "</div>" +
                      "<div class='libraryTitle m-lib textBtmMyeverday'>"+subLevel.title+"</div>"+
                      "<div class='status'><div class='comingSoon "+comingsoon+"'>COMING SOON</div></div>"+
                      "</div>" +
                      "<div class='arrow align-self-center'>" +
                      "<div class='icon-arrow'><i class='fa fa-angle-right'></i></div>" +
                      "</div>" +
                      "</div>" +
                      "</div>";
        boxSubLevel.append($dataBox);
      });
      setTimeout(function () {
        $(".subSlider").slick(getSliderSettings());
        activeSubCurrent();
      }, 100);
    }
  });
}

// เรียก subSheet
function callSubSheet(planId, levelId, subLevelId) {
  $.ajax({
    type: "GET",
    url: "<?=base_url('MyProfile/get_sub_sheet')?>",
    data: {plan_id : planId, library_level_id : levelId, sub_level_id : subLevelId},
    dataType: "json",
    success: function(data) {
      let boxSubLevelSheet = $('.subSheet[data-plan-id="'+planId+'"]').find('.slider');
      destroySlickSheet();
      boxSubLevelSheet.empty();
      data.sheets.sort(function(a, b) {
        return a.sheetcode.localeCompare(b.sheetcode)
      });
      $.each(data.sheets, function(key, subLevelSheet) {
        let comingsoon = subLevelSheet.is_comingsoon == 0 ? 'd-none' : '';
        let $dataBox = "";
        if(!subLevelSheet.is_comingsoon) {
          $dataBox = "<div class='myEveryDay-slide-box sheet-box position-relative'" +
                        "data-is-comingsoon="+subLevelSheet.is_comingsoon+" data-level-id="+subLevelSheet.libraryLevelId+" " +
                        "data-sub-level-id="+subLevelSheet.id+" data-title-level="+subLevelSheet.title+" data-plan-id="+subLevelSheet.plan_id+">"+
                        "<div class='d-flex'>" +
                        "<div class='content'>" +
                        "<a class='link-video' href='<?=base_url('MyEveryDay/Video/')?>"+subLevelSheet.id+"'>" +
                        "<img src="+subLevelSheet.imgUrl+">" +
                        "</a>" +
                        "<div class='progress'>" +
                        "<div class='progress-bar bg-success' role='progressbar' style='width: "+subLevelSheet.progress_bar+"%' aria-valuenow='"+subLevelSheet.progress_bar+"' aria-valuemin='0' aria-valuemax='100'></div>" +
                        "</div>" +
                        "<div class='libraryTitle m-lib textBtmMyeverday'>"+subLevelSheet.title+"</div>"+
                        "<div class='status'><div class='comingSoon "+comingsoon+"'>COMING SOON</div></div>"+
                        "</div>" +
                        "<div class='arrow align-self-center'>" +
                        "<div class='icon-arrow'><i class='fa fa-angle-right'></i></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
        }
        else {
          $dataBox = "<div class='myEveryDay-slide-box sheet-box position-relative'" +
                        "data-is-comingsoon="+subLevelSheet.is_comingsoon+" data-level-id="+subLevelSheet.libraryLevelId+" " +
                        "data-sub-level-id="+subLevelSheet.id+" data-title-level="+subLevelSheet.title+" data-plan-id="+subLevelSheet.plan_id+">"+
                        "<div class='d-flex'>" +
                        "<div class='content'>" +
                        "<img src="+subLevelSheet.imgUrl+">" +
                        "<div class='progress'>" +
                        "<div class='progress-bar bg-success' role='progressbar' style='width: "+subLevelSheet.progress_bar+"%' aria-valuenow='"+subLevelSheet.progress_bar+"' aria-valuemin='0' aria-valuemax='100'></div>" +
                        "</div>" +
                        "<div class='libraryTitle m-lib textBtmMyeverday'>"+subLevelSheet.title+"</div>"+
                        "<div class='status'><div class='comingSoon "+comingsoon+"'>COMING SOON</div></div>"+
                        "</div>" +
                        "<div class='arrow align-self-center'>" +
                        "<div class='icon-arrow'><i class='fa fa-angle-right'></i></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
        }

        boxSubLevelSheet.append($dataBox);
      });

      if(data.test_sheets.length > 0) {
        $.each(data.test_sheets, function(key, subLevelSheetTest) {
          let $dataBox = "<div class='myEveryDay-slide-box sheet-box'" +
                        "data-level-id="+subLevelSheetTest.libraryLevelId+" " +
                        "data-sub-level-id="+subLevelSheetTest.id+" data-title-level="+subLevelSheetTest.title+" data-plan-id="+subLevelSheetTest.plan_id+">"+
                        "<div class='d-flex'>" +
                        "<div class='content'>" +
                        "<a href='<?=base_url('Test/index/')?>"+subLevelSheetTest.id+"/"+subLevelSheetTest.libraryLevelId+"/"+subLevelSheetTest.plan_id+"'>" +
                        "<img src='<?=base_url('resources/img/cover/Everyday_Placeholder_Test.png')?>'>" +
                        "</a>" +
                        "<div class='libraryTitle m-lib textBtmMyeverday'>"+subLevelSheetTest.title+"</div>"+
                        "</div>" +
                        "<div class='arrow align-self-center'>" +
                        "<div class='icon-arrow'><i class='fa fa-angle-right'></i></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>";

          boxSubLevelSheet.append($dataBox);
        });
      }
      setTimeout(function () {
        $(".sheetSlider").slick(getSliderSettings());
      }, 100);
    }
  });
}