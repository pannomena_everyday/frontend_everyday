<div id="wrap">
  <div id="container">
    
    <div class="whiteContentBox">
      <div class="testTitle">คำอธิบาย</div>
      <div class="testDetail">
        <div class="testDetailRow d-flex align-content-center">
          <div class="testDetailNo align-self-center">1</div>
          <div class="px-2 p-t-5 flex-grow-1">แบบทดสอบก่อนเรียน</div>
        </div>
        <div class="testDetailRow d-flex align-content-center">
          <div class="testDetailNo align-self-center">2</div>
          <div class="px-2 p-t-5 flex-grow-1">จำนวน <?php if (!empty($plan->planExercices)) { echo count($plan->planExercices); } ;?> ข้อ</div>
        </div>
        <div class="testDetailRow d-flex align-content-center">
          <div class="testDetailNo align-self-center">3</div>
          <div class="px-2 p-t-5 flex-grow-1">ไม่จำกัดเวลาในการทำข้อสอบ</div>
        </div>
        <div class="testDetailRow d-flex align-content-center">
          <div class="testDetailNo align-self-center">4</div>
          <div class="px-2 p-t-5 flex-grow-1">ต้องทำจนครบทุกข้อ</div>
        </div>
        <div class="testDetailRow d-flex align-content-center">
          <div class="testDetailNo align-self-center">5</div>
          <div class="px-2 p-t-5 flex-grow-1">คะแนนสอบจะช่วยดูความเหมาะสมในการเลือกแผนการเรียน</div>
        </div>
      </div>
      <div class="center">
        <?php $planId = $this->input->get('plan')?>
        <!-- <a href="<?=base_url('PreTest/question?plan='.$planId);?>"><button class="test-btn btn-green-white"><i class="fa fa-play"></i> เริ่มทำแบบทดสอบก่อนเรียน</button></a> -->
        <button onclick="createTest()" class="test-btn btn-green-white"><i class="fa fa-play"></i> เริ่มทำแบบทดสอบก่อนเรียน</button>
      </div>
    </div>  
    
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script>
var isNotSend = true;
function createTest(){
  if(isNotSend){
    isNotSend = false
    let payload = {
      plan_id: <?=$planId;?>
    }
    $.post("<?=base_url("PreTest/createTest");?>", payload)
    .done(res => {
      window.location.href = '<?=base_url("PreTest/Question?plan=".$planId);?>';
    })
  }
}
</script>