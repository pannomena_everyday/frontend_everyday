<div id="wrap">
  <div id="container">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="T-Score-box">
          <div class="title">เฉลยคำตอบ</div>
          <div class="subTitle"><?= $tests->title ?> </div>
          <div class="row column-2">
            <!-- <div class="col-md-6 subBox"> -->
            <div class="<?= count($tests->subLevelTestQuestions) >= 16 ? 'col-md-6' : 'col-md-12' ?>">
              <?php $row = 1; ?>
              <?php //foreach ($answer as $idx => $item): ?>
              <?php foreach ($tests->subLevelTestQuestions as $key => $test): ?>
                <div class="AnswerSheetTable">
                  <span><?= $row?></span>
                  <?php 
                      $ansText =  array('A','B','C','D','E');
                      $isAnswer = false;
                      $styleAnswer = '';
                      $incorrectAnswer = -1;
                      if(isset($test_answer[$test->id])){
                        $isAnswer = true;
                        $styleAnswer = 'correct';
                        if($test_answer[$test->id]->question_choice != $test->answer){
                          $incorrectAnswer =  $test_answer[$test->id]->question_choice;
                          $styleAnswer = 'answer';
                        }
                      }else{
                        $styleAnswer = 'answer';
                      }

                      for($i = 0; $i < $test->num_of_answer; $i++): 
                      // $showIncorrect = false;
                      // if(isset($answer->{$test->id}->question_choice)){
                      //   if($answer->{$test->id}->question_choice == $i){
                      //     if(!$answer->{$test->id}->is_correct){
                      //       $showIncorrect = true;
                      //     }
                      //   }
                      // }
                  ?>
                  <div class="choiceScore 
                    <?= $incorrectAnswer == $i ? 'inCorrect' : '' ?>
                    <?= $test->answer == $i ? $styleAnswer : '' ?>
                    <?= !$isAnswer  && $test->answer != $i ? 'disabled' : ''?>
                    "
                  >
                    <?= $ansText[$i]?>
                  </div>
                  <?php endfor ?>
                </div>
              <?php $row += 1; ?>
              <?php if(ceil($key + 1) == ceil(count($tests->subLevelTestQuestions)/2) && count($tests->subLevelTestQuestions) >= 16): ?>
                </div>
                <div class="col-md-6">
              <?php endif ?>
              <?php endforeach ?>
              </div>
          </div> <!-- //.row -->
          <div class="btn-div">
          	<div class="d-flex justify-content-center flex-wrap">
            <?php
              if(!$is_pass):
            ?>
          	<div class="p-2 m-w-300 w-100">
          		<a href="<?= $prev_sheet != "" ? base_url("MyEveryDay?subject=$subject&l=".$prev_obj['level']."&sl=".$prev_obj['sub_level']."&sh=".$prev_obj['sheet']) : base_url('MyEveryDay')?>"><button class="btn-60 btn-green-white">เรียนอีกครั้ง</button></a>
          	</div>
            <?php endif;?>
          	<div class="p-2 m-w-300 w-100">
          		<a href="<?= $next_sheet != "" ? base_url("MyEveryDay?subject=$subject&l=".$next_obj['level']."&sl=".$next_obj['sub_level']."&sh=".$next_obj['sheet']) : base_url('MyEveryDay')?>"><button class="btn-60 <?= $is_pass ? "btn-green-white" : "btn-white-green" ?>">เนื้อหาถัดไป</button></a>
          	</div>
          	<div class="p-2 m-w-300 w-100">
          		<a href="<?=base_url("Test/Result/".$sheet_id."/".$level_id."/".$plan_id."/".$test_id);?>"><button class="btn-60 btn-white-green">ดูสรุปคะแนน</button></a>
          	</div>
          </div>
            
          </div>
        </div> <!-- //.T-Score-box -->
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->