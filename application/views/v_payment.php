<!-- css form payment -->

<!-- สรุปรายการสั่งซื้อ และชำระเงิน -->
<div id="wrap">
    <div id="container" class="bg-gray-f7">
        <div class="whiteContentBox set-full-page p-3 p-md-4">
            <h1 class="payment-sumary text-center mb-70">สรุปรายการสั่งซื้อ<br class="break-line">และชำระเงิน</h1>
            <div class="row">
                <div class="col-md-6 mb-4 mb-md-0">
                    <div class="card">
                        <div class="card-body p-3">
                            <h5 class="card-title mb-3">สรุปยอดชำระเงิน</h5>
                            <div class="d-flex justify-content-between">
                                <ul class="payment-detail list-unstyled pl-0 pr-3 mb-0">
                                    <li><?= $subscription_type->title ?></li>
                                    <li class="info text-blue">ชำระทุก <?=$subscription_type->month_period ?> เดือน</li>
                                    <?php 
                                        $dt = DateTime::createFromFormat('Y-m-d', $payment->expiredate != null ? $payment->expiredate : $payment->billdate);
                                    ?>
                                    <li class="remark"><?= $payment->expiredate != null ? "วันหมดอายุ" : "เริ่มตัดเงินวันที่" ?> <?= $dt->format("d/m/Y")?></li>
                                </ul>
                                <div class="text-right"><?= number_format($payment->packaage->price, 2) ?></div>
                            </div>
                            <hr>
                            <?php
                              if($payment->discount > 0):
                            ?>
                            <div class="payment-discount">
                                <p>ส่วนลด</p>
                                <div class="d-flex justify-content-between">
                                    <div class="info text-blue">ลดราคา <?= number_format($payment->discount, 2) ?> บาท</div>
                                    <div class="text-right">-<?= number_format($payment->discount, 2) ?></div>
                                </div>
                            </div>
                            <hr>
                            <?php
                              endif;
                            ?>
                            <div class="d-flex justify-content-between text-blue">
                                <div class="info">ยอดชำระสุทธิ</div>
                                <div class="text-right"><strong><?= number_format($payment->net_price, 2) ?></strong></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex flex-column">
                    <p class="text-blue">บัตรเครดิต / เดบิต</p>
                    <p class="mb-4">สามารถชำระเงินผ่านบัตรเครดิตได้ทุกธนาคาร</p>
                    <div class="mb-3">
                        <img src="/resources/img/payment/visa.svg" alt="VISA" class="creditcard mr-3 d-inline-block">
                        <img src="/resources/img/payment/master.svg" alt="MASTER" class="creditcard dinline-block">
                    </div>
                    <div class="mb-3 info-user">
                        <label class="form-check-label" for="input-confirm">
                        <input type="checkbox" class="form-check-input" id="input-confirm" autocomplete="off">
                            ข้าพเจ้าอ่าน เข้าใจและยอมรับในเงื่อนไขการใช้บริการ
                            และนโยบายการชําระเงินและการส่งเสริมการขาย
                        </label>
                    </div>
                    <div class="row">  
                        <div class="col-sm-4 mb-4 mb-sm-0">              
                            <a href="<?= base_url('Membertype')?>"><button class="col btn-lg rounded-pill btn-gray" type="button">กลับ</button></a>
                        </div>
                        <div class="col-12 col-sm-6 col-md-8 col-lg-7 col-xl-6">              
                            <button class="col btn-lg rounded-pill btn-gray" type="button" id="checkoutButton" disabled>ดำเนินการชำระเงิน</button>
                        </div>
                    </div>
                      <!-- <button class="btn-lg rounded-pill mt-auto btn-black" type="button" id="checkoutButton">ดำเนินการชำระเงิน</button> -->
                    
                </div>
            </div>
        </div>
    </div>
</div>
<form id="checkoutForm" method="POST" action="<?=base_url('payment/add_card')?>">
<input type="hidden" name="promotion_code" value=<?= isset($payment->promotion) ? $payment->promotion->promoCode : '' ?>>
<input type="hidden" name="subscription_id" value="<?=$payment->packaage->id?>">
<input type="hidden" name="amount" value="<?= $payment->net_price ?> ">
</form>
<!-- End สรุปรายการสั่งซื้อ และชำระเงิน -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script>
  var button = document.querySelector("#checkoutButton");
  var form = document.querySelector("#checkoutForm");

  button.addEventListener("click", (event) => {
    form.submit();
  });

  
  $(document).ready(function() {
    $('#input-confirm').change(function(){
        $('#checkoutButton').prop('disabled', !this.checked)
        $('#checkoutButton').removeClass('btn-gray btn-black')
        $('#checkoutButton').addClass(this.checked ? 'btn-black' : 'btn-gray')
    })
  })
</script>