<link rel="stylesheet" href="<?=base_url('resources/slick-master/slick/slick.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/slick-master/slick/slick-theme.css');?>">
<style>

</style>
<div id="wrap">
  <div id="container" style="background: #000;">
    <div class="libraryHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="library-slide slider">
            <div class="libraryBox"><div><img src="<?=base_url()."resources/img/math_active.png"?>" class="icon-subject"/>คณิตศาสตร์ </div></div>
            <div class="libraryBox"><div><img src="<?=base_url()."resources/img/physics.png"?>" class="icon-subject"/>ฟิสิกส์ </div></div>
            <div class="libraryBox"><div><img src="<?=base_url()."resources/img/bio.png"?>" class="icon-subject"/>ชีววิทยา </div></div>
            <div class="libraryBox"><div><img src="<?=base_url()."resources/img/english.png"?>" class="icon-subject"/>ภาษาอังกฤษ </div></div>
          </section>
        </div>
      </div>
      <div class="myEveryDay-info"><img src="<?=base_url()."resources/img/information.png"?>"/></div>
    </div>

    <section class="myEveryDay-section">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <div class="myEveryDay-addPlan-box">
            <img src="<?=base_url()."resources/img/startyoureveryday.png"?>" class="img-addplan"/>
            <a href="#"><button class="myEveryDay-addPlan-btn btn-green-white">เพิ่มแผนการเรียน</button></a>
          </div>
        </div>
      </div>
    </section>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/slick-master/slick/slick.min.js"></script>
<script>
  $(".library-slide").slick({
    slidesToShow: 6,
      slidesToScroll: 6,
      dots: false,
      arrows: false,
      infinite: false,
      responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              slidesToShow: 3,
              slidesToScroll: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
            arrows: false,
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          }
        ]
  });
</script>