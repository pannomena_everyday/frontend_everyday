<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">

<div id="wrap">
  <div id="container">
    <div class="myProfileHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="myProfile-slide slider">
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile"?>"><img src="<?=base_url()."resources/img/account.png"?>" class="icon-subject"/>บัญชีผู้ใช้ </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyPlan"?>"><img src="<?=base_url()."resources/img/myplan.png"?>" class="icon-subject"/>แผนการเรียน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyPerformance"?>"><img src="<?=base_url()."resources/img/performance.png"?>" class="icon-subject"/>ติดตามผลงาน </a>
            </div>
            <div class="myProfileBox active">
              <img src="<?=base_url()."resources/img/sheet_active.png"?>" class="icon-subject"/><span>คลัง Sheet </span>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyLog"?>"><img src="<?=base_url()."resources/img/loghistory.png"?>" class="icon-subject"/>ประวัติการเรียน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyMembership"?>"><img src="<?=base_url()."resources/img/membership.png"?>" class="icon-subject"/>สถานะสมาชิก </a>
            </div>
          </section>
        </div>
      </div>
    </div>
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <?php $this->load->view('i_myProfileStatus'); ?>
        <section class="myNote-container">
          <div class="myNote-header">
            <div class="row">
              <div class="col-sm-12 col-md-5">
                <div class="input-group">
                  <input class="form-control py-2" placeholder="ค้นหาคลัง Sheet" id="searchMyNote" onsearch="clearSearch()" type="search" autocomplete="off">
                  <div id="DropdownSearchMyNote" class="txtsearchMyNote dropdown-menu"></div>
                  <span class="input-group-append">
                    <button class="btn btn-outline-secondary f-white btnSearchMyNote" type="button">
                      <i class="fa fa-search"></i>
                    </button>
                  </span>
                </div>
              </div>
              <div class="col-sm-12 col-md-2 myNoteDate-div">
                <span class="txtMid">ตั้งแต่วันที่</span>
              </div>
              <div class="col-sm-12 col-md-2 myNoteDate-div">
                <div class="input-group myNoteDate">
                  <input id="dateFrom" class="form-control" placeholder="" type="text" autocomplete="off">
                  <span class="input-group-append">
                    <button class="btn btn-outline-secondary f-white btnDateFrom" type="button">
                      <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    </button>
                  </span>
                </div>
              </div>
              <div class="col-sm-12 col-md-1 myNoteDate-div">
                <span class="txtMid">ถึง</span>
              </div>
              <div class="col-sm-12 col-md-2 myNoteDate-div">
                <div class="input-group myNoteDate">
                  <input id="dateTo" class="form-control" placeholder=""  type="text" autocomplete="off">
                  <span class="input-group-append">
                    <button class="btn btn-outline-secondary f-white btnDateTo" type="button">
                      <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    </button>
                  </span>
                </div>
              </div><!-- .myNoteDate-div -->
            </div><!-- .row -->
          </div><!-- .myNote-header -->

          <div class="myNote-body">
            <!-- <div class="sheet-menu-list">
              <div class="ls gradient-box" data-id="#Subject-1">คณิตศาสตร์</div>
              <div class="ls" data-id="#Subject-2">ฟิสิกส์ </div>
              <div class="ls" data-id="#Subject-3">วิทยาศาสตร์ </div>
            </div>  -->
            <div class="sheet-menu-list m-767-hide">
              <div class="d-flex justify-content-center flex-wrap">
                <!-- <div class="ls px-3 gradient-box" data-id="#Subject-2">คณิตศาสตร์</div>
                <div class="ls px-3" data-id="#Subject-3">วิทยาศาสตร์ </div> -->
                <?php
                $i = 0;
                foreach($subject->result() as $row){
                  if($i == 0){
                    echo '<div class="ls px-3 gradient-box" data-id="'.$row->id.'">'.$row->title.'</div>';
                  }else{
                    echo '<div class="ls px-3" data-id="'.$row->id.'">'.$row->title.'</div>';
                  }
                  $i++;
                }
                ?>
              </div>
            </div>

            <div class="myNote-menu-list-mobile m-767-show">
              <div class="form-group">
                <select class="custom-select" id="subjectMobile">
                  <?php
                  foreach($subject->result() as $row){
                    echo '<option value="'.$row->id.'">'.$row->title.'</option>';
                  }
                  ?>
                  <!-- <option value="#Subject-2">ฟิสิกส์</option>
                  <option value="#Subject-3">วิทยาศาสตร์</option> -->
                </select>
              </div>
            </div>

            <div class="sheet-box">
              <div class="row">
                <!-- <div class="col-xs-2-5-10 col-sm-5-10 col-md-2-10 col-lg-2-10 m-b-30">
                  <img class="mw-100" src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>" class="log-cover"/>
                  <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
                </div>
                <div class="col-xs-2-5-10 col-sm-5-10 col-md-2-10 col-lg-2-10 m-b-30">
                  <img class="mw-100" src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>" class="log-cover"/>
                  <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
                </div> -->
              </div>
            </div>

          </div><!-- .myNote-body -->
        </section><!-- .myNote-container -->
      </div>
    </div> <!-- //.containner-padding -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script>
  if($( window ).width() < 768){
    $(".myProfile-slide").slick('slickGoTo', 3);
  }
  $(document).ready(function() {
    getMyNoteHTML();
    $('#dateFrom,#dateTo').datepicker({
      changeYear: true,
      changeMonth: true,
      dateFormat: 'yy-mm-dd',
      yearRange: '-100:+0'
    });

    $('.btnDateFrom').on('click', function() {
      $('#dateFrom').focus();
    });

    $('.btnDateTo').on('click', function() {
      $('#dateTo').focus();
    });

    $('.btnSearchMyNote').on('click', function() {
      getMyNoteHTML();
    });

    $('#dateFrom,#dateTo').on('change', function() {
      getMyNoteHTML();
    });

    $("#searchMyNote").keyup(function () {
      var subject = $.trim($('#subjectMobile').val());
      var keyword = $.trim($("#searchMyNote").val());
      if(keyword.length >= 3){
        $.ajax({
          type: "POST",
          url: "<?=base_url()."MyProfile/GetSheetTitle";?>",
          data: {keyword:keyword, subject:subject},
          dataType: "json",
          success: function (data) {
            $('#DropdownSearchMyNote').empty();
            $.each(data, function (key,value) {
                if (data.length >= 0)
                //$('#DropdownSearchMyNote').append('<li role="displaySearch" ><a role="menuitem dropdownSearchli" class="dropdownlivalue">' + value['ProvinceName'] + '</a></li>');
                $('#DropdownSearchMyNote').append('<button class="dropdown-item dropdownlivalue" type="button">' + value['title'] + '</button>');
            });
            if (data.length > 0) {
                $('#searchMyNote').attr("data-toggle", "dropdown");
                $('#DropdownSearchMyNote').dropdown('toggle');
                $('#DropdownSearchMyNote').show();
            }else{
              $('#DropdownSearchMyNote').hide();
            }
            $('#searchMyNote').delay( 100 ).removeAttr('data-toggle');
          }
        });
      }else{
        $('#DropdownSearchMyNote').empty();
        $('#searchMyNote').removeAttr('data-toggle');
        $('#DropdownSearchMyNote').hide();
      }
    });

    $('.txtsearchMyNote').on('click', 'button', function () {
        $('#DropdownSearchMyNote').hide();
        $('#searchMyNote').val($(this).text());
        $('#searchMyNote').removeAttr('data-toggle');
        getMyNoteHTML();
        //searchEvent2();
    });
  });

  $( "#profile" ).click(function() {
    $('#profileBox').toggle('slow');
  });

  searchMyNote
  $('#searchMyNote').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      getMyNoteHTML();
    }
  });

  function getMyNoteHTML(){
    var subject = $.trim($('#subjectMobile').val());
    var search = $.trim($('#searchMyNote').val());
    var dateFrom = $.trim($('#dateFrom').val());
    var dateTo = $.trim($('#dateTo').val());
    $.ajax({
      type: "POST",
      url: "<?=base_url()."MyProfile/getMyNoteHTML";?>",
      data: {subject:subject,search:search,dateFrom:dateFrom,dateTo:dateTo},
      success: function (data) {
        $('.sheet-box .row').html(data);
      }
    });
  }

  $(".sheet-menu-list .ls").click(function() {
    $('.sheet-box .row').html('');
    var id = $.trim($(this).data("id"));
    var search = $.trim($('#searchMyNote').val());
    var dateFrom = $.trim($('#dateFrom').val());
    var dateTo = $.trim($('#dateTo').val());
    $("#subjectMobile").val(id);
    // $(".sheet-box").hide();
    // $(id).show();
    $(".sheet-menu-list .ls").removeClass("gradient-box");
    $(this).addClass("gradient-box");
    $.ajax({
      type: "POST",
      url: "<?=base_url()."MyProfile/getMyNoteHTML";?>",
      data: {subject:id,search:search,dateFrom:dateFrom,dateTo:dateTo},
      success: function (data) {
        $('.sheet-box .row').html(data);
      }
    });
  });

  $("#subjectMobile").change(function() {
    $('.sheet-box .row').html('');
    var id = $.trim($(this).val());
    var search = $.trim($('#searchMyNote').val());
    var dateFrom = $.trim($('#dateFrom').val());
    var dateTo = $.trim($('#dateTo').val());
    $(".sheet-menu-list .ls").removeClass("gradient-box");
    $(".sheet-menu-list .ls[data-id='" + id +"']").addClass("gradient-box");
    $.ajax({
      type: "POST",
      url: "<?=base_url()."MyProfile/getMyNoteHTML";?>",
      data: {subject:id,search:search,dateFrom:dateFrom,dateTo:dateTo},
      success: function (data) {
        $('.sheet-box .row').html(data);
      }
    });
    // var id = $(this).val();
    // $(".sheet-box").hide();
    // $(id).show();
  });
</script>