
<?php 
    // $dt = new DateTime();
    // $dt->setTimezone(new DateTimeZone('Asia/Bangkok'));
    // $dt->modify('+'.$subscription_type->free_trial.' day');
    // $date = $dt->format("d F Y");
    
    $dt = DateTime::createFromFormat('Y-m-d', $validate->expiredate != null ? $validate->expiredate : $validate->billdate);
    $date = $dt->format("d/m/Y");
?>

<div id="wrap">
    <div id="container" class="bg-gray-f7">
        <div class="whiteContentBox set-full-page d-flex justify-content-center align-items-center">
            <div class="col col-sm-8 col-md-5 text-center">
                <div class="mb-4"><img src="/resources/img/correct.svg" alt="correct" width="50"></div>
                <h5 class="mb-70">ชำระเงินสำเร็จ</h5>
                <p class=" mb-4">ท่านได้ชำระเงินจำนวน <strong><?= number_format($validate->net_price, 2) ?></strong> บาท<br> ระบบจะส่งอีเมลยืนยันการชําระเงินไปที่<br><?= $email ?> </p>
                <div class="text-left mb-5">
                    <div class="row">                        
                        <div class="col-5 pr-0 text-blue">Ref. ID</div>
                        <div class="col-7 pl-0"><?= str_pad($payment->invoice_no, 7, '0', STR_PAD_LEFT) ?></div>
                    </div>
                    <hr>
                    <div class="row">                        
                        <div class="col-5 pr-0 text-blue">สินค้า-บริการ</div>
                        <div class="col-7 pl-0"><?= $subscription_type->title ?><br> ชำระทุก <?= $subscription_type->month_period ?> เดือน</div>
                    </div>
                    <hr>
                    <?php if($validate->discount > 0): ?>
                    <div class="row">
                        <div class="col-5 pr-0 text-blue">ส่วนลด</div>
                        <div class="col-7 pl-0"><?= number_format($validate->discount, 2) ?> บาท</div>
                    </div>
                    <hr>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-5 pr-0 text-blue">ยอดชำระ</div>
                        <div class="col-7 pl-0"><?= number_format($validate->net_price, 2) ?> บาท</div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-5 pr-0 text-blue">ชำระให้</div>
                        <div class="col-7 pl-0 text-truncate">บริษัท มังกี้เอเวอรี่เดย์ จํากัด</div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-5 pr-0 text-blue"><?= $validate->expiredate != null ? "วันหมดอายุ" : "วันที่ตัดชำระเงิน" ?></div>
                        <div class="col-7 pl-0"><?= $date ?></div>
                    </div>
                    <hr>
                </div>
                <button class="col-sm-12 btn-lg rounded-pill btn-black" id="btnSubmit" type="button">ถัดไป</button>
            </div>
        </div>
    </div>
</div>
<!-- End ชำระเงินสำเร็จ -->

<form id="formSendMailFromBillingInfomation" method="POST" action="<?=base_url($url_next)?>">
<input type="hidden" name="amount" value="<?= $validate->net_price ?>">
<input type="hidden" name="subscription_id" value="<?= $subscription_type->id ?>">
<input type="hidden" name="invoice_no" value="<?= $payment->invoice_no ?>">
<?php 
    $dt = new DateTime();
    $dt->setTimezone(new DateTimeZone('Asia/Bangkok'));
?>
<input type="hidden" name="payment_date" value="<?= $date?>">
</form> 

<script>
var submit  = document.getElementById('btnSubmit'),
    form = document.getElementById("formSendMailFromBillingInfomation");

    submit.addEventListener('click', function() {
        form.submit()
    })
</script>
<!-- End ชำระเงินไม่สำเร็จ -->
