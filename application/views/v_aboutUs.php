<div id="wrap">
  <div id="container">
    <div class="about-box-1 w-100 text-center px-3 pt-5 mb-5 gradient-box-10">
      <div class="f35 af-yellow">ที่ Monkey Monkey เราเน้น <br/> “การเรียนที่เห็นผล”</div>
      <div class="f26 text-white">
        <div>ตลอด 5 ปี ที่ผ่านมา ทีมงานของเราไม่เคยหยุดออกแบบและพัฒนา</div>
        <div>จนได้ค้นพบ "Monkey Hybrid"</div>
        <div>ระบบการเรียนที่พัฒนาผู้เรียนได้อย่างตรงจุด</div>
        <div>สร้างนิสัยการเรียนรู้ที่ดี และวัดผลได้จริง</div>
      </div>
      <img class="about-1-img w-100 mt-2" src="<?=base_url("resources/img/aboutus/pic_aboutus.png")?>"/>
    </div>

    <div class="w-100 text-center pt-5 my-5 px-3">
      <div class="f26">วันนี้ เรานำประสบการณ์จากการสร้าง</div>
      <div class="f26">ระบบการเรียนที่เห็นผลจริงมาส่งต่อสู่ "Online"</div>
      <div class="f35">ด้วยแพล็ตฟอร์มการเรียนรูปแบบใหม่แบบ</div>
      <div class="f35 font-weight-bold af-red">"Monkey Everyday"</div>
    </div>

    <div class="about-box-3 mx-auto mb-5 px-3">
      <div class="d-flex flex-column flex-sm-row my-4">
        <div class="flex-fill my-2 mx-4 text-center"><img class="about-3-img w-100" src="<?=base_url("resources/img/aboutus/About_1.png")?>"/></div>
        <div class="flex-fill my-2 mx-4">
          <div class="f30 af-red">"ระบบจัดการเนื้อหา"</div>
          <div class="f26">
            ครบถ้วน ค้นหาง่าย
            เชื่อมโยงความรู้ได้เป็นระบบ
            อย่างที่ไม่เคยมีมาก่อน
          </div>
        </div>
      </div>
      <div class="d-flex flex-column flex-sm-row-reverse my-4">
        <div class="flex-fill my-2 mx-4 text-center"><img class="about-3-img w-100" src="<?=base_url("resources/img/aboutus/About_2.png")?>"/></div>
        <div class="flex-fill my-2 mx-4 text-sm-right">
          <div class="f30 af-red">"แผนการเรียนเฉพาะตัว"</div>
          <div class="f26">
            <div>เรียนได้ตรงความต้องการ</div>
            <div>ไม่เสียเวลา ถึงเป้าหมายได้ไวกว่า</div>
          </div>
        </div>
      </div>
      <div class="d-flex flex-column flex-sm-row my-4">
        <div class="mx-4 my-2 text-center"><img class="about-3-img w-100" src="<?=base_url("resources/img/aboutus/About_3.png")?>"/></div>
        <div class="mx-4 my-2">
          <div class="f30 af-red">"คลังข้อสอบ"</div>
          <div class="f26">
            ค้นหาจุดอ่อนของนักเรียน
            ด้วยข้อสอบจากสนามจริง
            พร้อมระบบประมวลผลอัจฉริยะ
            ที่ช่วยแนะนำการเรียนได้อย่างตรงจุด
          </div>
        </div>
      </div>
      <div class="d-flex flex-column flex-sm-row-reverse my-4">
        <div class="mx-4 my-2 text-center"><img class="about-3-img w-100" src="<?=base_url("resources/img/aboutus/About_4.png")?>"/></div>
        <div class="mx-4 my-2 text-sm-right">
          <div class="f30 af-red">"แชทถามได้ทันที"</div>
          <div class="f26">
            <div>สงสัยอะไร ถามได้ทุกเรื่อง</div>
            <div>ทีมงานเราพร้อมตอบทุกข้อสงสัย!</div>
          </div>
        </div>
      </div>
    </div>

    <div class="about-box-4 w-100 text-center px-3 py-5">
      <div class="f35 mb-4">เรียน Monkey Everyday ดียังไง!?</div>
      <div class="about-box-4-mw f26 text-left mx-auto">
        <div class="d-flex">
          <div><img class="about-4-img my-4 mr-4" src="<?=base_url("resources/img/aboutus/No1.png")?>"/></div>
          <div class="d-flex align-self-center">
            <div class="font-weight-bold mr-2">เรียนคุ้ม</div>
            <div class="text-white">เข้าถึงบทเรียนได้ทั้งเว็บ ทุกวิชา ทุกระดับชั้น</div>
          </div>
        </div>
        <div class="d-flex">
          <div><img class="about-4-img my-4 mr-4" src="<?=base_url("resources/img/aboutus/No2.png")?>"/></div>
          <div class="d-flex align-self-center">
            <div class="font-weight-bold mr-2">สะดวกสุด</div>
            <div class="text-white">อยู่ที่ไหนก็เรียนได้ ไม่ต้องเดินทาง</div>
          </div>
        </div>
        <div class="d-flex">
          <div><img class="about-4-img my-4 mr-4" src="<?=base_url("resources/img/aboutus/No3.png")?>"/></div>
          <div class="d-flex align-self-center">
            <div class="font-weight-bold mr-2">เข้าใจง่าย</div>
            <div class="text-white">เนื้อหาละเอียดครบ ทบทวนเฉพาะจุดได้ตลอด</div>
          </div>
        </div>
        <div class="d-flex">
          <div><img class="about-4-img my-4 mr-4" src="<?=base_url("resources/img/aboutus/No4.png")?>"/></div>
          <div class="d-flex align-self-center">
            <div class="font-weight-bold mr-2">สร้างนิสัย</div>
            <div class="text-white">มีระบบกระตุ้นให้เกิดวินัย ในการเรียนด้วยตัวเอง</div>
          </div>
        </div>
        <div class="d-flex">
          <div><img class="about-4-img my-4 mr-4" src="<?=base_url("resources/img/aboutus/No5.png")?>"/></div>
          <div class="d-flex align-self-center">
            <div class="font-weight-bold mr-2">อัปเดตใหม่</div>
            <div class="text-white">ทุกสัปดาห์</div>
          </div>
        </div>
      </div>
    </div>

  </div> <!-- //.Container -->
</div> <!-- //#Warp -->