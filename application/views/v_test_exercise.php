<div id="wrap">
  <div id="container">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="T-Score-box">
          <div class="title">ตรวจคำตอบ</div>

          <?php
            // echo "<pre>";
            // print_r($sheet_exercises);
            // echo "</pre>";
          ?>
          <div class="subTitle mb-2"><?=$librarysheet->title?> : <?=$librarysheet->sheetCode?></div>
          <div class="row column-2">
            <div class="<?= count($sheet_exercises) >= 16 ? 'col-md-6' : 'col-md-12' ?> subBox">
              <div class="AnswerSheetTable">
                <?php 
                    //create sessionstorage for answer state
                    $answerState = array();?>
                <?php foreach ($sheet_exercises as $key => $sheet_exercise):?>
                <?php 
                      // $arr =  array('1' => 'A', '2' => 'B', '3' => 'C', '4' => 'D', '5' => 'E');
                      $arr =  array('A','B','C','D','E');
                      $hint = getHint($api_url, $sheet_exercise->hint_path);
                      $answer = getAnswer($api_url, $sheet_exercise->question_path); ?>

                <div class="row mb-2 d-flex flex-nowrap justify-content-between align-items-center">
                  <span class="numberEx"><?=$sheet_exercise->ex_no+1?>.</span>
                  <div class="flex-grow-1 d-flex mb-1">
                  <?php 
                      for($i = 0; $i < $sheet_exercise->num_of_answer; $i++): 
                        
                      //create sessionstorage for answer state
                      $objAnswer = array(
                        "question_id" => $sheet_exercise->id,
                        "question_choice" => $i,
                        "choice_disabled" => 0,
                        "class" => "btn-dark",
                        "isDisabled" => false,
                        'remove_class' => ''
                      );
                      array_push($answerState, $objAnswer);

                  ?>
                    <!-- <button class="btn btn-dark btn-outline choices" id="<?=$sheet_exercise->ex_no+1?>" value="<?=$arr[$i]?>"><?=$arr[$i]?></button> -->
                    <button class="btn btn-outline choices" question_id="<?= $sheet_exercise->id?>" question_choice="<?= $i?>" choice_disabled="0" value="<?=$i?>"><?=$arr[$i]?></button>
                  <?php endfor; ?>
                  </div>

                  <div class="icon-hint" data-toggle="modal" data-target=".P-modal-<?=$key?>"><i class="fa fa-question-circle"></i></div>
                </div>

                <div id="hint-<?=$key?>" class="modal fade P-modal-<?=$key?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <button type="button" class="close close_ex" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <div class="P-hint">
                        <?php if($sheet_exercise->hint_path != ''): ?>
                        <div class="hintHeader">คำใบ้</div>
                        <div class="hintNo"><?=$sheet_exercise->ex_no+1?></div>
                        <div class="hintImg"><img src="<?=$hint[0]?>" class="img-answer"/></div>
                        <?php else: ?>
                        <div class="hintHeader">พยายามอีกนิด</div>
                        <div class="hintImg"><img src="<?=base_url('resources/img/EveryDay_12_5_Test_PopUp_Tryagain.jpg')?>" class="img-hint"/></div>
                        <?php endif; ?>
                        <a class="btn btn-link collapsed" onclick="seeAnswer('<?= $sheet_exercise->id?>','<?= $sheet_exercise->answer?>')" data-toggle="collapse" data-target="#answer-detail-<?=$key?>" aria-expanded="false" aria-controls="answer-detail-<?=$key?>">
                          <div class="answerTxt1">เฉลยคำตอบ</div>
                          <div class="answerTxt2">*ข้อนี้จะไม่ได้คะแนน หากกดดูเฉลย</div>
                        </a>

                        <div id="answer-detail-<?=$key?>" class="collapse">
                          <div class="answerTxt3">ตอบ : <?=$arr[$sheet_exercise->answer]?></div>
                          <?php if($sheet_exercise->question_path != ''): ?>
                          <div class="hintImg"><img src="<?=$answer[0]?>" class="img-answer"/></div>
                          <?php endif; ?>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
                <?php if($key == floor(count($sheet_exercises)/2) && count($sheet_exercises) >= 16): ?>
                    </div>
                    </div>
                    <div class="col-md-6 subBox">
                    <div class="AnswerSheetTable">
                <?php endif; ?>
                <?php endforeach; ?>
              </div>
            </div>
          </div> <!-- //.row -->

          <div class="btn-div mt-2">
            <div class="title"><span id="txt-Score">0</span>/<?= count($sheet_exercises) ?></div>
            <a href="<?=base_url($page.'/Video/'.$librarysheet->id)?>"><button class="Q-btn back btn-white my-1 m-w-200">กลับ</button></a>
            <button id="btnCheckAns" class="btn-60 btn-black m-w-200">ตรวจคำตอบ</button>
          </div>
        </div> <!-- //.T-Score-box -->
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<?php
function getHint($api_url, $hintName){
  $headers = array("accept: application/json",'Content-Type: application/json');
  $method = 'GET';
  $url = $api_url.'gen-signed-url/'.$hintName;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
  $response = curl_exec($curl);
  curl_close($curl);
  return json_decode($response);
  }

function getAnswer($api_url, $answerName){
  $headers = array("accept: application/json",'Content-Type: application/json');
  $method = 'GET';
  $url = $api_url.'gen-signed-url/'.$answerName;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
  $response = curl_exec($curl);
  curl_close($curl);
  return json_decode($response);
  }
?>

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script type="text/javascript">
	var choicesData = [];
  const sheetID = '<?= $librarysheet->id ?>'
  const sessionQuestion = 'question_' + sheetID
  const sessionAnswerState = 'answer_state_' + sheetID
  const sessionAnswer = 'answer_' + sheetID
  initSessionStore()
  calculationScore()

  $('.choices').click(function(){ 
    
    let question_id = $(this).attr('question_id');
    let question_choice = $(this).attr('question_choice');
    
    // remove previos selected
    let answerState = getSessionStore(sessionAnswerState)
    let btnInActive = answerState.filter(a => a.question_id == question_id && a.choice_disabled == '0')
    answerState = answerState.filter(a => !(a.question_id == question_id && a.choice_disabled == '0'))
    btnInActive = btnInActive.map(b => ({ ...b, remove_class: 'active' }))
    answerState = [...answerState, ...btnInActive] 
    setSessionStore(sessionAnswerState, answerState)
    
    // disabled wrong answer
    answerState = getSessionStore(sessionAnswerState)
    let btnInCorrect = answerState.filter(a => a.question_id == question_id && a.choice_disabled == '1')
    answerState = answerState.filter(a => !(a.question_id == question_id && a.choice_disabled == '1'))
    btnInCorrect = btnInCorrect.map(b => ({ ...b, class: 'btn-disabled-choice active', remove_class:'' }))
    answerState = [...answerState,...btnInCorrect]
    setSessionStore(sessionAnswerState, answerState)
    
    //active selected answer
    answerState = getSessionStore(sessionAnswerState)
    let btnActive = answerState.filter(a => a.question_id == question_id && a.question_choice == question_choice)
    answerState = answerState.filter(a => !(a.question_id == question_id && a.question_choice == question_choice))
    btnActive = btnActive.map(b => ({ ...b, class: 'btn-dark active', remove_class:'' }))
 
    answerState = [...answerState, ...btnActive]
    setSessionStore(sessionAnswerState, answerState)
    setStyleChoices()
    
    
    
    // $(`[question_id='${question_id}'][choice_disabled='0']`)
    //   .removeClass('active');
      
    // $(`[question_id='${question_id}'][choice_disabled='1']`)
    //   .removeClass('btn-incorrect')
    //   .addClass('btn-disabled-choice');

    // $(this).addClass('active');
    let ans = $(this).attr('value');
    
    // remove old ans
    let currentAns = getSessionStore(sessionAnswer)
    currentAns = currentAns.filter( q => q.question_id != question_id)
    currentAns.push({ question_id, ans, isShowAnswer: false })
    setSessionStore(sessionAnswer, currentAns)
  });

$('#btnCheckAns').click(function(){ 
  let questionList = getSessionStore(sessionQuestion)
  let currentAns = getSessionStore(sessionAnswer)
  validateAns(questionList)
  // if(questionList.length === currentAns.length){
  //   validateAns(questionList)
  // }
})

function validateAns(questionList){
  let score = 0
  questionList.forEach(item => {
    let currentAns = getSessionStore(sessionAnswer)
    let result = currentAns.find( q => q.question_id === item.id)
    if(!result)
      return
    let answerState = getSessionStore(sessionAnswerState)
    let selectAnswer = []
    if(result.ans === item.answer){
      if(result.isShowAnswer)
        return
      score += 1
      //active correct answer
      selectAnswer = answerState.filter(a => a.question_id == item.id && a.question_choice == item.answer)
      answerState = answerState.filter(a => !(a.question_id == item.id && a.question_choice == item.answer))
      selectAnswer = selectAnswer.map(b => ({ ...b, class:'btn-correct active', remove_class:'', choice_disabled: '1' }))
      answerState = [...answerState, ...selectAnswer]
      
      selectAnswer = answerState.filter(a => a.question_id == item.id)
      answerState = answerState.filter(a => !(a.question_id ==item.id ))
      selectAnswer = selectAnswer.map(b => ({ ...b, isDisabled: true, remove_class:'' }))
      answerState = [...answerState, ...selectAnswer]
      
      // disabled other answer that not correct
      selectAnswer = answerState.filter(a => a.question_id == item.id && a.question_choice != item.answer)
      answerState = answerState.filter(a => !(a.question_id == item.id && a.question_choice != item.answer))
      selectAnswer = selectAnswer.map(b => ({ ...b, isDisabled: true, class: 'btn-disabled-choice active', remove_class:'' }))
      answerState = [...answerState, ...selectAnswer]
      
    }else{
      
      selectAnswer = answerState.filter(a => a.question_id == item.id && a.question_choice == result.ans)
      answerState = answerState.filter(a => !(a.question_id == item.id && a.question_choice == result.ans))
      selectAnswer = selectAnswer.map(b => ({ ...b, class:'btn-incorrect active', isDisabled: true, choice_disabled: '1', remove_class:'' }))
      answerState = [...answerState, ...selectAnswer]

      let submitCount = answerState.filter(a => a.question_id == item.id && a.choice_disabled == '1').length
      let numOfAns = Number.parseInt(item.num_of_answer)
      if(submitCount >= numOfAns - 1){
        selectAnswer = answerState.filter(a => a.question_id == item.id && a.question_choice == item.answer && a.choice_disabled == '0')
        answerState = answerState.filter(a => !(a.question_id == item.id && a.question_choice == item.answer && a.choice_disabled == '0'))
        selectAnswer = selectAnswer.map(b => ({ ...b, class:'btn-answer', isDisabled: true, remove_class:'' }))
        answerState = [...answerState, ...selectAnswer]
        
          // disabled wrong answer
        let btnInCorrect = answerState.filter(a => a.question_id == item.id && a.choice_disabled == '1' && a.question_choice != item.answer)
        answerState = answerState.filter(a => !(a.question_id == item.id && a.choice_disabled == '1' && a.question_choice != item.answer))
        btnInCorrect = btnInCorrect.map(b => ({ ...b, class: 'btn-disabled-choice active', remove_class:'' }))
        answerState = [...answerState,...btnInCorrect]
      }
    }
    setSessionStore(sessionAnswerState, answerState)
    setStyleChoices()
  })
  $("#txt-Score").html(score)
}

function seeAnswer(question_id, ans){
  let currentAns = getSessionStore(sessionAnswer)
  currentAns = currentAns.filter( q => q.question_id != question_id)
  currentAns.push({ question_id, ans, isShowAnswer: true })
  setSessionStore(sessionAnswer, currentAns)

  let answerState = getSessionStore(sessionAnswerState)
  let btnAnswer = answerState.filter(a => a.question_id == question_id && a.question_choice == ans && a.choice_disabled == '0')
  // console.log(btnAnswer)
  btnAnswer = btnAnswer.map(b => ({ ...b, class: 'btn-answer', isDisabled: true, choice_disabled: '1', remove_class:'' }))
  answerState = answerState.filter(a => !(a.question_id == question_id && a.question_choice == ans && a.choice_disabled == '0'))

  let btnDisabled = answerState.filter(a => a.question_id == question_id && a.choice_disabled == '0')
  btnDisabled = btnDisabled.map(b => ({ ...b, class: 'btn-disabled-choice active', isDisabled: true, choice_disabled: 1, remove_class: '' }))
  answerState = answerState.filter(a => !(a.question_id == question_id  && a.choice_disabled == '0'))
  
  answerState = [...answerState, ...btnAnswer, ...btnDisabled]
  setSessionStore(sessionAnswerState, answerState)

  
    // disabled wrong answer
  answerState = getSessionStore(sessionAnswerState)
  let btnInCorrect = answerState.filter(a => a.question_id == question_id && a.choice_disabled == '1' && a.question_choice != ans)
  answerState = answerState.filter(a => !(a.question_id == question_id && a.choice_disabled == '1' && a.question_choice != ans))
  btnInCorrect = btnInCorrect.map(b => ({ ...b, class: 'btn-disabled-choice active', remove_class:'' }))
  answerState = [...answerState,...btnInCorrect]
  setSessionStore(sessionAnswerState, answerState)
  
  setStyleChoices()
  // console.log(answerState)
  // $(`[question_id='${question_id}'][question_choice='${ans}']`)
  //   .removeClass('btn-dark')
  //   .addClass('btn-answer')
  //   .prop('disabled', true)
  //   .attr('choice_disabled', '1');
  
  // $(`[question_id='${question_id}'][choice_disabled='0']`)
  //   .removeClass('btn-dark')
  //   .addClass('btn-disabled-choice active')
  //   .prop('disabled', true)
  //   .attr('choice_disabled', '1');
}

function setSessionStore(item, value){
	sessionStorage.setItem(item, JSON.stringify(value));
}
function getSessionStore(item){
  return	JSON.parse(sessionStorage.getItem(item));
}

function initSessionStore(){
   if(getSessionStore(sessionQuestion) == null){
    setSessionStore(sessionQuestion, <?php echo json_encode($sheet_exercises) ?>);
    setSessionStore(sessionAnswerState, <?php echo json_encode($answerState) ?>);
    setSessionStore(sessionAnswer, []);
   }
   setStyleChoices()
}

function setStyleChoices(){
  let answerState = getSessionStore(sessionAnswerState)
  answerState.forEach(item => {
    $(`[question_id='${item.question_id}'][question_choice='${item.question_choice}']`)
      .removeClass('btn-dark btn-correct btn-incorrect btn-answer btn-disabled-choice')
      .addClass(item.class)
      .removeClass(item.remove_class)
      .prop('disabled', item.isDisabled)
      .attr('choice_disabled', item.choice_disabled);
  })
}


function calculationScore(){
  let questionList = getSessionStore(sessionQuestion)
  let score = 0
  questionList.forEach(item => {
    let currentAns = getSessionStore(sessionAnswer)
    let result = currentAns.find( q => q.question_id === item.id)
    if(!result)
      return
    if(result.ans === item.answer){
      if(result.isShowAnswer)
        return
      score += 1
    }
  })
  $("#txt-Score").html(score)
}

</script>