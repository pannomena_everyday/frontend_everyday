<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<style>
  .p-image {display: block;}
</style>
<div id="wrap">
  <div id="container">
    <div class="myProfileHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="myProfile-slide slider">
            <div class="myProfileBox active">
              <img src="<?=base_url()."resources/img/account_active.png"?>" class="icon-subject"/><span>บัญชีผู้ใช้ </span>
            </div>
            <?php if($myProfile->subscriptionTypeId != 1): ?>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyPlan"?>"><img src="<?=base_url()."resources/img/myplan.png"?>" class="icon-subject"/>แผนการเรียน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyPerformance"?>"><img src="<?=base_url()."resources/img/performance.png"?>" class="icon-subject"/>ติดตามผลงาน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyNote"?>"><img src="<?=base_url()."resources/img/sheet.png"?>" class="icon-subject"/>คลัง Sheet </a>
            </div>
            <?php endif; ?>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyLog"?>"><img src="<?=base_url()."resources/img/loghistory.png"?>" class="icon-subject"/>ประวัติการเรียน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyMembership"?>"><img src="<?=base_url()."resources/img/membership.png"?>" class="icon-subject"/>สถานะสมาชิก </a>
            </div>
          </section>
        </div>
      </div>
    </div>

    <div class="containner-1200 mx-auto my-4">
      <div class="containner-1200-padding">
      <?php $this->load->view('i_myProfileStatus', $this->data); ?>
      <section class="myProfile-form">
        <!-- <div class="d-flex flex-wrap"> -->
        <div class="d-none">
          <?php if(isset($myProfile->socialLinks)): ?>
            <?php
              $found_key = array_search('GG', array_column($myProfile->socialLinks, 'socialtype'));
              if($found_key !== false):
            ?>
              <a class="btn-40 btn-black my-1 mr-2" href="<?= base_url("MyProfile/disconnect_social/".$myProfile->socialLinks[$found_key]->socialid) ?>"> ยกเลิกการเชื่อมต่อกับ Google</a>
            <?php else:?>
              <a class="btn-40 btn-black my-1 mr-2" href="<?= $google_client->createAuthUrl()?>"> เชื่อมต่อกับ Google</a>
            <?php endif; ?>

            <?php
              $found_key = array_search('FB', array_column($myProfile->socialLinks, 'socialtype'));
              if($found_key !== false):
            ?>
              <a class="btn-40 btn-blac my-1 mr-2" href="<?= base_url("MyProfile/disconnect_social/".$myProfile->socialLinks[$found_key]->socialid) ?>"> ยกเลิกการเชื่อมต่อกับ Facebook</a>
            <?php else: ?>
              <a class="btn-40 btn-black my-1 mr-2" href="#"> เชื่อมต่อกับ Facebook</a>
            <?php endif; ?>

          <?php else: ?>
            <a class="btn-40 btn-black my-1 mr-2" href="<?= $google_client->createAuthUrl()?>"> เชื่อมต่อกับ Google</a>
            <a class="btn-40 btn-black my-1 mr-2" href="#"> เชื่อมต่อกับ Facebook</a>
          <?php endif; ?>
        </div>
        <div class="d-flex justify-content-between mb-5">
          <div class="form-header f26">ข้อมูลส่วนตัว</div>
          <button class="btn-40 btn-green-white saveChange">บันทึก</button>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <div class="form-group">
              <label for="inputName">ชื่อ</label>
              <input type="text" class="form-control" id="firstname" value="<?=$myProfile->firstname?>" >
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div class="form-group">
              <label for="inputLastName">นามสกุล</label>
              <input type="text" class="form-control" id="lastname" value="<?=$myProfile->lastname?>" >
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div class="form-group">
              <label for="inputBirthDate">วันเกิด</label>
              <div class="input-group input-focus">
                <input autocomplete="off" type="text" value="<?=$myProfile->birddate?>" id="birthdate" class="form-control f18">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-white f-ccc icon-calendar"><i class="fa fa-calendar-o"></i></span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div class="form-group">
              <label for="inputGender">เพศ</label>
              <select id="gender" class="custom-select form-control f18">
                <?=($myProfile->gender == '') ? '<option value="" selected>เพศ</option>' : '';?>
                <option value="M" <?=($myProfile->gender == 'M') ? 'selected="selected"' : '';?>>ชาย</option>
                <option value="F" <?=($myProfile->gender == 'F') ? 'selected="selected"' : '';?>>หญิง</option>
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label for="inputID">เลขบัตรประชาชน</label>
              <input type="text" class="form-control" id="id_card" value="<?=$myProfile->id_card?>" >
            </div>
          </div>
        </div>
        <div class="form-header f26">ข้อมูลการศึกษา</div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="inputSchoolName">ชื่อโรงเรียน/มหาวิทยาลัย</label>
              <input type="text" class="form-control" id="schoolname" value="<?=$myProfile->education->schoolname?>" >
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label for="inputEducationlevel">ระดับการศึกษา</label>
              <select id="level" class="custom-select form-control f18">
                <?php
                foreach($dataEducation as $row){
                  if($education->id == $row->id){
                    echo "<option value='".$row->id."' selected='selected'>".$row->title."</option>";
                  }else{
                    echo "<option value='".$row->id."'>".$row->title."</option>";
                  }
                }
                ?>
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label for="inputProvince">จังหวัด</label>
                <select id="provinceid" class="custom-select form-control f18">
                <?php
                foreach($dataProvince as $row){
                  if($province->ProvinceID == $row->ProvinceID){
                    echo "<option value='".$row->ProvinceID."' selected='selected'>".$row->ProvinceName."</option>";
                  }else{
                    echo "<option value='".$row->ProvinceID."'>".$row->ProvinceName."</option>";
                  }
                }
                ?>
              </select>
            </div>
          </div>
        </div>
      </section>

      <section class="myProfile-form">
        <div class="form-header f26">ข้อมูลบัญชีผู้ใช้</div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="inputEmail">อีเมล์</label>
              <input type="email" class="form-control" id="email" value="<?=$myProfile->email?>" disabled>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label for="inputMobileNumber">เบอร์โทรศัพท์</label>
              <input type="text" class="form-control" id="mobile" value="<?=$myProfile->mobile?>" disabled>
            </div>
          </div>
          <div class="col-md-12">
            <div class="d-flex justify-content-between">
              <div class="txtResultChange">รหัสผ่าน ********</div>
              <div><button class="btn-40 btn-gray" data-toggle="collapse" href="#collapsePassword">เปลี่ยนรหัสผ่าน</button></div>
            </div>
            <div class="collapse my-3" id="collapsePassword">
              <div class="card card-body">
                <div class="form-group oldPassword-box my-0">
                  <input type="password" class="form-control" id="oldPassword" placeholder="กรอกรหัสผ่านของคุณ">
                  <button class="btn-40 btn-gray mt-3" id="btnCheckOldPassword">ถัดไป</button>
                </div>
                <div class="form-group newPassword-box my-0">
                  <input type="password" class="form-control" id="password" placeholder="รหัสผ่านใหม่">
                  <input type="password" class="form-control mt-3" id="conf_password" placeholder="ยืนยันรหัสผ่านใหม่">
                  <button class="btn-40 btn-gray mt-3" id="btnSubmitPassword">ยืนยันเปลี่ยนรหัสผ่าน</button>
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="col-sm-12 col-md-6">
            <div class="form-group">
              <label for="inputPassword">รหัสผ่าน</label>
              <input type="password" class="form-control" id="password"  >
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div class="form-group">
              <label for="inputConfPassword">ยืนยันรหัสผ่าน</label>
              <input type="password" class="form-control" id="conf_password"  >
            </div>
          </div> -->
        </div>
      </section>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script>
  var chkPassword = false;
  $(document).ready(function() {
    $('#birthdate').datepicker({
      changeYear: true,
      changeMonth: true,
      dateFormat: 'yy-mm-dd',
      yearRange: '-100:+0'
    });
    $('.icon-calendar').on('click', function() {
      $('#birthdate').focus();
    });
    $(".upload-button").on('click', function() {
      $(".file-upload").click();
    });
    $(".file-upload").on('change', function(){
      if (this.files && this.files[0]) {
        var fileType = $(".file-upload").val().split('\\').pop().split('.').pop().toLowerCase();
        var fileExtension = ['jpeg', 'jpg', 'png'];
        if ($.inArray(fileType, fileExtension) == -1) {
          alert("Only formats are allowed : "+fileExtension.join(', '));
        }else{
          var file = $(".file-upload")[0].files[0];
          var formData = new FormData();
          formData.append('file', file);

          var opts = {
            method: 'POST',
            headers: {
              "Authorization": "Bearer <?=$token;?>"
            },
            body: formData
          }
          fetch('<?=$this->config->item('api_url').'upload-file'?>', opts).then(function (response) {
            var data = response.json();
            // await
            data.then(function(result) {
              //console.log(result.data.filename)
              // update profile_image filename
              var filename = result.data.filename;
              var opts2 = {
                method: 'PATCH',
                headers: {
                    "Content-Type":"application/json",
                    "Authorization": "Bearer <?=$token;?>"
                },
                body: JSON.stringify({
                  "profile_image": filename
                })
              };
              fetch('<?=$this->config->item('api_url').'member/update-image-profile'?>', opts2).then(function (response) {
                if(response.ok){
                  return response;
                }else{
                  return response.json();
                }
              })
              .then(function (body) {
                //console.log(body)
                if(body.ok != true){
                  alert("ไม่สามารถแก้ไขรูปโปรไฟล์ได้ กรุณาลองใหม่อีกครั้ง");
                }else{
                  $.ajax({
                    url: "<?=base_url()."MyProfile/updateProfileImg"?>",
                    type: "post",
                    data: {filename:filename} ,
                    success: function (data) {
                      alert("แก้ไขรูปโปรไฟล์เรียบร้อยแล้ว");
                      location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                      console.log(textStatus, errorThrown);
                    }
                  });
                }
              }); //then Upload profile
            }) //data.then
          })
        }
      }
    });
  }); //$(document).ready

  function checkID_Card(id){
    if(id.length != 13) return false;
    for(i=0, sum=0; i < 12; i++)
    sum += parseFloat(id.charAt(i))*(13-i); if((11-sum%11)%10!=parseFloat(id.charAt(12)))
    return false; return true;
  }

  $('#btnCheckOldPassword').click(function(){
    var mobile = $.trim($('#mobile').val());
    var password = $.trim($('#oldPassword').val());
    if(password == ''){
      alert('กรุณากรอกรหัสผ่านปัจจุบบัน');
    }else{
      var opts = {
        method: 'POST',
        headers: {
            "Content-Type":"application/json"
        },
        body: JSON.stringify({
        "mobile": mobile,
        "password": password
        })
      };
      fetch('<?=$this->config->item('api_url').'/members/login';?>', opts).then(function (response) {
        response.json().then(function (result) {
          if(response.ok){
            chkPassword = true;
            $('.oldPassword-box').hide();
            $('.newPassword-box').show();
          }else{
            alert('กรุณากรอกรหัสผ่านที่ถูกต้อง');
          }
        })
      })
    }
  });

  $('#btnSubmitPassword').click(function(){
    var strength = 0;
    var mobile = $.trim($('#mobile').val());
    var password = $.trim($('#password').val());
    var conf_password = $.trim($('#conf_password').val());
    var arr = [/.{8,}/, /[a-z]+/, /[0-9]+/, /[A-Z]+/];
    jQuery.map(arr, function(regexp) {
      if(password.match(regexp))
        strength++;
    });
    if(!chkPassword){
      alert('กรุณากรอกรหัสผ่าน');
      $('.oldPassword-box').show();
      $('.newPassword-box').hide();
      $('#oldPassword').val('');
      $('#password').val('');
      $('#conf_password').val('');
    }else{
      if(strength < 4){
        alert('กําหนดรหัสผ่าน \nตัวอักษรมากกว่าหรือเท่ากับ 8 ตัว \nตัวอักษรตัวใหญ่อย่างน้อย 1 ตัว \nตัวอักษรตัวเล็กอย่างน้อย 1 ตัว \nตัวเลขอย่างน้อย 1 ตัว');
      }else if(conf_password == ''){
        alert('กรุณากรอกยืนยันรหัสผ่าน');
      }else if(password != conf_password){
        alert('รหัสผ่านและยืนยันรหัสผ่านไม่ตรงกัน');
      }else{
        // Change password
        var opts = {
          method: 'PATCH',
          headers: {
              "Content-Type":"application/json",
              "Authorization": "Bearer <?=$token;?>"
          },
          body: JSON.stringify({
          "mobile": mobile,
          "password": password
          })
        };
        fetch('<?=$this->config->item('api_url').'/member/change-password';?>', opts).then(function (response) {
          if(response.ok){
            return response;
          }else{
            return response.json();
          }
        })
        .then(function (body) {
          console.log(body)
          if(body.ok != true){
            //alert(body.error.message);
            //errorTxt = 'Change Password false.\n';
            $('div.txtResultChange').html('รหัสผ่าน <span class="f-red">เกิดข้อผิดพลาดในการเปลี่ยนรหัสผ่าน กรุณาลองอีกครั้ง</span>');
          }else{
            $('div.txtResultChange').html('รหัสผ่าน <span class="f-green">เปลี่ยนรหัสผ่านใหม่เรียบร้อยแล้ว</span>');
            $('#collapsePassword').collapse('hide')
            $('#oldPassword').val('');
            $('#password').val('');
            $('#conf_password').val('');
            chkPassword = false;
            $('.oldPassword-box').show();
            $('.newPassword-box').hide();
          }
        });
      }
    }
  });

  $('.saveChange').click(function(){
    var errorTxt = '';
    var strength = 0;
    var firstname = $.trim($('#firstname').val());
    var lastname = $.trim($('#lastname').val());
    var birthdate = $.trim($('#birthdate').val());
    var gender = $.trim($('#gender').val());
    var id_card = $.trim($('#id_card').val());
    var mobile = $.trim($('#mobile').val());
    //var email = $.trim($('#email').val());
    var password = $.trim($('#password').val());
    var conf_password = $.trim($('#conf_password').val());
    var schoolname = $.trim($('#schoolname').val());
    var level = $.trim($('#level').val());
    //var majer =  $.trim($('#').val());
    var provinceId = $.trim($('#provinceid').val());
    var arr = [/.{8,}/, /[a-z]+/, /[0-9]+/, /[A-Z]+/];
    jQuery.map(arr, function(regexp) {
      if(password.match(regexp))
        strength++;
    });

    if(firstname == ''){
      alert('กรุณากรอกชื่อ');
    }else if(lastname == ''){
      alert('กรุณากรอกนามสกุล');
    }else if(birthdate == ''){
      alert('กรุณาเลือกวันเกิด');
    }else if(gender == ''){
      alert('กรุณาเลือกเพศ');
    }else if(id_card != '' && !checkID_Card(id_card)){
      alert('กรุณใส่เลขบัตรประชาชนที่ถูกต้อง');
    }else if(schoolname == ''){
      alert('กรุณากรอกชื่อโรงเรียน');
    }else if(level == ''){
      alert('กรุณาเลือกระดับการศึกษา');
    }else if(provinceId == ''){
      alert('กรุณาเลือกจังหวัด');
    }else if(mobile == ''){
      alert('กรุณากรอกหมายเลขโทรศัพท์');
    }else{
      //alert('test');
      //console.log(provinceId);
      var opts = {
        method: 'PATCH',
        headers: {
            "Content-Type":"application/json",
            "Authorization": "Bearer <?=$token;?>"
        },
        body: JSON.stringify({
          "firstname": firstname,
          "lastname": lastname,
          "birddate": birthdate,
          "gender": gender,
          "id_card": id_card,
          "mobile": mobile,
          "schoolname": schoolname,
          "level": level,
          "provinceId": parseInt(provinceId),
          "password": ''
        })
      };
      // Change profile
      fetch('<?=$this->config->item('api_url').'members/'.$myProfile->id;?>', opts).then(function (response) {
        if(response.ok){
          return response;
        }else{
          return response.json();
        }
      })
      .then(function (body) {
        //console.log(body)
        if(body.ok != true){
          //alert(body.error.message);
          alert('เกิดข้อผิดพลาดในการแก้ไขข้อมูล');
        }else{
          alert("แก้ไขข้อมูลส่วนตัวบันทึกเรียบร้อยแล้ว");
          $.ajax({
            url: "<?=base_url()."MyProfile/updateName"?>",
            type: "post",
            data: {firstname:firstname} ,
            success: function (data) {
              location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
            }
          });
        }
      }); //then update profile
    }
  });

  $('.changePassBtn').click(function(){
    
  });

  $( "#profile" ).click(function() {
    $('#profileBox').toggle('slow');
  });
</script>