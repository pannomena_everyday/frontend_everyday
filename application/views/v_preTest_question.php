<?php $n=1; ?>
<div id="wrap">
  <div id="container">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <!-- <form class="boxed"> -->
        <?php if(!empty($plan->planExercices)): ?>
        <?php 
          //create sessionstorage for answer state
          // $answerState = array();
        ?>
        <?php foreach ($plan->planExercices as $key => $plan_exercice):?>
        <?php
          $previous = array_key_exists($key - 1, $plan->planExercices) ? $plan->planExercices[$key -1] : false;
          $next = array_key_exists($key + 1, $plan->planExercices) ? $plan->planExercices[$key +1] : false;
        ?>
        <?php $question = getQuestion($api_url, $plan_exercice->question_path); ?>
        <div class="questionBox questionNo_<?=$n;?> <?= $plan_exercice->ex_no != 0 ? "d-none" : ""?>" id="question_id_<?= $plan_exercice->id ?>">
          <div class="questionTitle d-flex flex-column flex-md-row justify-content-between align-content-center align-content-md-center flex-nowrap">
            <div class="f26 flex-grow-1 align-self-center">คำถามข้อที่ <?=$plan_exercice->ex_no+1?> / <?=count($plan->planExercices )?></div>
            <div class="d-flex justify-content-between align-content-center my-3 my-md-0">
              <div class="align-self-center">
                <div class="btn-Q-modal AnswerSheetMobile m-767-show" data-toggle="modal" data-target=".P-modal">
                  <img class="" src="<?=base_url('resources/img/AnswerSheet.png')?>" />
                </div> 
              </div>
              <!-- <div class="px-0 px-md-3 align-self-center"><a href="<?=base_url()."PreTest/Result2";?>"><button class="Q-btnSubmit">ส่งคำตอบ</button></a></div> -->
              <div class="px-0 px-md-3 align-self-center"><a href="#"><button class="Q-btnSubmit btn-green-white" onclick="sendAnswer()">ส่งคำตอบ</button></a></div>
            </div>
          </div>
          <div class="questionDetail">
            <div class="row">
              <div class="col-md-8 hfit">
                <div class='prog-bar-q'>
                  <div class="prog-bar">
                    <div class="background progress-bar" style="-webkit-clip-path: inset(0 <?=(100-10)?>% 0 0); clip-path: inset(0 <?=(100)?>% 0 0);"></div>
                  </div>
                </div>
                <div class="qImg-div">
                  <img class="qImg" src="<?=$question[0]?>" />
                </div>
              </div>
              <div class="col-md-4 center">
                <div class="btn-Q-modal m-767-hide" data-toggle="modal" data-target=".P-modal">
                  <img class="imgAnswerSheet" src="<?=base_url('resources/img/AnswerSheet.png')?>" />
                  <div class="txtAnswerSheet"><span>กระดาษคำตอบ</span></div>
                </div>
                <div class="d-flex flex-md-column mt-3 mt-md-0 align-items-center">
                  <?php //$arr =  array('1' => 'A', '2' => 'B', '3' => 'C', '4' => 'D', '5' => 'E');
                    $ansText =  array('A','B','C','D','E');
                  ?>

                  <?php for($i = 0; $i < $plan_exercice->num_of_answer; $i++): ?>
                  <?php 
                    //create sessionstorage for answer state
                    // $objAnswer = array(
                    //   "question_id" => $plan_exercice->id,
                    //   "question_choice" => $i,
                    //   "isActive" => false
                    // );
                    // array_push($answerState, $objAnswer);
                  ?>
                  <!-- <input type="radio" id="choice_<?=$plan_exercice->ex_no+1?>_<?=$arr[$i]?>" name="choice" onclick="myanswer(this.id, this.value)" value="<?=$i?>">
                  <label for="choice_<?=$plan_exercice->ex_no+1?>_<?=$arr[$i]?>"><?=$arr[$i]?></label> -->
                    <div class="choice ref-choice" question_id="<?= $plan_exercice->id?>" question_choice="<?= $i?>"><?=$ansText[$i]?></div>
                  <?php endfor; ?>
                </div>
                <div class="Q-btn-div">
                  <?php if($plan_exercice->ex_no != 0):?>
                    <a><button class="Q-btn back btn-white my-1 btn-go-prev" data-current-id="<?= $plan_exercice->id?>" data-previous-id="<?= $previous->id ?>">กลับ</button></a>
                  <?php endif ?>
                  <?php if($plan_exercice->ex_no != count($plan->planExercices)  - 1):?>
                  <a><button class="Q-btn next btn-gray my-1 btn-go-next" data-current-id="<?= $plan_exercice->id?>" data-next-id="<?= $next->id ?>">ถัดไป</button></a>
                  <?php endif ?>
                </div>
              </div>
            </div>
          </div>
        </div> <!-- //.questionBox -->
        <?php $n++; ?>
        <?php endforeach; ?>
        <?php endif; ?>
        <!-- </form> -->
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->
<div class="modal fade P-modal Q-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="P-AnswerSheet">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <div class="row p-2">
          <!-- <div class="<?= count($plan->planExercices) >= 16 ? 'col-md-6' : 'col-md-12' ?> d-flex align-items-center flex-column"> -->
          <div class="<?= count($plan->planExercices) >= 16 ? 'col-md-6' : 'col-md-12' ?>">
            <?php foreach ($plan->planExercices as $key => $test): ?>
              <div class="AnswerSheetTable mb-2 d-flex flex-nowrap justify-content-between align-items-center">
                  <span class="numberEx" data-question_id="<?= $test->id?>"><?=$test->ex_no+1?></span>
                  <div class="flex-grow-1">
                    <?php 
                      $ansText =  array('A','B','C','D','E');
                      for($i = 0; $i < $test->num_of_answer; $i++): 
                    ?>
                    <div class="choiceAnswerSheet rounded-circle" data-question_id="<?= $test->id?>" question_id="<?= $test->id?>" question_choice="<?= $i?>"><?=$ansText[$i]?></div>
                    <?php endfor ?>
                  </div>
              </div>
            <?php if($key == floor(count($plan->planExercices)/2) && count($plan->planExercices) >= 16): ?>
              </div>
              <div class="col-md-6">
            <?php endif ?>
            <?php endforeach ?>
          </div>
          <!-- <div class="col-md-6">
            <?php
            for($i=16; $i<=30; $i++){
                echo '<div class="AnswerSheetTable mb-2 d-flex flex-nowrap justify-content-between align-items-center">
                <span class="numberEx">'.$i.'. </span>
                <div class="flex-grow-1">
                  <div class="choiceAnswerSheet rounded-circle">A</div>
                  <div class="choiceAnswerSheet rounded-circle active">B</div>
                  <div class="choiceAnswerSheet rounded-circle">C</div>
                  <div class="choiceAnswerSheet rounded-circle">D</div>
                  <div class="choiceAnswerSheet rounded-circle">E</div>
                </div>
              </div>';
            }
            ?>
          </div> -->
        </div>
      <!-- <div class="row">
        <div class="col-md-6 col-lg-3 AnswerCenter">
          <?php 
          for($i=1; $i<=15; $i++){
              echo '<div class="AnswerSheetTable">
                <span>'.$i.'. </span>
                <div class="P-choiceAnswerSheet">A</div>
                    <div class="P-choiceAnswerSheet active">B</div>
                    <div class="P-choiceAnswerSheet">C</div>
                    <div class="P-choiceAnswerSheet">D</div>
                    <div class="P-choiceAnswerSheet">E</div>
              </div>';
          }
          ?>
        </div>
        <div class="col-md-6 col-lg-3 AnswerCenter">
          <?php 
          for($i=16; $i<=30; $i++){
              echo '<div class="AnswerSheetTable">
                <span>'.$i.'. </span>
                <div class="P-choiceAnswerSheet">A</div>
                    <div class="P-choiceAnswerSheet">B</div>
                    <div class="P-choiceAnswerSheet active">C</div>
                    <div class="P-choiceAnswerSheet">D</div>
                    <div class="P-choiceAnswerSheet">E</div>
              </div>';
          }
          ?>
        </div>
        <div class="col-md-6 col-lg-3 AnswerCenter">
          <?php 
          for($i=31; $i<=45; $i++){
              echo '<div class="AnswerSheetTable">
                <span>'.$i.'. </span>
                <div class="P-choiceAnswerSheet">A</div>
                    <div class="P-choiceAnswerSheet">B</div>
                    <div class="P-choiceAnswerSheet active">C</div>
                    <div class="P-choiceAnswerSheet">D</div>
                    <div class="P-choiceAnswerSheet">E</div>
              </div>';
          }
          ?>
        </div>
        <div class="col-md-6 col-lg-3 AnswerCenter">
          <?php 
          for($i=46; $i<=50; $i++){
              echo '<div class="AnswerSheetTable">
                <span>'.$i.'. </span>
                <div class="P-choiceAnswerSheet">A</div>
                    <div class="P-choiceAnswerSheet">B</div>
                    <div class="P-choiceAnswerSheet active">C</div>
                    <div class="P-choiceAnswerSheet">D</div>
                    <div class="P-choiceAnswerSheet">E</div>
              </div>';
          }
          ?>
        </div> -->
      </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modelNotComplete" tabindex="-1" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="timeAlertContent">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <div class="txt">กรุณาตอบคำถามให้ครบ</div>
          <!-- <div class='prog-bar-timeAlert'>
              <div class="prog-bar">
                <div class="background" style=" -webkit-clip-path: inset(0 0 0 0); clip-path: inset(0 0 0 0);">
                </div>
              </div>
          </div> -->
          <div class="px-0 px-md-3 align-self-center"><button onclick="closeAnswerAlert()" class="Q-btnTimeAlert btn-gray">รับทราบ</button></div>
          <!-- <div class="prog-text-question f18"><i class="fa fa-clock-o" aria-hidden="true"></i> 00 : 05 : 00</div> -->
          <!-- <div class="prog-text-ansSheet f18"><i class="fa fa-clock-o" aria-hidden="true"></i> 00 : 44 : 55</div> -->
      </div>
    </div>
  </div>
</div>

<?php
function getQuestion($api_url, $questionName){
  $headers = array("accept: application/json",'Content-Type: application/json');
  $method = 'GET';
  $url = $api_url.'gen-signed-url/'.$questionName;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
  $response = curl_exec($curl);
  curl_close($curl);
  return json_decode($response);
  }
?>
<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/js/sweetalert2.all.min.js"></script>
<script type="text/javascript">
  
  const testID = '<?= $test_id ?>'
  const sheetID = '<?= $plan->id ?>'
  const sessionStart = 'pre_test_start_' + sheetID
  const sessionQuestion = 'pre_test_question_' + sheetID
  const sessionAnswer = 'pre_test_answer_' + sheetID

  // var count = setTimer()
  initSessionStore()
    const boxQuestionId = 'question_id_'
    $(document).ready(function () {
      
      $('.choiceAnswerSheet').click(function (e) { 
        e.preventDefault();
        let id = $(this).data('question_id')
        $('.questionBox').addClass('d-none')
        $(`#${boxQuestionId}${id}`).removeClass('d-none')
        $('.Q-modal').modal('hide')
      });
      
      $('.numberEx').click(function (e) { 
        e.preventDefault();
        let id = $(this).data('question_id')
        $('.questionBox').addClass('d-none')
        $(`#${boxQuestionId}${id}`).removeClass('d-none')
        $('.Q-modal').modal('hide')
      });

      $('.btn-go-next').click(function (e) { 
        e.preventDefault();
        let nextId = $(this).data('next-id')
        let currentId = $(this).data('current-id')

        $(`#${boxQuestionId}${currentId}`).addClass('d-none')
        $(`#${boxQuestionId}${nextId}`).removeClass('d-none')
      });
      
      $('.btn-go-prev').click(function (e) { 
        e.preventDefault();
        let previousId = $(this).data('previous-id')
        let currentId = $(this).data('current-id')

        $(`#${boxQuestionId}${currentId}`).addClass('d-none')
        $(`#${boxQuestionId}${previousId}`).removeClass('d-none')
      });

      
      $('.ref-choice').click(function(){ 
        
        let question_id = $(this).attr('question_id');
        let question_choice = $(this).attr('question_choice');

        let answerList = getSessionStore(sessionAnswer)
        let selectedAnswer = answerList.find(a => a.question_id == question_id)
        if(selectedAnswer){
          let idx = answerList.findIndex(a => a.question_id == question_id)
          answerList[idx] = {question_id, question_choice}
        }else{
          answerList.push({question_id, question_choice})
        }
        setSessionStore(sessionAnswer, answerList)
        $.post("<?=base_url("PreTest/UpdateAnswer");?>", {answerList, test_id: testID})
        .done(res => console.log(res))
        setStyleChoices()
  
      });
    });
  
  function setProgressBar(){
    
    let answerList = getSessionStore(sessionAnswer)
    let btnActive = answerList.length
    
    let questionList = getSessionStore(sessionQuestion)
    let percent = btnActive / questionList * 100
    
    $(".progress-bar").css({"clip-path" : `inset(0 ${100 - percent}% 0 0)`})
  }
    
function setStyleChoices(){
  let answerList = getSessionStore(sessionAnswer)
  $('.ref-choice').removeClass('active')
  $('.choiceAnswerSheet').removeClass('active')
  answerList.forEach(item => {
    $(`[question_id='${item.question_id}'][question_choice='${item.question_choice}']`)
      .addClass('active')
  })
  
  setProgressBar()
}
    
function setSessionStore(item, value){
	localStorage.setItem(item, JSON.stringify(value));
}
function getSessionStore(item){
  return	JSON.parse(localStorage.getItem(item));
}

function initSessionStore(){
  setSessionStore(sessionAnswer, <?php echo json_encode($answer) ?>);
  setSessionStore(sessionQuestion, <?php echo count($plan->planExercices) ?>);
  setSessionStore(sessionStart, new Date('<?= $start_time?>').toISOString());
  setStyleChoices()
}

function deleteStore(){
	localStorage.removeItem(sessionQuestion);
	localStorage.removeItem(sessionAnswer);
	localStorage.removeItem(sessionStart);
}


function post_to_url(path, params, method) {
  method = method || "post";

  var form = document.createElement("form");
  form.setAttribute("method", method);
  form.setAttribute("action", path);

  for(var key in params) {
      if(params.hasOwnProperty(key)) {
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("name", key);
          hiddenField.setAttribute("value", params[key]);

          form.appendChild(hiddenField);
        }
  }

  document.body.appendChild(form);
  form.submit();
}

function sendAnswer(){

let answerList = getSessionStore(sessionAnswer)
let btnActive = answerList.length
let questionList = getSessionStore(sessionQuestion)
let start = getSessionStore(sessionStart)
let duration = Number.parseInt((new Date() - new Date(start)) / 1000)
// console.log(duration)

if(btnActive !== questionList)
  {
    $('#modelNotComplete').modal('show')
    return false
  }
updateFinishTest(answerList, duration)

}

function updateFinishTest(answerList, duration){
  let answer = JSON.stringify(answerList)
  let payload = {
    plan_id: sheetID,
    answer: answer,
    test_id: testID,
    duration: duration 
  }
  $.post("<?=base_url("PreTest/FinishTest");?>", payload)
        .done(res => {
            deleteStore()
            window.location.href = '<?=base_url("PreTest/Result/".$plan->id."/".$test_id);?>';
        })
}

function closeAnswerAlert(){
    $('#modelNotComplete').modal('hide')
  }
</script>