$(document).ready(function() {
  $('.box-subject-items').addClass('d-none');
  $('.see-more').addClass('d-none');
  $('.btnLib').each(function() {
    $(this).removeClass('un-active');
    $(this).removeClass('active');

    img = $(this).data('img');
    $(this).find("img").attr("src", img);
    $(this).addClass('un-active');
  });

  initSlick();
});

function destroyCarousel() {
  if ($('.regular').hasClass('slick-initialized')) {
      $('.regular').slick('unslick');
  }

  if ($('.library-slide').hasClass('slick-initialized')) {
      $('.library-slide').slick('unslick');
  }
}

function initSlick() {
  $(".library-slide").slick({
    slidesToShow: 6.5,
    slidesToScroll: 6,
    dots: false,
    arrows: false,
    infinite: false,
    //focusOnSelect: true,
    //swipeToSlide: true,
    responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false, slidesToShow: 3.5, slidesToScroll: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false, slidesToShow: 2.5, slidesToScroll: 2
      }
    }
    ]
  });

  $(".regular:not(.slick-initialized)").slick({
    slidesToShow: 4.5,
    slidesToScroll: 4,
    dots: false,
    infinite: false,
    arrows: false,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          arrows: false, slidesToShow: 4.5, slidesToScroll: 4
        }
      },
      {
        breakpoint: 769,
        settings: {
          arrows: false, slidesToShow: 3.5, slidesToScroll: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false, slidesToShow: 2.5, slidesToScroll: 2
        }
      }
    ]
  });
}

function destroySlickSub() {
  if ($('.subSlider').hasClass('slick-initialized')) {
    $('.subSlider').slick('destroy');
  }      
}

function destroySlickSheet() {
  if ($('.sheetSlider').hasClass('slick-initialized')) {
    $('.sheetSlider').slick('destroy');
  }      
}

function getSliderSettings(){
  return {
    slidesToShow: 4.5,
    slidesToScroll: 4,
    dots: false,
    infinite: false,
    arrows: false,
    slickSetOption: true,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          arrows: false, slidesToShow: 4.5, slidesToScroll: 4
        }
      },
      {
        breakpoint: 769,
        settings: {
          arrows: false, slidesToShow: 3.5, slidesToScroll: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false, slidesToShow: 2.5, slidesToScroll: 2
        }
      }
    ]
  }
}


// เรียก Sublevel
function callSublevel(planId, levelId) {
  // Get ข้อมูล sub level
  $.ajax({
    type: "GET",
    url: "<?=base_url('MyProfile/get_sub_level')?>",
    data: {plan_id : planId, library_level_id : levelId},
    dataType: "json",
    success: function(data) {
      let boxSubLevel = $('.subSlider');
      destroySlickSub()
      boxSubLevel.empty()
      $.each(data, function(key, subLevel) {
        let comingsoon = subLevel.is_comingsoon == 0 ? 'd-none' : '';
        let $dataBox = "<div class='myEveryDay-slide-box slide-box-sub'" +
                      "data-is-comingsoon="+subLevel.is_comingsoon+" data-level-id="+subLevel.libraryLevelId+" " +
                      "data-sub-level-id="+subLevel.id+" data-title-level='"+subLevel.title+"' data-plan-id="+subLevel.plan_id+" data-index_row="+key+">"+
                      "<div class='d-flex relative'>" +
                      "<div class='content'>" +
                      "<img src="+subLevel.imgUrl+">" +
                      "<div class='progress'>" +
                      "<div class='progress-bar bg-success' role='progressbar' style='width: "+subLevel.progress_bar+"%' aria-valuenow='"+subLevel.progress_bar+"' aria-valuemin='0' aria-valuemax='100'></div>" +
                      "</div>" +
                      "<div class='m-t-5 textBtmMyeverday'>"+subLevel.title+"</div>"+
                      "<div class='status'><div class='comingSoon "+comingsoon+"'>COMING SOON</div></div>"+
                      "</div>" +
                      "<div class='arrow align-self-center'> <div class='icon-arrow'><i class='fa fa-angle-right'></i></div> </div>" +
                      "</div>" +
                      "</div>";      
        boxSubLevel.append($dataBox);
      });
      setTimeout(function () {
        $(".subSlider").slick(getSliderSettings());
      }, 100);
    }
  });
}

// เรียก subSheet
function callSubSheet(planId, levelId, subLevelId) {
  $.ajax({
    type: "GET",
    url: "<?=base_url('MyProfile/get_sub_sheet')?>",
    data: {plan_id : planId, library_level_id : levelId, sub_level_id : subLevelId},
    dataType: "json",
    success: function(data) {
      let boxSubLevelSheet = $('.subSheet[data-plan-id="'+planId+'"]').find('.slider');
      destroySlickSheet();
      boxSubLevelSheet.empty();
      $.each(data.sheets, function(key, subLevelSheet) {
        let comingsoon = subLevelSheet.is_comingsoon == 0 ? 'd-none' : '';
        let $dataBox = "<div class='myEveryDay-slide-box sheet-box'" +
                      "data-is-comingsoon="+subLevelSheet.is_comingsoon+" data-level-id="+subLevelSheet.libraryLevelId+" " +
                      "data-sub-level-id="+subLevelSheet.id+" data-title-level="+subLevelSheet.title+" data-plan-id="+subLevelSheet.plan_id+">"+
                      "<div class='d-flex relative'>" +
                      "<div class='content'>" +
                      "<a href='<?=base_url('MyEveryDay/Video/')?>"+subLevelSheet.id+"'>" +
                      "<img src="+subLevelSheet.imgUrl+">" +
                      "</a>" +
                      "<div class='progress'>" +
                      "<div class='progress-bar bg-success' role='progressbar' style='width: "+subLevelSheet.progress_bar+"%' aria-valuenow='"+subLevelSheet.progress_bar+"' aria-valuemin='0' aria-valuemax='100'></div>" +
                      "</div>" +
                      "<div class='m-t-5 textBtmMyeverday'>"+subLevelSheet.title+"</div>"+
                      "<div class='status'><div class='comingSoon "+comingsoon+"'>COMING SOON</div></div>"+
                      "</div>" +
                      "<div class='arrow align-self-center'> <div class='icon-arrow'><i class='fa fa-angle-right'></i></div> </div>" +
                      "</div>" +
                      "</div>";

        boxSubLevelSheet.append($dataBox);
      });

      if(data.test_sheets.length > 0) {
        $.each(data.test_sheets, function(key, subLevelSheetTest) {
          let $dataBox = "<div class='myEveryDay-slide-box sheet-box'" +
                        "data-level-id="+subLevelSheetTest.libraryLevelId+" " +
                        "data-sub-level-id="+subLevelSheetTest.id+" data-title-level="+subLevelSheetTest.title+" data-plan-id="+subLevelSheetTest.plan_id+">"+
                        "<div class='d-flex relative'>" +
                        "<div class='content'>" +
                        "<a href='<?=base_url('Test/index/')?>"+subLevelSheetTest.id+"/"+subLevelSheetTest.libraryLevelId+"/"+subLevelSheetTest.plan_id+"'>" +
                        "<img src='<?=base_url('resources/img/cover/Everyday_Placeholder_Test.png')?>'>" +
                        "</a>" +
                        "<div class='m-t-5 textBtmMyeverday'>"+subLevelSheetTest.title+"</div>"+
                        "</div>" +
                        "<div class='arrow align-self-center'> <div class='icon-arrow'><i class='fa fa-angle-right'></i></div> </div>" +
                        "</div>" +
                        "</div>";

          boxSubLevelSheet.append($dataBox);
        });
      }
      setTimeout(function () {
        $(".sheetSlider").slick(getSliderSettings());
      }, 100);
    }
  });

}

$(".regular").on( "click", ".level-box", function() {
  $(".levelSlider").slick('slickGoTo', $(this).data('index_row')); //Tricker for active it self
  let isComingsoon = $(this).data('is-comingsoon');
  if(isComingsoon)
    return false

  $('.level-box').removeClass('active');
  $('.level-box').addClass('gray');
  $(this).addClass('active');
  $(this).removeClass('gray');

  // ซ้อน sheet
  $('.sheet-header-box, .subSheet, .sheet-box, .sheet-test-box').addClass('d-none');
  // ซ้อน Sub level
  $('.slide-box-sub, .subLevel-box-header, .subDetail').addClass('d-none');

  let title = $(this).data('title-level');
  let levelId = $(this).data('level-id');
  let planId = $(this).data('plan-id');

  // แสดง Sublevel
  $('.subLevel-title').text(title);
  $('.slide-box-sub').addClass('d-none');
  // แสดง Sub level

  $('.subLevel-box-header[data-plan-id="'+planId+'"]').removeClass('d-none');
  $('.subDetail[data-plan-id="'+planId+'"]').removeClass('d-none');
  $('.slide-box-sub[data-level-id="'+levelId+'"][data-plan-id="'+planId+'"]').removeClass('d-none');

  callSublevel(planId, levelId);
});

// เลือก Sub level
$(".regular").on( "click", ".slide-box-sub", function() {
  $(".subSlider").slick('slickGoTo', $(this).data('index_row')); //Tricker for active it self
  let isComingsoon = $(this).data('is-comingsoon');
  if(isComingsoon)
    return false

  $('.slide-box-sub').removeClass('active');
  $('.slide-box-sub').addClass('gray');
  $(this).addClass('active');
  $(this).removeClass('gray');

  // ซ้อน sheet
  $(this).find('.sheet-box, .sheet-test-box').addClass('d-none');

  let title = $(this).data('title-level');
  let levelId = $(this).data('level-id');
  let subLevelId = $(this).data('sub-level-id');
  let planId = $(this).data('plan-id');
  // แสดงกล่อง sheet
  $('.sheet-header-box, .subSheet').addClass('d-none');

  $('.sheet-title').text(title);
  $('.sheet-header-box[data-plan-id="'+planId+'"]').removeClass('d-none');
  $('.subSheet[data-plan-id="'+planId+'"]').removeClass('d-none');
  $('.sheet-box[data-level-id="'+levelId+'"][data-sub-level-id="'+subLevelId+'"][data-plan-id="'+planId+'"]').removeClass('d-none');
  $('.sheet-test-box[data-level-id="'+levelId+'"][data-sub-level-id="'+subLevelId+'"][data-plan-id="'+planId+'"]').removeClass('d-none');

  callSubSheet(planId, levelId, subLevelId);
});
