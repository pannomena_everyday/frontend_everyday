<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<style>

</style>
<div id="wrap">
  <div id="container">
    <div class="libraryHeader">
      <section class="library-slide slider">
        <div class="libraryBox"><div><img src="<?=base_url()."resources/img/math_active.png"?>" class="icon-subject"/>คณิตศาสตร์ </div></div>
        <div class="libraryBox"><div><img src="<?=base_url()."resources/img/physics.png"?>" class="icon-subject"/>ฟิสิกส์ </div></div>
        <div class="libraryBox"><div><img src="<?=base_url()."resources/img/bio.png"?>" class="icon-subject"/>ชีววิทยา </div></div>
        <div class="libraryBox"><div><img src="<?=base_url()."resources/img/english.png"?>" class="icon-subject"/>ภาษาอังกฤษ </div></div>
      </section>
    </div>

    <section class="containner-padding lib-section-header center bg-white">
      <div class="f35">เนื้อหา ม.ต้น</div>
      <div class="lib-section-subHeader">
        <span class="active">ก</span>
        <span>ข</span>
        <span>ค</span>
        <span>A</span>
        <span>B</span>
        <span>C</span>
      </div>
    </section>
      
    <section class="library-section-guest containner-padding">
      <div class="library-section-txtHeader f35">ก</div>
      <div class="row">
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_6.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_5.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_4.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_3.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_2.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_1.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_6.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_5.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_4.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_3.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
      </div>
    </section>
      
    <section class="library-section-guest containner-padding">
      <div class="library-section-txtHeader f35">ข</div>
      <div class="row">
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_2.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_1.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_6.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_5.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_4.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_3.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_2.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_1.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_6.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_5.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
      </div>
    </section>
      
    <section class="library-section-guest containner-padding">
      <div class="libraryTextHeader f35">A</div>
      <div class="row">
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_4.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_3.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_2.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_1.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_6.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_5.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_4.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_3.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_2.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
        <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
          <img src="<?=base_url()."resources/img/top_1.jpg"?>"/>
          <p class="m-t-5">สับเซตและเพาเวอร์เซต</p>
        </div>
      </div>
    </section>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/slick-master/slick/slick.min.js"></script>
<script>
  $(".library-slide").slick({
    slidesToShow: 6,
      slidesToScroll: 6,
      dots: false,
      arrows: false,
      infinite: false,
      responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              slidesToShow: 3,
              slidesToScroll: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
            arrows: false,
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          }
        ]
  });
</script>