<link rel="stylesheet" href="<?=base_url('resources/slick-master/slick/slick.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/slick-master/slick/slick-theme.css');?>">
<link rel="stylesheet" koko="in" href="<?=base_url();?>resources/css/tutorial.min.css">
<style>
  #myEveryDayLibrary.tour-step-backdrop a{
    color: rgba(255,255,255,0.3);
  }
  .subLevel.tour-step-backdrop{
    background: transparent !important;
  }
  .subLevel.tour-step-backdrop .myEveryDayTextHeader, 
  .subLevel.tour-step-backdrop .textBtmMyeverday{ 
    color: #fff !important;
  }
  /*---- hide step end ------*/
  #step-0.step-end .btn-next,
  #step-1.step-end .btn-next,
  #step-3.step-end .btn-next, 
  #step-4 .btn-next {
    display: none;
  }
  /*---- set Centet step end ------*/
  #step-0.step-end .popover-navigation, 
  #step-1.step-end .popover-navigation, 
  #step-3.step-end .popover-navigation, 
  #step-4 .popover-navigation {
    -ms-flex-pack: center !important;
    justify-content: center !important;
  }
  .regular > .slick-list >.slick-track, .con_regular > .slick-list >.slick-track{
    margin-left:0;
    margin-right:0;
  }
  .slick-prev, .slick-next{top: 40%;}
  .myEveryday-slide{width: 85%; margin: 0px auto; height: fit-content; text-align:center;}
  .myEveryday-slide .libraryBox img{margin: 0 auto 10px auto; max-width: 55px;}
</style>
<div id="wrap" style="background: #000;">
  <div id="container">
    <div class="libraryHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="myEveryday-slide slider" id="myEveryDayLibrary">
            <?php 
            $index = 0;
            $findIndex = 0;
            foreach ($subjects as $key => $subject) :
              if($current_subject == $subject->id){
                $findIndex = $index;
              }
              ?>
              <?php if($subject->is_active = 1):?>
                <div class="libraryBox">
                  <a href="<?=base_url('MyEveryDay/?subject='.$subject->id);?>"><div class="btnLib <?=$subject->id;?>" data-box-subject="<?=$subject->id;?>" data-box-id="box_<?=$key?>" data-img="<?=$subject->icon1_url?>" data-img-active="<?=$subject->icon2_url?>"><img src="<?=$subject->icon2_url?>" class="icon-subject"/> <?=$subject->title?> </div></a>
                </div>
              <?php endif; ?>
              <?php 
              $index++; 
            endforeach; 
            ?>
          </section>
        </div>
      </div>
      <div class="myEveryDay-info tour-info js-restart-tour"><img src="<?=base_url()."resources/img/information.png"?>"/></div>
    </div>

    <?php if (!empty($my_plan['sheets']) && count($progress_continue_sheets) > 0):?>
    <section class="myEveryDay-section pb-0">  
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <div class="subLevel pb-0 bg-white" id="continueLearning">
            <div class="myEveryDayTextHeader f30 mb-0 text-dark">Continue Learning</div>
            <div class="subLevel-slide">
              <div class="con_regular slider">
                <?php foreach ($progress_continue_sheets as $sheet):?> 
                  <div class="myEveryDay-slide-box position-relative">
                    <div class="d-flex">
                      <div class="content hil-box orge">                  
                        <a href="<?=base_url('MyEveryDay/Video/').$sheet['sheet']->id?>">
                          <img src="<?=$sheet['sheet']->imgUrl?>"/>
                          <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: <?=$sheet['progress']?>%" aria-valuenow="<?=$sheet['progress']?>" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                          <p class="libraryTitle m-lib textBtmMyeverday text-truncate text-dark"><?=$sheet['sheet']->title?></p>
                        </a>
                      </div>
                    </div>
                  </div>
                <?php endforeach;?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php endif;?>

  <section class="containner-1200 mx-auto">
    <div class="containner-1200-padding">
      <div class="div-plan">
        <?php if(!empty($my_plan) && !empty($current_plan_id) && !empty($current_subject)): ?>
        <div class="row myEveryDay-H-box">
          <div class="allPlanHeader"><a href="<?=base_url('MyProfile/ChangePlan/'.$current_plan_id.'/'.$current_subject)?>">แก้ไขแผนการเรียน</a></div>
        </div>
        <div class="d-flex justify-content-between align-content-center" >
          <div class='new-prog-bar-cont align-self-center flex-grow-1'>
            <div class="prog-bar">
              <div class="background" style=" -webkit-clip-path: inset(0 <?=100-$my_plan['progress_bar']?>% 0 0); clip-path: inset(0 <?=100-$my_plan['progress_bar']?>% 0 0);"></div>
            </div>
          </div>
          <div class="p-2 new-progPercent f-white">
            <span class="f16">จำนวนวิดีโอ</span> <?=$my_plan['complete_vdo']?>/<?=$my_plan['all_vdo']?>
          </div>
        </div>
        <div id="levelwrapper">
          <div class="Level" style="background: <?=$my_plan['color_code']?> 0% 0% no-repeat padding-box;">
            <div class="subLevel-header">แผนการเรียน : <?=$my_plan['title_plan']?></div>
          </div>
          <div class="subLevel">
            <div class="subLevel-header"></div>
            <div class="subLevel-slide">
              <section class="regular levelSlider slider">
                <?php foreach ($my_plan['levels'] as $index => $level):?>
                  <div class="myEveryDay-slide-box level-box position-relative" data-is-comingsoon="<?= $level->is_comingsoon ?>" data-level-id="<?=$level->id;?>" data-title-level="<?=$level->title?>" data-plan-id="<?=$my_plan['plan_id']?>" data-index_row="<?=$index?>">
                    <div class="d-flex">
                      <div class="content">
                        <img src="<?=$level->imgUrl?>">
                        <div class="progress">
                          <div class="progress-bar bg-success" role="progressbar" style="width: <?=$level->progress_bar?>%" aria-valuenow="<?=$level->progress_bar?>" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="libraryTitle m-lib textBtmMyeverday"><?=$level->title?></div>
                        <?php if($level->is_comingsoon == 1): ?>
                          <div class="status"><div class="comingSoon">COMING SOON</div></div>
                        <?php endif; ?>
                      </div>
                      <div class="arrow align-self-center <?=($level->is_comingsoon == 1 ? 'getComingSoon"':'') ?>">
                        <div class="icon-arrow"><i class="fa fa-angle-right"></i></div>
                      </div>
                    </div>
                  </div>
                <?php endforeach;?>
              </section>
            </div>
          </div>
        </div>
        <div id="subLevel">
          <div class="subLevel-H-Box subLevel-box-header d-none" style="background: <?=$my_plan['color_code']?> 0% 0% no-repeat padding-box;">
            <div class="subLevel-header p-l-40 subLevel-title"></div>
          </div>
          <div class="subLevel subDetail p-t-50 d-none" data-plan-id="<?=$my_plan['plan_id'];?>">
            <div class="subLevel-slide">
              <section class="regular subSlider slider"></section>
            </div>
          </div>
        </div>

        <div id="sheet">
          <div class="subLevel-H-Box sheet-header-box d-none" style="background: <?=$my_plan['color_code']?> 0% 0% no-repeat padding-box;">
            <div class="subLevel-header p-l-40 sheet-title"></div>
          </div>

          <div class="subLevel subSheet p-t-50 d-none" data-plan-id="<?=$my_plan['plan_id'];?>">
            <div class="subLevel-slide">
              <section class="regular sheetSlider slider"></section>
            </div>
          </div>
        </div>

        <?php else:?>
          <?php if($count_plan > 0): ?>
            <div class="Level d-flex align-items-center" style="background: <?=$color_code?> 0% 0% no-repeat padding-box;">
              <div class="text">แผนการเรียน</div>
            </div>
            <div class="recommend">
              <div class="recommend-header">วิชานี้ยังไม่มีแผนการเรียน<br>กรุณาเพิ่มแผนการเรียนโดยคลิกปุ่มด้านล่างนี้</div>
              <div class="startPlanBtn-div d-flex justify-content-center flex-wrap flex-md-nowrap">
                <div class="w-100 m-w-360 m-15">
                  <a class="w-100" href="<?=base_url('Plan/AddPlan?subject='.$current_subject)?>"><button class="btn-60 btn-white-green">เพิ่มแผนการเรียน</button></a>
                </div>
              </div>
            </div>
            <?php else: ?>
              <div class="Level d-flex align-items-center" style="background: <?=$color_code?> 0% 0% no-repeat padding-box;">
                <div class="text">แผนการเรียน</div>
              </div>
              <div class="recommend">
                <div class="recommend-header">พบกับแผนการเรียนใหม่ เร็วๆนี้</div>
                <div class="startPlanBtn-div d-flex justify-content-center flex-wrap flex-md-nowrap">
                  <div class="w-100 m-w-360 m-15">
                  </div>
                </div>
              </div>
            <?php endif; ?>
          <?php endif;?>
        </div>
      </div>
    </section>

  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.11.0/js/bootstrap-tour.js"></script>

<script>
// Tutorials
$(function(){
  var totalMenu = $('.libraryBox').length;
  var index = <?=$findIndex?>;
  if($( window ).width() < 480){
    if(totalMenu != 0 && index >= (totalMenu-1)){
      index = totalMenu-2;
    }
  }else if($( window ).width() < 768){
    if(totalMenu != 0 && index >= (totalMenu-2)){
      index = totalMenu-3;
    }
  }
  $(".myEveryday-slide").slick('slickGoTo', index);

  <?php $recentVdo = json_encode($recent_vdo); ?>
  var levelID     = $('.level-box').data('level-id');
  // subLevelID  = $('.slide-box-sub').data('sub-level-id');
  if (initSlick){ //if true run tutorial    
    $('.js-restart-tour').on('click', function(e){
      e.preventDefault();
      <?php //check First Learning
      if ($recentVdo === 'null'): 
        if(!empty($my_plan)): ?>          
          tour.end();
          $('.level-box[data-level-id="'+levelID+'"]').trigger('click');
          setTimeout(function() {
            subLevelID  = document.querySelectorAll('.slide-box-sub')[0].attributes[3].value;
          }, 400); 
          setTimeout(function() {           
            $('.slide-box-sub[data-level-id="'+levelID+'"][data-sub-level-id="'+subLevelID+'"]').trigger('click');
          }, 600);         
        <?php endif; 
      endif; ?> 
      tour.restart();
    })	
    var tour = new Tour({
      name: 'start-myEveryDay',
      backdrop: true,
      keyboard: false,
      template: '<div class="start-myEveryDay text-light popover tour tutorial-step"> <p class="popover-content mb-2"></p> <div class="d-flex justify-content-between popover-navigation"> <button class="btn btn-skip" data-role="end">ปิด <i class="fa fa-times-circle"></i></button> <button class="btn btn-next" data-role="next">ถัดไป <i class="fa fa-chevron-circle-right"></i></button></div></div>',
<?php //check First Learning
if ($recentVdo === 'null'): 
  ?>
  <?php if(empty($my_plan)): ?>
    onShown: function (tour) {
      $('#step-0').addClass('step-end');
    },    
    <?php else: ?>
      onShown: function (tour) {
        $('#step-3').addClass('step-end');
      },              
    <?php endif; ?>  
  <?php endif; ?>  
<?php //check Continue Learning if not empty
if (empty($my_plan['sheets']) && count($progress_continue_sheets) < 0):
  ?>  
onShown: function (tour) {
  $('#step-3').addClass('step-end');
},    
<?php endif; ?>    
onEnd: function(){
  $('html,body').animate({ scrollTop: $('body').offset().top}, 'slow');
},
steps: [
{
  element: "#myEveryDayLibrary",
  content: 'แถบเมนู เลือก "วิชา"<br>ที่ต้องการเรียน',
  placement: "auto bottom",
  onNext: function(){
    $('.level-box[data-level-id="'+levelID+'"]').trigger('click');
    setTimeout(function() {
      subLevelID  = document.querySelectorAll('.slide-box-sub')[0].attributes[3].value;
    }, 400); 
    setTimeout(function() {           
      $('.slide-box-sub[data-level-id="'+levelID+'"][data-sub-level-id="'+subLevelID+'"]').trigger('click');
    }, 600);              
  }        
},
<?php
      //check Continue Learning if not empty
if (!empty($my_plan['sheets']) && count($progress_continue_sheets) > 0):
  ?>      
{
  element: "#continueLearning",
  content: 'รายการ "วีดีโอ"<br>ที่ยังดูไม่จบ',
  placement: function(context, source) {
    var position = $(source).position();
    if (position.left >= 40) {
      return "auto top";
    }
    if (position.left < 40 ) {
      return "top";
    }
  }
},
<?php endif; ?>
{
  element: "#levelwrapper",
  content: 'เลื่อนซ้าย ขวา<br>เพื่อดู "เรื่อง" ในแผนการเรียน<br>และกดเพื่อดู "หัวข้อ"',
  placement: "bottom"    
},
{
  element: "#subLevel",
  content: 'เลื่อนซ้าย ขวา<br>เพื่อดู "หัวข้อ" และกดเพื่อดู "ชีท"<br>ที่ต้องการเรียน',
  placement: "bottom"
}, 
{
  element: "#sheet",
  content: 'เลื่อนซ้าย ขวา<br>และกดเพื่อ "เริ่มเรียน"',
  placement: "top"
}     
]
});
    // Start the tour
    <?php if(!empty($my_plan)): ?>
      setTimeout(function() {
        tour.init();
        tour.start();    
      }, 1500);
    <?php endif; ?>  
    <?php //check First Learning
    if ($recentVdo === 'null'): 
      if(empty($my_plan)): ?>          
        tour.end();
      // $('.level-box[data-level-id="'+levelID+'"').trigger('click');
      // $('.slide-box-sub[data-level-id="'+levelID+'"][data-sub-level-id="'+subLevelID+'"]').trigger('click');
      // $('.slide-box-sub[data-level-id="'+levelID+'"][data-sub-level-id="'+subLevelID+'"]').trigger('click');    
      <?php  
    endif; 
    else: ?>
    if(hasRecentVdo){ //Check Resent form video section
      setTimeout(function() {
        tour.restart();  
      }, 1500);      
    }    
  <?php endif; ?>     
}
});
</script>