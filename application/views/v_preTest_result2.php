<div id="wrap">
  <div id="container">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <section class="preTest-result">
          <!-- <div class="preTest-result-info"><img src="<?=base_url()."resources/img/information.png"?>"/></div> -->
          <img src="<?=base_url()."resources/img/PreTest_NotPassed.png"?>" />
          <div class="f-red f30 m-t-20">แผนการเรียนนี้ไม่เหมาะสมกับนักเรียน</div>
          <div class="preTest-result-box">
            <div class="row">
              <div class="col-6 col-sm-6 col-md-3 box boxResult1">
                <div class="resultHeader f-blue">คิดเป็นเปอร์เซ็นต์</div>
                <div class="resultDetail rainbow-text"><?= round($score/$total_score*100) ?>%</div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult2">
                <div class="resultHeader">คะแนนที่ได้</div>
                <div class="resultDetail"><?= $score ?></div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult3">
                <div class="resultHeader">คะแนนเต็ม</div>
                <div class="resultDetail"><?= $total_score ?></div>
              </div>
              <div class="col-6 col-sm-6 col-md-3 box boxResult4">
                <div class="resultHeader">เวลา (นาที)</div>
                <div class="resultDetail"><?= fmtMSS($duration)?></div>
              </div>
            </div>
          </div>
          <div class="f30">แนะนำให้เลือกแผนการเรียนนี้</div>
          <div class="preTest-newPlan"><div class="txt f24"><?= $image_title?></div></div>
          <div class="d-flex justify-content-center flex-wrap">
          	<div class="p-2 m-w-250 w-100">
          		<!-- <a href="<?=base_url()."MyEveryDay";?>"><button class="btn-60 btn-green-white">ยอมรับ</button></a> -->
          		<a href="#"><button class="btn-60 btn-green-white" onclick="addPlan('<?=$fail_plan?>')">ยอมรับ</button></a>
          	</div>
          	<div class="p-2 m-w-250 w-100">
          		<!-- <a href="<?=base_url()."MyProfile/AddPlan";?>"><button class="btn-60 btn-white-green">ปฏิเสธ</button></a> -->
          		<a href="#"><button class="btn-60 btn-white-green" onclick="addPlan('<?=$pass_plan?>')">ปฏิเสธ</button></a>
          	</div>
          </div>
        </section>
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->
<?php
function fmtMSS($seconds) {
  $t = round($seconds);
  return sprintf('%02d:%02d',($t/60), $t%60);
}
?>

<!-- <script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script> -->
<script type="text/javascript">
  function addPlan(plan){
    $.ajax({
      type: "POST",
      url: "<?=base_url()."PreTest/AddPlan";?>",
      data: {plan},
      dataType: "json",
      success: function (response) {
        window.location = "<?=base_url()."MyEveryDay";?>"
      }
    });
  }
</script>