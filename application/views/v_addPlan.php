<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<div id="wrap">
  <div id="container">
    <div class="containner-padding">
     <div class="div-progAddPlan">
      <div class="d-flex justify-content-between align-content-center" >
        <div class='new-prog-bar-cont align-self-center flex-grow-1'>
          <div class="prog-bar">
            <div class="background" style="-webkit-clip-path: inset(0 80% 0 0); clip-path: inset(0 80% 0 0);"></div>
          </div>
        </div>
        <div class="p-2 new-progPercent f-white">
          <span class="f24 text-dark txtProg">20%</span>
        </div>
      </div>
    </div> <!-- End .div-progAddPlan -->

    <div class="box-1000-addPlan">
      <div class="step1">
        <div class="addPlanHeader">
          <div class="addPlanHeadertxt d-flex justify-content-between">
            <span>เพิ่มแผนการเรียน</span>
            <button class="addPlanHeaderbtn btn-green-white m-480-hide" <?= empty($this->input->get('subject')) ? 'disabled' : '' ?>>ถัดไป</button>
          </div>
        </div>

        <?php if(!empty($subject_arr)):?>
          <div class="addPlan-boxstep">
            <div class="addPlan-step-title">เลือกวิชา</div>
            <?php foreach ($plan_trees as $plan_tree):?>
              <?php if(in_array($plan_tree->title, $subject_arr)):?>
                <button class="addPlan-step-btn subject" id="<?=$plan_tree->id;?>" disabled><?=$plan_tree->title;?></button>
              <?php else:?>
                <button class="addPlan-step-btn subject" id="<?=$plan_tree->id;?>"><?=$plan_tree->title;?></button>
              <?php endif;?>
            <?php endforeach;?>
          </div>
        <?php else:?>
          <div class="addPlan-boxstep">
            <div class="addPlan-step-title">เลือกวิชา</div>
            <?php foreach ($plan_trees as $plan_tree):?>
              <?php if($plan_tree->title == $subject_title):?>
                <button class="addPlan-step-btn subject active" id="<?=$plan_tree->id;?>"><?=$plan_tree->title;?></button>
              <?php else:?>
                <button class="addPlan-step-btn subject" <?=($this->input->get('subject')? 'disabled' : '') ?> id="<?=$plan_tree->id;?>"><?=$plan_tree->title;?></button>            
              <?php endif; ?>
            <?php endforeach;?>
          </div>
        <?php endif;?>
        <div class="addPlan-mobile-btn d-flex justify-content-around align-items-center">
          <button class="addPlanHeaderbtn btn-green-white dp-none m-480-show align-self-center mb-4">ถัดไป</button>
        </div>
      </div> <!-- Step 1 -->

      <div class="step2" style="display: none;">
        <div class="addPlanHeader">
          <div class="addPlanHeadertxt d-flex justify-content-between flex-wrap">
            <span>เพิ่มแผนการเรียน</span>
            <div>
              <button class="addPlanPreviousbtn btn-gray m-480-hide step2-pre-btn">ย้อนกลับ</button>
              <button class="addPlanHeaderbtn btn-green-white m-480-hide">ถัดไป</button>
            </div>
          </div>
        </div>
        <div class="addPlan-boxstep">
        </div>
        <div class="addPlan-mobile-btn d-flex justify-content-around align-items-center">
          <button class="addPlanPreviousbtn btn-gray dp-none m-480-show step2-pre-btn align-self-center mb-4">ย้อนกลับ</button>
          <button class="addPlanHeaderbtn btn-green-white dp-none m-480-show align-self-center mb-4">ถัดไป</button>
        </div>
      </div> <!-- Step 2 -->

      <div class="step3" style="display: none;">
        <div class="addPlanHeader">
          <div class="addPlanHeadertxt d-flex justify-content-between flex-wrap">
            <span>เพิ่มแผนการเรียน</span>
            <div>
              <button class="addPlanPreviousbtn btn-gray m-480-hide step3-pre-btn">ย้อนกลับ</button>
              <button class="addPlanHeaderbtn btn-green-white m-480-hide">ถัดไป</button>
            </div>
          </div>
        </div>
        <div class="addPlan-boxstep">
        </div>
        <div class="addPlan-mobile-btn d-flex justify-content-around align-items-center">
          <button class="addPlanPreviousbtn btn-gray dp-none m-480-show step3-pre-btn align-self-center mb-4">ย้อนกลับ</button>
          <button class="addPlanHeaderbtn btn-green-white dp-none m-480-show align-self-center mb-4">ถัดไป</button>
        </div>
      </div> <!-- Step 3 -->

      <div class="step4" style="display: none;">
        <div class="addPlanHeader">
          <div class="addPlanHeadertxt d-flex justify-content-between flex-wrap">
            <span>เพิ่มแผนการเรียน</span>
            <div>
              <button class="addPlanPreviousbtn btn-gray m-480-hide step4-pre-btn">ย้อนกลับ</button>
              <button class="addPlanHeaderbtn btn-green-white m-480-hide">ถัดไป</button>
            </div>
          </div>
        </div>
        <div class="addPlan-boxstep">
        </div>
        <div class="addPlan-mobile-btn d-flex justify-content-around align-items-center">
          <button class="addPlanPreviousbtn btn-gray dp-none m-480-show step4-pre-btn align-self-center mb-4">ย้อนกลับ</button>
          <button class="addPlanHeaderbtn btn-green-white dp-none m-480-show align-self-center mb-4">ถัดไป</button>
        </div>
      </div> <!-- Step 4 -->

      <div class="step5" style="display: none;">
        <div class="addPlanHeader">
          <div class="addPlanHeadertxt d-flex justify-content-between flex-wrap">
            <span>เพิ่มแผนการเรียน</span>
            <div>
              <button class="addPlanPreviousbtn btn-gray m-480-hide step5-pre-btn">ย้อนกลับ</button>
              <button class="addPlanHeaderbtn btn-green-white m-480-hide">ถัดไป</button>
            </div>
          </div>
        </div>
        <div class="addPlan-boxstep">
        </div>
        <div class="addPlan-mobile-btn d-flex justify-content-around align-items-center">
          <button class="addPlanPreviousbtn btn-gray dp-none m-480-show step5-pre-btn align-self-center mb-4">ย้อนกลับ</button>
          <button class="addPlanHeaderbtn btn-green-white dp-none m-480-show align-self-center mb-4">ถัดไป</button>
        </div>
      </div> <!-- Step 5 -->
    </div>
  </div> <!-- //.containner-padding -->
</div> <!-- //.Container -->
</div> <!-- //#Warp -->
<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script type="text/javascript">

  var planData = [];
  var planObject = {};
  <?php if(!empty($this->input->get('subject'))){ ?>
    if(<?=$this->input->get('subject')?>) { // ถ้ากดเพิ่ม plan จากหน้า myeveryday และส่ง subject มาด้วย
      planData.subject = $('.subject.active').attr('id');
    }
  <?php } ?>

  // เลือกวิชา
  $('.subject').click(function(){
    $('.subject').removeClass('active');

    delete planData.subject;

    $(this).addClass('active');
    planData.subject = $(this).attr('id');
    $('.addPlanHeaderbtn').prop('disabled', false);
  });

  // เลือก นักเรียนมีความประสงค์จะสอบแข่งขันหรือไม่
  $('.step2').on('click', '.sub-plan-tree', function() {
    $('.sub-plan-tree').removeClass('active');

    delete planData.sub_plan_tree;
    delete planData.has_child;
    delete planData.plan_key;

    $(this).addClass('active');
    planData.sub_plan_tree = $(this).attr('id');
    planData.has_child = $(this).data('has-child');
    planData.plan_key = $(this).data('plan-key');
    $('.addPlanHeaderbtn').prop('disabled', false);
  });

  // ความสนใจในระดับวิชาที่ต้องการเรียน
  $('.step3').on('click', '.sub-sub-plan-tree', function() {
    $('.sub-sub-plan-tree').removeClass('active');

    delete planData.sub_sub_plan_tree;
    delete planData.has_child;
    delete planData.plan_key;

    $(this).addClass('active');
    planData.sub_sub_plan_tree = $(this).attr('id');
    planData.has_child = $(this).data('has-child');
    planData.plan_key = $(this).data('plan-key');
    $('.addPlanHeaderbtn').prop('disabled', false);
  });

  $('.step4').on('click', '.sub-sub-sub-plan-tree', function() {
    $('.sub-sub-sub-plan-tree').removeClass('active');

    delete planData.sub_sub_sub_plan_tree;
    delete planData.has_child;
    delete planData.plan_key;

    $(this).addClass('active');
    planData.sub_sub_sub_plan_tree = $(this).attr('id');
    planData.has_child = $(this).data('has-child');
    planData.plan_key = $(this).data('plan-key');
    $('.addPlanHeaderbtn').prop('disabled', false);
  });

  $('.step5').on('click', '.last-plan', function() {
    $('.last-plan').removeClass('active');

    delete planData.last_plan;
    delete planData.plan_key;

    $(this).addClass('active');
    planData.last_plan = $(this).attr('id');
    planData.plan_key = $(this).data('plan-key');
    $('.addPlanHeaderbtn').prop('disabled', false);
  });

  // ปุ่มย้อนกลับ
  $('.step2-pre-btn').on('click', function() {
    $('.step2').hide();
    $('.step1').fadeIn( "1000" );
    $('.prog-bar .background').css({"-webkit-clip-path": "inset(0 80% 0 0)", "clip-path": "inset(0 80% 0 0)"});
    $('.txtProg').html('20%');

    delete planData.sub_plan_tree;
    delete planData.has_child;
    delete planData.plan_key;

    $('.addPlanHeaderbtn').prop('disabled', false);
    getSubPlan();
  });

  $('.step3-pre-btn').on('click', function() {
    $('.step3, .step1').hide();
    $('.step2').fadeIn( "1000" );
    $('.prog-bar .background').css({"-webkit-clip-path": "inset(0 60% 0 0)", "clip-path": "inset(0 60% 0 0)"});
    $('.txtProg').html('40%');

    delete planData.sub_sub_plan_tree;
    delete planData.has_child;
    delete planData.plan_key;

    $('.addPlanHeaderbtn').prop('disabled', false);
    getChildrenSubPlan();
  });

  $('.step4-pre-btn').on('click', function() {
    $('.step4, .step2, .step1').hide();
    $('.step3').fadeIn( "1000" );
    $('.prog-bar .background').css({"-webkit-clip-path": "inset(0 40% 0 0)", "clip-path": "inset(0 40% 0 0)"});
    $('.txtProg').html('60%');

    delete planData.sub_sub_sub_plan_tree;
    delete planData.has_child;
    delete planData.plan_key;

    $('.addPlanHeaderbtn').prop('disabled', false);
    getChildrenSubSubPlan();
  });

  $('.step5-pre-btn').on('click', function() {
    $('.step5, .step3, .step2, .step1').hide();
    $('.step4').fadeIn( "1000" );
    $('.prog-bar .background').css({"-webkit-clip-path": "inset(0 20% 0 0)", "clip-path": "inset(0 20% 0 0)"});
    $('.txtProg').html('80%');

    delete planData.last_plan;
    delete planData.plan_key;

    $('.addPlanHeaderbtn').prop('disabled', false);
    getChildrenSubSubSubPlan();
  });

  $('.step1 .addPlanHeaderbtn').click(function(){
    $('.step1').hide();
    $('.step2').fadeIn( "1000" );
    $('.prog-bar .background').css({"-webkit-clip-path": "inset(0 60% 0 0)", "clip-path": "inset(0 60% 0 0)"});
    $('.txtProg').html('40%');

    // แสดงเฉพาะ education_major ตาม education_level
    $('.education-level-' + planData.education_level).removeClass('d-none');
    $('.addPlanHeaderbtn').prop('disabled', true);
    getSubPlan();
  });

  $('.step2 .addPlanHeaderbtn').click(function(){
    $('.step2').hide();
    $('.step3').fadeIn( "1000" );
    $('.prog-bar .background').css({"-webkit-clip-path": "inset(0 40% 0 0)", "clip-path": "inset(0 40% 0 0)"});
    $('.txtProg').html('60%');

    $('.addPlanHeaderbtn').prop('disabled', true);
    getChildrenSubPlan();
  });

  $('.step3 .addPlanHeaderbtn').click(function(){
    $('.step3').hide();
    $('.step4').fadeIn( "1000" );
    $('.prog-bar .background').css({"-webkit-clip-path": "inset(0 20% 0 0)", "clip-path": "inset(0 20% 0 0)"});
    $('.txtProg').html('80%');

    $('.addPlanHeaderbtn').prop('disabled', true);
    getChildrenSubSubPlan();
  });

  $('.step4 .addPlanHeaderbtn').click(function(){
    $('.step4').hide();
    $('.step5').fadeIn( "1000" );
    $('.prog-bar .background').css({"-webkit-clip-path": "inset(0 0% 0 0)", "clip-path": "inset(0 0% 0 0)"});
    $('.txtProg').html('100%');

    $('.addPlanHeaderbtn').prop('disabled', true);
    getChildrenSubSubSubPlan();
  });

  $('.step5 .addPlanHeaderbtn').click(function() {
    getPlanLibraries(true);
  });

  function getSubPlan() {
    $.ajax({
      type: "GET",
      url: "<?=base_url('plan/get_sub_plan')?>",
      data: {subject : planData.subject},
      dataType: "json",
      success: function(data) {
        // เคลียของเก่าก่อน
        $('.step2').find('.addPlan-boxstep').empty();

        $('.step2-pre-btn').prop('disabled', false);
        let subtitle = data.subtitle == undefined ? '' : data.subtitle;
        $('.step2').find('.addPlan-boxstep').append('<div class="addPlan-step-title">'+subtitle+'</div>');
        $.each(data.children, function(key, value) {
          $('.step2').find('.addPlan-boxstep').append('<button class="addPlan-step-btn sub-plan-tree" id="'+ value.id +'" data-has-child="'+ value.hasChild +'" data-plan-key="'+value.plan_key+'">' + value.title + '</button>');
        });
      }
    });
  }

  function getChildrenSubPlan() {
    $.ajax({
      type: "GET",
      url: "<?=base_url('plan/get_children_sub_plan')?>",
      data: {subplan : planData.sub_plan_tree, subject : planData.subject},
      dataType: "json",
      success: function(data) {
        // เคลียของเก่าก่อน
        $('.step3').find('.addPlan-boxstep').empty();

        $('.step3-pre-btn').prop('disabled', false);
        if(planData.has_child == false) {
          // หาค่า planId
          getPlanLibraries(true);
        }
        else {
          let subtitle = data.subtitle == undefined ? '' : data.subtitle;
          $('.step3').find('.addPlan-boxstep').append('<div class="addPlan-step-title">'+subtitle+'</div>');
          $.each(data.children, function(key, value) {
            $('.step3').find('.addPlan-boxstep').append('<button class="addPlan-step-btn sub-sub-plan-tree" id="'+ value.id +'" data-has-child="'+ value.hasChild +'" data-plan-key="'+value.plan_key+'">' + value.title + '</button>');
          });
        }
      }
    });
  }

  function getChildrenSubSubPlan() {
   $.ajax({
    type: "GET",
    url: "<?=base_url('plan/get_children_sub_sub_plan')?>",
    data: {subsubplan : planData.sub_sub_plan_tree, subplan : planData.sub_plan_tree, subject : planData.subject},
    dataType: "json",
    success: function(data) {
      // เคลียของเก่าก่อน
      $('.step4').find('.addPlan-boxstep').empty()

      $('.step4-pre-btn').prop('disabled', false);
      if(planData.has_child == false) {
          // หาค่า planId
          getPlanLibraries(true);
        }
        else {
          let subtitle = data.subtitle == undefined ? '' : data.subtitle;
          $('.step4').find('.addPlan-boxstep').append('<div class="addPlan-step-title">'+subtitle+'</div>');
          $.each(data.children, function(key, value) {
            $('.step4').find('.addPlan-boxstep').append('<button class="addPlan-step-btn sub-sub-sub-plan-tree" id="'+ value.id +'" data-has-child="'+ value.hasChild +'" data-plan-key="'+value.plan_key+'">' + value.title + '</button>');
          });
        }
      }
    });
  }

  function getChildrenSubSubSubPlan() {
    $.ajax({
      type: "GET",
      url: "<?=base_url('plan/get_children_sub_sub_sub_plan')?>",
      data: {subsubsubplan : planData.sub_sub_sub_plan_tree, subsubplan : planData.sub_sub_plan_tree, subplan : planData.sub_plan_tree, subject : planData.subject},
      dataType: "json",
      success: function(data) {
        // เคลียของเก่าก่อน
        $('.step5').find('.addPlan-boxstep').empty();

        $('.step5-pre-btn').prop('disabled', false);
        if(planData.has_child == false) {
            // หาค่า planId
            getPlanLibraries(true);
          }
          else {
            let subtitle = data.subtitle == undefined ? '' : data.subtitle;
            $('.step5').find('.addPlan-boxstep').append('<div class="addPlan-step-title">'+subtitle+'</div>');
            $.each(data.children, function(key, value) {
              $('.step5').find('.addPlan-boxstep').append('<button class="addPlan-step-btn last-plan" id="'+ value.id +'" data-has-child="'+ value.hasChild +'" data-plan-key="'+value.plan_key+'">' + value.title + '</button>');
            });
          }
        }
      }); 
  }

  function getPlanLibraries(children = true) {
    let data = {
      plan_key : planData.plan_key,
      children : children,
      sub_plan_tree : planData.sub_plan_tree,
      sub_sub_plan_tree : planData.sub_sub_plan_tree,
      sub_sub_sub_plan_tree : planData.sub_sub_sub_plan_tree,
    }

    $.ajax({
      type: "GET",
      url: "<?=base_url('plan/get_plan_libraries')?>",
      data: data,
      dataType: "json",
      success: function(data) {
        if(data.status == null)
          window.location.href = "<?=base_url("Plan/addPlanComplete?plan=")?>" + data.planId;
        else
          window.location.href = "<?=base_url("Plan/addPlan")?>";
      }
    });
  }
</script>

