<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">

<div id="wrap">
  <div id="container" class="bg-white">
  <div class="libraryHeader">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <section class="library-slide slider">
          <div class="libraryBox"><div><img src="<?=base_url()."resources/img/math_active.png"?>" class="icon-subject"/>คณิตศาสตร์ </div></div>
          <div class="libraryBox"><div><img src="<?=base_url()."resources/img/physics.png"?>" class="icon-subject"/>ฟิสิกส์ </div></div>
          <div class="libraryBox"><div><img src="<?=base_url()."resources/img/bio.png"?>" class="icon-subject"/>ชีววิทยา </div></div>
          <div class="libraryBox"><div><img src="<?=base_url()."resources/img/english.png"?>" class="icon-subject"/>ภาษาอังกฤษ </div></div>
        </section>
      </div>
    </div>
    </div>

    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="px-3">
          <a href="<?=base_url()."Plan/AddPlan"?>"><button class="addPlanBtn btn-green-white">เพิ่มแผนการเรียน</button></a>
        </div>
      </div>
    </div>

    <div class="div-addPlan">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <div class="addPlan-header">แนะนำเนื้อหา ม.ต้น</div>
          <div class="row">
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100" src="<?=base_url()."resources/img/cover/level/AP-EM-MT.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100" src="<?=base_url()."resources/img/cover/level/AP-EM-MT.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100" src="<?=base_url()."resources/img/cover/level/AP-EM-MT.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100" src="<?=base_url()."resources/img/cover/level/AP-EM-MT.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100" src="<?=base_url()."resources/img/cover/level/AP-EM-MT.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100"  src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100"  src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100"  src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100"  src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100"  src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- .div-addPlan  -->
      
    <div class="div-addPlan">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <div class="addPlan-header">แนะนำเนื้อหา ม.ปลาย</div>
          <div class="row">
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100" src="<?=base_url()."resources/img/cover/level/AP-EM-MT.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100" src="<?=base_url()."resources/img/cover/level/AP-EM-MT.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100" src="<?=base_url()."resources/img/cover/level/AP-EM-MT.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100" src="<?=base_url()."resources/img/cover/level/AP-EM-MT.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100" src="<?=base_url()."resources/img/cover/level/AP-EM-MT.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100"  src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100"  src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100"  src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100"  src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
            <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10">
              <div class="m-w-200">
                <img class="mw-100"  src="<?=base_url()."resources/img/cover/level/M-OL-AI.png"?>"/>
                <p>สับเซตและเพาเวอร์เซต</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- .div-addPlan  -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/slick-master/slick/slick.min.js"></script>
<script>
  $(".library-slide").slick({
    slidesToShow: 6,
      slidesToScroll: 6,
      dots: false,
      arrows: false,
      infinite: false,
      responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              slidesToShow: 3,
              slidesToScroll: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
            arrows: false,
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          }
        ]
  });
  
  $( "#profile" ).click(function() {
    $('#profileBox').toggle('slow');
  });
</script>