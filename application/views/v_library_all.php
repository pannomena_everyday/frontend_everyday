<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<style>

</style>
<div id="wrap">
  <div id="container">
    <div class="libraryHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="library-slide slider">
            <?php foreach ($subjects as $key => $subject) :?>
            <div class="libraryBox">
              <a href="<?=base_url('Library/All?subject='.$subject->id);?>"><div class="btnLib <?=$subject->id;?>" data-box-subject="<?=$subject->id;?>" data-box-id="box_<?=$key?>" data-img="<?=$subject->icon1_url?>" data-img-active="<?=$subject->icon2_url?>"><img src="<?=$subject->icon2_url?>" class="icon-subject"/> <?=$subject->title?> </div></a>
            </div>
            <?php endforeach; ?>
          </section>
        </div>
      </div>
    </div>

  <?php if(!empty($libraries)): ?>
    <?php foreach ($libraries as $index => $library):?>
    <section id="box1" class="library-section" style="padding-top: 0;">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200">
          <div class="p-4">
            <?php if($library['name'] != ''): ?>
            <div class="libraryTextHeader box-subject-items box-subject-<?=$library['subjectId'];?> f30"><?=$library['name']?></div>
            <?php else: ?>
              <div class="libraryTextHeader box-subject-items box-subject-<?=$library['subjectId'];?> f30">เนื้อหา -</div>
            <?php endif; ?>
            <div class="row">
              <?php foreach ($library['lists'] as $key => $value):?>
              <div class="col-xs-2-10 col-xs-2-5-10 col-sm-2-10 col-md-2-10 col-lg-2-10 box-subject-items box-subject-<?=$value->subjectId;?>">
                <?php if($value->is_comingsoon): ?>
                <a zhref="#">
                  <img src="<?=$value->imgUrl?>"/>
                  <p class="libraryTitle m-lib"><?=$value->title?></p>
                  <div class="status"><div class="comingSoon">COMING SOON</div></div>
                </a>
                <?php else: ?>
                <a href="<?=base_url('Library/SubList/'.$value->id.'?subject='.$value->subjectId)?>">
                  <img src="<?=$value->imgUrl?>"/>
                  <p class="libraryTitle m-lib"><?=$value->title?></p>
                </a>
                <?php endif; ?>
              </div>
              <?php endforeach;?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php endforeach; ?>
  <?php endif; ?>

  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/slick-master/slick/slick.min.js"></script>
<script>
  var img = '';
  var imgActive = '';
  var boxId = '';

  $(document).ready(function() {
    $('.box-subject-items').addClass('d-none');
    $('.see-more').addClass('d-none');
    $('.btnLib').each(function() {
      $(this).removeClass('un-active');
      $(this).removeClass('active');

      img = $(this).data('img');
      $(this).find("img").attr("src", img);
      $(this).addClass('un-active');
    });

    var activeSub = "<?=$this->input->get('subject');?>";
    btnClick($('div.' + activeSub));

    $('.btnLib').on("click", function() {
      $('.btnLib').each(function() {
        $(this).removeClass('un-active');
        $(this).removeClass('active');

        img = $(this).data('img');
        $(this).find("img").attr("src", img);
        $(this).addClass('un-active');

        $('.box-subject-items').addClass('d-none');
        $('.see-more').addClass('d-none');
      });
      btnClick($(this));
    });

    function btnClick(btn) {
      boxId = $(btn).data('box-id');
      imgActive = $(btn).data('img-active');

      $('.box-subject-' + $(btn).data('box-subject')).removeClass('d-none');
      $('.see-more').removeClass('d-none');

      btn.find("img").attr("src", imgActive);
      btn.removeClass('un-active');
      btn.removeClass('active');
      btn.addClass('active');
    }
  });

  $(".library-slide").slick({
    slidesToShow: 6,
      slidesToScroll: 6,
      dots: false,
      arrows: false,
      infinite: false,
      responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              slidesToShow: 3,
              slidesToScroll: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
            arrows: false,
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          }
        ]
  });
</script>

