<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<div id="wrap">
  <div id="container">
    <div class="libraryHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="library-slide slider">
            <?php foreach ($subjects as $key => $subject) :?>
            <div class="libraryBox">
              <div class="btnLib <?=$subject->id;?>" data-lib-no="<?=$key?>" data-box-subject="<?=$subject->id;?>" data-box-id="box_<?=$key?>" data-img="<?=$subject->icon1_url?>" data-img-active="<?=$subject->icon2_url?>"><img src="<?=$subject->icon2_url?>" class="icon-subject"/> <?=$subject->title?> </div>
            </div>
            <?php endforeach; ?>
          </section>
        </div>
      </div>
    </div>
    <div class="bg-white">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <div class="p-4">
            <div class="library-select-sub p-t-80 p-b-40">
              <select class="form-control py-2 full-gradient-box select-cat">
                  <option value="">เลือกบทเรียนทั้งหมด</option>
                <?php foreach ($sublevels as $key => $sublevel):?>
                  <option value="<?=$key;?>">เลือกบทเรียนตามตัวอักษร : <?=$key;?></option>
                <?php endforeach;?>
              </select>
            </div>
            <?php foreach ($sublevels as $key => $sublevel):?>
            <section class="library-section-sub <?=$key;?>">
              <div class="libraryTextHeader f35"><?=$key;?></div>
              <div class="row">
                <?php foreach ($sublevel['items'] as $item):?>
                <div class="col-xs-2-10 col-xs-2-5-10 col-sm-2-10 col-md-2-10 col-lg-2-10 subject-items subject-key-<?=$key?> subject-id-<?=$item->subjectId;?>">
                  <?php if($item->only_subscriber == 1): ?>
                  <a zhref="#">
                    <img loading="lazy" src="<?=$item->imgUrl?>"/>
                    <p class="libraryTitle text-left m-lib"><?=$item->title?></p>
                    <?php if(!empty($item->is_comingsoon)): ?>
                    <div class="status-sublist-guest"><div class="comingsoon-guest">COMING SOON</div></div>
                    <?php endif; ?>
                    <div class="icon-lock">
                      <img src="<?=base_url('resources/img/lock.png')?>">
                    </div>
                  </a>
                  <?php else: ?>
                    <?php if(!empty($item->is_comingsoon)): ?>
                    <a zhref="#">
                      <img loading="lazy" src="<?=$item->imgUrl?>"/>
                      <p class="libraryTitle text-left m-lib"><?=$item->title?></p>
                      <div class="status-sublist-guest"><div class="comingsoon-guest">COMING SOON</div></div>
                    </a>
                    <?php else: ?>
                    <a href="<?=base_url('Library/SubList/'.$item->id.'?subject='.$item->subjectId)?>">
                      <img loading="lazy" src="<?=$item->imgUrl?>"/>
                      <p class="libraryTitle text-left m-lib"><?=$item->title?></p>
                    </a>
                    <?php endif; ?>
                  <?php endif; ?>
                </div>
                <?php endforeach; ?>
              </div>
            </section>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div> <!-- //.containner-padding -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/slick-master/slick/slick.min.js"></script>
<script>
  const list_key = JSON.parse('<?php echo json_encode($list_key); ?>')
  $(".library-slide").slick({
    slidesToShow: 6.5,
    slidesToScroll: 6,
    dots: false,
    arrows: false,
    infinite: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 3.5,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 480,
        settings: {
        arrows: false,
          slidesToShow: 2.5,
          slidesToScroll: 2,
        }
      }
    ]
  });

  if(sessionStorage.getItem('current_library_subject') != null){
    var index = $('.btnLib[data-box-subject='+sessionStorage.getItem('current_library_subject')+']').data("lib-no");
    var totalMenu = $('.libraryBox').length;
    // if($( window ).width() < 480){
    //   if(totalMenu != 0 && index >= (totalMenu-1)){
    //     index = totalMenu-2;
    //   }
    // }else if($( window ).width() < 768){
    //   if(totalMenu != 0 && index >= (totalMenu-2)){
    //     index = totalMenu-3;
    //   }
    // }
    $(".library-slide").slick('slickGoTo', index);
  }

    $(document).ready(function() {
    $('.select-cat').change(function() {
      filters();
    });

    $('.btnLib').each(function() {
      $(this).removeClass('un-active');
      $(this).removeClass('active');

      img = $(this).data('img');
      $(this).find("img").attr("src", img);
      $(this).addClass('un-active');
    });

    $('.btnLib').on("click", function() {
      $('.btnLib').each(function() {
        $(this).removeClass('un-active');
        $(this).removeClass('active');

        img = $(this).data('img');
        $(this).find("img").attr("src", img);
        $(this).addClass('un-active');
      });
      btnClick($(this));
    });

    // active เมนูด้านบนจาก sub ที่ส่งมา
    var activeSub = "<?=$this->input->get('subject');?>";

    if(activeSub){
      sessionStorage.setItem('current_library_subject', activeSub);
      // btnClick($('div.' + activeSub));
    }

    if(sessionStorage.getItem('current_library_subject') == null){
      btnClick($('div.' + $($('div.btnLib')[0]).data('box-subject')));
    }else{
      btnClick($('div.' + sessionStorage.getItem('current_library_subject')), false)
    }

    if(sessionStorage.getItem('current_library_cat') != null){
        $('.select-cat').val(sessionStorage.getItem('current_library_cat'))
        filters()
      }

    function btnClick(btn, isNotInit=true) {
      sessionStorage.setItem('current_library_subject', $(btn).data('box-subject'));
      if(isNotInit){
        sessionStorage.removeItem('current_library_cat');
        $('.select-cat').val('');
      }
      boxId = $(btn).data('box-id');
      imgActive = $(btn).data('img-active');

      // แสดงเฉพาะข้อมูลตาม sub id
      $('.subject-items').addClass('d-none');
      $('.subject-id-' + $(btn).data('box-subject')).removeClass('d-none');

      btn.find("img").attr("src", imgActive);
      btn.removeClass('un-active');
      btn.removeClass('active');
      btn.addClass('active');
      filters();
      hideCategory();
    }

    function filters() {
      $('.library-section-sub').addClass('d-none');
      let catVal = $('.select-cat option:selected').val();
      if(catVal){
        sessionStorage.setItem('current_library_cat', catVal);
        $('.' + catVal).removeClass('d-none');
      }
      else {
        $('.library-section-sub').removeClass('d-none');
        hideCategory();
      }

    }
    
    function hideCategory(){
      let subjectId = sessionStorage.getItem('current_library_subject')
      let optionCat = ['<option value="">เลือกบทเรียนทั้งหมด</option>']
      $(`.library-section-sub`).addClass('d-none');
      list_key.forEach(key => {
        let $el = $(`.subject-items.subject-id-${subjectId}.subject-key-${key}`)
        if($el.length > 0) {
          $(`.library-section-sub.${key}`).removeClass('d-none');
          optionCat.push(`<option value="${key}">เลือกบทเรียนตามตัวอักษร : ${key}</option>`)
        }
      })
      $('.select-cat').html(optionCat.join(''))
    }
  })
  

</script>