<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">

<div id="wrap">
  <div id="container">
    <div class="myProfileHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="myProfile-slide slider">
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile"?>"><img src="<?=base_url()."resources/img/account.png"?>" class="icon-subject"/>บัญชีผู้ใช้ </a> 
            </div>
            <div class="myProfileBox active">
              <img src="<?=base_url()."resources/img/plan_active.png"?>" class="icon-subject"/><span>แผนการเรียน</span> 
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyPerformance"?>"><img src="<?=base_url()."resources/img/performance.png"?>" class="icon-subject"/>ติดตามผลงาน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyNote"?>"><img src="<?=base_url()."resources/img/sheet.png"?>" class="icon-subject"/>คลัง Sheet </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyLog"?>"><img src="<?=base_url()."resources/img/loghistory.png"?>" class="icon-subject"/>ประวัติการเรียน </a>
            </div>
            <div class="myProfileBox">
              <a href="<?=base_url()."MyProfile/MyMembership"?>"><img src="<?=base_url()."resources/img/membership.png"?>" class="icon-subject"/>สถานะสมาชิก </a>
            </div>
          </section>
        </div>
      </div>
    </div>

    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding mb-4">
        <?php $this->load->view('i_myProfileStatus'); ?>
        <section class="myProfile-form">
          <div class="mw-350 mx-auto" >
            <img class="w-100" src="<?=base_url('resources/img/img_addplan.png')?>">
          </div>
          <a href="<?=base_url()."MyProfile/AllPlan"?>"><button class="myPlanBtn btn-black">แผนการเรียนทั้งหมด</button></a>
          <!-- <a href="<?=base_url()."MyProfile/AddPlan"?>"><button class="myPlanBtn btn-green-white">เพิ่มแผนการเรียน</button></a> -->
          <!-- <a href="<?=base_url()."MyProfile/ChangePlan"?>"><button class="myPlanBtn btn-yellow">เปลี่ยนแผนการเรียน</button></a> -->
        </section>
      </div>
    </div> <!-- //.containner-padding -->
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script>
  $( "#profile" ).click(function() {
    $('#profileBox').toggle('slow');
  });
</script>