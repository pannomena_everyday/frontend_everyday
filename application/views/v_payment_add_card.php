<!-- css form payment -->
<!-- Payment form -->
<div id="wrap">
    <div id="container" class="bg-gray-f7">
        <div class="whiteContentBox set-full-page p-4 d-flex justify-content-center align-items-center">
            <section class="col-md-6">
                <h1 class="payment-sumary text-center mb-5">ชำระด้วยบัตรเครดิต / เดบิต</h1>
                <div class="row mb-4">
                    <div class="col-6 text-right">
                        <img src="/resources/img/payment/visa.svg" alt="VISA" width="80">
                    </div>
                    <div class="col-6 text-left"> 
                        <img src="/resources/img/payment/master.svg" alt="MASTER" width="80">
                    </div>    
                </div>        
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-xl-9 p-0 px-md-3">
                        <div class="form-group position-relative field">
                            <label for="ccnum" class="form-label text-blue">หมายเลขบัตร</label>
                            <input class="form-control" placeholder="15-16 หลัก" type="tel" size="19" name="ccnum" value="" id="ccnum">
                        </div>
                        <div class="form-group field">
                            <label for="cname" class="form-label text-blue">ชื่อเจ้าของบัตร</label>
                            <input class="form-control" placeholder="ชื่อตามที่ปรากฏบนบัตร" type="name" size="19" name="cname" value="" id="cname">
                        </div>                        
                        <div class="row">
                            <div class="col-6 form-group field">
                                <label for="expiry" class="form-label text-blue">วันหมดอายุ</label>
                                <input class="form-control" placeholder="MM / YY" type="tel" size="7" name="expiry" value="" id="expiry">
                            </div>
                            <div class="col-6 position-relative form-group cvc field">
                                <label for="cvc" class="form-label text-blue">รหัสความปลอดภัย</label>
                                <input class="form-control" placeholder="***" type="tel" size="4" name="cvc" value="" id="cvc">
                                <a href="javascript:void(0)" class="tooltip-cvc js-tooltip-cvc" data-toggle="tooltip" data-html="true" title="หมายเลข 3 หลัก ที่ด้านหลังบัตรเครดิตของคุณ"><i class="fa fa-question-circle"></i></a>
                            </div>
                        </div>
                        <div class="form-group mb-5">
                            <label for="country" class="form-label text-blue">ประเทศที่ออกบัตร</label>
                            <select class="form-control" id="country" name="country">
                                <option value="AF">Afghanistan</option> 
                                <option value="AL">Albania</option> 
                                <option value="DZ">Algeria</option> 
                                <option value="AS">American Samoa</option> 
                                <option value="AD">Andorra</option> 
                                <option value="AO">Angola</option> 
                                <option value="AI">Anguilla</option> 
                                <option value="AQ">Antarctica</option> 
                                <option value="AG">Antigua and Barbuda</option> 
                                <option value="AR">Argentina</option> 
                                <option value="AM">Armenia</option> 
                                <option value="AW">Aruba</option> 
                                <option value="AU">Australia</option> 
                                <option value="AT">Austria</option> 
                                <option value="AZ">Azerbaijan</option> 
                                <option value="BS">Bahamas</option> 
                                <option value="BH">Bahrain</option> 
                                <option value="BD">Bangladesh</option> 
                                <option value="BB">Barbados</option> 
                                <option value="BY">Belarus</option> 
                                <option value="BE">Belgium</option> 
                                <option value="BZ">Belize</option> 
                                <option value="BJ">Benin</option> 
                                <option value="BM">Bermuda</option> 
                                <option value="BT">Bhutan</option> 
                                <option value="BO">Bolivia</option> 
                                <option value="BA">Bosnia and Herzegovina</option> 
                                <option value="BW">Botswana</option> 
                                <option value="BV">Bouvet Island</option> 
                                <option value="BR">Brazil</option> 
                                <option value="IO">British Indian Ocean Territory</option> 
                                <option value="BN">Brunei Darussalam</option> 
                                <option value="BG">Bulgaria</option> 
                                <option value="BF">Burkina Faso</option> 
                                <option value="BI">Burundi</option> 
                                <option value="KH">Cambodia</option> 
                                <option value="CM">Cameroon</option> 
                                <option value="CA">Canada</option> 
                                <option value="CV">Cape Verde</option> 
                                <option value="KY">Cayman Islands</option> 
                                <option value="CF">Central African Republic</option> 
                                <option value="TD">Chad</option> 
                                <option value="CL">Chile</option> 
                                <option value="CN">China</option> 
                                <option value="CX">Christmas Island</option> 
                                <option value="CC">Cocos (Keeling) Islands</option> 
                                <option value="CO">Colombia</option> 
                                <option value="KM">Comoros</option> 
                                <option value="CG">Congo, Republic of</option> 
                                <option value="CD">Congo, The Democratic Republic of The</option> 
                                <option value="CK">Cook Islands</option> 
                                <option value="CE">Costa Rica</option> 
                                <option value="CI">Cote D'ivoire</option> 
                                <option value="HR">Croatia</option> 
                                <option value="CU">Cuba</option> 
                                <option value="CY">Cyprus</option> 
                                <option value="CZ">Czech Republic</option> 
                                <option value="DK">Denmark</option> 
                                <option value="DJ">Djibouti</option> 
                                <option value="DM">Dominica</option> 
                                <option value="DO">Dominican Republic</option> 
                                <option value="EC">Ecuador</option> 
                                <option value="EG">Egypt</option> 
                                <option value="SV">El Salvador</option> 
                                <option value="GQ">Equatorial Guinea</option> 
                                <option value="ER">Eritrea</option> 
                                <option value="EE">Estonia</option> 
                                <option value="ET">Ethiopia</option> 
                                <option value="FK">Falkland Islands (Malvinas)</option> 
                                <option value="FO">Faroe Islands</option> 
                                <option value="FJ">Fiji</option> 
                                <option value="FI">Finland</option> 
                                <option value="FE">France</option> 
                                <option value="GF">French Guiana</option> 
                                <option value="PF">French Polynesia</option> 
                                <option value="TF">French Southern Territories</option> 
                                <option value="GA">Gabon</option> 
                                <option value="GM">Gambia</option> 
                                <option value="GE">Georgia</option> 
                                <option value="DE">Germany</option> 
                                <option value="GH">Ghana</option> 
                                <option value="GI">Gibraltar</option> 
                                <option value="GR">Greece</option> 
                                <option value="GL">Greenland</option> 
                                <option value="GD">Grenada</option> 
                                <option value="GP">Guadeloupe</option> 
                                <option value="GU">Guam</option> 
                                <option value="GT">Guatemala</option> 
                                <option value="GN">Guinea</option> 
                                <option value="GW">Guinea-bissau</option> 
                                <option value="GY">Guyana</option> 
                                <option value="HT">Haiti</option> 
                                <option value="HM">Heard Island and Mcdonald Islands</option> 
                                <option value="VA">Holy See (Vatican City State)</option> 
                                <option value="HN">Honduras</option> 
                                <option value="HK">Hong Kong</option> 
                                <option value="HU">Hungary</option> 
                                <option value="IS">Iceland</option> 
                                <option value="IN">India</option> 
                                <option value="ID">Indonesia</option> 
                                <option value="IR">Iran, Islamic Republic of</option> 
                                <option value="IQ">Iraq</option> 
                                <option value="IE">Ireland</option> 
                                <option value="IL">Israel</option> 
                                <option value="IT">Italy</option> 
                                <option value="JM">Jamaica</option> 
                                <option value="JP">Japan</option> 
                                <option value="JO">Jordan</option> 
                                <option value="KZ">Kazakhstan</option> 
                                <option value="KE">Kenya</option> 
                                <option value="KI">Kiribati</option> 
                                <option value="KP">Korea, Democratic People's Republic of</option> 
                                <option value="KR">Korea, Republic of</option> 
                                <option value="KW">Kuwait</option> 
                                <option value="KG">Kyrgyzstan</option> 
                                <option value="LA">Lao People's Democratic Republic</option> 
                                <option value="LV">Latvia</option> 
                                <option value="LB">Lebanon</option> 
                                <option value="LS">Lesotho</option> 
                                <option value="LR">Liberia</option> 
                                <option value="LY">Libyan Arab Jamahiriya</option> 
                                <option value="LI">Liechtenstein</option> 
                                <option value="LT">Lithuania</option> 
                                <option value="LU">Luxembourg</option> 
                                <option value="MO">Macao</option> 
                                <option value="MK">Macedonia, The Former Yugoslav Republic of</option> 
                                <option value="MG">Madagascar</option> 
                                <option value="MW">Malawi</option> 
                                <option value="MY">Malaysia</option> 
                                <option value="MV">Maldives</option> 
                                <option value="ML">Mali</option> 
                                <option value="MT">Malta</option> 
                                <option value="MH">Marshall Islands</option> 
                                <option value="MQ">Martinique</option> 
                                <option value="MR">Mauritania</option> 
                                <option value="MU">Mauritius</option> 
                                <option value="YT">Mayotte</option> 
                                <option value="MX">Mexico</option> 
                                <option value="FM">Micronesia, Federated States of</option> 
                                <option value="MD">Moldova, Republic of</option> 
                                <option value="MC">Monaco</option> 
                                <option value="MN">Mongolia</option> 
                                <option value="MS">Montserrat</option> 
                                <option value="MA">Morocco</option> 
                                <option value="MZ">Mozambique</option> 
                                <option value="MM">Myanmar</option> 
                                <option value="NA">Namibia</option> 
                                <option value="NR">Nauru</option> 
                                <option value="NP">Nepal</option> 
                                <option value="NL">Netherlands</option> 
                                <option value="AN">Netherlands Antilles</option> 
                                <option value="NC">New Caledonia</option> 
                                <option value="NZ">New Zealand</option> 
                                <option value="NI">Nicaragua</option> 
                                <option value="NE">Niger</option> 
                                <option value="NG">Nigeria</option> 
                                <option value="NU">Niue</option> 
                                <option value="NF">Norfolk Island</option> 
                                <option value="MP">Northern Mariana Islands</option> 
                                <option value="NO">Norway</option> 
                                <option value="OM">Oman</option> 
                                <option value="PK">Pakistan</option> 
                                <option value="PW">Palau</option> 
                                <option value="PS">Palestinian Territory, Occupied</option> 
                                <option value="PA">Panama</option> 
                                <option value="PG">Papua New Guinea</option> 
                                <option value="PY">Paraguay</option> 
                                <option value="PE">Peru</option> 
                                <option value="PH">Philippines</option> 
                                <option value="PN">Pitcairn</option> 
                                <option value="PL">Poland</option> 
                                <option value="PT">Portugal</option> 
                                <option value="PR">Puerto Rico</option> 
                                <option value="QA">Qatar</option> 
                                <option value="RE">Reunion</option> 
                                <option value="RO">Romania</option> 
                                <option value="RU">Russian Federation</option> 
                                <option value="RW">Rwanda</option> 
                                <option value="SH">Saint Helena</option> 
                                <option value="KN">Saint Kitts and Nevis</option> 
                                <option value="LC">Saint Lucia</option> 
                                <option value="PM">Saint Pierre and Miquelon</option> 
                                <option value="VC">Saint Vincent and The Grenadines</option> 
                                <option value="WS">Samoa</option> 
                                <option value="SM">San Marino</option> 
                                <option value="ST">Sao Tome and Principe</option> 
                                <option value="SA">Saudi Arabia</option> 
                                <option value="SN">Senegal</option> 
                                <option value="CS">Serbia and Montenegro</option> 
                                <option value="SC">Seychelles</option> 
                                <option value="SL">Sierra Leone</option> 
                                <option value="SG">Singapore</option> 
                                <option value="SK">Slovakia</option> 
                                <option value="SI">Slovenia</option> 
                                <option value="SB">Solomon Islands</option> 
                                <option value="SO">Somalia</option> 
                                <option value="ZA">South Africa</option> 
                                <option value="GS">South Georgia and The South Sandwich Islands</option> 
                                <option value="ES">Spain</option> 
                                <option value="LK">Sri Lanka</option> 
                                <option value="SD">Sudan</option> 
                                <option value="SR">Suriname</option> 
                                <option value="SJ">Svalbard and Jan Mayen</option> 
                                <option value="SZ">Swaziland</option> 
                                <option value="SE">Sweden</option> 
                                <option value="CH">Switzerland</option> 
                                <option value="SY">Syrian Arab Republic</option> 
                                <option value="TW">Taiwan, Province of China</option> 
                                <option value="TJ">Tajikistan</option> 
                                <option value="TZ">Tanzania, United Republic of</option> 
                                <option value="TH" selected>Thailand</option> 
                                <option value="TL">Timor-leste</option> 
                                <option value="TG">Togo</option> 
                                <option value="TK">Tokelau</option> 
                                <option value="TO">Tonga</option> 
                                <option value="TT">Trinidad and Tobago</option> 
                                <option value="Tunisia">TN</option> 
                                <option value="TR">Turkey</option> 
                                <option value="TM">Turkmenistan</option> 
                                <option value="TC">Turks and Caicos Islands</option> 
                                <option value="TV">Tuvalu</option> 
                                <option value="UG">Uganda</option> 
                                <option value="UA">Ukraine</option> 
                                <option value="UA">United Arab Emirates</option> 
                                <option value="UK">United Kingdom</option> 
                                <option value="US">United States</option> 
                                <option value="UM">United States Minor Outlying Islands</option> 
                                <option value="UY">Uruguay</option> 
                                <option value="UZ">Uzbekistan</option> 
                                <option value="VU">Vanuatu</option> 
                                <option value="VE">Venezuela</option> 
                                <option value="VN">Viet Nam</option> 
                                <option value="VG">Virgin Islands, British</option> 
                                <option value="VI">Virgin Islands, U.S.</option> 
                                <option value="WF">Wallis and Futuna</option> 
                                <option value="EH">Western Sahara</option> 
                                <option value="YE">Yemen</option> 
                                <option value="ZM">Zambia</option> 
                                <option value="ZW">Zimbabwe</option>
                            </select>   
                        </div>                       
                        <div class="row justify-content-center mb-5">
                            <div class="col-sm-4 mb-4 mb-sm-0">              
                                <a href="<?= base_url("Payment?subscriber_id=$subscription_id&promotion=$promotion_code")?>"><button class="col btn-lg rounded-pill btn-gray" type="button">กลับ</button></a>
                            </div>
                            <div class="col-sm-8">              
                                <button class="col btn-lg rounded-pill btn-black" type="button" id="submit">ชำระเงิน <?= @number_format($amount, 2) ?> บาท</button>
                            </div>
                            <!-- <button class="col-sm-9 btn-lg rounded-pill btn-black" type="button" id="submit">ชำระเงิน <?= @number_format($amount, 2) ?> บาท</button> -->
                        </div>                        
                        <div class="row justify-content-center">                            
                            <small class="d-inline-block mr-1 text-muted">Secured by</small> <img src="/resources/img/payment/logo_omise.svg" alt="omise" width="80">
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<form id="checkoutForm" method="POST" action="<?=base_url('payment/create')?>">      
    <input type="hidden" name="promotion_code" value=<?= $promotion_code ?>>
    <input type="hidden" name="subscription_id" value="<?=$subscription_id?>">
    <input type="hidden" name="amount" value="<?= $amount ?> ">
    <input type="hidden" name="omiseToken">
</form>
<!-- for payment form -->
<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/js/payform.min.js"></script>
<script type="text/javascript" src="https://cdn.omise.co/omise.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script>
  Omise.setPublicKey("<?= OMISE_PUBLIC_KEY ?>");
</script>
<script>
// Medthod Creditcard
$(function() {
    $('.js-tooltip-cvc').tooltip();
});
$(function() {
    var ccnum   = document.getElementById('ccnum'),
        cname   = document.getElementById('cname'),
        type    = document.getElementById('ccnum-type'),
        expiry  = document.getElementById('expiry'),
        cvc     = document.getElementById('cvc'),
        submit  = document.getElementById('submit'),
        result  = document.getElementById('result');
        country  = document.getElementById('country');
        form = document.getElementById("checkoutForm");
        

    payform.cardNumberInput(ccnum);
    payform.expiryInput(expiry);
    payform.cvcInput(cvc);

    ccnum.addEventListener('input', updateType);

    ccnum.addEventListener('change', function() {
        var validC = []
        validC.push(fieldStatus(ccnum, payform.validateCardNumber(ccnum.value)));
        if (validC.every(Boolean)) {
            console.log('input payment'); //for action payment
        } else {
            console.log('piput Invalid');
        }
    });

    expiry.addEventListener('change', function() {
        var validE = [],
            expiryObjE = payform.parseCardExpiry(expiry.value);
        
        validE.push(fieldStatus(expiry, payform.validateCardNumber(expiryObjE)));

        if (validE.every(Boolean)) {
            console.log('expiry payment ',expiryObjE);
        } else {
            console.log('expiry Invalid ',expiryObjE);
        }
    });

    submit.addEventListener('click', function() {
        var valid = [],
            expiryObj = payform.parseCardExpiry(expiry.value);

        valid.push(fieldStatus(ccnum, payform.validateCardNumber(ccnum.value)));
        valid.push(fieldStatus(cname, cname.value));
        valid.push(fieldStatus(expiry, payform.validateCardExpiry(expiryObj)));
        valid.push(fieldStatus(cvc, payform.validateCardCVC(cvc.value)));

        if (valid.every(Boolean)) {
            createCardToken(ccnum.value, cname.value, expiryObj, cvc.value, country.value)
            
        } else {
            alert('Invalid Card');
            // console.log('Invalid');
        }
        // result.className = 'emoji ' + (valid.every(Boolean) ? 'valid' : 'invalid');
    });

    function updateType(e) {
        var cardType = payform.parseCardType(e.target.value);
        
        // Check card type
        if(cardType){
            addClass(ccnum.parentNode, cardType);
        }else{
            removeAllCardType(ccnum);
        }
        // type.innerHTML = cardType || 'invalid';
    }

    function removeAllCardType(input) {
        for (const card of payform.cards) {
            removeClass(input.parentNode, card.type);
        }
    }
    
    function fieldStatus(input, valid) {
        if (valid) {
            removeClass(input.parentNode, 'c-error');
        } else {
            addClass(input.parentNode, 'c-error');
        }
        return valid;
    }

    function addClass(ele, _class) {
        if (ele.className.indexOf(_class) === -1) {
            ele.className += ' ' + _class;
        }
    }

    function removeClass(ele, _class) {
        if (ele.className.indexOf(_class) !== -1) {
            ele.className = ele.className.replace(_class, '');
        }
    }
});

function createCardToken(card_num, card_name, expiry_obj, cvc_no, contry_code){
        tokenParameters = {
            "country": contry_code,
            "expiration_month": expiry_obj.month,
            "expiration_year": expiry_obj.year,
            "name": card_name,
            "number": card_num,
            "security_code": cvc_no
            };

        Omise.createToken("card",
            tokenParameters,
            function(statusCode, response) {
                if(statusCode != 200){
                    alert('ERROR')
                }else{
                    form.omiseToken.value = response['id']
                    form.submit();
                }

        // response["id"] is token identifier

        console.log(response)
        });
    }
</script>
