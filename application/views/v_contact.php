<script src='https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css' rel='stylesheet' />
<div id="wrap">
  <div id="container">
    <div class="whiteContentBox">
      <div id="contactGoogleMap">
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="contactHeader f30">ติดต่อเรา</div>
          <div class="contacDetail f20">
            บริษัท มังกี้เอเวอรี่เดย์ จํากัด <br/>
            444 ชั้น 5 อาคารเอ็ม บี เค เซ็นเตอร์ <br/>
            ตําแหน่งสถานที่เช่า PLA.F05.D001000 <br/>
            ถนนพญาไท แขวงวังใหม่ เขตปทุมวัน <br/>
            กรุงเทพมหานคร 10330 <br/>
            โทร : 02 288 0493 <br/>
            อีเมล : support@monkeyeveryday.com <br/>
            <div class="my-3">
              <a target="_blank" href="https://www.facebook.com/MonkeyEverydayOfficial"><img class="mr-3 my-2" src="<?=base_url();?>resources/img/icon_social/contact_facebook.png" /></a>
              <a target="_blank" href="https://www.twitter.com/monkeyeveryday"><img class="mr-3 my-2" src="<?=base_url();?>resources/img/icon_social/contact_twitter.png" /></a>
              <a target="_blank" href="https://www.instagram.com/monkey_everyday_official"><img class="mr-3 my-2" src="<?=base_url();?>resources/img/icon_social/contact_instargram.png" /></a>
              <a target="_blank" href="https://www.youtube.com/channel/UC72k8jnlOUgjuGcMsrZHtAg"><img class="my-2" src="<?=base_url();?>resources/img/icon_social/contact_youtube.png" /></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->
<script>
// TO MAKE THE MAP APPEAR YOU MUST
// ADD YOUR ACCESS TOKEN FROM
// https://account.mapbox.com
mapboxgl.accessToken = 'pk.eyJ1IjoicG9wYmJiaW53IiwiYSI6ImNrajN6dzFibTB1eHQyc3BrNnR2cjRqZHcifQ.tfF00HGFkUxdg30aUD3iUg';
var map = new mapboxgl.Map({
container: 'contactGoogleMap',
style: 'mapbox://styles/popbbbinw/ckj6yrtlaexka19mh6t5oeh43', // stylesheet location
center: [100.52993990406492,13.744952200723915], // starting position [lng, lat]
zoom: 18 // starting zoom
});
var marker = new mapboxgl.Marker()
.setLngLat([100.52993990406492,13.744952200723915])
.addTo(map);
</script>