<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<link rel="stylesheet" href="<?=base_url('resources/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css');?>">
<script type="module" src="<?=base_url();?>resources/js/everyday-player.bundled.js"></script>
<link rel="stylesheet" koko="in" href="<?=base_url();?>resources/css/tutorial.min.css">
<style>
  #step-1 .btn-next {
    /*---- hide step end ------*/
      display: none;
  }
  #step-1 .popover-navigation {
      -ms-flex-pack: center !important;
      justify-content: center !important;
  }
  .step-2{
    z-index:1101 !important;
  }
  .step-2 .view, .step-2 .date, .step-2 .wrapper-video, .step-2 .owl-nav{
    opacity: 0.1;
  }
</style>
<div id="wrap">
  <div id="container" class="bg-black">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="startPlanInfo js-restart-tour">
          <div class="subHeader-info tour-info"><img src="<?=base_url()."resources/img/information.png"?>" /></div>
        </div>        
        <div class="vid-uploadSheetGuest <?=($page_active == 'myEveryDay' ? '' : '')?>">
          <img src="<?=base_url();?>resources/img/upload_guest.jpg"/>
          <p class="f-a9">อัปโหลดชีท</p>
        </div>
        <input class="sheet-upload" type="file"/>

        <div class="d-flex justify-content-between">
          <div class="align-self-center vid-prev">
            <?=$prev?>
          </div>
          <div class="vid-header center">
            <div class="vid-h1"><?=$sheet->title?> : <?=$sheet->sheetCode?> | <?=count($vdos)?> วีดีโอ</div>
            <a href="<?=base_url("Membertype")?>"><button class="vid-Btn btn-green-white">ดาวน์โหลด Sheet</button></a>
          </div>
          <div class="align-self-center vid-next">
            <?=$next?>
          </div>
        </div>
      </div>
    </div>

    <div style="width: 100%; text-align: center; /*height: 700px; background: #ccc;*/">
      <div class="vid-h2 txt"></div>
        <div class="owl-carousel owl-theme" id="playVideo">
          <?php
          $no = 0;
          foreach ($vdos as $vdo):
            $link = getVideo($api_url,$vdo->gcsName);
          ?>
            <div class="item">
              <div class="vid-h2 d-none">VIDEO <?=++$no?>/<?=count($vdos)?> : <?=$vdo->title?></div>
              <div class="wrapper-video" style="width: 100%;">
                <everyday-player
                  nid="<?=$myProfile->mobile?>"
                  stdName="<?=$myProfile->firstname?> <?=$myProfile->lastname?>"
                  src="<?=$link[0]?>"
                  name="<?=$vdo->title?>"
                  imageUrl="<?=$vdo->imgUrl?>"
                  code="<?=$sheet->sheetCode?>"
                  tag="everyday"
                  contentId="content_<?=$vdo->order_item?>"
                  resumePlayback="<?=$vdo->progress == null ? 0 : $vdo->progress?>"
                  profile_id=<?=$myProfile->id?>
                  vdo_id=<?=$vdo->sheetVideoId?>
                  >
                </everyday-player>
              </div>
              <div class="vid-detail-box">
                <div class="vid-detail d2 view" data-vid='<?=$vdo->sheetVideoId?>'></div>
                <div class="vid-detail d3 date"><i class="fa fa-calendar-o" aria-hidden="true"></i> <?=DateThai($vdo->createdtime)?></div>
                <div class="vid-detail vidLike d4" id="vidLike" data-vid='<?=$vdo->sheetVideoId?>'></div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
        <!-- Video player parameter description -->
        <!-- src = video url
        name = video name
        nid = รหัสบัตรประชาชนของนักเรียน
        stdName = ชื่อนักเรียน
        imageUrl = url ของปกคลิป video
        code = sheet code เช่น MK-A-B-1-1
        tag = ใน staging ใช้ test-everyday ใน production ใช้ everyday
        contentId = uniqKey for player
        resumePlayback(optional) = เวลาเริ่มต้นของ player (วินาที)
        มีการ return attribute ของ element 3 นะครับคือ
        1. progress คือ % ที่เล่นถึง (0-100)
        2. currentTime คือ เวลาที่เล่นอยู่ในปัจจุบัน (วินาที)
        3. duration คือ ความยาวของ video (วินาที) -->
    </div>

    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="vid-btn-div">
          <a href="<?=base_url("Membertype")?>"><button class="vid-Btn1 btn-gray">ตรวจคำตอบ</button></a>
        </div>
      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<?php
function getVideo($api_url, $videoName){
  $headers = array("accept: application/json",'Content-Type: application/json');
  $method = 'GET';
  $url = $api_url.'gen-signed-url/'.$videoName;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
  $response = curl_exec($curl);
  curl_close($curl);
  return json_decode($response);
}
function DateThai($strDate){
  $strDate = date("Y-m-d H:i:s", strtotime('+7 hours', strtotime($strDate)));
  $strYear = date("Y",strtotime($strDate))+543;
  $strMonth= date("n",strtotime($strDate));
  $strDay= date("j",strtotime($strDate));
  $strHour= date("H",strtotime($strDate));
  $strMinute= date("i",strtotime($strDate));
  $strSeconds= date("s",strtotime($strDate));
  $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
  $strMonthThai=$strMonthCut[$strMonth];
  return "$strDay $strMonthThai $strYear";
}

?>
<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.11.0/js/bootstrap-tour.js"></script>

<script>
  var memberId = '<?=$myProfile->id?>';
  $(window).on('load', function () {
    getVideoView();
    getVideoLikeHtml();
    for(let i=0; i < document.getElementsByTagName("everyday-player").length ; i++){
      document.getElementsByTagName("everyday-player")[i].videoController.video.addEventListener('play', () => updateProgressOnPlay(i));
    } 

    var owl = $('.owl-carousel');
    //get init owl Carousel for set tutorial
    owl.on('initialized.owl.carousel', function(event){
      $('.js-restart-tour').on('click', function(e){
        e.preventDefault();
        tour.restart();
      }); 

      var tour = new Tour({
          name: 'guest-video-tutor',
          backdrop: true,
          keyboard: false,
          autoscroll: false,
          template: '<div class="startPlan text-light popover tour tutorial-step"> <p class="popover-content mb-2"></p> <div class="d-flex justify-content-between popover-navigation"> <button class="btn btn-skip" data-role="end">ปิด <i class="fa fa-times-circle"></i></button> <button class="btn btn-next" data-role="next">ถัดไป <i class="fa fa-chevron-circle-right"></i></button></div></div>',
          onEnd: function(){
            $('#playVideo').removeClass('step-2');
            $('html,body').animate({ scrollTop: $('body').offset().top}, 'slow');
          },         
          steps: [{
              element: "#playVideo",
              content: "1. ดูวีดีโอ",
              placement: 'top',
              onNext: function(){
                $('#playVideo').addClass('step-2');
                $('html,body').animate({ scrollTop: $('#vidLike').offset().top}, 'slow');                
              }               
            },  
            {
              element: "#vidLike",
              content: "2. ปุ่มแสดงความคิดเห็น ในความพึงพอใจในวีดีโอนี้<br>เพื่อนำไปปรับปรุงแก้ไข",
              placement: function(context, source) {
                var position = $(source).position();
                if (position.left >= 40) {
                  return "auto top";
                }
                if (position.left < 40 ) {
                  return "left";
                }
              }
            }
          ]
        });

        // Start the tour
        setTimeout(function() {
          tour.init()
          tour.start();
          //check current step for setp 2
          if(!tour.ended() && tour.getCurrentStep() == 1){
            var getStepVideo = tour.getStep(1);
            console.log(getStepVideo);
            if (typeof(getStepVideo) != 'undefined' && getStepVideo.id === 'step-1'){
              $('#playVideo').addClass('step-2'); //Add Class step-2 for set z-index
            }
          }
        }, 1500);
    });  


    //Init owl Carousel
    owl.owlCarousel({
      stagePadding: 100,
      items:1,
      center: true,
      mouseDrag: false,
      touchDrag: false,
      margin:5,
      dots: false,
      nav: true,
      navText: ["<i class='mx-2 f30 fa fa-angle-left'></i>","<i class='mx-2 f30 fa fa-angle-right'></i>"],
      responsive:{
        0:{stagePadding: 5}
        ,575:{stagePadding: 50}
        ,1024:{stagePadding: 200}
        ,1200:{stagePadding: 300}
        ,1600:{stagePadding: 400}
      }
    });
    owl.on('click', '.owl-item', function(event) {
        var target = jQuery(this).index();
        owl.trigger('to.owl.carousel', target);
    });

    owl.on('translated.owl.carousel', function(event) {
      for(i=0; i < document.getElementsByTagName("everyday-player").length ; i++){
        document.getElementsByTagName("everyday-player")[i].videoController.video.pause()
      }
      var txt = $('.owl-item.center .vid-h2').html();
      $('.vid-h2.txt').html(txt);
      $('.owl-item').removeClass('owlBlur');
      $('.owl-item').not(".center").addClass('owlBlur');
    });

    var txt2 = $('.owl-item.center .vid-h2').html();
    $('.vid-h2.txt').html(txt2);
    $('.owl-item').removeClass('owlBlur');
    $('.owl-item').not(".center").addClass('owlBlur');

    setInterval(updateProgress, 10000);

  });

  function clickLike(sheetVideoId,val){
    var opts = {
      method: 'POST',
      headers: {
          "Content-Type":"application/json",
          "Authorization": "Bearer <?=$token;?>"
      },
      body: JSON.stringify({
      "sheetVideoId": parseInt(sheetVideoId),
      "action": parseInt(val)
      })
    };
    fetch('<?=$this->config->item('api_url').'front-end/video-review';?>', opts).then(function (response) {
      response.json().then(function (result) {
        if(response.ok){
          getVideoLikeHtml();
        }
      });
    });
  };

  function getVideoLikeHtml(){
    $( ".vidLike" ).each(function( index ) {
      var thisVidLike = $(this);
      var sheetVideoId = $(this).data('vid');
      //console.log( index + ": " + sheetVideoId);
      $.ajax({
        type: "POST",
        url: "<?=base_url()."MyEveryDay/getLikeHTML";?>",
        data: {memberId:memberId,sheetVideoId:sheetVideoId},
        success: function (ret) {
          var obj = jQuery.parseJSON(ret);
          thisVidLike.html(obj.html);
        }
      });
    });
  }

  function getVideoView(){
    $( ".vid-detail.d2" ).each(function( index ) {
      var thisVidLike = $(this);
      var sheetVideoId = $(this).data('vid');
      //console.log( index + ": " + sheetVideoId);
      $.ajax({
        type: "POST",
        url: "<?=base_url()."MyEveryDay/getVideoView";?>",
        data: {sheetVideoId:sheetVideoId},
        success: function (ret) {
          var obj = jQuery.parseJSON(ret);
          thisVidLike.html(obj.html);
        }
      });
    });
  }

  
  var video_progress_send = []
  function updateProgress(){
    let list_progress = []
    $('everyday-player').each(function(){
      let el = $(this)
      let obj = {
        currentTime: Number.parseInt(el.attr('currentTime')),
        progress: Number.parseInt(el.attr('progress')),
        resumePlayback: Number.parseInt(el.attr('resumePlayback')),
        profile_id: Number.parseInt(el.attr('profile_id')),
        vdo_id: Number.parseInt(el.attr('vdo_id'))
      }

      if(obj.resumePlayback < obj.currentTime ){
        exists_vdo = video_progress_send.find(v => v.vdo_id == obj.vdo_id)
        if(exists_vdo){
          if(exists_vdo.currentTime < obj.currentTime){
            list_progress.push(obj)
            let idx = video_progress_send.findIndex(v => v.vdo_id == obj.vdo_id)
            video_progress_send[idx] = obj
          }
        }else{
          video_progress_send.push(obj)
        }
      }
    })

    if(list_progress.length > 0){
      $.post("<?=base_url()."MyEveryDay/UpdateProgress";?>", {list_progress})
        .done(res => console.log(res))
    }
  }

  function updateProgressOnPlay(idx){
    // console.log(this)
    let list_progress = []
    var count = -1
    $('everyday-player').each(function(){
      count += 1
      if(count != idx){
        return true;
      }
      let el = $(this)
      let obj = {
        currentTime: Number.parseInt(el.attr('currentTime')),
        progress: Number.parseInt(el.attr('progress')),
        resumePlayback: Number.parseInt(el.attr('resumePlayback')),
        profile_id: Number.parseInt(el.attr('profile_id')),
        vdo_id: Number.parseInt(el.attr('vdo_id'))
      }

      if(obj.resumePlayback == 0){
        list_progress.push(obj)
      }
    })

    if(list_progress.length > 0){
      $.post("<?=base_url()."MyEveryDay/UpdateProgress";?>", {list_progress})
        .done(res => console.log(res))
    }
    updateVideoView(idx)
  }
  var vdo_already_play = []
  function updateVideoView(idx){
    // console.log(el)
    let vdo_id = document.getElementsByTagName("everyday-player")[idx].getAttribute('vdo_id')
    if(!vdo_already_play.includes(vdo_id)){
      vdo_already_play.push(vdo_id)
      $.post("<?=base_url()."MyEveryDay/UpdateView";?>", {vdo_id})
    }
  }
</script>
