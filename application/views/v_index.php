<?php
(get_cookie('token') != null && !isset($isLogout))? $isLogin = true: $isLogin = false;
?>
<link rel="stylesheet" href="<?=base_url('resources/slick-master/slick/slick.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/slick-master/slick/slick-theme.css');?>">
<!-- Owl Stylesheets -->
<link rel="stylesheet" href="<?=base_url('resources/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css');?>">
<link rel="stylesheet" href="<?=base_url('resources/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css');?>">

<div id="wrap">
  <div id="container" class="bg-white">
<!--
  <div class="header_img" style="background-image: url(<?=base_url()."resources/img/index_img.png"?>); ">
    <div class="containner-1200 mx-auto h-100 d-flex align-items-center">
      <div class="containner-1200-padding">
        <div class="header_img_text rainbow-text index-gradient-box">DO IT</div>
        <div class="header_img_text rainbow-text index-gradient-box">EVERYDAY</div>
        <div class="header_img_detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.</div>
        <div class="header_registerBtn">ลงทะเบียน</div>
      </div>
    </div>
  </div>
 -->
   <div class="mySection">
    <div class="containner-1920 mx-auto">
      <div class="owl-carousel owl-H-Banner owl-theme">
        <?php
          usort($dataBanners, function($a, $b) {
            return $a->order_item <=> $b->order_item;
          });
          foreach($dataBanners as $row){
            if($row->is_active === true){
              if($row->imgUrlMobile == ''){
                $row->imgUrlMobile = $row->imgUrl;
              }
              if($row->imgUrlTablet == ''){
                $row->imgUrlTablet = $row->imgUrl;
              }
              if($row->link != ''){
                echo '<div class="item">
                  <a href="'.$row->link.'">
                    <img class="imgBannerRotate" src="'.$row->imgUrl.'" data-desktop="'.$row->imgUrl.'" data-tablet="'.$row->imgUrlTablet.'" data-mobile="'.$row->imgUrlMobile.'" />
                  </a>
                </div>';
              }else{
                echo '<div class="item">
                  <img class="imgBannerRotate" src="'.$row->imgUrl.'" data-desktop="'.$row->imgUrl.'" data-tablet="'.$row->imgUrlTablet.'" data-mobile="'.$row->imgUrlMobile.'" />
                </div>';
              }
            }
          }
        ?>
      </div> <!-- owl-carousel -->
    </div>
  </div> <!-- mySection -->

  <div class="div-btn-home my-5 center">
    <a href="https://www.youtube.com/watch?v=agfSouXkmHQ" target="_blank">
      <button class="btn-custom btn-purple-yellow m-3 f26 btn-home"><i class="fa fa-play-circle-o" aria-hidden="true"></i> ดูวิธีการใช้งาน</button>
    </a>
    <a href="<?=($isLogin ? base_url("Membertype") : base_url("Index/SignUp"))?>"><button class="btn-custom btn-yellow-purple m-3 f26 btn-home">ทดลองเรียนฟรี 7 วัน</button></a>
  </div>

  <div class="mySection">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="index-header-slide"><h2>อัปเดตล่าสุด</h2></div>
        <section class="index-slide regular slider">
          <?php foreach ($last_update as $lasted):?>
            <?php $date = DateTime::createFromFormat('Y-m-d H:i:s', $lasted->updatedtime);?>
            <div>
              <a href="<?=($isLogin ? base_url('Library/SubList/'.$lasted->libraryLevelId.'?subject='.$lasted->subjectId): base_url('Index/SignIn'))?>">
                <img src="<?=$lasted->imgUrl?>">
                <p class="libraryTitle text-ellipsis m-lib"><?=$lasted->title?></p>
                <span class="f14 text-gray"><i class="fa fa-calendar"></i> <?=date_format($date, 'd M Y')?></span>
              </a>
            </div>
          <?php endforeach; ?>
        </section>
      </div>
    </div>
  </div>
  <!-- .mySection -->
  <div class="mySection">
    <div class="containner-1920 mx-auto">
        <div class="">
          <img class="w-100 imgRotate" src="<?=base_url()."resources/img/index/Main_Content_Desktop_01.jpg"?>" data-desktop="Main_Content_Desktop_01.jpg" data-tablet="Main_Content_Tablet_01.jpg" data-mobile="Main_Content_Mobile_01.jpg" />
        </div>
    </div>
  </div>

  <div class="div-btn-home my-5 center">
    <a href="https://www.youtube.com/watch?v=agfSouXkmHQ" target="_blank">
      <button class="btn-custom btn-purple-yellow m-3 f26 btn-home"><i class="fa fa-play-circle-o" aria-hidden="true"></i> ดูวิธีการใช้งาน</button>
    </a>
    <a href="<?=($isLogin ? base_url("Membertype") : base_url("Index/SignUp"))?>"><button class="btn-custom btn-yellow-purple m-3 f26 btn-home">ทดลองเรียนฟรี 7 วัน</button></a>
  </div>

  <!-- mySection -->
  <div class="mySection index-section-2 bg-<?=$dayOfWeek;?>">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="section-2"><h2>เนื้อหายอดนิยม</h2></div>
        <div class="row">
          <?php $index = 0; foreach ($populates as $populate):?>
          <div class="col-6 col-md-4 m-b-30">
            <a href="<?=($isLogin ? base_url('Library/SubList/'.$populate->libraryLevelId.'?subject='.$populate->subjectId): base_url('Index/SignIn'))?>">
              <div class="row top-content">
                <div class="d-flex">
                  <div class="number f-white align-self-start"><?=++$index?></div>
                  <div class="flex-grow-1">
                    <img src="<?=$populate->imgUrl?>" class="cover">
                    <p class="libraryTitle m-lib f-white text-left"><?=$populate->title?></p>
                  </div>
                </div>
              </div>
            </a>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
  <!-- .mySection.section-2 -->

<!--   <div class="mySection section-3">
    <div class="containner-1200 w-100 mx-auto">
      <div class="containner-1200-padding d-flex justify-content-center flex-wrap">
        <div class="section-3-box">
          <div><img src="<?=base_url('resources/img/career.png')?>" class="logo-career-promo"> ร่วมงานกับเรา</div>
        </div>
        <div class="section-3-box">
          <div><img src="<?=base_url('resources/img/promotion.png')?>" class="logo-career-promo"> แพ็คเกจราคาพิเศษสำหรับโรงเรียน</div>
        </div>
      </div>
    </div>
  </div> --> <!-- //.section-3 -->
  <?php
    $countItem = 0;
    foreach($dataTestimonials as $row){
      if($row->is_active === true){
        $countItem += 1;
      }
    }
    if($countItem > 0){
  ?>
  <div class="mySection section-4">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="index-header-section-4"><h2>รีวิว</h2></div>
        <div class="slide-sec4-padding">
          <section class="index-slide slide-sec4 slider">
            <?php
            usort($dataTestimonials, function($a, $b) {
              return $a->order_item <=> $b->order_item;
            });
            foreach($dataTestimonials as $row){
              if($row->is_active === true){
                echo '<div class="sec4box">
                  <div class="d-flex flex-column flex-md-row flex-md-nowrap justify-content-center justify-content-md-start">
                      <div class="sec4Img align-self-center"><img src="'.$row->imgUrl.'"></div>
                      <div class="sec4Content flex-grow-1 d-flex flex-column align-items-start justify-content-center">
                        <div>'.$row->remark.'</div>
                        <div class="bold">'.$row->name.'</div>
                      </div>
                  </div>
                </div>';
              }
            }
            ?>
          </section>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>

  <!-- //.section-4 -->
  <div class="mySection section-5 px-0 px-sm-1 d-flex justify-content-center">
          <div class="row col-md-10 col-lg-9 m-w-1100">
            <div class="col-md-4 mb-4 mb-md-0">
              <p>บริษัท มังกี้เอเวอรี่เดย์ จํากัด<br/>
                (สำนักงานใหญ่)<br/>
                <span>
                  5 ซอยลาดพร้าว 101 ซอย 45 <br/>
                  แขวงคลองจั่น เขตบางกะปิ <br/>
                  กรุงเทพมหานคร 10240<br/>
                  โทร : <a class="mail" href="tel:0809974144">080 997 4144</a>
                </span>
              </p>
            </div>
            <div class="col-md-4 mb-4">
              <p>
                <span>
                  444 ชั้น 5 อาคารเอ็ม บี เค เซ็นเตอร์<br/>
                  ตำแหน่งสถานที่เช่า PLA.F05.D001000<br/>
                  ถนนพญาไท แขวงวังใหม่ เขตปทุมวัน<br/>
                  กรุงเทพมหานคร 10330<br/>
                  โทร : <a class="mail" href="tel:022880493">02 288 0493</a> <br/>
                  อีเมล : <a href="mailto:support@monkeyeveryday.com" target="_blank" class="mail">support@monkeyeveryday.com</a>
                </span>
              </p>
            </div>
            <div class="col-md-4">
              <p>ติดต่อเรา</p>
              <div class="my-3">
                <a target="_blank" href="https://www.facebook.com/MonkeyEverydayOfficial"><img class="mr-3 my-2" src="<?=base_url();?>resources/img/icon_social/footer_facebook.png" /></a>
                <a target="_blank" href="https://www.twitter.com/monkeyeveryday"><img class="mr-3 my-2" src="<?=base_url();?>resources/img/icon_social/footer_twitter.png" /></a>
                <a target="_blank" href="https://www.instagram.com/monkey_everyday_official"><img class="mr-3 my-2" src="<?=base_url();?>resources/img/icon_social/footer_instargram.png" /></a>
                <a target="_blank" href="https://www.youtube.com/channel/UC72k8jnlOUgjuGcMsrZHtAg"><img class="my-2" src="<?=base_url();?>resources/img/icon_social/footer_youtube.png" /></a>
              </div>
            </div>
          </div>
      </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->
<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url('resources/slick-master/slick/slick.min.js');?>"></script>

<script  type="text/javascript">
  $(document).ready(function() {
    hBanner();
    getImgByScreenSize();
  });

  $( window ).resize(function() {
    getImgByScreenSize();
  });

  function getImgByScreenSize(){
    $('.imgBannerRotate').each(function(){
        var desktop = $(this).data( "desktop" );
        var tablet = $(this).data( "tablet" );
        var mobile = $(this).data( "mobile" );
        if ($( window ).width() <= 414) {
          $(this).attr("src",mobile);
        } else if ($( window ).width() <= 1024) {
          $(this).attr("src",tablet);
        } else{
          $(this).attr("src",desktop);
        }
    });

    $('.imgRotate').each(function(){
        var desktop = '<?=base_url("resources/img/index/");?>'+$(this).data( "desktop" );
        var tablet = '<?=base_url("resources/img/index/");?>'+$(this).data( "tablet" );
        var mobile = '<?=base_url("resources/img/index/");?>'+$(this).data( "mobile" );
        if ($( window ).width() <= 414) {
          $(this).attr("src",mobile);
        } else if ($( window ).width() <= 1024) {
          $(this).attr("src",tablet);
        } else{
          $(this).attr("src",desktop);
        }
    });
  }

  function hBanner() {
    var owl = $('.owl-H-Banner').owlCarousel({
            items: 1,
            margin: 10,
            autoHeight: true,
            dots: false,
            autoplay: true,
            loop: true
    });
    
    jQuery(document.documentElement).keyup(function (event) {    
      // handle cursor keys
      if (event.keyCode == 37) {
        owl.trigger('prev.owl.carousel');
      } else if (event.keyCode == 39) {
        owl.trigger('next.owl.carousel');
      }
    });
  }

  $(".regular").slick({
    slidesToShow: 4.5,
    slidesToScroll: 4,
    dots: false,
    infinite: false,
    //arrows: false,
    responsive: [
    {
      breakpoint: 1025,
      settings: {
        arrows: false, slidesToShow: 4.5, slidesToScroll: 4
      }
    },
    {
      breakpoint: 769,
      settings: {
        arrows: false, slidesToShow: 3.5, slidesToScroll: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false, slidesToShow: 2.5, slidesToScroll: 2
      }
    }
    ]
  });

  $(".slide-sec4").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    infinite: true
  });
</script>
