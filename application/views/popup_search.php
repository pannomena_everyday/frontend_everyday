<style>
.input-search{display: none;}
</style>
<div id="wrap" class="bg-black">
  <div id="container" class="bg-black">
    <div class="containner-1200 mx-auto">
      <div class="containner-1200-padding">
        <div class="searchHeader">
          <div class="input-group col-md-12 gradient-box">
            <!-- <input class="form-control py-2 border-0 bg-black" placeholder="" value="<?=$keyword?>" type="search"> -->

            <input id="search2" value="<?=$searchResult['keyword']?>" type="search" onsearch="clearSearch()" aria-haspopup="true" aria-expanded="false" autocomplete="off" class="form-control inp-search py-2 border-0" placeholder="ค้นหาบทเรียน">
            <div id="DropdownSearch2" class="txtsearch2 dropdown-menu" aria-labelledby="search"></div>
            <span class="input-group-append">
              <button class="btn-search2 btn btn-outline-secondary border-0 text-white" type="button">
                <i class="fa fa-search"></i>
              </button>
            </span>
          </div>
        </div>

        <div class="searchMenu">
          <div class="row">
            <div class="col-md-6 p-2 mb-1">
              <select id="subject" class="form-control custom-select form-black">
                <?=$searchResult['dataSubject']?>
              </select>
            </div>
            <div class="col-md-6 p-2 mb-1">
              <select id="library" class="form-control custom-select form-black">
                <option value="" selected>เลือก ระดับชั้น / เนื้อหา</option>
              </select>
            </div>
          </div>

          <div class="search-menu-list">
          	<div class="d-flex justify-content-center flex-wrap">
              <?php if($myProfile->subscriptionTypeId == 'disable'){ ?>
                <div class="sheets-list ls px-5 gradient-box" data-id="#search-sheet"><span class="libTotal"><?=$searchResult['libResult'];?></span> หัวข้อ</div>
              <?php }else{ ?>
                <div class="videos-list ls px-5 gradient-box" data-id="#search-video"><span class="videoTotal"><?=$searchResult['videoResult'];?></span> วีดีโอ</div>
                <div class="sheets-list ls px-5" data-id="#search-sheet"><span class="libTotal"><?=$searchResult['libResult'];?></span> หัวข้อ</div>
              <?php } ?>
            </div>
          </div>
        </div>

        <div class="searchResult">
        <?=$searchResult['data']?>
        </div>

        <div class="modal fade" id="playVideo" tabindex="-1" role="dialog" aria-labelledby="vidLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            </div>
          </div>
        </div>

      </div>
    </div>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<?php

?>

<style>
 iframe body{margin: 0;}
</style>

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script>
  var searchKeyword = '<?=$searchKeyword?>';
  $(document).ready(function() {
    $('#playVideo').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var vid = button.data('vid');
      var modal = $(this);
      var url = '<iframe class="w-100 mh50" src="<?=base_url()?>Search/playVideo/'+vid+'"  frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
      modal.find('.modal-content').html(url);
    });

    $('#subject').change(function(){
      var subjectId = $.trim($("#subject").val());
      var libraryId = '';
      $.ajax({
        type: "POST",
        url: "<?=base_url()."Search/getSearchResultJS";?>",
        data: {keyword:searchKeyword,subjectId:subjectId,libraryId:libraryId},
        dataType: "json",
        success: function (data) {
          $(".searchResult").html(data.data);
          $("#subject").html(data.dataSubject);
          $("#library").html(data.dataLibrary);
          $(".videoTotal").html(data.videoResult);
          $(".libTotal").html(data.libResult);
          $(".search-menu-list .ls").removeClass("gradient-box");
          if ($(".search-menu-list .videos-list").length > 0) {
            $(".search-menu-list .videos-list").addClass("gradient-box");
          }else{
            $(".search-menu-list .sheets-list").addClass("gradient-box");
          }
        }
      });
    });

    $('#library').change(function(){
      var subjectId = $.trim($("#subject").val());
      var libraryId = $.trim($("#library").val());
      $.ajax({
        type: "POST",
        url: "<?=base_url()."Search/getSearchResultJS";?>",
        data: {keyword:searchKeyword,subjectId:subjectId,libraryId:libraryId},
        dataType: "json",
        success: function (data) {
          $(".searchResult").html(data.data);
          $("#subject").html(data.dataSubject);
          $("#library").html(data.dataLibrary);
          $(".videoTotal").html(data.videoResult);
          $(".libTotal").html(data.libResult);
          $(".search-menu-list .ls").removeClass("gradient-box");
          if ($(".search-menu-list .videos-list").length > 0) {
            $(".search-menu-list .videos-list").addClass("gradient-box");
          }else{
            $(".search-menu-list .sheets-list").addClass("gradient-box");
          }
        }
      });
    });

    $("#search2").keyup(function () {
      var keyword = $.trim($("#search2").val());
      if(keyword.length >= 3){
        $.ajax({
          type: "POST",
          url: "<?=base_url()."Search/GetSearchData";?>",
          data: {keyword: keyword},
          dataType: "json",
          success: function (data) {
            $('#DropdownSearch2').empty();
            $.each(data, function (key,value) {
                if (data.length >= 0)
                //$('#DropdownSearch2').append('<li role="displaySearch" ><a role="menuitem dropdownSearchli" class="dropdownlivalue">' + value['ProvinceName'] + '</a></li>');
                $('#DropdownSearch2').append('<button class="dropdown-item dropdownlivalue" type="button">' + value['title'] + '</button>');
            });
            if (data.length > 0) {
                $('#search2').attr("data-toggle", "dropdown");
                $('#DropdownSearch2').dropdown('toggle');
                $('#DropdownSearch2').show();
            }else{
              $('#DropdownSearch2').hide();
            }
            $('#search2').delay( 100 ).removeAttr('data-toggle');
          }
        });
      }else{
        $('#DropdownSearch2').empty();
        $('#search2').removeAttr('data-toggle');
        $('#DropdownSearch2').hide();
      }
    });

    $('.txtsearch2').on('click', 'button', function () {
        $('#DropdownSearch2').hide();
        $('#search2').val($(this).text());
        $('#search2').removeAttr('data-toggle');
        searchEvent2();
    });

  });

  $("#search2").on('keyup', function (e) {
    if (e.key === 'Enter' || e.keyCode === 13) {
      searchEvent2();
    }
  });

  $('.btn-search2').click(function(){
    searchEvent2();
  });

  function searchEvent2(){
    var keyword = $.trim($("#search2").val());
    if(keyword.length > 0){
      $.ajax({
        url: "<?=base_url()."Search/SearchEncode"?>",
        type: "post",
        data: {keyword:keyword} ,
        success: function (response) {
          window.location.href = "<?=base_url().'Search/index?keyword='?>"+response;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
      });
    }
  }


  $(".search-menu-list .ls").click(function() {
    var id = $(this).data("id")
    $(".search-box").hide();
    $(id).show();
    $(".search-menu-list .ls").removeClass("gradient-box");
    $(this).addClass("gradient-box");
  });
</script>