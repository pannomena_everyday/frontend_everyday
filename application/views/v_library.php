<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">
<style>

</style>
<div id="wrap">
  <div id="container">
    <div class="libraryHeader">
      <div class="containner-1200 mx-auto">
        <div class="containner-1200-padding">
          <section class="library-slide slider">
            <?php
            foreach ($subjects as $key => $subject) :
            ?>
            <div class="libraryBox">
              <div class="btnLib <?=$subject->id;?>" data-lib-no="<?=$key?>" data-box-subject="<?=$subject->id;?>" data-box-id="box_<?=$key?>" data-img="<?=$subject->icon1_url?>" data-img-active="<?=$subject->icon2_url?>"><img src="<?=$subject->icon2_url?>" class="icon-subject"/> <?=$subject->title?> </div>
            </div>
            <?php endforeach; ?>
          </section>
        </div>
      </div>
    </div>

    <section id="box1" class="library-section">
    <?php $idx = 0; ?>
    <?php
      if(count($libraries) > 0){
        $current_subject = reset($libraries)['subjectId'];
      }
    ?>
    <?php foreach ($libraries as $index => $library):?>
    <?php
      if($current_subject != $library['subjectId']){
        $current_subject = $library['subjectId'];
        $idx = 0;
      }
    ?>
    <?php $idx += 1; ?>
      <div class=" <?= $idx % 2 == 0 ? 'bg-gray' : '' ?> box-subject-<?=$library['subjectId'];?> box-subject-items">
        <div class="containner-1200 mx-auto">
          <div class="containner-1200 my-4">
            <div class="p-4">
              <?php if($library['name'] != ''): ?>
              <div class="libraryTextHeader box-subject-items box-subject-<?=$library['subjectId'];?> f30"><?=$library['name']?></div>
              <?php else: ?>
                <div class="libraryTextHeader box-subject-items box-subject-<?=$library['subjectId'];?> f30">เนื้อหา -</div>
              <?php endif; ?>
              <div class="row">
                <?php foreach ($library['lists'] as $key => $value):?>
                <!-- <?php if($index <= 9):?> -->
                <div class="col-xs-2-10 col-xs-2-5-10 col-sm-2-10 col-md-2-10 col-lg-2-10 box-subject-items box-subject-<?=$value->subjectId;?>">
                  <?php if($value->is_comingsoon): ?>
                  <a zhref="#">
                    <img loading="lazy" src="<?=$value->imgUrl?>"/>
                    <p class="libraryTitle m-lib"><?=$value->title?></p>
                    <div class="status"><div class="comingSoon">COMING SOON</div></div>
                  </a>
                  <?php else: ?>
                  <a href="<?=base_url('Library/SubList/'.$value->id.'?subject='.$value->subjectId)?>">
                    <img loading="lazy" src="<?=$value->imgUrl?>"/>
                    <p class="libraryTitle m-lib"><?=$value->title?></p>
                  </a>
                  <?php endif; ?>
                </div>
                <!-- <?php endif; ?> -->
                <?php endforeach;?>
                <!-- <div class="col-xs-2-10 col-sm-2-10 col-md-2-10 col-lg-2-10 box-subject-items see-more">
                  <a href="<?=base_url('Library/ALl/'.$value->libraryGroup.'?subject='.$value->subjectId)?>">
                    <img src="<?=base_url('resources/img/cover/Cover_SeeAll1.jpg')?>"/>
                  </a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
    </section>
  </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>
<script src="<?=base_url();?>resources/slick-master/slick/slick.min.js"></script>
<script>
  var imgPath = '<?=base_url()."resources/img/"?>';
  var img = '';
  var imgActive = '';
  var boxId = '';

  $(document).ready(function() {
    $('.box-subject-items').addClass('d-none');
    $('.see-more').addClass('d-none');
    $('.btnLib').each(function() {
      $(this).removeClass('un-active');
      $(this).removeClass('active');

      img = $(this).data('img');
      $(this).find("img").attr("src", img);
      $(this).addClass('un-active');
    });

    if(sessionStorage.getItem('current_library_subject') == null){
      btnClick($('div.' + $($('div.btnLib')[0]).data('box-subject')));
    }else{
      btnClick($('div.' + sessionStorage.getItem('current_library_subject')))
    }

    $('.btnLib').on("click", function() {
      $('.btnLib').each(function() {
        $(this).removeClass('un-active');
        $(this).removeClass('active');

        img = $(this).data('img');
        $(this).find("img").attr("src", img);
        $(this).addClass('un-active');

        $('.box-subject-items').addClass('d-none');
        $('.see-more').addClass('d-none');
      });
      sessionStorage.setItem('current_library_subject', $(this).data('box-subject'));
      btnClick($(this));
      gotoMenu($(this).data('lib-no'));
    });

    function btnClick(btn) {
      boxId = $(btn).data('box-id');
      imgActive = $(btn).data('img-active');

      $('.box-subject-' + $(btn).data('box-subject')).removeClass('d-none');
      $('.see-more').removeClass('d-none');

      btn.find("img").attr("src", imgActive);
      btn.removeClass('un-active');
      btn.removeClass('active');
      btn.addClass('active');

      // set url สำหรับดูทั้งหมด
      // $('.see-more').find('a').attr('href', "<?=base_url('Library/ALl?subject=')?>" + $(btn).data('box-subject'));
    }
  });

  var menuSlider = $(".library-slide").slick({
    slidesToShow: 6.5,
    slidesToScroll: 6,
    dots: false,
    arrows: false,
    infinite: false,
    //focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false, slidesToShow: 3.5, slidesToScroll: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false, slidesToShow: 2.5, slidesToScroll: 2
        }
      }
    ]
  });

  function gotoMenu(index){
    menuSlider.slick('slickGoTo', index);
  }

  if(sessionStorage.getItem('current_library_subject') != null){
    var index = $('.btnLib[data-box-subject='+sessionStorage.getItem('current_library_subject')+']').data("lib-no");
    var totalMenu = $('.libraryBox').length;
    // if($( window ).width() < 480){
    //   if(totalMenu != 0 && index >= (totalMenu-1)){
    //     index = totalMenu-2;
    //   }
    // }else if($( window ).width() < 768){
    //   if(totalMenu != 0 && index >= (totalMenu-2)){
    //     index = totalMenu-3;
    //   }
    // }
    $(".library-slide").slick('slickGoTo', index);
  }
</script>

