<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick.css">
<link rel="stylesheet" href="<?=base_url();?>resources/slick-master/slick/slick-theme.css">

<div id="wrap">
    <div id="container" class="bg-gray-f7">
        <div class="whiteContentBox set-full-page p-3">
            <div class="text-center">
                <div class="subscriberTxt-header f30 mb-2">เลือกประเภทสมาชิก</div>
                <div class="mb-4 mb-md-5"></div>
            </div>
            <div class="d-flex justify-content-center mb-4 d-md-flex d-xl-none">
                <div class="row col col-sm-8 p-0">
                    <div class="prev-package col-6 d-flex align-items-center invisible"><small><?=$subscriptionTypes[0]->title;?></small></div>
                    <?php
                        if (!empty($subscriptionTypes[1]->title)){
                    ?>        
                        <div class="next-package col-6 d-flex align-items-center"><small class="text-right"><?=$subscriptionTypes[1]->title;?></small></div>
                    <?php } ?>    
                </div>
            </div>            
            <div class="div-subscriber boxType">
                <!-- <div class="div-memberTypeSlick ">
                    <div class="member-border subBoxMemberType">
                        <div class="subscriber-header">Guest</div>
                        <div class="subscriber-detail js-match-height">
                            <div class="d-flex flex-column subscriber-height plan-1">
                                <div class="center m-b-20"><span class="hd">FREE</span></div>
                                <div class="dt"><i class="fa fa-check-circle"></i> ดูวีดีโอได้ไม่จำกัด</div>
                                <div class="dt"><i class="fa fa-check-circle"></i> เนื้อหาพื้นฐานครบทุกระดับชั้น<br
                                        class="break-line"> ทุกวิชา
                                </div>
                                <div class="mt-auto"><a href="<?=base_url('Plan/StartPlan')?>"><button
                                            class="subscriberBtn btn-white">เลือก</button></a></div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <?php
                    foreach($subscriptionTypes as $subscriptionType):
                ?>
                <div class="div-memberTypeSlick ">
                    <div class="member-border-rainbow subBoxMemberType">
                        <div class="subscriber-header"><?= $subscriptionType->title ?></div>
                        <div class="subscriber-detail js-match-height">
                        <?php if($currentPackage != $subscriptionType->id): ?>
                            <form class="formSubscriber" id="formSubcriber-<?=$subscriptionType->id ?>" action="<?=base_url('payment')?>"
                                method="get">
                        <?php endif; ?>
                                <div class="d-flex flex-column subscriber-height plan-2">
                                    <div class="center m-b-20 l-h-1">
                                        <span class="hd"><?= $subscriptionType->shortdesc ?></span>
                                        <!-- <span class="hd"><sup
                                                class="f22">฿</sup><?= $subscriptionType->price ?></span><span>/เดือน</span> -->
                                        <!-- <p class="subscriber-average"><?= $subscriptionType->shortdesc ?> </p> -->
                                    </div>
                                    <?= $subscriptionType->description ?>
                                    <!-- <div class="dt f-blue"><i class="fa fa-check-circle"></i> Free Trial ใช้งานฟรี 7 วัน
                                    </div>
                                    <div class="dt"><i class="fa fa-check-circle"></i> ดูวีดีโอได้ไม่จำกัด</div>
                                    <div class="dt"><i class="fa fa-check-circle"></i> เนื้อหาพื้นฐานครบทุกระดับชั้น<br
                                            class="break-line"> ทุกวิชา
                                    </div>
                                    <div class="dt"><i class="fa fa-check-circle"></i> ปลดล็อคเนื้อหา ตะลุยโจทย์
                                        และเทคนิคพิเศษ ครบทุกระดับชั้น</div>
                                    <div class="dt"><i class="fa fa-check-circle"></i> สามารถกำหนดแผนการเรียนของตนเองได้
                                    </div>
                                    <div class="dt"><i class="fa fa-check-circle"></i> แบบฝึกหัดทั้งง่าย และยาก พร้อมคำใบ้
                                        และเฉลยอย่างละเอียด</div>
                                    <div class="dt"><i class="fa fa-check-circle"></i> มีแบบทดสอบวัดความเข้าใจ
                                        สำหรับทุกเนื้อหา</div>
                                    <div class="dt"><i class="fa fa-check-circle"></i> โปรไฟล์ส่วนตัว
                                        จัดเก็บแบบฝึกหัดที่ทำเสร็จแล้ว</div>
                                    <div class="dt"><i class="fa fa-check-circle"></i> ติดตามผลงานของตนเอง ประวัติการเรียน
                                        และผลสอบ</div> -->
                                    <?php if($subscriptionType->id != 1): ?>
                                    <div class="form-group mt-4">
                                        <input type="text" class="form-control pomotion-code" name="promotion"
                                            placeholder="กรอกรหัสโปรโมชั่น">
                                    </div>
                                    <?php endif; ?>
                                    <input type="hidden" name="subscriber_id" value=<?= $subscriptionType->id ?>>
                                    <input type="hidden" name="currentPackage" value=<?= $currentPackage ?>>
                                    <?php if($subscriptionType->id != 1): ?>
                                        <?php if($currentPackage == $subscriptionType->id): ?>
                                            <div class="mt-auto justify-self-end"><button type="button" disabled
                                                    class="subscriberBtn btn-gray">แพ็กเกจที่ใช้อยู่</button></div>
                                        <?php else: ?>
                                            <div class="mt-auto justify-self-end"><button type="button"
                                                    onclick="submitFormSubcription('#formSubcriber-<?=$subscriptionType->id ?>')"
                                                    class="subscriberBtn btn-black">เลือก</button></div>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if($unsubscribe): ?>
                                            <div class="mt-auto justify-self-end"><button type="button"
                                                    onclick="unsubscribePackage()"
                                                    class="subscriberBtn btn-white">เลือก</button></div>
                                        <?php else: ?>
                                            <div class="mt-auto justify-self-end"><a href="<?= base_url('Library')?>"><button type="button"
                                                    class="subscriberBtn btn-white">เลือก</button></a></div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                        <?php if($currentPackage != $subscriptionType->id): ?>
                            </form>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php
                    endforeach;
                ?>
                <!-- <div class="div-memberTypeSlick ">
                    <div class="member-border-rainbow subBoxMemberType">
                        <div class="subscriber-header">3 เดือน</div>
                        <div class="subscriber-detail js-match-height">
                            <div class="d-flex flex-column subscriber-height plan-3">
                                <div class="center m-b-20 l-h-1">
                                    <span class="hd bold"><sup class="f22">฿</sup>1400</span><span>/เดือน</span>
                                    <p class="subscriber-average">เฉลี่ย 46 บาท / วัน</p>
                                </div>
                                <div class="dt f-blue"><i class="fa fa-check-circle"></i> Free Trial ใช้งานฟรี 7 วัน
                                </div>
                                <div class="dt"><i class="fa fa-check-circle"></i> ดูวีดีโอได้ไม่จำกัด</div>
                                <div class="dt"><i class="fa fa-check-circle"></i> เนื้อหาพื้นฐานครบทุกระดับชั้น <br
                                        class="break-line">ทุกวิชา
                                </div>
                                <div class="dt"><i class="fa fa-check-circle"></i> ปลดล็อคเนื้อหา ตะลุยโจทย์
                                    และเทคนิคพิเศษ ครบทุกระดับชั้น</div>
                                <div class="dt"><i class="fa fa-check-circle"></i> สามารถกำหนดแผนการเรียนของตนเองได้
                                </div>
                                <div class="dt"><i class="fa fa-check-circle"></i> แบบฝึกหัดทั้งง่าย และยาก พร้อมคำใบ้
                                    และเฉลยอย่างละเอียด</div>
                                <div class="dt"><i class="fa fa-check-circle"></i> มีแบบทดสอบวัดความเข้าใจ
                                    สำหรับทุกเนื้อหา</div>
                                <div class="dt"><i class="fa fa-check-circle"></i> โปรไฟล์ส่วนตัว
                                    จัดเก็บแบบฝึกหัดที่ทำเสร็จแล้ว</div>
                                <div class="dt"><i class="fa fa-check-circle"></i> ติดตามผลงานของตนเอง ประวัติการเรียน
                                    และผลสอบ</div>
                                <div class="form-group mt-4">
                                    <input type="text" class="form-control pomotion-code" id="inputCode-3"
                                        placeholder="กรอกรหัสโปรโมชั่น">
                                </div>
                                <div class="mt-auto"><a href="<?=base_url('Plan/StartPlan')?>"><button
                                            class="subscriberBtn btn-black">เลือก</button></a></div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div> <!-- //.subscriberBox -->
    </div> <!-- //.Container -->
</div> <!-- //#Warp -->

<script src="<?=base_url();?>resources/js/jquery-1.11.0.js"></script>

<!-- User Slick in Controller -->
<script>
$(function() {
    $(".boxType").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        arrows: true,
        adaptiveHeight: true,   
        responsive: [{
                breakpoint: 1080,
                settings: {
                    dot: false,
                    arrows: true,
                    prevArrow: $('.prev-package'),
                    nextArrow: $('.next-package'),                      
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 550,
                settings: {
                    arrows: true,
                    prevArrow: $('.prev-package'),
                    nextArrow: $('.next-package'),                      
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
    $('.boxType').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
        var lenghtSlick = $('.slick-slide').length - 1,
            slickIndex = $('.slick-slide.slick-active').data('slick-index');

        $('.plan').removeClass('active');
        $('.plan').eq(currentSlide).addClass('active');

        if(currentSlide >= 1){
            $('.prev-package').removeClass('invisible');            
        }else{
            $('.prev-package').addClass('invisible');
        }
        if( currentSlide >= lenghtSlick){
            $('.next-package').addClass('invisible');
        }else{
            $('.next-package').removeClass('invisible');            
        }
        $('.prev-package small').text($('[data-slick-index="'+(slickIndex-1)+'"] .subscriber-header').text())
        $('.next-package small').text($('[data-slick-index="'+(slickIndex+1)+'"] .subscriber-header').text())
    });

    $('.select-plan').on('click', function(e) {
        e.preventDefault();
        $(this).parent().addClass('active').siblings().removeClass('active');
        var planNo = $(this).data('plan');
        $('.boxType').slick('slickGoTo', planNo - 1);
    });

    $("#profile").click(function() {
        $('#profileBox').toggle('slow');
    });

    $(".formSubscriber").submit(function(event) {
        event.preventDefault()
        let formId = $(this).attr('id')
        submitFormSubcription('#' + formId)
    });
});

function submitFormSubcription(formId) {
    let form = document.querySelector(formId);

    if (form.currentPackage.value == form.subscriber_id.value)
    {
        alert('แพ็กเกจที่เลือกกําลังใช้อยู่ในขณะนี้ กรุณาเลือกเปลี่ยนแพ็กเกจอื่น')
        return false
    }

    if (form.promotion.value != '') {
        $.ajax({
            type: "post",
            url: "<?=base_url('Membertype/check_promotion')?>",
            data: {
                promotion: form.promotion.value,
                subscription_id : form.subscriber_id.value
            },
            dataType: "json",
            success: function(response) {
                if (response.status)
                    window.location.href = `<?=base_url('Payment')?>?subscriber_id=${form.subscriber_id.value}&promotion=${form.promotion.value}`;
                else
                    alert(response.message)
            }
        });
    } else {
        window.location.href = `<?=base_url('Payment')?>?subscriber_id=${form.subscriber_id.value}&promotion=`;
        // form.submit();
    }
}

function unsubscribePackage()
{
    $.ajax({
        type: "post",
        url: "<?=base_url('Membertype/unsubscribe')?>",
        dataType: "json",
        success: function(response) {
            window.location.href = `<?=base_url('MyProfile/MyMembership')?>`
        }
    });
}
</script>
