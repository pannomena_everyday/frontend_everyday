/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
const t="undefined"!=typeof window&&null!=window.customElements&&void 0!==window.customElements.polyfillWrapFlushCallback,i=(t,i,e=null)=>{for(;i!==e;){const e=i.nextSibling;t.removeChild(i),i=e}},e=`{{lit-${String(Math.random()).slice(2)}}}`,s=`\x3c!--${e}--\x3e`,o=new RegExp(`${e}|${s}`);class n{constructor(t,i){this.parts=[],this.element=i;const s=[],n=[],h=document.createTreeWalker(i.content,133,null,!1);let a=0,d=-1,u=0;const{strings:v,values:{length:p}}=t;for(;u<p;){const t=h.nextNode();if(null!==t){if(d++,1===t.nodeType){if(t.hasAttributes()){const i=t.attributes,{length:e}=i;let s=0;for(let t=0;t<e;t++)r(i[t].name,"$lit$")&&s++;for(;s-- >0;){const i=v[u],e=c.exec(i)[2],s=e.toLowerCase()+"$lit$",n=t.getAttribute(s);t.removeAttribute(s);const r=n.split(o);this.parts.push({type:"attribute",index:d,name:e,strings:r}),u+=r.length-1}}"TEMPLATE"===t.tagName&&(n.push(t),h.currentNode=t.content)}else if(3===t.nodeType){const i=t.data;if(i.indexOf(e)>=0){const e=t.parentNode,n=i.split(o),h=n.length-1;for(let i=0;i<h;i++){let s,o=n[i];if(""===o)s=l();else{const t=c.exec(o);null!==t&&r(t[2],"$lit$")&&(o=o.slice(0,t.index)+t[1]+t[2].slice(0,-"$lit$".length)+t[3]),s=document.createTextNode(o)}e.insertBefore(s,t),this.parts.push({type:"node",index:++d})}""===n[h]?(e.insertBefore(l(),t),s.push(t)):t.data=n[h],u+=h}}else if(8===t.nodeType)if(t.data===e){const i=t.parentNode;null!==t.previousSibling&&d!==a||(d++,i.insertBefore(l(),t)),a=d,this.parts.push({type:"node",index:d}),null===t.nextSibling?t.data="":(s.push(t),d--),u++}else{let i=-1;for(;-1!==(i=t.data.indexOf(e,i+1));)this.parts.push({type:"node",index:-1}),u++}}else h.currentNode=n.pop()}for(const t of s)t.parentNode.removeChild(t)}}const r=(t,i)=>{const e=t.length-i.length;return e>=0&&t.slice(e)===i},h=t=>-1!==t.index,l=()=>document.createComment(""),c=/([ \x09\x0a\x0c\x0d])([^\0-\x1F\x7F-\x9F "'>=/]+)([ \x09\x0a\x0c\x0d]*=[ \x09\x0a\x0c\x0d]*(?:[^ \x09\x0a\x0c\x0d"'`<>=]*|"[^"]*|'[^']*))$/;function a(t,i){const{element:{content:e},parts:s}=t,o=document.createTreeWalker(e,133,null,!1);let n=u(s),r=s[n],h=-1,l=0;const c=[];let a=null;for(;o.nextNode();){h++;const t=o.currentNode;for(t.previousSibling===a&&(a=null),i.has(t)&&(c.push(t),null===a&&(a=t)),null!==a&&l++;void 0!==r&&r.index===h;)r.index=null!==a?-1:r.index-l,n=u(s,n),r=s[n]}c.forEach(t=>t.parentNode.removeChild(t))}const d=t=>{let i=11===t.nodeType?0:1;const e=document.createTreeWalker(t,133,null,!1);for(;e.nextNode();)i++;return i},u=(t,i=-1)=>{for(let e=i+1;e<t.length;e++){const i=t[e];if(h(i))return e}return-1};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
const v=new WeakMap,p=t=>"function"==typeof t&&v.has(t),f={},w={};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
class b{constructor(t,i,e){this.t=[],this.template=t,this.processor=i,this.options=e}update(t){let i=0;for(const e of this.t)void 0!==e&&e.setValue(t[i]),i++;for(const t of this.t)void 0!==t&&t.commit()}_clone(){const i=t?this.template.element.content.cloneNode(!0):document.importNode(this.template.element.content,!0),e=[],s=this.template.parts,o=document.createTreeWalker(i,133,null,!1);let n,r=0,l=0,c=o.nextNode();for(;r<s.length;)if(n=s[r],h(n)){for(;l<n.index;)l++,"TEMPLATE"===c.nodeName&&(e.push(c),o.currentNode=c.content),null===(c=o.nextNode())&&(o.currentNode=e.pop(),c=o.nextNode());if("node"===n.type){const t=this.processor.handleTextExpression(this.options);t.insertAfterNode(c.previousSibling),this.t.push(t)}else this.t.push(...this.processor.handleAttributeExpressions(c,n.name,n.strings,this.options));r++}else this.t.push(void 0),r++;return t&&(document.adoptNode(i),customElements.upgrade(i)),i}}
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */const g=window.trustedTypes&&trustedTypes.createPolicy("lit-html",{createHTML:t=>t}),m=` ${e} `;class y{constructor(t,i,e,s){this.strings=t,this.values=i,this.type=e,this.processor=s}getHTML(){const t=this.strings.length-1;let i="",o=!1;for(let n=0;n<t;n++){const t=this.strings[n],r=t.lastIndexOf("\x3c!--");o=(r>-1||o)&&-1===t.indexOf("--\x3e",r+1);const h=c.exec(t);i+=null===h?t+(o?m:s):t.substr(0,h.index)+h[1]+h[2]+"$lit$"+h[3]+e}return i+=this.strings[t],i}getTemplateElement(){const t=document.createElement("template");let i=this.getHTML();return void 0!==g&&(i=g.createHTML(i)),t.innerHTML=i,t}}
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */const x=t=>null===t||!("object"==typeof t||"function"==typeof t),k=t=>Array.isArray(t)||!(!t||!t[Symbol.iterator]);class S{constructor(t,i,e){this.dirty=!0,this.element=t,this.name=i,this.strings=e,this.parts=[];for(let t=0;t<e.length-1;t++)this.parts[t]=this._createPart()}_createPart(){return new $(this)}_getValue(){const t=this.strings,i=t.length-1,e=this.parts;if(1===i&&""===t[0]&&""===t[1]){const t=e[0].value;if("symbol"==typeof t)return String(t);if("string"==typeof t||!k(t))return t}let s="";for(let o=0;o<i;o++){s+=t[o];const i=e[o];if(void 0!==i){const t=i.value;if(x(t)||!k(t))s+="string"==typeof t?t:String(t);else for(const i of t)s+="string"==typeof i?i:String(i)}}return s+=t[i],s}commit(){this.dirty&&(this.dirty=!1,this.element.setAttribute(this.name,this._getValue()))}}class ${constructor(t){this.value=void 0,this.committer=t}setValue(t){t===f||x(t)&&t===this.value||(this.value=t,p(t)||(this.committer.dirty=!0))}commit(){for(;p(this.value);){const t=this.value;this.value=f,t(this)}this.value!==f&&this.committer.commit()}}class z{constructor(t){this.value=void 0,this.i=void 0,this.options=t}appendInto(t){this.startNode=t.appendChild(l()),this.endNode=t.appendChild(l())}insertAfterNode(t){this.startNode=t,this.endNode=t.nextSibling}appendIntoPart(t){t.s(this.startNode=l()),t.s(this.endNode=l())}insertAfterPart(t){t.s(this.startNode=l()),this.endNode=t.endNode,t.endNode=this.startNode}setValue(t){this.i=t}commit(){if(null===this.startNode.parentNode)return;for(;p(this.i);){const t=this.i;this.i=f,t(this)}const t=this.i;t!==f&&(x(t)?t!==this.value&&this.o(t):t instanceof y?this.h(t):t instanceof Node?this.l(t):k(t)?this.u(t):t===w?(this.value=w,this.clear()):this.o(t))}s(t){this.endNode.parentNode.insertBefore(t,this.endNode)}l(t){this.value!==t&&(this.clear(),this.s(t),this.value=t)}o(t){const i=this.startNode.nextSibling,e="string"==typeof(t=null==t?"":t)?t:String(t);i===this.endNode.previousSibling&&3===i.nodeType?i.data=e:this.l(document.createTextNode(e)),this.value=t}h(t){const i=this.options.templateFactory(t);if(this.value instanceof b&&this.value.template===i)this.value.update(t.values);else{const e=new b(i,t.processor,this.options),s=e._clone();e.update(t.values),this.l(s),this.value=e}}u(t){Array.isArray(this.value)||(this.value=[],this.clear());const i=this.value;let e,s=0;for(const o of t)e=i[s],void 0===e&&(e=new z(this.options),i.push(e),0===s?e.appendIntoPart(this):e.insertAfterPart(i[s-1])),e.setValue(o),e.commit(),s++;s<i.length&&(i.length=s,this.clear(e&&e.endNode))}clear(t=this.startNode){i(this.startNode.parentNode,t.nextSibling,this.endNode)}}class M{constructor(t,i,e){if(this.value=void 0,this.i=void 0,2!==e.length||""!==e[0]||""!==e[1])throw new Error("Boolean attributes can only contain a single expression");this.element=t,this.name=i,this.strings=e}setValue(t){this.i=t}commit(){for(;p(this.i);){const t=this.i;this.i=f,t(this)}if(this.i===f)return;const t=!!this.i;this.value!==t&&(t?this.element.setAttribute(this.name,""):this.element.removeAttribute(this.name),this.value=t),this.i=f}}class C extends S{constructor(t,i,e){super(t,i,e),this.single=2===e.length&&""===e[0]&&""===e[1]}_createPart(){return new _(this)}_getValue(){return this.single?this.parts[0].value:super._getValue()}commit(){this.dirty&&(this.dirty=!1,this.element[this.name]=this._getValue())}}class _ extends ${}let j=!1;(()=>{try{const t={get capture(){return j=!0,!1}};window.addEventListener("test",t,t),window.removeEventListener("test",t,t)}catch(t){}})();class A{constructor(t,i,e){this.value=void 0,this.i=void 0,this.element=t,this.eventName=i,this.eventContext=e,this.v=t=>this.handleEvent(t)}setValue(t){this.i=t}commit(){for(;p(this.i);){const t=this.i;this.i=f,t(this)}if(this.i===f)return;const t=this.i,i=this.value,e=null==t||null!=i&&(t.capture!==i.capture||t.once!==i.once||t.passive!==i.passive),s=null!=t&&(null==i||e);e&&this.element.removeEventListener(this.eventName,this.v,this.p),s&&(this.p=P(t),this.element.addEventListener(this.eventName,this.v,this.p)),this.value=t,this.i=f}handleEvent(t){"function"==typeof this.value?this.value.call(this.eventContext||this.element,t):this.value.handleEvent(t)}}const P=t=>t&&(j?{capture:t.capture,passive:t.passive,once:t.once}:t.capture)
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */;function E(t){let i=O.get(t.type);void 0===i&&(i={stringsArray:new WeakMap,keyString:new Map},O.set(t.type,i));let s=i.stringsArray.get(t.strings);if(void 0!==s)return s;const o=t.strings.join(e);return s=i.keyString.get(o),void 0===s&&(s=new n(t,t.getTemplateElement()),i.keyString.set(o,s)),i.stringsArray.set(t.strings,s),s}const O=new Map,F=new WeakMap;
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */const T=new
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
class{handleAttributeExpressions(t,i,e,s){const o=i[0];if("."===o){return new C(t,i.slice(1),e).parts}if("@"===o)return[new A(t,i.slice(1),s.eventContext)];if("?"===o)return[new M(t,i.slice(1),e)];return new S(t,i,e).parts}handleTextExpression(t){return new z(t)}};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */"undefined"!=typeof window&&(window.litHtmlVersions||(window.litHtmlVersions=[])).push("1.3.0");const V=(t,...i)=>new y(t,i,"html",T)
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */,H=(t,i)=>`${t}--${i}`;let U=!0;void 0===window.ShadyCSS?U=!1:void 0===window.ShadyCSS.prepareTemplateDom&&(console.warn("Incompatible ShadyCSS version detected. Please update to at least @webcomponents/webcomponentsjs@2.0.2 and @webcomponents/shadycss@1.3.1."),U=!1);const I=t=>i=>{const s=H(i.type,t);let o=O.get(s);void 0===o&&(o={stringsArray:new WeakMap,keyString:new Map},O.set(s,o));let r=o.stringsArray.get(i.strings);if(void 0!==r)return r;const h=i.strings.join(e);if(r=o.keyString.get(h),void 0===r){const e=i.getTemplateElement();U&&window.ShadyCSS.prepareTemplateDom(e,t),r=new n(i,e),o.keyString.set(h,r)}return o.stringsArray.set(i.strings,r),r},B=["html","svg"],L=new Set,N=(t,i,e)=>{L.add(t);const s=e?e.element:document.createElement("template"),o=i.querySelectorAll("style"),{length:n}=o;if(0===n)return void window.ShadyCSS.prepareTemplateStyles(s,t);const r=document.createElement("style");for(let t=0;t<n;t++){const i=o[t];i.parentNode.removeChild(i),r.textContent+=i.textContent}(t=>{B.forEach(i=>{const e=O.get(H(i,t));void 0!==e&&e.keyString.forEach(t=>{const{element:{content:i}}=t,e=new Set;Array.from(i.querySelectorAll("style")).forEach(t=>{e.add(t)}),a(t,e)})})})(t);const h=s.content;e?function(t,i,e=null){const{element:{content:s},parts:o}=t;if(null==e)return void s.appendChild(i);const n=document.createTreeWalker(s,133,null,!1);let r=u(o),h=0,l=-1;for(;n.nextNode();){l++;for(n.currentNode===e&&(h=d(i),e.parentNode.insertBefore(i,e));-1!==r&&o[r].index===l;){if(h>0){for(;-1!==r;)o[r].index+=h,r=u(o,r);return}r=u(o,r)}}}(e,r,h.firstChild):h.insertBefore(r,h.firstChild),window.ShadyCSS.prepareTemplateStyles(s,t);const l=h.querySelector("style");if(window.ShadyCSS.nativeShadow&&null!==l)i.insertBefore(l.cloneNode(!0),i.firstChild);else if(e){h.insertBefore(r,h.firstChild);const t=new Set;t.add(r),a(e,t)}};window.JSCompiler_renameProperty=(t,i)=>t;const q={toAttribute(t,i){switch(i){case Boolean:return t?"":null;case Object:case Array:return null==t?t:JSON.stringify(t)}return t},fromAttribute(t,i){switch(i){case Boolean:return null!==t;case Number:return null===t?null:Number(t);case Object:case Array:return JSON.parse(t)}return t}},R=(t,i)=>i!==t&&(i==i||t==t),D={attribute:!0,type:String,converter:q,reflect:!1,hasChanged:R};class W extends HTMLElement{constructor(){super(),this.initialize()}static get observedAttributes(){this.finalize();const t=[];return this._classProperties.forEach((i,e)=>{const s=this._attributeNameForProperty(e,i);void 0!==s&&(this._attributeToPropertyMap.set(s,e),t.push(s))}),t}static _ensureClassProperties(){if(!this.hasOwnProperty(JSCompiler_renameProperty("_classProperties",this))){this._classProperties=new Map;const t=Object.getPrototypeOf(this)._classProperties;void 0!==t&&t.forEach((t,i)=>this._classProperties.set(i,t))}}static createProperty(t,i=D){if(this._ensureClassProperties(),this._classProperties.set(t,i),i.noAccessor||this.prototype.hasOwnProperty(t))return;const e="symbol"==typeof t?Symbol():"__"+t,s=this.getPropertyDescriptor(t,e,i);void 0!==s&&Object.defineProperty(this.prototype,t,s)}static getPropertyDescriptor(t,i,e){return{get(){return this[i]},set(s){const o=this[t];this[i]=s,this.requestUpdateInternal(t,o,e)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this._classProperties&&this._classProperties.get(t)||D}static finalize(){const t=Object.getPrototypeOf(this);if(t.hasOwnProperty("finalized")||t.finalize(),this.finalized=!0,this._ensureClassProperties(),this._attributeToPropertyMap=new Map,this.hasOwnProperty(JSCompiler_renameProperty("properties",this))){const t=this.properties,i=[...Object.getOwnPropertyNames(t),..."function"==typeof Object.getOwnPropertySymbols?Object.getOwnPropertySymbols(t):[]];for(const e of i)this.createProperty(e,t[e])}}static _attributeNameForProperty(t,i){const e=i.attribute;return!1===e?void 0:"string"==typeof e?e:"string"==typeof t?t.toLowerCase():void 0}static _valueHasChanged(t,i,e=R){return e(t,i)}static _propertyValueFromAttribute(t,i){const e=i.type,s=i.converter||q,o="function"==typeof s?s:s.fromAttribute;return o?o(t,e):t}static _propertyValueToAttribute(t,i){if(void 0===i.reflect)return;const e=i.type,s=i.converter;return(s&&s.toAttribute||q.toAttribute)(t,e)}initialize(){this._updateState=0,this._updatePromise=new Promise(t=>this._enableUpdatingResolver=t),this._changedProperties=new Map,this._saveInstanceProperties(),this.requestUpdateInternal()}_saveInstanceProperties(){this.constructor._classProperties.forEach((t,i)=>{if(this.hasOwnProperty(i)){const t=this[i];delete this[i],this._instanceProperties||(this._instanceProperties=new Map),this._instanceProperties.set(i,t)}})}_applyInstanceProperties(){this._instanceProperties.forEach((t,i)=>this[i]=t),this._instanceProperties=void 0}connectedCallback(){this.enableUpdating()}enableUpdating(){void 0!==this._enableUpdatingResolver&&(this._enableUpdatingResolver(),this._enableUpdatingResolver=void 0)}disconnectedCallback(){}attributeChangedCallback(t,i,e){i!==e&&this._attributeToProperty(t,e)}_propertyToAttribute(t,i,e=D){const s=this.constructor,o=s._attributeNameForProperty(t,e);if(void 0!==o){const t=s._propertyValueToAttribute(i,e);if(void 0===t)return;this._updateState=8|this._updateState,null==t?this.removeAttribute(o):this.setAttribute(o,t),this._updateState=-9&this._updateState}}_attributeToProperty(t,i){if(8&this._updateState)return;const e=this.constructor,s=e._attributeToPropertyMap.get(t);if(void 0!==s){const t=e.getPropertyOptions(s);this._updateState=16|this._updateState,this[s]=e._propertyValueFromAttribute(i,t),this._updateState=-17&this._updateState}}requestUpdateInternal(t,i,e){let s=!0;if(void 0!==t){const o=this.constructor;e=e||o.getPropertyOptions(t),o._valueHasChanged(this[t],i,e.hasChanged)?(this._changedProperties.has(t)||this._changedProperties.set(t,i),!0!==e.reflect||16&this._updateState||(void 0===this._reflectingProperties&&(this._reflectingProperties=new Map),this._reflectingProperties.set(t,e))):s=!1}!this._hasRequestedUpdate&&s&&(this._updatePromise=this._enqueueUpdate())}requestUpdate(t,i){return this.requestUpdateInternal(t,i),this.updateComplete}async _enqueueUpdate(){this._updateState=4|this._updateState;try{await this._updatePromise}catch(t){}const t=this.performUpdate();return null!=t&&await t,!this._hasRequestedUpdate}get _hasRequestedUpdate(){return 4&this._updateState}get hasUpdated(){return 1&this._updateState}performUpdate(){if(!this._hasRequestedUpdate)return;this._instanceProperties&&this._applyInstanceProperties();let t=!1;const i=this._changedProperties;try{t=this.shouldUpdate(i),t?this.update(i):this._markUpdated()}catch(i){throw t=!1,this._markUpdated(),i}t&&(1&this._updateState||(this._updateState=1|this._updateState,this.firstUpdated(i)),this.updated(i))}_markUpdated(){this._changedProperties=new Map,this._updateState=-5&this._updateState}get updateComplete(){return this._getUpdateComplete()}_getUpdateComplete(){return this._updatePromise}shouldUpdate(t){return!0}update(t){void 0!==this._reflectingProperties&&this._reflectingProperties.size>0&&(this._reflectingProperties.forEach((t,i)=>this._propertyToAttribute(i,this[i],t)),this._reflectingProperties=void 0),this._markUpdated()}updated(t){}firstUpdated(t){}}W.finalized=!0;
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
const J=t=>i=>"function"==typeof i?((t,i)=>(window.customElements.define(t,i),i))(t,i):((t,i)=>{const{kind:e,elements:s}=i;return{kind:e,elements:s,finisher(i){window.customElements.define(t,i)}}})(t,i),G=(t,i)=>"method"===i.kind&&i.descriptor&&!("value"in i.descriptor)?Object.assign(Object.assign({},i),{finisher(e){e.createProperty(i.key,t)}}):{kind:"field",key:Symbol(),placement:"own",descriptor:{},initializer(){"function"==typeof i.initializer&&(this[i.key]=i.initializer.call(this))},finisher(e){e.createProperty(i.key,t)}};function X(t){return(i,e)=>void 0!==e?((t,i,e)=>{i.constructor.createProperty(e,t)})(t,i,e):G(t,i)}
/**
@license
Copyright (c) 2019 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at
http://polymer.github.io/LICENSE.txt The complete set of authors may be found at
http://polymer.github.io/AUTHORS.txt The complete set of contributors may be
found at http://polymer.github.io/CONTRIBUTORS.txt Code distributed by Google as
part of the polymer project is also subject to an additional IP rights grant
found at http://polymer.github.io/PATENTS.txt
*/const K=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,Y=Symbol();class Q{constructor(t,i){if(i!==Y)throw new Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){return void 0===this._styleSheet&&(K?(this._styleSheet=new CSSStyleSheet,this._styleSheet.replaceSync(this.cssText)):this._styleSheet=null),this._styleSheet}toString(){return this.cssText}}const Z=(t,...i)=>{const e=i.reduce((i,e,s)=>i+(t=>{if(t instanceof Q)return t.cssText;if("number"==typeof t)return t;throw new Error(`Value passed to 'css' function must be a 'css' function result: ${t}. Use 'unsafeCSS' to pass non-literal values, but\n            take care to ensure page security.`)})(e)+t[s+1],t[0]);return new Q(e,Y)};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
(window.litElementVersions||(window.litElementVersions=[])).push("2.4.0");const tt={};class it extends W{static getStyles(){return this.styles}static _getUniqueStyles(){if(this.hasOwnProperty(JSCompiler_renameProperty("_styles",this)))return;const t=this.getStyles();if(Array.isArray(t)){const i=(t,e)=>t.reduceRight((t,e)=>Array.isArray(e)?i(e,t):(t.add(e),t),e),e=i(t,new Set),s=[];e.forEach(t=>s.unshift(t)),this._styles=s}else this._styles=void 0===t?[]:[t];this._styles=this._styles.map(t=>{if(t instanceof CSSStyleSheet&&!K){const i=Array.prototype.slice.call(t.cssRules).reduce((t,i)=>t+i.cssText,"");return new Q(String(i),Y)}return t})}initialize(){super.initialize(),this.constructor._getUniqueStyles(),this.renderRoot=this.createRenderRoot(),window.ShadowRoot&&this.renderRoot instanceof window.ShadowRoot&&this.adoptStyles()}createRenderRoot(){return this.attachShadow({mode:"open"})}adoptStyles(){const t=this.constructor._styles;0!==t.length&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow?K?this.renderRoot.adoptedStyleSheets=t.map(t=>t instanceof CSSStyleSheet?t:t.styleSheet):this._needsShimAdoptedStyleSheets=!0:window.ShadyCSS.ScopingShim.prepareAdoptedCssText(t.map(t=>t.cssText),this.localName))}connectedCallback(){super.connectedCallback(),this.hasUpdated&&void 0!==window.ShadyCSS&&window.ShadyCSS.styleElement(this)}update(t){const i=this.render();super.update(t),i!==tt&&this.constructor.render(i,this.renderRoot,{scopeName:this.localName,eventContext:this}),this._needsShimAdoptedStyleSheets&&(this._needsShimAdoptedStyleSheets=!1,this.constructor._styles.forEach(t=>{const i=document.createElement("style");i.textContent=t.cssText,this.renderRoot.appendChild(i)}))}render(){return tt}}it.finalized=!0,it.render=(t,e,s)=>{if(!s||"object"!=typeof s||!s.scopeName)throw new Error("The `scopeName` option is required.");const o=s.scopeName,n=F.has(e),r=U&&11===e.nodeType&&!!e.host,h=r&&!L.has(o),l=h?document.createDocumentFragment():e;if(((t,e,s)=>{let o=F.get(e);void 0===o&&(i(e,e.firstChild),F.set(e,o=new z(Object.assign({templateFactory:E},s))),o.appendInto(e)),o.setValue(t),o.commit()})(t,l,Object.assign({templateFactory:I(o)},s)),h){const t=F.get(l);F.delete(l);const s=t.value instanceof b?t.value.template:void 0;N(o,l,s),i(e,e.firstChild),e.appendChild(l),F.set(e,t)}!n&&r&&window.ShadyCSS.styleElement(e.host)};const et=["requestFullscreen","webkitRequestFullscreen","mozRequestFullScreen","msRequestFullscreen"],st=["exitFullscreen","webkitExitFullscreen","webkitCancelFullScreen","mozCancelFullScreen","msExitFullscreen"],ot=["fullscreenElement","webkitFullscreenElement","webkitCurrentFullScreenElement","mozFullScreenElement","msFullscreenElement"];function nt(t){for(const i of ot)if("object"==typeof t[i])return t[i];return null}function rt(t,i,e){nt(i)||nt(e)?function(t){for(const i of st)try{t[i]()}catch(t){}}(e):function(t){for(const i of et)try{t[i]()}catch(t){}}(t)}function ht(){return V`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path
        d="M8 6.82v10.36c0 .79.87 1.27 1.54.84l8.14-5.18c.62-.39.62-1.29 0-1.69L9.54 5.98C8.87 5.55 8 6.03 8 6.82z"
      />
    </svg>
  `}function lt(){return V`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path
        d="M11.99 5V2.21c0-.45-.54-.67-.85-.35L7.35 5.65c-.2.2-.2.51 0 .71l3.79 3.79c.31.31.85.09.85-.35V7c3.73 0 6.68 3.42 5.86 7.29-.47 2.27-2.31 4.1-4.57 4.57-3.57.75-6.75-1.7-7.23-5.01-.06-.48-.48-.85-.98-.85-.6 0-1.08.53-1 1.13.62 4.39 4.8 7.64 9.53 6.72 3.12-.61 5.63-3.12 6.24-6.24.99-5.13-2.9-9.61-7.85-9.61zm-1.1 11h-.85v-3.26l-1.01.31v-.69l1.77-.63h.09V16zm4.28-1.76c0 .32-.03.6-.1.82s-.17.42-.29.57-.28.26-.45.33-.37.1-.59.1-.41-.03-.59-.1-.33-.18-.46-.33-.23-.34-.3-.57-.11-.5-.11-.82v-.74c0-.32.03-.6.1-.82s.17-.42.29-.57.28-.26.45-.33.37-.1.59-.1.41.03.59.1.33.18.46.33.23.34.3.57.11.5.11.82v.74zm-.85-.86c0-.19-.01-.35-.04-.48s-.07-.23-.12-.31-.11-.14-.19-.17-.16-.05-.25-.05-.18.02-.25.05-.14.09-.19.17-.09.18-.12.31-.04.29-.04.48v.97c0 .19.01.35.04.48s.07.24.12.32.11.14.19.17.16.05.25.05.18-.02.25-.05.14-.09.19-.17.09-.19.11-.32.04-.29.04-.48v-.97z"
      />
    </svg>
  `}function ct(){return V`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path
        d="M18.92 13c-.5 0-.91.37-.98.86-.48 3.37-3.77 5.84-7.42 4.96-2.25-.54-3.91-2.27-4.39-4.53C5.32 10.42 8.27 7 12 7v2.79c0 .45.54.67.85.35l3.79-3.79c.2-.2.2-.51 0-.71l-3.79-3.79c-.31-.31-.85-.09-.85.36V5c-4.94 0-8.84 4.48-7.84 9.6.6 3.11 2.9 5.5 5.99 6.19 4.83 1.08 9.15-2.2 9.77-6.67.09-.59-.4-1.12-1-1.12zm-8.02 3v-4.27h-.09l-1.77.63v.69l1.01-.31V16zm3.42-4.22c-.18-.07-.37-.1-.59-.1s-.41.03-.59.1-.33.18-.45.33-.23.34-.29.57-.1.5-.1.82v.74c0 .32.04.6.11.82s.17.42.3.57.28.26.46.33.37.1.59.1.41-.03.59-.1.33-.18.45-.33.22-.34.29-.57.1-.5.1-.82v-.74c0-.32-.04-.6-.11-.82s-.17-.42-.3-.57-.29-.26-.46-.33zm.01 2.57c0 .19-.01.35-.04.48s-.06.24-.11.32-.11.14-.19.17-.16.05-.25.05-.18-.02-.25-.05-.14-.09-.19-.17-.09-.19-.12-.32-.04-.29-.04-.48v-.97c0-.19.01-.35.04-.48s.06-.23.12-.31.11-.14.19-.17.16-.05.25-.05.18.02.25.05.14.09.19.17.09.18.12.31.04.29.04.48v.97z"
      />
    </svg>
  `}function at(){return V`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path
        d="M8 19c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2s-2 .9-2 2v10c0 1.1.9 2 2 2zm6-12v10c0 1.1.9 2 2 2s2-.9 2-2V7c0-1.1-.9-2-2-2s-2 .9-2 2z"
      />
    </svg>
  `}function dt(){return V`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="M0 0h24v24H0z" fill="none" />
      <path
        d="M16.5 12c0-1.77-1.02-3.29-2.5-4.03v2.21l2.45 2.45c.03-.2.05-.41.05-.63zm2.5 0c0 .94-.2 1.82-.54 2.64l1.51 1.51C20.63 14.91 21 13.5 21 12c0-4.28-2.99-7.86-7-8.77v2.06c2.89.86 5 3.54 5 6.71zM4.27 3L3 4.27 7.73 9H3v6h4l5 5v-6.73l4.25 4.25c-.67.52-1.42.93-2.25 1.18v2.06c1.38-.31 2.63-.95 3.69-1.81L19.73 21 21 19.73l-9-9L4.27 3zM12 4L9.91 6.09 12 8.18V4z"
      />
    </svg>
  `}class ut{constructor(t,i,e){this.contentId=t,this.viewerController=i,this.videoController=e}}var vt;!function(t){t.PLAY="PLAY",t.PAUSE="PAUSE",t.FORWARD="FORWARD",t.BACKWARD="BACKWARD",t.MUTE="MUTE"}(vt||(vt={}));class pt{constructor(){this._context=null,this._isLoading=!1,this.videoLength=0,this.video=document.createElement("video"),this.video.setAttribute("playsinline","true"),this.video.setAttribute("preload","metadata"),this.video.setAttribute("playsinline","true"),this.video.addEventListener("loadedmetadata",()=>{var t;this._canvas&&(this._canvas.width=this.video.videoWidth,this._canvas.height=this.video.videoHeight,this.videoLength=this.video.duration,(null===(t=this._element)||void 0===t?void 0:t.resumePlayback)&&(this.video.currentTime=+this._element.resumePlayback,this._element.requestUpdate()),this.setCover())},!1),this.video.addEventListener("play",()=>{this.drawCanvas(),this.state.viewerController.isSeeking||(this.state.viewerController.hasUserActivity(),this.state.viewerController.showActionOverlay(vt.PLAY))},!1),this.video.addEventListener("pause",()=>{this.state.viewerController.isSeeking||(this.state.viewerController.hasUserActivity(),this.state.viewerController.showActionOverlay(vt.PAUSE))}),this.video.addEventListener("waiting",()=>{var t;this._isLoading=!0,null===(t=this._element)||void 0===t||t.requestUpdate()}),this.video.addEventListener("canplay",()=>{var t;this._isLoading=!1,null===(t=this._element)||void 0===t||t.requestUpdate()}),this.image=new Image,this.image.onload=()=>{this.setCover()}}set canvas(t){this._canvas=t,this._context=t.getContext("2d")}set imageSrc(t){this.image.src=t||""}set videoSrc(t){this.video.src=t}get isPlay(){return!this.video.paused}get videoTime(){const t=this.videoDuration-this.currentTime;return`${this.twoDigit(""+Math.floor(t/60))}:${this.twoDigit(""+Math.floor(t%60))}`}get videoDuration(){return this.videoLength}get currentTime(){return this.video.currentTime}set element(t){this._element=t}get playbackSpeed(){return this.video.playbackRate}set playbackSpeed(t){var i;this.video.playbackRate=t,null===(i=this._element)||void 0===i||i.requestUpdate()}get volume(){return Math.floor(100*this.video.volume)}set volume(t){this.video.muted=!1,this.video.volume=t/100}get muted(){return this.video.muted}set volumeControlElement(t){this._volumeControlElement=t}get isLoading(){return this._isLoading}togglePlay(){var t;this.video.paused?(this.video.play(),wt.instance.pauseOther(this.state)):this.video.pause(),null===(t=this._element)||void 0===t||t.requestUpdate()}pause(){var t;this.video.pause(),null===(t=this._element)||void 0===t||t.requestUpdate()}seekDuration(t){const i=Math.min(Math.max(0,this.video.currentTime+t),this.videoLength);this.video.currentTime=i,this.drawCanvas()}toggleMute(){var t;this.video.muted=!this.video.muted,this.muted&&this.state.viewerController.showActionOverlay(vt.MUTE),null===(t=this._volumeControlElement)||void 0===t||t.requestUpdate(),this.state.viewerController.hasUserActivity()}onSeek(t){void 0===this._prevPlayState&&(this.state.viewerController.onSeek(),this._prevPlayState=!this.video.paused,this.video.pause()),this.video.currentTime=t,this.drawCanvas()}async onSeekEnded(){void 0!==this._prevPlayState&&(this._prevPlayState?await this.video.play():this.video.pause(),this.state.viewerController.onSeekEnded(),this._prevPlayState=void 0)}drawCanvas(){var t,i,e,s,o;if(this._element&&(this._element.progress=""+this.video.currentTime/this.video.duration*100,this._element.currentTime=""+this.video.currentTime,this._element.duration=""+this.video.duration),this._context){this._context.save(),this._context.drawImage(this.video,0,0,this.video.videoWidth,this.video.videoHeight);const n=this.video.videoHeight/6;this._context.translate(this.video.videoWidth/2,this.video.videoHeight/2),this._context.rotate(-Math.PI/6),this._context.fillStyle="rgba(0, 0, 0, 0.08)",this._context.font=`bold ${n}px monospace`,this._context.textBaseline="middle",this._context.textAlign="center",this._context.fillText(null!==(i=null===(t=this._element)||void 0===t?void 0:t.stdName)&&void 0!==i?i:"",0,0-n,this.video.videoWidth),this._context.fillText(null!==(s=null===(e=this._element)||void 0===e?void 0:e.nid)&&void 0!==s?s:"",0,0+n,this.video.videoWidth),this._context.restore(),null===(o=this._element)||void 0===o||o.requestUpdate(),this.video.paused||setTimeout(()=>{this.drawCanvas()},20)}}twoDigit(t){return("0"+t).slice(-2)}setCover(){if(this.image.src&&this._canvas&&this._context){const t=this._canvas.width,i=this._canvas.height,e=this.image.width,s=this.image.height,o=t*s/e,n=o>i?i*e/s:t,r=o>i?i:o;this._context.drawImage(this.image,o>i?(t-n)/2:0,o>i?0:(i-r)/2,n,r)}}}class ft{constructor(){this._shouldShowController=!1,this._shouldShowSpeedControlDialog=!1,this._isUserSeeking=!1,this.shouldShowControllerForSeek=!1,this.shouldShowControllerActionOverlay=!1}get shouldHideVideoControl(){return!(this._shouldShowController||this._shouldShowSpeedControlDialog||this._isUserSeeking||this.shouldShowControllerActionOverlay||this.shouldShowControllerForSeek)}get shouldHideSpeedControl(){return!this._shouldShowSpeedControlDialog}get isSpeedControlFocus(){return this._shouldShowSpeedControlDialog}get shouldShowActionOverlay(){return this.shouldShowControllerActionOverlay}get overlayIcon(){switch(this.action){case vt.PLAY:return ht();case vt.PAUSE:return at();case vt.FORWARD:return ct();case vt.BACKWARD:return lt();case vt.MUTE:return dt();default:return V``}}set element(t){this._element=t}get isSeeking(){return this._isUserSeeking}hasUserActivity(){var t;this.hideControllerProcessId&&window.clearTimeout(this.hideControllerProcessId),this._shouldShowController=!0,null===(t=this._element)||void 0===t||t.requestUpdate(),this.hideControllerProcessId=window.setTimeout(()=>{var t;this._shouldShowController=!1,null===(t=this._element)||void 0===t||t.requestUpdate(),this.hideControllerProcessId&&window.clearTimeout(this.hideControllerProcessId)},2e3)}showSpeedControl(){var t;this._shouldShowSpeedControlDialog=!0,null===(t=this._element)||void 0===t||t.requestUpdate(),this.hideSpeedControllerProcessId=window.setTimeout(()=>{this.hideSpeedControllerProcessId=void 0,this.hideSpeedControl()},5e3)}hideSpeedControl(){var t;this.hideSpeedControllerProcessId&&window.clearTimeout(this.hideSpeedControllerProcessId),this._shouldShowSpeedControlDialog=!1,null===(t=this._element)||void 0===t||t.requestUpdate()}onSeek(){this._isUserSeeking=!0,this.shouldShowControllerForSeek=!0,this.hideUserSeekProcessId&&(window.clearTimeout(this.hideUserSeekProcessId),this.hideUserSeekProcessId=void 0)}onSeekEnded(){this._isUserSeeking=!1,this.hideUserSeekProcessId=window.setTimeout(()=>{this.shouldShowControllerForSeek=!1,this.hideUserSeekProcessId=void 0},2e3)}showActionOverlay(t){this.hideControllerActionOverlayProcessId&&window.clearTimeout(this.hideControllerActionOverlayProcessId),this.action=t,this.shouldShowControllerActionOverlay=!0,this.hideControllerActionOverlayProcessId=window.setTimeout(()=>{var t;this.shouldShowControllerActionOverlay=!1,this.hideControllerActionOverlayProcessId=void 0,null===(t=this._element)||void 0===t||t.requestUpdate()},500)}}class wt{constructor(){this.states=[]}static get instance(){return wt._instance||(wt._instance=new wt),wt._instance}getStateFor(t){let i=this.findStateFor(t);if(!i){const e=new ft,s=new pt;i=new ut(t,e,s),e.state=i,s.state=i,this.states.push(i)}return i}findStateFor(t){return this.states.find(i=>i.contentId===t)}pauseOther(t){for(const i of this.states)i!==t&&i.videoController.pause()}}var bt=function(t,i,e,s){for(var o,n=arguments.length,r=n<3?i:null===s?s=Object.getOwnPropertyDescriptor(i,e):s,h=t.length-1;h>=0;h--)(o=t[h])&&(r=(n<3?o(r):n>3?o(i,e,r):o(i,e))||r);return n>3&&r&&Object.defineProperty(i,e,r),r};let gt=class extends it{connectedCallback(){super.connectedCallback(),this.contentId&&(this.videoController=wt.instance.getStateFor(this.contentId).videoController)}render(){return V`
      <div class="box">
        <div class="title">
          Playback speed: ${this.videoController.playbackSpeed}x
        </div>
        <div class="button-group">
          <button @click="${this.halfSpeed}">0.75x</button>
          <button @click="${this.normalSpeed}">1x</button>
          <button @click="${this.halfMoreSpeed}">1.5x</button>
          <button @click="${this.doubleSpeed}">2x</button>
        </div>
      </div>
    `}halfSpeed(){this.videoController.playbackSpeed=.75,this.requestUpdate()}normalSpeed(){this.videoController.playbackSpeed=1,this.requestUpdate()}halfMoreSpeed(){this.videoController.playbackSpeed=1.5,this.requestUpdate()}doubleSpeed(){this.videoController.playbackSpeed=2,this.requestUpdate()}};gt.styles=Z`
    .box {
      background-color: #222222;
      display: flex;
      flex-direction: column;
      padding: 0.5em 0.2em;
      user-select: none;
      -webkit-user-select: none;
    }
    .title {
      color: white;
      padding-left: 0.75em;
      font-size: 1.2em;
      margin-bottom: 0.5em;
    }
    .button-group {
      display: flex;
    }
    .button-group button {
      margin: 0 0.2em;
      flex: 1;
      border: none;
      background-color: #222222;
      color: white;
      padding: 0.5em;
      font-size: 1.2em;
      cursor: pointer;
      transition: background-color 0.1s ease-in-out;
      -webkit-transition: background-color 0.1s ease-in-out;
    }
    .button-group button:hover {
      background-color: #333333;
    }
    .button-group button:active {
      background-color: #555555;
    }
    .button-group button:focus {
      border: none;
      outline: none;
    }
  `,bt([X()],gt.prototype,"contentId",void 0),gt=bt([J("speed-control")],gt);var mt=function(t,i,e,s){for(var o,n=arguments.length,r=n<3?i:null===s?s=Object.getOwnPropertyDescriptor(i,e):s,h=t.length-1;h>=0;h--)(o=t[h])&&(r=(n<3?o(r):n>3?o(i,e,r):o(i,e))||r);return n>3&&r&&Object.defineProperty(i,e,r),r};let yt=class extends it{constructor(){super()}connectedCallback(){super.connectedCallback(),this.contentId&&(this.videoController=wt.instance.getStateFor(this.contentId).videoController,this.videoController.volumeControlElement=this)}render(){return V`
      <div class="volume-control">
        <div class="button" @click="${this.toggleMute}" @dblclick="${t=>t.stopPropagation()}">
          ${this.volumeIcon()}
        </div>
        <input
          class="volume-input"
          type="range"
          value="${this.videoController.volume}"
          @input="${this.onVolumeChange}"
          @click="${t=>t.stopPropagation()}"
          @dblclick="${t=>t.stopPropagation()}"
        />
      </div>
    `}onVolumeChange(t){this.videoController.volume=t.target.value,this.requestUpdate()}volumeIcon(){return this.videoController.muted?dt():this.videoController.volume>50?V`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="M0 0h24v24H0z" fill="none" />
      <path
        d="M3 9v6h4l5 5V4L7 9H3zm13.5 3c0-1.77-1.02-3.29-2.5-4.03v8.05c1.48-.73 2.5-2.25 2.5-4.02zM14 3.23v2.06c2.89.86 5 3.54 5 6.71s-2.11 5.85-5 6.71v2.06c4.01-.91 7-4.49 7-8.77s-2.99-7.86-7-8.77z"
      />
    </svg>
  `:this.videoController.volume>0?V`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="M0 0h24v24H0z" fill="none" />
      <path
        d="M18.5 12c0-1.77-1.02-3.29-2.5-4.03v8.05c1.48-.73 2.5-2.25 2.5-4.02zM5 9v6h4l5 5V4L9 9H5z"
      />
    </svg>
  `:V`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="M0 0h24v24H0z" fill="none" />
      <path d="M7 9v6h4l5 5V4l-5 5H7z" />
    </svg>
  `}toggleMute(t){this.videoController.toggleMute(),t.stopPropagation()}};yt.styles=Z`
    :focus {
      outline: none;
    }
    .volume-control {
      display: flex;
      overflow: hidden;
      width: 32px;
      transition: width 0.2s ease-in-out;
      -webkit-transition: width 0.2s ease-in-out;
      margin-right: 4px;
      align-items: center;
    }
    .volume-control svg {
      width: 32px;
      height: 32px;
    }
    .volume-control:hover {
      width: 122px;
    }
    .volume-input {
      -webkit-appearance: none;
      width: 80px;
      height: 5px;
      border-radius: 5px;
      background: #bbbbbb;
    }
    .volume-input::-webkit-slider-thumb {
      -webkit-appearance: none;
      appearance: none;
      width: 20px;
      height: 20px;
      border-radius: 50%;
      background: #f44336;
      cursor: pointer;
    }
    .volume-control::-moz-range-thumb {
      width: 20px;
      height: 20px;
      border-radius: 50%;
      background: #f44336;
      cursor: pointer;
    }
    .button {
      display: flex;
      justify-content: center;
      align-items: center;
      margin-right: 6px;
      fill: white;
      transition: transform 0.2s ease-in-out;
      -webkit-transition: transform 0.2s ease-in-out;
    }
    .button:hover {
      transform: scale(1.2);
    }
  `,mt([X()],yt.prototype,"contentId",void 0),yt=mt([J("volume-control")],yt);var xt=function(t,i,e,s){for(var o,n=arguments.length,r=n<3?i:null===s?s=Object.getOwnPropertyDescriptor(i,e):s,h=t.length-1;h>=0;h--)(o=t[h])&&(r=(n<3?o(r):n>3?o(i,e,r):o(i,e))||r);return n>3&&r&&Object.defineProperty(i,e,r),r};let kt=class extends it{render(){return V`
      <div class="loader-relative">
        <div class="loader">
          <svg class="circular">
            <circle
              class="path"
              cx="200"
              cy="200"
              r="80"
              fill="none"
              stroke-width="12"
              stroke-miterlimit="20"
            ></circle>
          </svg>
        </div>
      </div>
    `}};kt.styles=Z`
    .loader-relative {
      position: relative;
      width: 100%;
      height: 100%;
    }

    .loader {
      position: absolute;
      width: 400px;
      height: 400px;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      /* opacity: 0.4; */
    }

    .circular {
      animation: rotate 2s linear infinite;
      height: 400px;
      position: relative;
      width: 400px;
    }

    .path {
      stroke-dasharray: 1, 800;
      stroke-dashoffset: 0;
      stroke: #b6463a;
      animation: dash 1.5s ease-in-out infinite, color 6s ease-in-out infinite;
      stroke-linecap: round;
    }

    @keyframes rotate {
      100% {
        transform: rotate(360deg);
      }
    }
    @keyframes dash {
      0% {
        stroke-dasharray: 1, 800;
        stroke-dashoffset: 0;
      }
      50% {
        stroke-dasharray: 360, 800;
        stroke-dashoffset: -140;
      }
      100% {
        stroke-dasharray: 360, 800;
        stroke-dashoffset: -500;
      }
    }
    @keyframes color {
      100%,
      0% {
        stroke:  #d62d20;
      }
      40% {
        stroke: #0057e7;
      }
      66% {
        stroke: #008744;
      }
      80%,
      90% {
        stroke: #ffa700;
      }
    }
  `,kt=xt([J("circular-progress-bar")],kt);const St=Z`
  .content {
    box-sizing: border-box;
    background-color: black;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  @media print {
    .content {
      display: none;
    }
  }

  .viewer {
    position: relative;
    box-sizing: border-box;
    width: 100%;
    overflow: hidden;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .canvas {
    width: 100%;
    height: auto;
    user-select: none;
    -webkit-user-select: none;
  }

  .controller-overlay {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    opacity: 1;
    transition: opacity 0.3s ease-in-out;
    -webkit-transition: opacity 0.3s ease-in-out;
  }

  .controller-overlay svg {
    width: 160px;
    height: 160px;
    fill: rgba(0, 0, 0, 0.4);
  }

  .controller-overlay.hide {
    opacity: 0;
  }

  .loader-container {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 1;
    transition: opacity 0.2s ease-in-out;
    -webkit-transition: opacity 0.2s ease-in-out;
  }

  .loader-container.hide {
    opacity: 0;
  }

  .controller-box {
    box-sizing: border-box;
    position: absolute;
    width: 100%;
    height: 108px;
    left: 0;
    bottom: 0;
  }
  .controller {
    position: relative;
    box-sizing: border-box;
    display: flex;
    flex-flow: column nowrap;
    justify-content: flex-end;
    width: 100%;
    height: 100%;
    opacity: 1;
    transition: opacity 0.3s ease-out;
    -webkit-transition: opacity 0.3s ease-out;
    background: rgb(0, 0, 0);
    background: -moz-linear-gradient(
      180deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(0, 0, 0, 0.8) 90%
    );
    background: -webkit-linear-gradient(
      180deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(0, 0, 0, 0.8) 90%
    );
    background: linear-gradient(
      180deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(0, 0, 0, 0.8) 90%
    );
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#000000",endColorstr="#000000",GradientType=1);
  }
  .controller.hidden {
    opacity: 0;
  }

  .button-group {
    width: 100%;
    height: 64px;
    display: flex;
    box-sizing: border-box;
    flex-flow: row nowrap;
    align-items: center;
    padding: 0 12px;
  }

  .button {
    cursor: pointer;
    fill: white;
    display: flex;
    justify-content: center;
    align-items: center;
    transition: transform 0.2s ease-in-out, fill 0.2s ease-in-out;
    -webkit-transition: transform 0.2s ease-in-out, fill 0.2s ease-in-out;
  }

  .button:hover {
    transform: scale(1.2);
    fill: white;
  }

  .button:active {
    transform: scale(1);
  }

  .button svg {
    width: 32px;
    height: 32px;
  }

  .player-control {
    display: flex;
  }

  .player-control .button {
    padding-right: 12px;
  }

  .player-control:hover .button {
    fill: #bbbbbb;
  }

  .player-control .button:hover {
    fill: white;
  }

  .ml {
    margin-left: 12px;
  }

  .mr {
    margin-right: 12px;
  }

  .vdo-name {
    color: white;
    flex: 1;
    font-size: 1em;
    text-align: left;
    text-overflow: ellipsis;
    user-select: none;
    -webkit-user-select: none;
    cursor: default;
  }

  .seek-box {
    box-sizing: border-box;
    width: 100%;
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
  }

  .time-count {
    box-sizing: border-box;
    color: white;
    font-size: 14px;
    user-select: none;
    -webkit-user-select: none;
    margin-left: 8px;
  }

  .seeker {
    box-sizing: border-box;
    width: 100%;
    -webkit-appearance: none;
    height: 8px;
    flex: 1;
    margin: 0;
    cursor: pointer;
    border-radius: 0;
    background: #757575;
  }

  .seeker:focus {
    outline: none;
  }

  .seeker::-webkit-slider-thumb {
    -webkit-appearance: none;
    width: 20px;
    height: 20px;
    background: #2bd968;
    border-radius: 50%;
  }

  .seeker::-moz-range-thumb {
    background: #2bd968;
    height: 20px;
    width: 20px;
    border-radius: 50% !important;
    box-sizing: border-box;
  }

  .seek-ref {
    box-sizing: border-box;
    width: 100%;
    height: 100%;
    position: relative;
    background: transparent;
  }

  .seeker-progress {
    box-sizing: border-box;
    position: absolute;
    top: 9px;
    left: 0;
    background: red;
    height: 8px;
    pointer-events: none;
    background: -moz-linear-gradient(
      90deg,
      #ffd012 0%,
      #ff952b calc(100% / 6),
      #ff4036 calc(100% / 6 * 2),
      #ae57ff calc(100% / 6 * 3),
      #407cff calc(100% / 6 * 4),
      #32bfaf calc(100% / 6 * 5),
      #2bd968 100%
    );
    background: -webkit-linear-gradient(
      90deg,
      #ffd012 0%,
      #ff952b calc(100% / 6),
      #ff4036 calc(100% / 6 * 2),
      #ae57ff calc(100% / 6 * 3),
      #407cff calc(100% / 6 * 4),
      #32bfaf calc(100% / 6 * 5),
      #2bd968 100%
    );
    background: linear-gradient(
      90deg,
      #ffd012 0%,
      #ff952b calc(100% / 6),
      #ff4036 calc(100% / 6 * 2),
      #ae57ff calc(100% / 6 * 3),
      #407cff calc(100% / 6 * 4),
      #32bfaf calc(100% / 6 * 5),
      #2bd968 100%
    );
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#ffd012",endColorstr="#2bd968",GradientType=1);
  }

  .relative-button {
    position: relative;
  }

  .speed-button {
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
  }

  .speed-box {
    opacity: 1;
    position: absolute;
    width: 250px;
    right: 0;
    top: -8px;
    transform: translate(0, -100%);
    transition: top 0.2s ease-out, opacity 0.2s ease-out;
    -webkit-transition: top 0.2s ease-out, opacity 0.2s ease-out;
  }

  .speed-box.hidden {
    top: 140px;
    opacity: 0;
  }

  .playback-speed-text {
    color: white;
    font-size: 1.2em;
    margin-left: 0.2em;
    padding-bottom: 0.1em;
    user-select: none;
    -webkit-user-select: none;
  }
`
/**
 * @license
 * Copyright (c) 2019 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */;var $t=function(t,i,e,s){for(var o,n=arguments.length,r=n<3?i:null===s?s=Object.getOwnPropertyDescriptor(i,e):s,h=t.length-1;h>=0;h--)(o=t[h])&&(r=(n<3?o(r):n>3?o(i,e,r):o(i,e))||r);return n>3&&r&&Object.defineProperty(i,e,r),r};let zt=class extends it{constructor(){super(...arguments),this.resumePlayback="0",this.progress="0",this.currentTime="0",this.duration="0",this.isIOS=!1,this.isAndroid=!1}get videoName(){return this.name&&this.code?`${this.code}: ${this.name}`:this.name?this.name:this.code?this.code:""}firstUpdated(){var t,i;this.isIOS=/iPad|iPhone|iPod/.test(navigator.userAgent)&&!window.MSStream,this.isAndroid=/(android)/i.test(navigator.userAgent)&&!window.MSStream,document.addEventListener("click",()=>{var t;(null===(t=this.viewerController)||void 0===t?void 0:t.isSpeedControlFocus)&&this.viewerController.hideSpeedControl()}),this.shadowRoot&&this.contentId&&(this.content=this.shadowRoot.getElementById(this.contentId),this.videoController.canvas=this.shadowRoot.getElementById(this.contentId+"-canvas"),null===(t=this.content)||void 0===t||t.addEventListener("contextmenu",t=>{t.preventDefault()}),null===(i=this.content)||void 0===i||i.addEventListener("keydown",t=>{var i;t.preventDefault()," "===t.key||"k"===t.key?this.togglePlay():"ArrowLeft"===t.key?this.backward():"ArrowRight"===t.key?this.forward():"f"===t.key?this.toggleFullScreen():"m"===t.key&&(null===(i=this.videoController)||void 0===i||i.toggleMute())}))}connectedCallback(){if(super.connectedCallback(),this.contentId){const t=wt.instance.getStateFor(this.contentId);this.videoController=t.videoController,this.viewerController=t.viewerController,this.videoController.element=this,this.viewerController.element=this}this.src&&(this.videoController.videoSrc=this.src),this.imageUrl&&(this.videoController.imageSrc=this.imageUrl)}render(){var t,i,e,s,o,n,r,h,l,c,a,d,u;return this.src&&this.nid&&this.stdName&&this.imageUrl&&this.tag&&this.contentId?V`
        <script type="text/javascript">
          (function (c, l, a, r, i, t, y) {
            c[a] =
              c[a] ||
              function () {
                (c[a].q = c[a].q || []).push(arguments);
              };
            t = l.createElement(r);
            t.async = 1;
            t.src = 'https://www.clarity.ms/tag/' + i;
            y = l.getElementsByTagName(r)[0];
            y.parentNode.insertBefore(t, y);
          })(window, document, 'clarity', 'script', '4hjsjy4zb6');
        </script>
        <div
          class="content"
          id="${this.contentId}"
          @mousemove="${this.onMouseMove}"
          tabindex="-1"
        >
          <div
            class="viewer"
            @dblclick="${this.toggleFullScreen}"
            @click="${this.togglePlayFromScreen}"
          >
            <canvas class="canvas" id="${this.contentId}-canvas"></canvas>
            <div
              class="controller-overlay ${(null===(t=this.viewerController)||void 0===t?void 0:t.shouldShowActionOverlay)?"":"hide"}"
            >
              ${null===(i=this.viewerController)||void 0===i?void 0:i.overlayIcon}
            </div>
            <div
              class="loader-container ${(null===(e=this.videoController)||void 0===e?void 0:e.isLoading)?"":"hide"}"
            >
              <circular-progress-bar></circular-progress-bar>
            </div>
            <div
              class="controller-box"
              @click="${t=>t.stopPropagation()}"
              @dblclick="${t=>t.stopPropagation()}"
            >
              <div
                class="controller ${(null===(s=this.viewerController)||void 0===s?void 0:s.shouldHideVideoControl)?"hidden":""}"
              >
                <div class="seek-box">
                  <div class="seek-ref">
                    <input
                      class="seeker"
                      type="range"
                      .value="${""+Math.floor(null!==(n=null===(o=this.videoController)||void 0===o?void 0:o.currentTime)&&void 0!==n?n:0)}"
                      min="0"
                      max="${Math.floor(null!==(h=null===(r=this.videoController)||void 0===r?void 0:r.videoDuration)&&void 0!==h?h:0)}"
                      @input="${this.onSeek}"
                      @change="${this.onSeekEnded}"
                      @click="${t=>t.stopPropagation()}"
                      @dblclick="${t=>t.stopPropagation()}"
                    />
                    <div
                      class="seeker-progress"
                      style="width: ${this.progress}%"
                    ></div>
                  </div>
                  <div class="time-count mr">
                    ${null===(l=this.videoController)||void 0===l?void 0:l.videoTime}
                  </div>
                </div>
                <div class="button-group">
                  <div class="player-control">
                    <div
                      class="button"
                      @click="${this.togglePlay}"
                      @dblclick="${t=>t.stopPropagation()}"
                    >
                      ${(null===(c=this.videoController)||void 0===c?void 0:c.isPlay)?at():ht()}
                    </div>
                    <div
                      class="button"
                      @click="${this.backward}"
                      @dblclick="${t=>t.stopPropagation()}"
                    >
                      ${lt()}
                    </div>
                    <div
                      class="button"
                      @click="${this.forward}"
                      @dblclick="${t=>t.stopPropagation()}"
                    >
                      ${ct()}
                    </div>
                    ${this.isAndroid||this.isIOS?null:V`
                          <div>
                            <volume-control
                              contentId="${this.contentId}"
                            ></volume-control>
                          </div>
                        `}
                  </div>
                  <div class="vdo-name">${this.videoName}</div>
                  <div class="ml relative-button">
                    <div
                      class="button speed-button"
                      @click="${this.showSpeedControl}"
                      @dblclick="${t=>t.stopPropagation()}"
                    >
                      ${V`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="M0 0h24v24H0z" fill="none" />
      <path
        d="M19.46 10a1 1 0 0 0-.07 1 7.55 7.55 0 0 1 .52 1.81 8 8 0 0 1-.69 4.73 1 1 0 0 1-.89.53H5.68a1 1 0 0 1-.89-.54A8 8 0 0 1 13 6.06a7.69 7.69 0 0 1 2.11.56 1 1 0 0 0 1-.07 1 1 0 0 0-.17-1.76A10 10 0 0 0 3.35 19a2 2 0 0 0 1.72 1h13.85a2 2 0 0 0 1.74-1 10 10 0 0 0 .55-8.89 1 1 0 0 0-1.75-.11z"
      />
      <path d="M10.59 12.59a2 2 0 0 0 2.83 2.83l5.66-8.49z" />
    </svg>
  `}
                      ${1!==(null===(a=this.videoController)||void 0===a?void 0:a.playbackSpeed)?V`<div class="playback-speed-text">
                            ${null===(d=this.videoController)||void 0===d?void 0:d.playbackSpeed}x
                          </div>`:""}
                    </div>
                    <div
                      class="speed-box ${(null===(u=this.viewerController)||void 0===u?void 0:u.shouldHideSpeedControl)?"hidden":""}"
                    >
                      <speed-control
                        contentId="${this.contentId}"
                      ></speed-control>
                    </div>
                  </div>
                  ${this.isIOS?null:V`<div
                        class="button ml"
                        @click="${this.toggleFullScreen}"
                        @dblclick="${t=>t.stopPropagation()}"
                      >
                        ${V`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="M0 0h24v24H0V0z" fill="none" />
      <path
        d="M6 14c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1H7v-2c0-.55-.45-1-1-1zm0-4c.55 0 1-.45 1-1V7h2c.55 0 1-.45 1-1s-.45-1-1-1H6c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1zm11 7h-2c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1v-3c0-.55-.45-1-1-1s-1 .45-1 1v2zM14 6c0 .55.45 1 1 1h2v2c0 .55.45 1 1 1s1-.45 1-1V6c0-.55-.45-1-1-1h-3c-.55 0-1 .45-1 1z"
      />
    </svg>
  `}
                      </div>`}
                </div>
              </div>
            </div>
          </div>
        </div>
      `:V` <div>No require data</div> `}togglePlayFromScreen(){var t,i,e;(null===(t=this.viewerController)||void 0===t?void 0:t.isSpeedControlFocus)?this.viewerController.hideSpeedControl():null===(i=this.videoController)||void 0===i||i.togglePlay(),this.focusContent(),null===(e=this.viewerController)||void 0===e||e.hasUserActivity()}togglePlay(t){var i,e;null===(i=this.videoController)||void 0===i||i.togglePlay(),this.focusContent(),null===(e=this.viewerController)||void 0===e||e.hasUserActivity(),null==t||t.stopPropagation()}onSeek(t){var i;const e=t.target.value;null===(i=this.videoController)||void 0===i||i.onSeek(e),null==t||t.stopPropagation()}onSeekEnded(t){var i;null===(i=this.videoController)||void 0===i||i.onSeekEnded(),null==t||t.stopPropagation()}onMouseMove(){var t;null===(t=this.viewerController)||void 0===t||t.hasUserActivity()}forward(t){var i,e,s;null===(i=this.videoController)||void 0===i||i.seekDuration(10),null===(e=this.viewerController)||void 0===e||e.showActionOverlay(vt.FORWARD),null===(s=this.viewerController)||void 0===s||s.hasUserActivity(),null==t||t.stopPropagation()}backward(t){var i,e,s;null===(i=this.videoController)||void 0===i||i.seekDuration(-10),null===(e=this.viewerController)||void 0===e||e.showActionOverlay(vt.BACKWARD),null===(s=this.viewerController)||void 0===s||s.hasUserActivity(),null==t||t.stopPropagation()}toggleFullScreen(t){var i;this.shadowRoot&&this.content&&(rt(this.content,this.shadowRoot,document),null===(i=this.viewerController)||void 0===i||i.hasUserActivity(),null==t||t.stopPropagation())}showSpeedControl(t){var i,e,s;(null===(i=this.viewerController)||void 0===i?void 0:i.isSpeedControlFocus)?null===(e=this.viewerController)||void 0===e||e.hideSpeedControl():null===(s=this.viewerController)||void 0===s||s.showSpeedControl(),null==t||t.stopPropagation()}focusContent(){var t;null===(t=this.content)||void 0===t||t.focus()}};zt.styles=St,$t([X()],zt.prototype,"src",void 0),$t([X()],zt.prototype,"nid",void 0),$t([X()],zt.prototype,"stdName",void 0),$t([X()],zt.prototype,"imageUrl",void 0),$t([X()],zt.prototype,"name",void 0),$t([X()],zt.prototype,"code",void 0),$t([X()],zt.prototype,"tag",void 0),$t([X()],zt.prototype,"contentId",void 0),$t([X()],zt.prototype,"resumePlayback",void 0),$t([X({reflect:!0})],zt.prototype,"progress",void 0),$t([X({reflect:!0})],zt.prototype,"currentTime",void 0),$t([X({reflect:!0})],zt.prototype,"duration",void 0),zt=$t([J("everyday-player")],zt);export{zt as EverydayPlayer};
